// ############################################################################
// #
// # Quartex code library
// #        version 1.0.1
// ############################################################################
// #
// # Written by Jon L. Aasenden @ Quartex Components
// #
// # This software is released under Mozilla Public License V2
// #   https://www.mozilla.org/en-US/MPL/2.0/
// #
// ############################################################################

unit qtx.vectors;

{$IFDEF FPC}
  {$mode DELPHI}{$H+}
{$ENDIF}

{$DEFINE USE_ARRAYS}          // Bitbuffer use maths or array[] to access bytes
{$DEFINE USE_SEEK}            // File buffer use seek() call or direct offset
{$DEFINE USE_LOCAL_CACHE}     // per-proc buffer allocation [heap intensive]
{$DEFINE USE_RTL_FILL}        // Use fillchar() or TTyped.FillByte()
{$DEFINE USE_RAWFILE}         // Use file-api for disk IO, not stream [faster]
{$DEFINE BE_SILENT}           // Mute hints and warnings
{$DEFINE USE_BITVIEW}         // Support TBit for TQTXDataView<>

interface

uses
  SysUtils, Classes, Math,
  {$IFNDEF FPC}
  System.IOUtils,
  {$ENDIF}
  Generics.Collections;

type

  TVectorAllocatorType = (
    vaSequence,   // sequenced in memory
    vaDynamic,    // dynamic in memory
    vaFile,       // sequenced in file
    vaMapped,     // Mapped to a buffer
    vaCustom      // Custom user-type (see constructors)
    );

  {$Z1}
  {$MINENUMSIZE 1}

  {$IFDEF USE_BITVIEW}
  TBit = 0..1;
  {$ENDIF}

  TQTXStorage = class abstract
  public
    type

    TStorageCapabilities      = set of (mcScale, mcOwned, mcRead, mcWrite);
    TStorageTextOutOptions    = set of (hdSign, hdZeroPad);
    TStorageStackOrientation  = (soTopDown, soDownUp);
    TStorageCache           = array [1..10240] of byte;

    TProcessBeginsEvent = procedure (const Sender: TObject; const Primary: integer) of object;
    TProcessUpdateEvent = procedure (const Sender: TObject; const Value, Primary: integer) of object;
    TProcessEndsEvent   = procedure (const Sender: TObject; const Primary, Secondary: integer) of object;

    IBinarySearch = interface
      ['{10A75BDF-9FCB-422D-82D3-46ACB61F41FA}']
      function    Search(const Data; const DataLength: integer; var FoundbyteIndex: int64):  boolean;
    end;

    TStorage = class;

    IStorage = interface
      ['{37C2EC42-1B7F-48CE-91C9-14D42F3FDFA7}']
      procedure   LoadFromFile(Filename: string);
      procedure   SaveToFile(Filename: string);

      procedure   LoadFromStream(const Stream: TStream);
      procedure   SaveToStream(const Stream: TStream);

      function    ToStream: TStream;
      procedure   FromStream(const Stream: TStream; const Disposable: boolean = false);

      function    ToStorage: TStorage;
      procedure   FromStorage(const Storage: TStorage; const Disposable: boolean = false); overload;
      procedure   FromStorage(const Storage: IStorage); overload;

      function    ImportFrom(ByteIndex: Int64; DataLength: integer; const Reader: TReader):  integer;
      function    ExportTo(ByteIndex: Int64; DataLength: integer; const Writer: TWriter):  integer;

      function    GetCapabilities: TStorageCapabilities;
      function    GetEmpty: boolean;

      function    GetSize: int64;
      procedure   SetSize(const NewSize: Int64);

      function    Read(const ByteIndex: int64; DataLength: integer; var Data):  integer; overload;
      function    Write(const ByteIndex: int64; DataLength: integer; const Data):  integer; overload;

      procedure   Append(const Buffer: IStorage); overload;
      procedure   Append(const Stream: TStream); overload;
      procedure   Append(const Data; const DataLength:integer); overload;

      procedure   Insert(ByteIndex: int64;const Source; DataLength: integer); overload;
      procedure   Insert(ByteIndex: int64; const Source: IStorage); overload;
      procedure   Remove(ByteIndex: int64; DataLength: integer);

      function    Push(const Source; DataLength: integer;const Orientation: TStorageStackOrientation = soDownUp):  integer;
      function    Pop(var Target; DataLength: integer; const Orientation: TStorageStackOrientation = soDownUp):  integer;

      function    FillWith(const ByteIndex: int64; const BytesToFill: int64; const Source; const SourceLength: integer):  int64;
      procedure   FillAll(const Value: byte); overload;
      procedure   FillAll; overload;

      function    ToString(Boundary: integer = 16; const Options: TStorageTextOutOptions = [hdSign, hdZeroPad]) : string;

      function    HashCode: cardinal;
    end;

    TStorageSearch = class(TInterfacedObject, IBinarySearch)
    private
      FStorage:   IStorage;
    public
      function    Search(const Data; const DataLength: integer; var FoundbyteIndex: int64):  boolean;
      constructor Create(Storage: IStorage);
    end;

    TStorage = class(TInterfacedPersistent, IStorage)
    strict protected
      procedure   SetSize(const NewSize: Int64);

    {$IFNDEF USE_LOCAL_CACHE}
    strict protected
      FCache:     TStorageCache;
    {$ENDIF}

    public
      type
      EStorage = class(Exception);
      EStorageInvalidReference  = class(EStorage);
      EStorageInvalidDataSource = class(EStorage);
      EStorageInvalidDataTarget = class(EStorage);
      EStorageInvalidDataSize   = class(EStorage);
      EStorageReadUnSupported   = class(EStorage);
      EStorageWriteUnSupported  = class(EStorage);
      EStorageScaleUnSupported  = class(EStorage);
      EStorageEmpty             = class(EStorage);
      EStorageIndexViolation    = class(EStorage);
      EStorageScaleFailed       = class(EStorage);
      EStorageInActive          = class(EStorage);
    protected
      procedure   DefineProperties(Filer: TFiler); override;

    strict protected
      function    ObjectHasData: boolean;
      procedure   ReadObjBin(Stream: TStream);
      procedure   WriteObjBin(Stream: TStream);

      procedure   BeforeReadObject; virtual;
      procedure   AfterReadObject; virtual;

      procedure   BeforeWriteObject; virtual;
      procedure   AfterWriteObject; virtual;

      procedure   ReadObject(const Reader: TReader); virtual;
      procedure   WriteObject(const Writer: TWriter); virtual;
      function    GetEmpty: boolean; virtual;

      function    GetCapabilities: TStorageCapabilities; virtual; abstract;
      function    GetSize: Int64; virtual; abstract;

      procedure   DoReleaseData; virtual;abstract;
      procedure   DoGrowDataBy(const Value: integer); virtual;abstract;
      procedure   DoShrinkDataBy(const Value: integer); virtual;abstract;
      procedure   DoReadData(Start: Int64; var Buffer; BufLen: integer); virtual;abstract;
      procedure   DoWriteData(Start: int64; const Buffer; BufLen: integer); virtual;abstract;
      procedure   DoFillData(Start: Int64; FillLength: Int64; const Data; DataLen: integer); virtual;
      procedure   DoZeroData; virtual;
    public
      property    Empty: boolean read GetEmpty;
      property    Capabilities: TStorageCapabilities read GetCapabilities;

      procedure   Assign(Source: TPersistent); override;

      function    Read(const ByteIndex: int64; DataLength: integer; var Data):  integer; overload;
      function    Write(const ByteIndex: int64; DataLength: integer; const Data):  integer; overload;

      procedure   Append(const Buffer: IStorage); overload;
      procedure   Append(const Stream: TStream); overload;
      procedure   Append(const Data; const DataLength:integer); overload;

      procedure   Insert(ByteIndex: int64;const Source; DataLength: integer); overload;
      procedure   Insert(ByteIndex: int64; const Source: IStorage); overload;
      procedure   Remove(ByteIndex: int64; DataLength: integer);

      function    Push(const Source; DataLength: integer;const Orientation: TStorageStackOrientation = soDownUp):  integer;
      function    Pop(Var Target; DataLength: integer; const Orientation: TStorageStackOrientation = soDownUp):  integer;

      function    FillWith(const ByteIndex: int64; const BytesToFill: int64; const Source; const SourceLength: integer):  int64;
      procedure   FillAll(const Value: byte); overload;
      procedure   FillAll; overload;

      function    HashCode: cardinal; virtual;

      procedure   LoadFromFile(Filename: string);
      procedure   SaveToFile(Filename: string);
      procedure   SaveToStream(const Stream: TStream);
      procedure   LoadFromStream(const Stream: TStream);

      function    ToStream: TStream;
      procedure   FromStream(const Stream: TStream; const Disposable: boolean = false);

      function    ToStorage: TStorage;
      procedure   FromStorage(const Storage: TStorage; const Disposable: boolean = false); overload;
      procedure   FromStorage(const Storage: IStorage); overload;

      function    ExportTo(ByteIndex: Int64; DataLength: integer; const Writer: TWriter):  integer;
      function    ImportFrom(ByteIndex: Int64; DataLength: integer; const Reader: TReader):  integer;

      procedure   Release;
      procedure   BeforeDestruction; override;

      function    ToString(Boundary: integer = 16; const Options: TStorageTextOutOptions = [hdSign, hdZeroPad]) : string; reintroduce; virtual;

      property Size: int64 read GetSize write SetSize;
    end;

    TStorageMemory = class(TStorage)
    strict private
      FDataPTR:   Pbyte;
      FDataLen:   int64;
    strict protected
      function    BasePTR: Pbyte; inline;
      function    AddrOf(const byteIndex: Int64): Pbyte; {$IFDEF FPC} inline; {$ENDIF}

      function    GetCapabilities: TQTXStorage.TStorageCapabilities; override;
      function    GetSize: Int64; override;
      procedure   DoReleaseData; override;
      procedure   DoReadData(Start: Int64; var Buffer; BufLen: integer); override;
      procedure   DoWriteData(Start: Int64;const Buffer; BufLen: integer); override;
      procedure   DoFillData(Start: Int64; FillLength: Int64; const Data; DataLen: integer); override;

      procedure   DoGrowDataBy(const Value: integer); override;
      procedure   DoShrinkDataBy(const Value: integer); override;
      procedure   DoZeroData; override;
    public
      property    Data: Pbyte read FDataPTR;
    end;

    TStorageFile = class(TStorage)
    {$IFDEF USE_RAWFILE}
    strict private
      type
      TStorageFileIO = file of byte;
    {$ENDIF}
    strict private
      FFilename:  string;
      FMarked:    boolean;
      {$IFDEF USE_RAWFILE}
      FFile:      TStorageFileIO;
      {$ELSE}
      FFile:      TStream;
      {$ENDIF}
    strict protected
      function    GetActive: boolean; inline;
      function    GetEmpty: boolean; override;

      function    GetCapabilities: TStorageCapabilities; override;
      function    GetSize: Int64; override;
      procedure   DoReleaseData; override;
      procedure   DoReadData(Start: Int64; var Buffer; BufLen: integer); override;
      procedure   DoWriteData(Start: Int64;const Buffer; BufLen:integer); override;
      procedure   DoGrowDataBy(const Value:integer); override;
      procedure   DoShrinkDataBy(const Value:integer); override;
    protected
      procedure   MarkAsDisposable(const Value: boolean);
    public
      property    Filename: string read FFilename;
      property    Active: boolean read GetActive;
      procedure   Open(AFilename: string; StreamFlags: Word); virtual;
      procedure   Close; virtual;

      procedure   BeforeDestruction; override;
      constructor Create(AFilename: string; StreamFlags: Word); reintroduce; virtual;
    end;

    {$IFDEF FPC}
    THashString = Unicodestring;
    {$ELSE}
    THashString = string;
    {$ENDIF}

    THash = class abstract
    public
      class function ElfHash(const Data; DataLength: integer): cardinal; overload;
      class function ElfHash(const Text: THashString):  cardinal; overload;

      class function KAndRHash(const Data; DataLength: integer):  cardinal; overload;
      class function KAndRHash(const Text: THashString):  cardinal; overload;

      class function AdlerHash(const Adler: Cardinal; const Data; DataLength: integer):  cardinal; overload;
      class function AdlerHash(const Data; DataLength: integer):  cardinal; overload;
      class function AdlerHash(const Text: THashString):  cardinal; overload;

      class function BorlandHash(const Data; DataLength: integer):  cardinal; overload;
      class function BorlandHash(const Text: THashString):  cardinal; overload;

      class function BobJenkinsHash(const Data; DataLength: integer):  cardinal; overload;
      class function BobJenkinsHash(const Text: THashString):  cardinal; overload;
    end;

    TTyped = class abstract
    public
      type
      PTripleByte = ^TTripleByte;
      TTripleByte = packed record
        a:  byte;
        b:  byte;
        c:  byte;
      end;

      class function  GetTempFileName: string;

      // Note: FPC has similar routines in System (!)
      // You might want to use those instead, and fall back on these under Delphi
      class procedure FillByte(Target: Pbyte; const FillSize: integer; const Value: byte); inline; static;
      class procedure FillWord(Target: PWord; const FillSize: integer; const Value: word); inline; static;
      class procedure FillTriple(dstAddr: PTripleByte; const InCount: integer; const Value: TTripleByte); inline; static;
      class procedure FillLong(dstAddr: PCardinal; const InCount: integer; const Value: cardinal); inline; static;

      class function  ToNearest(const Value, Factor: integer): integer; inline; static;
      class procedure Swap(var Primary, Secondary: integer); inline; static;
      class function  Span(const Primary, Secondary: integer; const WithZero: boolean = false): integer; inline; static;

      class function  BitsOf(const FromByteCount: integer): integer; inline; static;
      class function  BytesOf(FromBitCount: integer): integer; inline; static;
      class function  BitsSetInByte(const Value: byte): integer; {$IFDEF FPC} inline; static; {$ENDIF}

      class Function  BitGet(const Index: integer; const Buffer): boolean; static;
      class procedure BitSet(const Index: integer; var Buffer; const Value: boolean); static;

      class function  CalcTrailFor(Index, ItemSize, TotalItems: integer): integer; overload; inline; static;
      class function  CalcTrailFor(Index, ItemSize, TotalItems: int64): int64; overload; inline; static;

      class function  CalcOffsetFor(Index, ItemSize, TotalItems: integer): integer; overload; inline; static;
      class function  CalcOffsetFor(Index, ItemSize, TotalItems: int64): int64; overload; inline; static;
    end;

    TStreamAdapter = class(TStream)
    strict private
      FBufObj:    TStorage;
      FOffset:    int64;
    strict protected
      type
      EQTXStreamAdapter = class(Exception);
    protected
      function    GetSize: Int64; override;
      procedure   SetSize(const NewSize: Int64); override;
    public
      property    BufferObj: TStorage read FBufObj;
      function    Read(var Buffer; Count: longint):  longint; override;
      function    Write(const Buffer; Count: longint):  longint; override;
      function    Seek(const Offset: Int64; Origin: TSeekOrigin): Int64; override;
      constructor Create(const SourceBuffer: TStorage); reintroduce;
    end;

    TBitStorage = class(TInterfacedPersistent)
    strict private
      FData:      PByte;
      FDataLen:   integer;
      FBitsMax:   integer;
      FReadyByte: integer;
    strict protected
      type
      EBitStorage = class(Exception);
      EBitStorageInvalidBitIndex  = class(EBitStorage);
      EBitStorageHeaderInvalid    = class(EBitStorage);
      EBitStorageInvalidByteIndex = class(EBitStorage);
      EBitStorageEmpty            = class(EBitStorage);

    strict protected
      function    GetByte(const Index: integer): byte; virtual;
      procedure   SetByte(const Index: integer; const Value: byte); virtual;

      function    GetBit(const Index: integer): boolean; virtual;
      procedure   SetBit(const Index: integer; const Value: boolean); virtual;

      function    GetSizeOfHeader: integer; virtual;
      procedure   WriteHeader(const Writer: TWriter); virtual;
      function    ReadHeader(const Reader: TReader): integer; virtual;

      function    GetEmpty: boolean; virtual;
      function    GetFileSize: integer; inline;
      function    GetFreeBits: integer; inline;

    protected
      // standard Delphi/LCL persistence methods
      function    ObjectHasData: boolean; virtual;
      procedure   ReadObjBin(Stream: TStream); virtual;
      procedure   WriteObjBin(Stream: TStream); virtual;
      procedure   DefineProperties(Filer: TFiler); override;
    public
      property    Empty: boolean read GetEmpty;
      property    Data: PByte read FData;
      property    ByteSize: integer read FDataLen;
      property    FileSize: integer read GetFileSize;
      property    BitsAvailable: integer read GetFreeBits;
      property    Count: integer read FBitsMax;
      property    Bytes[const Index: integer]: byte Read GetByte write SetByte;
      property    Bits[const Index: integer]: boolean Read GetBit write SetBit; default;

      procedure   Allocate(MaxBits: integer);
      procedure   ReAllocate(NewMaxBits: integer);
      procedure   Release;
      procedure   Zero; inline;

      procedure   Assign(Source: TPersistent); override;

      function    ToStream: TStream;
      procedure   FromStream(const Stream: TStream; const Disposable: boolean = false);
      function    ToString(const Boundary: integer = 16): string; reintroduce;

      procedure   SaveToStream(const Stream: TStream); virtual;
      procedure   LoadFromStream(const Stream: TStream); virtual;

      procedure   SetBitRange(First, Last: integer; const Bitvalue: boolean); inline;
      procedure   SetBits(const Values: TArray<integer>; const Bitvalue: boolean); inline;
      function    FindIdleBit(var Value: integer; const FromStart: boolean = false): boolean; inline;

      constructor Create; virtual;
      destructor  Destroy; override;
    end;

    IPropertyElement = interface
      ['{C6C937DF-50FA-4984-BA6F-EBB0B367D3F3}']
      function  GetAsInt: integer;
      procedure SetAsInt(const Value: integer);

      function  GetAsString: string;
      procedure SetAsString(const Value: string);

      function  GetAsBool: boolean;
      procedure SetAsBool(const Value: boolean);

      function  GetAsFloat: double;
      procedure SetAsFloat(const Value: double);

      function  GetEmpty: boolean;

      property Empty: boolean read GetEmpty;
      property AsFloat: double read GetAsFloat write SetAsFloat;
      property AsBoolean: boolean read GetAsBool write SetAsBool;
      property AsInteger: integer read GetAsInt write SetAsInt;
      property AsString: string read GetAsString write SetAsString;
    end;

    IPropertyBag = interface
      ['{EE457334-2FB9-4BC2-9A6B-4A28EB30223C}']
      function    Read(Name: string): IPropertyElement;
      function    Write(Name: string; Value: string): IPropertyElement;
      procedure   SaveToStream(const Stream: TStream);
      procedure   LoadFromStream(const Stream: TStream);
      function    ToString: string;
      procedure   Clear;
    end;

    TPropertyBag = Class(TInterfacedObject, IPropertyBag)
    strict private
      FLUT: TDictionary<string, string>;
    strict protected
      procedure   Parse(NameValuePairs: string);
    public
      type
      EPropertybag           = class(exception);
      EPropertybagReadError  = class(EPropertybag);
      EPropertybagWriteError = class(EPropertybag);
      EPropertybagParseError = class(EPropertybag);

    protected
      type
      TPropertyElement = class(TInterfacedObject, TQTXStorage.IPropertyElement)
      strict private
        FName:      string;
        FData:      string;
        FStorage:   TDictionary<string, string>;
      strict protected
        function    GetEmpty: boolean; inline;

        function    GetAsInt: integer; inline;
        procedure   SetAsInt(const Value: integer); inline;

        function    GetAsString: string; inline;
        procedure   SetAsString(const Value: string); inline;

        function    GetAsBool: boolean; inline;
        procedure   SetAsBool(const Value: boolean); inline;

        function    GetAsFloat: double; inline;
        procedure   SetAsFloat(const Value: double); inline;

      public
        property    AsFloat: double read GetAsFloat write SetAsFloat;
        property    AsBoolean: boolean read GetAsBool write SetAsBool;
        property    AsInteger: integer read GetAsInt write SetAsInt;
        property    AsString: string read GetAsString write SetAsString;
        property    Empty: boolean read GetEmpty;

        constructor Create(const Storage: TDictionary<string, string>; Name: string; Data: string); overload; virtual;
        constructor Create(Data: string); overload; virtual;
      end;

    public
      function    Read(Name: string): IPropertyElement;
      function    Write(Name: string; Value: string): IPropertyElement;

      procedure   SaveToStream(const Stream: TStream);
      procedure   LoadFromStream(const Stream: TStream);
      function    ToString: string; override;
      procedure   Clear; virtual;

      constructor Create(NameValuePairs: string); virtual;
      destructor  Destroy; override;
    end;

    class function  CreatePropertyBag(NameValuePairs: string): IPropertyBag;
    class function  CreateTempFileBuffer: IStorage;
    class function  CreateBinarySearch(const Storage: IStorage): IBinarySearch;
    class function  CreateMemoryStorage: IStorage;
    class function  CreateFileStorage(FileName: string; StreamFlags: word): IStorage;
  end;

  TView<T> = class(TEnumerable<T>)
  strict protected
    type
    TViewEnumerator = class(TEnumerator<T>)
    private
      FIndex:     integer;
      FParent:    TView<T>;
    protected
      function    DoGetCurrent: T; override;
      function    DoMoveNext: boolean; override;
    public
      constructor Create(Owner: TView<T>); virtual;
    end;
  strict private
    FBuffer:    TQTXStorage.TStorage;
  strict protected
    function    GetCount: integer; virtual;
    function    GetItem(const Index: integer): T; virtual;
    procedure   SetItem(const Index: integer; const Value: T); virtual;
  protected
    function    DoGetEnumerator: TEnumerator<T>;  override;
    {$IFDEF FPC}
    function    GetPtrEnumerator: TEnumerator<PT>; override;
    {$ENDIF}
  public
    property    Buffer: TQTXStorage.TStorage read FBuffer;
    property    Count: integer read GetCount;
    property    Items[const Index: integer]: T read GetItem write SetItem; default;

    procedure   ToArrayEx(var Value: TArray<T>); overload;
    function    ToArrayEx(const Index: integer; Size: integer): TArray<T>; overload;

    function    ToList: TList<T>; overload;
    procedure   ToList(var List: TList<T>); overload;

    procedure   SetItems(const Index: integer; Values: TArray<T>);

    constructor Create(const Buffer: TQTXStorage.TStorage); virtual;
  end;

  // Views for common, intrinsic types
  TUInt8View    = class(TView<byte>);
  TUInt16View   = class(TView<word>);
  TUInt32View   = class(TView<cardinal>);
  TUInt64View   = class(TView<Uint64>);
  TInt8View     = class(TView<shortint>);
  TInt16View    = class(TView<smallint>);
  TInt32View    = class(TView<integer>);
  TInt64View    = class(TView<int64>);
  TBoolView     = class(TView<bytebool>);
  TSingleView   = class(TView<single>);
  TDoubleView   = class(TView<double>);
  TDateTimeView = class(TView<TDateTime>);
  TTimeView     = class(TView<TTime>);
  TDateView     = class(TView<TDate>);

  TVector<T> = class(TEnumerable<T>)

  strict protected
    type
    TVectorEnumerator = class(TEnumerator<T>)
    private
      FIndex:     integer;
      FParent:    TVector<T>;
    protected
      function    DoGetCurrent: T; override;
      function    DoMoveNext: boolean; override;
    public
      constructor Create(Owner: TVector<T>); virtual;
    end;

  public
    type
    EVector                    = class(Exception);
    EVectorEmpty               = class(EVector);
    EVectorInvalidIndex        = class(EVector);
    EVectorAllocator           = class(Exception);
    EVectorAllocatorLocked     = class(EVectorAllocator);
    EVectorAllocatorNotLocked  = class(EVectorAllocator);
    EVectorUnSupported         = class(EVectorAllocator);
    EVectorReadOnly            = class(EVectorAllocator);

    IVectorAllocator = interface
      ['{9A3A02CB-5A4B-409F-B999-4728B3636A1E}']
      procedure   Resize(ItemCount: integer);
      procedure   LockMemory(var Data);
      procedure   UnLockMemory;
      function    GetDataSize: int64;
      procedure   SetItem(const index: integer; Value: T);
      function    GetItem(const index: integer): T;
      function    GetCount: integer;
      function    Add(const Value: T): integer;
      procedure   Remove(const Index: integer);
      function    Insert(const Index: integer; Value: T): integer;
      procedure   Clear;
    end;

    TVectorAllocator = class(TInterfacedPersistent, IVectorAllocator)
    public
      procedure   Resize(ItemCount: integer); virtual; abstract;
      procedure   LockMemory(var Data); virtual; abstract;
      procedure   UnLockMemory; virtual; abstract;
      function    GetDataSize: int64; virtual; abstract;
      procedure   SetItem(const index: integer; Value: T); virtual; abstract;
      function    GetItem(const index: integer): T; virtual; abstract;
      function    GetCount: integer;  virtual; abstract;
      function    Add(const Value: T): integer;  virtual; abstract;
      procedure   Remove(const Index: integer);  virtual; abstract;
      function    Insert(const Index: integer; Value: T): integer;  virtual; abstract;
      procedure   Clear;  virtual; abstract;
    end;
    TVectorAllocatorClass = class of TVectorAllocator;

    // Ancestor for allocators that use buffers internally to deal
    // with storage management
    TVectorAllocatorBuffered = class(TVectorAllocator)
    strict protected
      FBuffer:    TQTXStorage.TStorage;
      function    GetStorageReference: TQTXStorage.IStorage;
    public
      property    Buffer: TQTXStorage.IStorage read GetStorageReference;
      procedure   Resize(ItemCount: integer); override;
      function    GetDataSize: int64; override;
      procedure   SetItem(const index: integer; Value: T); override;
      function    GetItem(const index: integer): T;  override;
      function    GetCount: integer;  override;
      function    Add(const Value: T): integer;  override;
      procedure   Remove(const Index: integer);  override;
      function    Insert(const Index: integer; Value: T): integer;  override;
      procedure   Clear;  override;
    end;

    // Sequenced memory management
    TVectorAllocatorSequence = class(TVectorAllocatorBuffered)
    strict private
      FLock:      integer;
    public
      procedure   LockMemory(var Data); override;
      procedure   UnLockMemory; override;

      constructor Create; virtual;
      destructor  Destroy; override;
    end;

    // Sequential file management
    TVectorAllocatorFile = class(TVectorAllocatorBuffered)
    private
      FMode:      word;
      FName:      string;
    public
      property    Filename: string read FName;
      property    AccessMode: word read FMode;

      procedure   LockMemory(var Data); override;
      procedure   UnLockMemory; override;
      constructor Create(Filename: string; AccessFlags: word); virtual;
      destructor  Destroy; override;
    end;

    // External management (map to a buffer, buffer can read/write to anywhere)
    TVectorAllocatorMapped = class(TVectorAllocatorBuffered)
    public
      procedure   Resize(ItemCount: integer); override;
      function    GetDataSize: int64; override;
      procedure   LockMemory(var Data); override;
      procedure   UnLockMemory; override;
      procedure   SetItem(const index: integer; Value: T); override;
      function    GetItem(const index: integer): T;  override;
      function    GetCount: integer;  override;
      function    Add(const Value: T): integer;  override;
      procedure   Remove(const Index: integer);  override;
      function    Insert(const Index: integer; Value: T): integer;  override;
      procedure   Clear;  override;

      constructor Create(const Buffer: TQTXStorage.TStorage); virtual;
      destructor  Destroy; override;
    end;

    // Dynamic memory management
    TVectorAllocatorDynamic = class(TVectorAllocator)
    strict private
      FValues:    TArray<T>;
      FLock:      integer;
    public
      procedure   Resize(ItemCount: integer); override;
      function    GetDataSize: int64; override;
      procedure   LockMemory(var Data); override;
      procedure   UnLockMemory; override;
      procedure   SetItem(const index: integer; Value: T); override;
      function    GetItem(const index: integer): T;  override;
      function    GetCount: integer;  override;
      function    Add(const Value: T): integer;  override;
      procedure   Remove(const Index: integer);  override;
      function    Insert(const Index: integer; Value: T): integer;  override;
      procedure   Clear;  override;
      destructor  Destroy; override;
    end;

  strict private
    FMemory:    IVectorAllocator;
    FType:      TVectorAllocatorType;

  strict protected
    function    GetCount: integer; inline;
    function    GetItem(const index: integer): T; inline;
    procedure   SetItem(const index: integer; Value: T); inline;
    function    GetFront: T; inline;
    function    GetBack: T; inline;

  protected
    function    DoGetEnumerator: TEnumerator<T>;  override;
    {$IFDEF FPC}
    function    GetPtrEnumerator: TEnumerator<PT>; override;
    {$ENDIF}
  public
    property    Memory: IVectorAllocator read FMemory;
    property    AllocatorType: TVectorAllocatorType read FType;
    property    Items[const index: integer]:T read GetItem write SetItem; default;
    property    Count: integer read GetCount;

    property    Front: T read GetFront;
    property    Back: T read GetBack;

    procedure   Assign(const Source: TVector<T>); virtual;

    function    Add(const Value: T): integer; inline;
    procedure   Remove(const Index: integer); inline;
    function    Insert(const Index: integer; const Value: T): integer; inline;

    procedure   Clear; inline;

    procedure   PushBack(const Value: T); inline;
    procedure   PushFront(const Value: T); inline;
    procedure   Swap(const First, Second: integer);

    constructor Create; overload; virtual;
    constructor Create(const Allocator: TVectorAllocatorType = vaSequence); overload; virtual;
    constructor Create(const FileName: string; const Access: word); overload; virtual;
    constructor Create(const Buffer: TQTXStorage.TStorage); overload; virtual;
    constructor Create(const Allocator: IVectorAllocator); overload; virtual;

    destructor  Destroy; override;
  end;

// Note: The way generics work under FPC, references must be within the
// global symbol-table. So these cant be put in the implementation section
resourcestring
  CNT_ERR_VECTOR_InvalidIndex   = 'Invalid index, expected 0..0 not %d';
  CNT_ERR_VECTOR_Empty          = 'Invalid operation, vector list is empty';
  CNT_ERR_VECTOR_NoCustomAlloc  = 'Custom vector allocator cannot be created with this constructor';
  CNT_ERR_VECTOR_MemLocked      = 'Vector memory is locked';
  CNT_ERR_VECTOR_MemUnLocked    = 'Vector memory was unlocked';
  CNT_ERR_VECTOR_LockNotSupported = 'Vector does not support memory lock';
  CNT_ERR_VECTOR_ResizeFailed   = 'Resize failed, invalid item count [%d]';
  CNT_ERR_VECTOR_ReadOnly       = 'Vector is mapped or read only';

// Note: Delphi for some reason wants this in the global scope when accessed
// from an inline procedure
var
  CNT_BiTStorage_ByteTable:  array [0..255] of integer =
  (0, 1, 1, 2, 1, 2, 2, 3, 1, 2, 2, 3, 2, 3, 3, 4,
  1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
  1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
  2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
  1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
  2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
  2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
  3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
  1, 2, 2, 3, 2, 3, 3, 4, 2, 3, 3, 4, 3, 4, 4, 5,
  2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
  2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
  3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
  2, 3, 3, 4, 3, 4, 4, 5, 3, 4, 4, 5, 4, 5, 5, 6,
  3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
  3, 4, 4, 5, 4, 5, 5, 6, 4, 5, 5, 6, 5, 6, 6, 7,
  4, 5, 5, 6, 5, 6, 6, 7, 5, 6, 6, 7, 6, 7, 7, 8);

// Note: Delphi needs this in the global scope for inline
const
  CNT_BITStorage_SIGNATURE         = $2109FAAD;
  CNT_BiTStorage_Empty             = 'Buffer is empty error';
  CNT_BiTStorage_InvalidBitIndex   = 'Invalid bit index, expected 0..%d not %d';
  CNT_BiTStorage_InvalidByteIndex  = 'Invalid byte index, expected 0..%d not %d';
  CNT_BiTStorage_InternalError     = 'Internal error, index found beyond buffer boundary';
  CNT_BiTStorage_InvalidFileHeader = 'Invalid file header, expected $%s not $%s error';

implementation


resourcestring
  CNT_ERR_BUFFER_BASE  = 'Method %s threw exception %s with %s';
  CNT_ERR_BUFFER_RELEASENOTSUPPORTED  = 'Storage capabillities does not allow release';
  CNT_ERR_BUFFER_SCALENOTSUPPORTED    = 'Storage capabillities does not allow scaling';
  CNT_ERR_BUFFER_READNOTSUPPORTED     = 'Storage capabillities does not allow read';
  CNT_ERR_BUFFER_WRITENOTSUPPORTED    = 'Storage capabillities does not allow write';
  CNT_ERR_BUFFER_SOURCEREADNOTSUPPORTED = 'data-source does not allow read';
  CNT_ERR_BUFFER_SCALEFAILED          = 'Storage scale operation failed: %s';
  CNT_ERR_BUFFER_BYTEINDEXVIOLATION   = 'Storage byte index violation, expected %d..%d not %d';
  CNT_ERR_BUFFER_INVALIDDATASOURCE    = 'Invalid data-source for operation';
  CNT_ERR_BUFFER_INVALIDDATATARGET    = 'Invalid data-target for operation';
  CNT_ERR_BUFFER_EMPTY                = 'Storage resource contains no data error';
  CNT_ERR_BUFFER_NOTACTIVE            = 'File is not active error';
  CNT_ERR_STREAMADAPTER_INVALIDBUFFER  = 'Invalid buffer error, buffer is NIL';
  CNT_ERR_BUFFER_INVALIDFILENAME      = 'Invalid filename [%s] error';
  CNT_ERR_BUFFER_InvalidReference     = 'Storage reference of instance was nil error';
  CNT_ERR_BUFFER_InvalidDataSize      = 'Invalid size for operatin error';

{$IFDEF USE_ARRAYS}
type
  // Delphi has a 2Gb limit on arrays
  TByteAccessArray = packed array[0..2147483648-2] of byte;
  PByteAccessArray = ^TByteAccessArray;
{$ENDIF}

const
  cnt_err_sourceparameters_parse =
  'Failed to parse input, invalid or damaged text error [%s]';

  cnt_err_sourceparameters_write_id =
  'Write failed, invalid or empty identifier error';

  cnt_err_sourceparameters_read_id =
  'Read failed, invalid or empty identifier error';

//#############################################################################
// TPropertyElement
//#############################################################################

constructor TQTXStorage.TPropertyBag.TPropertyElement.Create(Data: string);
begin
  inherited Create;
  FData := Data.Trim();
end;

constructor TQTXStorage.TPropertyBag.TPropertyElement.Create(const Storage: TDictionary<string, string>;
  Name: string; Data: string);
begin
  inherited Create;
  FStorage := Storage;
  FName := Name.Trim().ToLower();
  FData := Data.Trim();
end;

function TQTXStorage.TPropertyBag.TPropertyElement.GetEmpty: boolean;
begin
  result := FData.Length < 1;
end;

function TQTXStorage.TPropertyBag.TPropertyElement.GetAsString: string;
begin
  result := FData;
end;

procedure TQTXStorage.TPropertyBag.TPropertyElement.SetAsString(const Value: string);
begin
  if Value <> FData then
  begin
    FData := Value;
    if FName.Length > 0 then
    begin
      if FStorage <> nil then
        FStorage.AddOrSetValue(FName, Value);
    end;
  end;
end;

function TQTXStorage.TPropertyBag.TPropertyElement.GetAsBool: boolean;
begin
  TryStrToBool(FData, result);
end;

procedure TQTXStorage.TPropertyBag.TPropertyElement.SetAsBool(const Value: boolean);
begin
  FData := BoolToStr(Value, true);

  if FName.Length > 0 then
  begin
    if FStorage <> nil then
      FStorage.AddOrSetValue(FName, FData);
  end;
end;

function TQTXStorage.TPropertyBag.TPropertyElement.GetAsFloat: double;
begin
  TryStrToFloat(FData, result);
end;

procedure TQTXStorage.TPropertyBag.TPropertyElement.SetAsFloat(const Value: double);
begin
  FData := FloatToStr(Value);
  if FName.Length > 0 then
  begin
    if FStorage <> nil then
      FStorage.AddOrSetValue(FName, FData);
  end;
end;

function TQTXStorage.TPropertyBag.TPropertyElement.GetAsInt: integer;
begin
  TryStrToInt(FData, Result);
end;

procedure TQTXStorage.TPropertyBag.TPropertyElement.SetAsInt(const Value: integer);
begin
  FData := IntToStr(Value);
  if FName.Length > 0 then
  begin
    if FStorage <> nil then
      FStorage.AddOrSetValue(FName, FData);
  end;
end;

//#############################################################################
// TPropertyBag
//#############################################################################

constructor TQTXStorage.TPropertyBag.Create(NameValuePairs: string);

begin
  inherited Create;
  FLUT := TDictionary<string, string>.Create();

  NameValuePairs := NameValuePairs.Trim();
  if NameValuePairs.Length > 0 then
    Parse(NameValuePairs);
end;

destructor TQTXStorage.TPropertyBag.Destroy;
begin
  FLut.Free;
  inherited;
end;

procedure TQTXStorage.TPropertyBag.Clear;
begin
  FLut.Clear;
end;

procedure TQTXStorage.TPropertyBag.Parse(NameValuePairs: string);
var
  LList:      TStringList;
  x:          integer;
  LId:        string;
  LValue:     string;
  LOriginal:  string;
begin
  // Reset content
  FLUT.Clear();

  // Make a copy of the original text
  LOriginal := NameValuePairs;

  // Trim and prepare
  NameValuePairs := NameValuePairs.Trim();

  // Anything to work with?
  if NameValuePairs.Length > 0 then
  begin
    (* Populate our lookup table *)
    LList := TStringList.Create;
    try
      LList.Delimiter := ';';
      LList.StrictDelimiter := true;
      LList.DelimitedText := NameValuePairs;

      if LList.Count = 0 then
        raise EPropertybagParseError.CreateFmt(cnt_err_sourceparameters_parse, [LOriginal]);

      try
        for x := 0 to LList.Count-1 do
        begin
          LId := LList.Names[x].Trim().ToLower();
          if (LId.Length > 0) then
          begin
            LValue := LList.ValueFromIndex[x].Trim();
            Write(LId, LValue);
          end;
        end;
      except
        on e: exception do
        raise EPropertybagParseError.CreateFmt(cnt_err_sourceparameters_parse, [LOriginal]);
      end;
    finally
      LList.Free;
    end;
  end;
end;

function TQTXStorage.TPropertyBag.ToString: string;
var
  LItem: TPair<string, string>;
begin
  setlength(result, 0);
  for LItem in FLut do
  begin
    if LItem.Key.Trim().Length > 0 then
    begin
      result := result + Format('%s=%s;', [LItem.Key, LItem.Value]);
    end;
  end;
end;

procedure TQTXStorage.TPropertyBag.SaveToStream(const Stream: TStream);
var
  LData: TStringStream;
begin
  LData := TStringStream.Create(ToString(), TEncoding.UTF8);
  try
    LData.SaveToStream(Stream);
  finally
    LData.Free;
  end;
end;

procedure TQTXStorage.TPropertyBag.LoadFromStream(const Stream: TStream);
var
  LData: TStringStream;
begin
  LData := TStringStream.Create('', TEncoding.UTF8);
  try
    LData.LoadFromStream(Stream);
    Parse(LData.DataString);
  finally
    LData.Free;
  end;
end;

function TQTXStorage.TPropertyBag.Write(Name: string; Value: string): IPropertyElement;
begin
  Name := Name.Trim().ToLower();
  if Name.Length > 0 then
  begin
    if not FLUT.ContainsKey(Name) then
      FLut.Add(Name, Value);

    result := TPropertyElement.Create(FLut, Name, Value) as IPropertyElement;
  end else
  raise EPropertybagWriteError.Create(cnt_err_sourceparameters_write_id);
end;

function TQTXStorage.TPropertyBag.Read(Name: string): IPropertyElement;
var
  LData:  String;
begin
  Name := Name.Trim().ToLower();
  if Name.Length > 0  then
  begin
    if FLut.TryGetValue(Name, LData) then
      result := TPropertyElement.Create(LData) as IPropertyElement
    else
      raise EPropertybagReadError.Create(cnt_err_sourceparameters_read_id);
  end else
  raise EPropertybagReadError.Create(cnt_err_sourceparameters_read_id);
end;

//#############################################################################
// TVector<T>
//#############################################################################

constructor TVector<T>.TVectorAllocatorMapped.Create(const Buffer: TQTXStorage.TStorage);
begin
  inherited Create();
  FBuffer := Buffer;
end;

destructor TVector<T>.TVectorAllocatorMapped.Destroy;
begin
  inherited;
end;

function TVector<T>.TVectorAllocatorMapped.Add(const Value: T): integer;
begin
  raise EVectorReadOnly.Create(CNT_ERR_VECTOR_ReadOnly);
end;

procedure TVector<T>.TVectorAllocatorMapped.Resize(ItemCount: integer);
begin
  raise EVectorReadOnly.Create(CNT_ERR_VECTOR_ReadOnly);
end;

function TVector<T>.TVectorAllocatorMapped.GetDataSize: int64;
begin
  result := FBuffer.Size;
end;

procedure TVector<T>.TVectorAllocatorMapped.LockMemory(var Data);
begin
  raise EVectorReadOnly.Create(CNT_ERR_VECTOR_ReadOnly);
end;

procedure TVector<T>.TVectorAllocatorMapped.UnLockMemory;
begin
  raise EVectorReadOnly.Create(CNT_ERR_VECTOR_ReadOnly);
end;

procedure TVector<T>.TVectorAllocatorMapped.Clear;
begin
  raise EVectorReadOnly.Create(CNT_ERR_VECTOR_ReadOnly);
end;

function TVector<T>.TVectorAllocatorMapped.GetCount: integer;
begin
  if FBuffer.Size > 0 then
    result := FBuffer.Size div SizeOf(T)
  else
    result := 0;
end;

function TVector<T>.TVectorAllocatorMapped.GetItem(const index: integer): T;
begin
  FBuffer.Read(SizeOf(T) * Index, SizeOf(T), result);
end;

procedure TVector<T>.TVectorAllocatorMapped.SetItem(const index: integer; Value: T);
begin
  raise EVectorReadOnly.Create(CNT_ERR_VECTOR_ReadOnly);
end;

function TVector<T>.TVectorAllocatorMapped.Insert(const Index: integer; Value: T): integer;
begin
  raise EVectorReadOnly.Create(CNT_ERR_VECTOR_ReadOnly);
end;

procedure TVector<T>.TVectorAllocatorMapped.Remove(const Index: integer);
begin
  raise EVectorReadOnly.Create(CNT_ERR_VECTOR_ReadOnly);
end;

//#############################################################################
// TVector<T>
//#############################################################################

constructor TVector<T>.TVectorEnumerator.Create(Owner: TVector<T>);
begin
  inherited Create;
  FParent := Owner;
  FIndex := -1;
end;

function TVector<T>.TVectorEnumerator.DoGetCurrent: T;
begin
  if FIndex < 0 then
    inc(FIndex);
  result := FParent[FIndex];
end;

function TVector<T>.TVectorEnumerator.DoMoveNext: boolean;
begin
  result := (FIndex + 1) < FParent.GetCount();
  if result then
    inc(FIndex);
end;

//#############################################################################
// TVector<T>
//#############################################################################

constructor TVector<T>.Create;
begin
  inherited Create;
  FType := vaDynamic;
  FMemory := TVectorAllocatorDynamic.Create();
end;

constructor TVector<T>.Create(const Allocator: IVectorAllocator);
begin
  inherited Create();
  FType := vaCustom;
  FMemory := Allocator;
end;

constructor TVector<T>.Create(const FileName: string; const Access: word);
var
  lTemp: TVectorAllocator;
begin
  inherited Create();
  FType := TVectorAllocatorType.vaFile;
  lTemp := TVectorAllocatorFile.Create(FileName, Access);
  FMemory := lTemp as IVectorAllocator;
end;

constructor TVector<T>.Create(const Buffer: TQTXStorage.TStorage);
var
  lTemp: TVectorAllocator;
begin
  inherited Create();
  FType := TVectorAllocatorType.vaMapped;
  lTemp := TVectorAllocatorMapped.Create(Buffer);
  FMemory := lTemp as IVectorAllocator;
end;

constructor TVector<T>.Create(const Allocator: TVectorAllocatorType = vaSequence);
var
  lTemp:  TVectorAllocator;
begin
  inherited Create();
  FType := Allocator;

  case FType of
  vaSequence: lTemp := TVectorAllocatorSequence.Create();
  vaDynamic:  lTemp := TVectorAllocatorDynamic.Create();
  vaFile:     lTemp := TVectorAllocatorFile.Create(TQTXStorage.TTyped.GetTempFileName(), fmCreate);
  else        raise EVector.Create(CNT_ERR_VECTOR_NoCustomAlloc);
  end;

  FMemory := lTemp as IVectorAllocator;
end;

destructor TVector<T>.Destroy;
begin
  FMemory := nil;
  inherited;
end;

{$IFDEF FPC}
function TVector<T>.GetPtrEnumerator: TEnumerator<PT>;
begin
  Result := nil;
end;
{$ENDIF}

function TVector<T>.DoGetEnumerator: TEnumerator<T>;
begin
  Result := TVectorEnumerator.Create(self);
end;

procedure TVector<T>.Assign(const Source: TVector<T>);
var
  x: integer;
begin
  FMemory.Clear();
  if Source <> nil then
  begin
    if Source.Count < 1 then
      exit;

    FMemory.Resize(Source.Count);
    for x := 0 to Source.Count-1 do
    begin
      FMemory.SetItem(x, Source[x]);
    end;
  end;
end;

procedure TVector<T>.Clear;
begin
  FMemory.Clear();
end;

procedure TVector<T>.SetItem(const index: integer; Value: T);
begin
  FMemory.SetItem(Index, Value);
end;

procedure TVector<T>.Swap(const First, Second: integer);
var
  lTemp: T;
  lCount: integer;
begin
  lCount := FMemory.GetCount();
  if lCount > 0 then
  begin
    if (First >=0) and (First < lCount) then
    begin
      if (Second >= 0) and (Second < lCount) then
      begin
        lTemp := FMemory.GetItem(First);
        FMemory.SetItem(First, FMemory.GetItem(Second));
        FMemory.SetItem(Second, lTemp);
      end;
    end;
  end else
  raise EVectorEmpty.Create(CNT_ERR_VECTOR_Empty);
end;

procedure TVector<T>.PushBack(const Value: T);
begin
  FMemory.Add(Value);
end;

procedure TVector<T>.PushFront(const Value: T);
begin
  if GetCount() > 0 then
    Insert(0, Value)
  else
    Add(Value);
end;

function TVector<T>.GetFront: T;
begin
  result := FMemory.GetItem(0);
end;

function TVector<T>.GetBack: T;
begin
  result := FMemory.GetItem( FMemory.GetCount() -1 );
end;

function TVector<T>.Add(const Value: T): integer;
begin
  result := FMemory.Add(Value);
end;

procedure TVector<T>.Remove(const Index: integer);
begin
  FMemory.Remove(Index);
end;

function TVector<T>.Insert(const Index: integer; const Value: T): integer;
begin
  result := FMemory.Insert(Index, Value);
end;

function TVector<T>.GetCount: integer;
begin
  result := FMemory.GetCount();
end;

function TVector<T>.GetItem(const index: integer): T;
begin
  result := FMemory.GetItem(Index);
end;

//#############################################################################
// TVectorAllocatorBuffered
//#############################################################################

function TVector<T>.TVectorAllocatorBuffered.GetStorageReference: TQTXStorage.IStorage;
begin
  result := TQTXStorage.IStorage(FBuffer);
end;

procedure TVector<T>.TVectorAllocatorBuffered.Clear;
begin
  FBuffer.Release();
end;

procedure TVector<T>.TVectorAllocatorBuffered.Resize(ItemCount: integer);
var
  lCount: integer;
begin
  lCount := GetCount();
  if ItemCount <> lCount then
  begin
    if (lCount > 0) and (itemCount < 1) then
    begin
      FBuffer.Release();
      exit;
    end;

    if ItemCount > 0 then
    begin
      try
        FBuffer.Size := SizeOf(T) * ItemCount;
      except
        on e: exception do
        raise EVectorAllocator.CreateFmt(CNT_ERR_VECTOR_ResizeFailed, [ItemCount]);
      end;
    end;

  end;
end;

function TVector<T>.TVectorAllocatorBuffered.GetDataSize: int64;
begin
  result := FBuffer.Size;
end;

{$IFDEF BE_SILENT}
  {$WARNINGS OFF}
  {$HINTS OFF}
{$ENDIF}
function TVector<T>.TVectorAllocatorBuffered.GetItem(const index: integer): T;
var
  lCount: integer;
begin
  if not FBuffer.Empty then
  begin
    lCount := GetCount();
    if (index >= 0) and (index < lCount ) then
      FBuffer.Read(Index * SizeOf(T), SizeOf(T), result)
    else
    raise EVectorInvalidIndex.CreateFmt(CNT_ERR_VECTOR_InvalidIndex, [0, lCount-1, Index]);
  end else
  raise EVectorEmpty.Create(CNT_ERR_VECTOR_Empty);
end;
{$IFDEF BE_SILENT}
  {$WARNINGS ON}
  {$HINTS ON}
{$ENDIF}

procedure TVector<T>.TVectorAllocatorBuffered.SetItem(const index: integer; Value: T);
var
  lCount: integer;
begin
  if not FBuffer.Empty then
  begin
    lCount := GetCount();
    if (index >= 0) and (index < lCount ) then
      FBuffer.Write(Index * SizeOf(T), SizeOf(T), Value)
    else
    raise EVectorInvalidIndex.CreateFmt(CNT_ERR_VECTOR_InvalidIndex, [0, lCount-1, Index]);
  end else
  raise EVectorEmpty.Create(CNT_ERR_VECTOR_Empty);
end;

function TVector<T>.TVectorAllocatorBuffered.GetCount: integer;
var
  lSize:  int64;
begin
  lSize := FBuffer.Size;
  if lSize > 0 then
    result := lSize div SizeOf(T)
  else
    result := 0;
end;

function TVector<T>.TVectorAllocatorBuffered.Add(const Value: T): integer;
begin
  FBuffer.Append(Value, SizeOf(T));
  result := FBuffer.Size div SizeOf(T);
end;

procedure TVector<T>.TVectorAllocatorBuffered.Remove(const Index: integer);
var
  lCount: integer;
  lNewSize: int64;
begin
  lCount := GetCount();

  if lCount < 1 then
    raise EVectorEmpty.Create(CNT_ERR_VECTOR_Empty);

  if Index < 0 then
    raise EVectorInvalidIndex.CreateFmt(CNT_ERR_VECTOR_InvalidIndex, [0, lCount-1, Index]);

  if Index > (lCount-1) then
    raise EVectorInvalidIndex.CreateFmt(CNT_ERR_VECTOR_InvalidIndex, [0, lCount-1, Index]);

  lNewSize := FBuffer.Size - SizeOf(T);

  // Will the list be empty after this?
  if lNewSize < 1 then
  begin
    FBuffer.Release();
    exit;
  end;

  // Truncate or scale?
  if Index = lCount-1 then
    FBuffer.Size := lNewSize
  else
    FBuffer.Remove(Index * SizeOf(T), SizeOf(T));
end;

function TVector<T>.TVectorAllocatorBuffered.Insert(const Index: integer; Value: T): integer;
var
  lCount: integer;
  lNewSize: integer;
begin
  lCount := GetCount();

  if lCount < 1 then
    raise EVectorEmpty.Create(CNT_ERR_VECTOR_Empty);

  if Index < 0 then
    raise EVectorInvalidIndex.CreateFmt(CNT_ERR_VECTOR_InvalidIndex, [0, lCount-1, Index]);

  // First item? Check that index is 0
  if FBuffer.Size < 1 then
  begin
    if Index = 0 then
    begin
      result := Add(Value);
      exit;
    end else
    raise EVectorInvalidIndex.CreateFmt(CNT_ERR_VECTOR_InvalidIndex, [0, 0, Index]);
  end;

  FBuffer.Insert(Index * SizeOf(T), Value, SizeOf(T));
end;

//#############################################################################
// TVectorAllocatorSequence
//#############################################################################

constructor TVector<T>.TVectorAllocatorSequence.Create;
begin
  inherited Create();
  FBuffer := TQTXStorage.TStorageMemory.Create();
end;

destructor TVector<T>.TVectorAllocatorSequence.Destroy;
begin
  FBuffer.Free;
  inherited;
end;

procedure TVector<T>.TVectorAllocatorSequence.LockMemory(var Data);
begin
  if FLock < 1 then
  begin
    inc(FLock);
    pointer(Data) := TQTXStorage.TStorageMemory(FBuffer).Data;
  end else
  raise EVectorAllocatorLocked.Create(CNT_ERR_VECTOR_MemLocked);
end;

procedure TVector<T>.TVectorAllocatorSequence.UnLockMemory;
begin
  if FLock > 0 then
    dec(FLock)
  else
  raise EVectorAllocatorNotLocked.Create(CNT_ERR_VECTOR_MemUnLocked);
end;

//#############################################################################
// TVector<T>.TVectorAllocatorDynamic
//#############################################################################

constructor TVector<T>.TVectorAllocatorFile.Create(Filename: string; AccessFlags: word);
begin
  inherited Create;
  FName := Filename;
  FMode := AccessFlags;
  FBuffer := TQTXStorage.TStorageFile.Create(FName, AccessFlags);
end;

destructor TVector<T>.TVectorAllocatorFile.Destroy;
begin
  FBuffer.free;
  FBuffer := nil;
  inherited;
end;

procedure TVector<T>.TVectorAllocatorFile.LockMemory(var Data);
begin
  raise EVectorUnSupported.Create(CNT_ERR_VECTOR_LockNotSupported);
end;

procedure TVector<T>.TVectorAllocatorFile.UnLockMemory;
begin
  raise EVectorUnSupported.Create(CNT_ERR_VECTOR_LockNotSupported);
end;

//#############################################################################
// TVector<T>.TVectorAllocatorDynamic
//#############################################################################

destructor TVector<T>.TVectorAllocatorDynamic.Destroy;
begin
  if length(FValues) > 0 then
    Clear();
  inherited;
end;

function TVector<T>.TVectorAllocatorDynamic.Add(const Value: T): integer;
begin
  result := length(FValues);
  SetLength(FValues, result + 1);
  FValues[result] := Value;
  inc(result);
end;

procedure TVector<T>.TVectorAllocatorDynamic.Resize(ItemCount: integer);
begin
  if ItemCount >= 0 then
    setlength(FValues, ItemCount)
  else
    EVectorAllocator.CreateFmt(CNT_ERR_VECTOR_ResizeFailed, [ItemCount]);
end;

function TVector<T>.TVectorAllocatorDynamic.GetDataSize: int64;
begin
  result := SizeOf(FValues);
end;

procedure TVector<T>.TVectorAllocatorDynamic.LockMemory(var Data);
begin
  if FLock < 1 then
  begin
    inc(FLock);
    Pointer(Data) := @FValues[0];
  end else
  raise EVectorAllocatorLocked.Create(CNT_ERR_VECTOR_MemLocked);
end;

procedure TVector<T>.TVectorAllocatorDynamic.UnLockMemory;
begin
  if FLock > 0 then
    dec(FLock)
  else
  raise EVectorAllocatorNotLocked.Create(CNT_ERR_VECTOR_MemUnLocked);
end;

procedure TVector<T>.TVectorAllocatorDynamic.Clear;
begin
  setlength(FValues, 0);
end;

function TVector<T>.TVectorAllocatorDynamic.GetCount: integer;
begin
  result := length(FValues);
end;

function TVector<T>.TVectorAllocatorDynamic.GetItem(const index: integer): T;
begin
  result := FValues[Index];
end;

procedure TVector<T>.TVectorAllocatorDynamic.SetItem(const index: integer; Value: T);
begin
  FValues[Index] := Value;
end;

function TVector<T>.TVectorAllocatorDynamic.Insert(const Index: integer; Value: T): integer;
begin
  system.insert(Value, FValues, Index);
  result := length(FValues);
end;

procedure TVector<T>.TVectorAllocatorDynamic.Remove(const Index: integer);
begin
  system.delete(FValues, Index, 1);
end;

//##########################################################################
// TQTXStorage
//##########################################################################

class function TQTXStorage.CreateMemoryStorage: IStorage;
var
  lTemp: TStorageMemory;
begin
  lTemp := TStorageMemory.Create();
  result := IStorage(lTemp);
end;

class function TQTXStorage.CreateFileStorage(FileName: string; StreamFlags: word): IStorage;
var
  lTemp: TStorageFile;
begin
  lTemp := TStorageFile.Create(FileName, StreamFlags);
  result := IStorage(lTemp);
end;

class function TQTXStorage.CreateBinarySearch(const Storage: IStorage): IBinarySearch;
begin
  result := TStorageSearch.Create(Storage);
end;

class function TQTXStorage.CreateTempFileBuffer: IStorage;
var
  lTemp: TStorageFile;
begin
  lTemp := TStorageFile.Create(TTyped.GetTempFileName(), fmCreate);
  lTemp.MarkAsDisposable(true);
  result := lTemp;
end;

class function TQTXStorage.CreatePropertyBag(NameValuePairs: string): IPropertyBag;
begin
  result := TQTXStorage.TPropertyBag.Create(NameValuePairs);
end;

//#############################################################################
// TView<T>
//#############################################################################

constructor TView<T>.TViewEnumerator.Create(Owner: TView<T>);
begin
  inherited Create;
  FParent := Owner;
  FIndex := -1;
end;

function TView<T>.TViewEnumerator.DoGetCurrent: T;
begin
  if FIndex < 0 then
    inc(FIndex);
  result := FParent[FIndex];
end;

function TView<T>.TViewEnumerator.DoMoveNext: boolean;
begin
  result := (FIndex + 1) < FParent.GetCount();
  if result then
    inc(FIndex);
end;

//##########################################################################
// TView<T>
//##########################################################################

constructor TView<T>.Create(const Buffer: TQTXStorage.TStorage);
begin
  inherited Create();
  FBuffer := Buffer;
end;

procedure TView<T>.SetItems(const Index: integer; Values: TArray<T>);
var
  dx, x:  integer;
  lCount: integer;
  lRightMost: integer;
begin
  lCount := GetCount();
  if lCount < 1 then
    raise Exception.Create('View is empty error');

  if Index < 0 then
    raise Exception.CreateFmt('Invalid index, expected 0..%d, not %d', [lCount-1, index]);

  lRightMost := ( index + length(Values) ) -1;
  if lRightMost > lCount-1 then
    raise Exception.CreateFmt('Invalid span, expected 0..%d, not %d', [lCount-1, lRightMost]);

  dx := index;
  for x := low(Values) to high(values) do
  begin
    SetItem(dx, Values[x]);
    inc(dx);
  end;
end;

function TView<T>.ToList: TList<T>;
var
  idx:  integer;
  lCount: integer;
  lFactor: integer;
begin
  result := TList<T>.Create();
  lCount := GetCount();
  if lCount < 1 then
    exit;

  idx := 0;
  lFactor := lCount shr 3;
  while lFactor > 0 do
  begin
    result.add( GetItem(idx) ); inc(idx);
    result.add( GetItem(idx) ); inc(idx);
    result.add( GetItem(idx) ); inc(idx);
    result.add( GetItem(idx) ); inc(idx);
    result.add( GetItem(idx) ); inc(idx);
    result.add( GetItem(idx) ); inc(idx);
    result.add( GetItem(idx) ); inc(idx);
    result.add( GetItem(idx) ); inc(idx);
    dec(lFactor);
  end;

  lFactor := lCount mod 8;
  while lFactor > 0 do
  begin
    result.add( GetItem(idx) ); inc(idx);
    dec(lFactor);
  end;
end;

procedure TView<T>.ToList(var List: TList<T>);
var
  idx:  integer;
  lCount: integer;
  lFactor: integer;
begin
  lCount := GetCount();
  if lCount < 1 then
    exit;

  if List = nil then
    List := TList<T>.Create();

  idx := 0;
  lFactor := lCount shr 3;
  while lFactor > 0 do
  begin
    List.add( GetItem(idx) ); inc(idx);
    List.add( GetItem(idx) ); inc(idx);
    List.add( GetItem(idx) ); inc(idx);
    List.add( GetItem(idx) ); inc(idx);
    List.add( GetItem(idx) ); inc(idx);
    List.add( GetItem(idx) ); inc(idx);
    List.add( GetItem(idx) ); inc(idx);
    List.add( GetItem(idx) ); inc(idx);
    dec(lFactor);
  end;

  lFactor := lCount mod 8;
  while lFactor > 0 do
  begin
    List.add( GetItem(idx) ); inc(idx);
    dec(lFactor);
  end;
end;


procedure TView<T>.ToArrayEx(var Value: TArray<T>);
var
  idx, idy:  integer;
  lOld: integer;
  lCount: integer;
  lFactor: integer;
begin
  lCount := GetCount();
  if lCount < 1 then
    exit;

  lOld := length(Value);
  setlength(Value, lOld + lCount);

  idx := 0;
  idy := lOld;
  lFactor := lCount shr 3;
  while lFactor > 0 do
  begin
    Value[idy] := GetItem(idx); inc(idx); inc(idy);
    Value[idy] := GetItem(idx); inc(idx); inc(idy);
    Value[idy] := GetItem(idx); inc(idx); inc(idy);
    Value[idy] := GetItem(idx); inc(idx); inc(idy);
    Value[idy] := GetItem(idx); inc(idx); inc(idy);
    Value[idy] := GetItem(idx); inc(idx); inc(idy);
    Value[idy] := GetItem(idx); inc(idx); inc(idy);
    Value[idy] := GetItem(idx); inc(idx); inc(idy);
    dec(lFactor);
  end;

  lFactor := lCount mod 8;
  while lFactor > 0 do
  begin
    Value[idy] := GetItem(idx); inc(idx); inc(idy);
    dec(lFactor);
  end;
end;

function TView<T>.ToArrayEx(const Index: integer; Size: integer):  TArray<T>;
var
  idx, idy:  integer;
  lCount: integer;
  lFactor: integer;
begin
  setlength(result, 0);
  lCount := GetCount();
  if lCount < 1 then
    exit;

  if Index < 0 then
    raise Exception.CreateFmt('Invalid index, expected 0..%d, not %d', [lCount-1, index]);

  if Count < 1 then
    raise Exception.CreateFmt('Invalid count [%d] error', [Size]);

  if (Index + Size-1) > lCount-1 then
    raise Exception.CreateFmt('Invalid span, expected 0..%d, not %d', [lCount-1, Index + Size-1]);

  setlength(result, Size);

  idx := 0;
  idy := Index;
  lFactor := Size shr 3;
  while lFactor > 0 do
  begin
    result[idx] := GetItem(idy); inc(idx); inc(idy);
    result[idx] := GetItem(idy); inc(idx); inc(idy);
    result[idx] := GetItem(idy); inc(idx); inc(idy);
    result[idx] := GetItem(idy); inc(idx); inc(idy);
    result[idx] := GetItem(idy); inc(idx); inc(idy);
    result[idx] := GetItem(idy); inc(idx); inc(idy);
    result[idx] := GetItem(idy); inc(idx); inc(idy);
    result[idx] := GetItem(idy); inc(idx); inc(idy);
    dec(lFactor);
  end;

  lFactor := Size mod 8;
  while lFactor > 0 do
  begin
    result[idx] := GetItem(idy); inc(idx); inc(idy);
    dec(lFactor);
  end;
end;

function TView<T>.GetCount: integer;
begin
  {$IFDEF USE_BITVIEW}
  if FBuffer.Size < 1 then
    exit(0);
  case TypeInfo(T) = TypeInfo(TBit)  of
  true:   result := FBuffer.Size * 8;
  false:  result := FBuffer.Size div SizeOf(T);
  end;
  {$ELSE}
  if FBuffer.Size > 0 then
    result := FBuffer.Size div SizeOf(T)
  else
    result := 0;
  {$ENDIF}
end;

{$IFDEF BE_SILENT}
  {$WARNINGS OFF}
  {$HINTS OFF}
{$ENDIF}
function TView<T>.GetItem(const Index: integer): T;
{$IFDEF USE_BITVIEW}
var
  lBitByte: integer;
  lValue: byte;
  BitOfs: 0 .. 255;
{$ENDIF}
begin
  {$IFDEF USE_BITVIEW}
  case TypeInfo(T) = TypeInfo(TBit)  of
  true:
    begin
      lValue := 0;
      lBitByte := Index div 8;
      FBuffer.Read(lBitByte, SizeOf(lValue), lValue);

      BitOfs := Index mod 8;
      if (lValue and (1 shl (BitOfs mod 8))) <> 0 then
        PByte(@result)^ := TBit(1)
      else
        PByte(@result)^ := TBit(0);
    end;
  false:  FBuffer.Read(Index * SizeOf(T), SizeOf(T), result);
  end;
  {$ELSE}
  FBuffer.Read(Index * SizeOf(T), SizeOf(T), result);
  {$ENDIF}
end;
{$IFDEF BE_SILENT}
  {$WARNINGS ON}
  {$HINTS ON}
{$ENDIF}

procedure TView<T>.SetItem(const Index: integer; const Value: T);
{$IFDEF USE_BITVIEW}
var
  lByte:  byte;
  BitOfs: 0 .. 255;
  lBitByte: integer;
  lCurrent: boolean;
{$ENDIF}
begin
  {$IFDEF USE_BITVIEW}
  case TypeInfo(T) = TypeInfo(TBit)  of
  true:
    begin
      lByte := 0;
      lBitByte := Index div 8;
      FBuffer.Read(lBitByte, SizeOf(lByte), lByte);

      BitOfs := Index mod 8;
      lCurrent := (lByte and (1 shl (BitOfs mod 8))) <> 0;

      case PByte(@Value)^ of
      1:
        begin
          // only set if needed
          if not LCurrent then
          begin
            lByte := lByte or (1 shl (BitOfs mod 8));
            FBuffer.Write(lBitByte, SizeOf(lByte), lByte);
          end;
        end;
      0:
        begin
          // only clear if needed
          if lCurrent then
          begin
            lByte := lByte and not (1 shl (BitOfs mod 8));
            FBuffer.Write(lBitByte, SizeOf(lByte), lByte);
          end;
        end;
      end;
    end;
  false:  FBuffer.Write(Index * SizeOf(T), SizeOf(T), Value);
  end;
  {$ELSE}
  FBuffer.Write(Index * SizeOf(T), SizeOf(T), Value);
  {$ENDIF}
end;

function TView<T>.DoGetEnumerator: TEnumerator<T>;
begin
  result := TView<T>.TViewEnumerator.Create(self);
end;

{$IFDEF FPC}
function TView<T>.GetPtrEnumerator: TEnumerator<PT>;
begin
  Result := nil;
end;
{$ENDIF}

//##########################################################################
// TStorageSearch
//##########################################################################

constructor TQTXStorage.TStorageSearch.Create(Storage: IStorage);
begin
  inherited Create();
  FStorage := Storage;
end;

function TQTXStorage.TStorageSearch.Search(const Data; const DataLength: integer; var FoundbyteIndex: int64):  boolean;
var
  lTotal:   Int64;
  lToScan:  Int64;
  src:      Pbyte;
  lbyte:    byte;
  lOffset:  Int64;
  x,  y:    Int64;
  lBasePtr: Pbyte;
begin
  // Initialize
  result := false;
  FoundbyteIndex := -1;
  lByte := 0;

  // Check reference
  if FStorage = nil then
    raise TStorage.EStorageInvalidReference.Create(CNT_ERR_BUFFER_InvalidReference);

  // Check read capability
  if not ( mcRead in FStorage.GetCapabilities() ) then
    raise TStorage.EStorageReadUnSupported.Create(CNT_ERR_BUFFER_READNOTSUPPORTED);

  // Check source PTR
  lBasePtr := addr(Data);
  if lBasePtr = nil then
    raise TStorage.EStorageInvalidDataSource.Create(CNT_ERR_BUFFER_INVALIDDATASOURCE);

  // Check search length
  if DataLength < 1 then
    raise TStorage.EStorageInvalidDataSize(CNT_ERR_BUFFER_InvalidDataSize);

  // Check buffer size
  lTotal := FStorage.GetSize();
  if lTotal < 1 then
    raise TStorage.EStorageEmpty(CNT_ERR_BUFFER_EMPTY);

  // Make sure we never scan beyond the boundary of the buffer
  // We have to subtract the length of the data
  lToScan := lTotal - DataLength;

  x := 0;
  While (x <= lToScan) do
  begin
    // setup source PTR
    src := lBasePtr;

    // setup target offset
    lOffset := x;

    // Check memory by step-sampling
    // We check each byte for a match with the source, at the same index.
    // if any byte does not match, we abort, inc our offset into the buffer
    // and keep on going until a match is found
    y := 1;
    while y < DataLength do
    begin
      // break if not equal
      FStorage.Read(lOffset, 1, lbyte);
      result := src^ = lbyte;
      if not result then
        break;

      inc(src);
      lOffset := lOffset + 1;
      Y := Y + 1;
    end;

    // success?
    if result then
    begin
      FoundbyteIndex := x;
      break;
    end;

    x := x + 1;
  end;
end;

//##########################################################################
// TStorageFile
//##########################################################################

constructor TQTXStorage.TStorageFile.Create(AFilename: string; StreamFlags: Word);
begin
  inherited Create();
  AFileName := AFileName.trim();
  if AFileName.Length > 0 then
    Open(AFilename, StreamFlags);
end;

procedure TQTXStorage.TStorageFile.MarkAsDisposable(const Value: boolean);
begin
  FMarked := Value;
end;

procedure TQTXStorage.TStorageFile.BeforeDestruction;
begin
  if GetActive() then
    Close();
  inherited;
end;

function TQTXStorage.TStorageFile.GetCapabilities: TStorageCapabilities;
begin
  result := [mcScale, mcOwned, mcRead, mcWrite];
end;

function TQTXStorage.TStorageFile.GetEmpty: boolean;
begin
  {$IFDEF USE_RAWFILE}
  if GetActive() then
    result := FileSize(FFile) = 0
  else
    result := true;
  {$ELSE}
  if FFile <> nil then
    result := (FFile.Size = 0)
  else
    result := true;
  {$ENDIF}
end;

function TQTXStorage.TStorageFile.GetActive: boolean;
begin
  {$IFDEF USE_RAWFILE}
  result  := FFilename.length > 0;
  {$ELSE}
  result := FFile <> nil;
  {$ENDIF}
end;

procedure TQTXStorage.TStorageFile.Open(AFilename: string; StreamFlags: Word);
begin
  if GetActive() then
    Close();

  {$IFDEF USE_RAWFILE}
  AssignFile(FFile, AFileName);
  if StreamFlags = fmCreate then
    ReWrite(FFile)
  else
    Reset(FFile);
  FFileName := AFileName;
  {$ELSE}
  AFileName := AFileName.trim();
  if AFileName.length > 0 then
  begin
    try
      FFile := TFileStream.Create(AFilename, StreamFlags);
    except
      on e: exception do
        raise EStorage.CreateFmt(CNT_ERR_BUFFER_BASE,['Open', e.ClassName, e.message]);
    end;

    FFileName := AFilename;
  end else
  raise EStorage.CreateFmt(CNT_ERR_BUFFER_INVALIDFILENAME, [AFileName]);
  {$ENDIF}
end;

procedure TQTXStorage.TStorageFile.Close;
begin
  if GetActive() then
  begin
    {$IFDEF USE_RAWFILE}
    try
      system.Close(FFile);
    finally
      if FMarked then
      begin
        if FileExists(FFilename, true) then
        begin
          try
            DeleteFile(FFileName);
          except
            // sink exception
          end;
        end;
      end;
      FFilename := '';
    end;
    {$ELSE}
    try
      FFile.free;
    finally
      FFile := nil;
      // Check if the file should be deleted on exit
      if FMarked then
      begin
        if FileExists(FFilename, true) then
        begin
          try
            DeleteFile(FFileName);
          except
            // sink exception
          end;
        end;
      end;
      FFilename := '';
    end;
    {$ENDIF}
  end;
end;

function TQTXStorage.TStorageFile.GetSize: Int64;
begin
  {$IFDEF USE_RAWFILE}
  if GetActive() then
    result := FileSize(FFile)
  else
    result := 0;
  {$ELSE}
  if FFile <> nil then
    result := FFile.Size
  else
    result := 0;
  {$ENDIF}
end;

procedure TQTXStorage.TStorageFile.DoReleaseData;
begin
  {$IFDEF USE_RAWFILE}
  Seek(FFile, 0);
  Truncate(FFile);
  {$ELSE}
  if FFile <> nil then
    FFile.Size := 0;
  {$ENDIF}
end;

procedure TQTXStorage.TStorageFile.DoReadData(Start: Int64; var Buffer; BufLen: integer);
begin
  if GetActive() then
  begin
    {$IFDEF USE_RAWFILE}
    Seek(FFile, Start);
    BlockRead(FFile, Buffer, BufLen);
    {$ELSE}
    if FFile.Position <> Start then
    {$IFDEF USE_SEEK}
    FFile.Seek(Start, TSeekOrigin.soBeginning);
    {$ELSE}
    FFile.Position := Start;
    {$ENDIF}
    FFile.ReadBuffer(Buffer, BufLen);
  {$ENDIF}
  end else
  raise EStorageInActive.Create(CNT_ERR_BUFFER_NOTACTIVE);
end;

procedure TQTXStorage.TStorageFile.DoWriteData(Start: Int64; const Buffer; BufLen: integer);
begin
  if GetActive() then
  begin
  {$IFDEF USE_RAWFILE}
    Seek(FFile, Start);
    BlockWrite(FFile, Buffer, BufLen);
  {$ELSE}
    if FFile.Position <> Start then
    {$IFDEF USE_SEEK}
    FFile.Seek(Start, TSeekOrigin.soBeginning);
    {$ELSE}
    FFile.Position := Start;
    {$ENDIF}
    FFile.WriteBuffer(Buffer, BufLen);
  {$ENDIF}
  end else
  raise EStorageInActive.Create(CNT_ERR_BUFFER_NOTACTIVE);
end;

procedure TQTXStorage.TStorageFile.DoGrowDataBy(const Value: integer);
{$IFDEF USE_RAWFILE}
var
  {$IFDEF USE_LOCAL_CACHE}
  lCache: array [1..10240] of byte;
  {$ENDIF}
  lToGo:  integer;
  lToWrite: integer;
{$ENDIF}
begin
  if GetActive() then
  begin
    {$IFDEF USE_RAWFILE}
    {$IFDEF USE_LOCAL_CACHE}
    Fillchar(lCache, SizeOf(lCache), byte(0) );
    {$ENDIF}
    lToGo := Value;
    while lToGo > 0 do
    begin
      {$IFDEF USE_LOCAL_CACHE}
      lToWrite := SizeOf(lCache);
      {$ELSE}
      lToWrite := SizeOf(FCache);
      {$ENDIF}
      if lToWrite > lToGo then
        lToWrite := lToGo;

      if lToWrite < 1 then
        break;
      Seek(FFile, FileSize(FFile) );
      {$IFDEF USE_LOCAL_CACHE}
      BlockWrite(FFile, lCache, lToWrite);
      {$ELSE}
      BlockWrite(FFile, FCache, lToWrite);
      {$ENDIF}
      lToGo := lToGo - lToWrite;
    end;
    {$ELSE}
    FFile.Size := FFile.Size + Value
    {$ENDIF}
  end else
  raise EStorageInActive.Create(CNT_ERR_BUFFER_NOTACTIVE);
end;

procedure TQTXStorage.TStorageFile.DoShrinkDataBy(const Value: integer);
var
  lNewSize: Int64;
begin
  if GetActive() then
  begin
  {$IFDEF USE_RAWFILE}
    lNewSize := FileSize(FFile) - Value;
    if lNewSize <= 0 then
      DoReleaseData()
    else
    begin
      Seek(FFile, lNewSize);
      truncate(FFile);
    end;
  {$ELSE}
    lNewSize := FFile.Size - Value;
    if lNewSize > 0 then
      FFile.Size := lNewSize
    else
      DoReleaseData();
  {$ENDIF}
  end else
  raise EStorageInActive.Create(CNT_ERR_BUFFER_NOTACTIVE);
end;

//##########################################################################
// TQTXStorage.TStorageMemory
//##########################################################################

function TQTXStorage.TStorageMemory.BasePTR: Pbyte;
begin
  result := FDataPTR;
end;

function TQTXStorage.TStorageMemory.AddrOf(const ByteIndex: Int64): Pbyte;
begin
  // Are we within range of the buffer?
  if (ByteIndex >= 0) and (ByteIndex < FDataLen) then
  begin
    result := FDataPTR;
    inc(result, byteIndex);
  end else
  raise EStorageIndexViolation.CreateFmt(CNT_ERR_BUFFER_BYTEINDEXVIOLATION,  [0, FDataLEN, ByteIndex]);
end;

function TQTXStorage.TStorageMemory.GetCapabilities: TStorageCapabilities;
begin
  result := [mcScale, mcOwned, mcRead, mcWrite];
end;

function TQTXStorage.TStorageMemory.GetSize: Int64;
begin
  if FDataPTR <> nil then
    result := FDataLen
  else
    result := 0;
end;

procedure TQTXStorage.TStorageMemory.DoReleaseData;
begin
  try
    if FDataPTR <> NIL then
      Freemem(FDataPTR);
  finally
    FDataPTR := NIL;
    FDataLEN := 0;
  end;
end;

procedure TQTXStorage.TStorageMemory.DoReadData(Start: Int64; var Buffer; BufLen: integer);
begin
  move(AddrOf(Start)^, Addr(Buffer)^, BufLen);
end;

procedure TQTXStorage.TStorageMemory.DoWriteData(Start: Int64; const Buffer; BufLen: integer);
begin
  move(Addr(Buffer)^, AddrOf(start)^, BufLen);
end;

procedure TQTXStorage.TStorageMemory.DoGrowDataBy(const Value: integer);
var
  lNewSize: int64;
begin
  try
    if FDataPTR <> nil then
    begin
      // Re-scale current memory
      LNewSize := FDataLEN + Value;
      ReAllocMem(FDataPTR, LNewSize);
      FDataLen := LNewSize;
    end else
    begin
      // Allocate new memory
      FDataPTR := AllocMem(Value);
      FDataLen := Value;
    end;
  except
    on e: exception do
    begin
      FDataLen := 0;
      FDataPTR := nil;
      raise EStorageScaleFailed.CreateFmt(CNT_ERR_BUFFER_SCALEFAILED,[e.message]);
    end;
  end;
end;

procedure TQTXStorage.TStorageMemory.DoShrinkDataBy(const Value: integer);
var
  lNewSize: int64;
begin
  if FDataPTR <> nil then
  begin
    lNewSize := EnsureRange(FDataLEN - Value, 0, FDataLen);
    if lNewSize > 0 then
    begin
      if lNewSize <> FDataLen then
      begin
        try
          ReAllocMem(FDataPTR, lNewSize);
          FDataLen := lNewSize;
        except
          on e: exception do
          begin
            raise EStorageScaleFailed.CreateFmt(CNT_ERR_BUFFER_SCALEFAILED,[e.message]);
          end;
        end;
      end;
    end else
      DoReleaseData;
  end else
  raise EStorageEmpty.Create(CNT_ERR_BUFFER_EMPTY);
end;

procedure TQTXStorage.TStorageMemory.DoZeroData;
begin
  if FDataPTR <> nil then
    {$IFDEF USE_RTL_FILL}
    fillchar(FDataPTR^, FDataLen, byte(0))
    {$ELSE}
    TQTXStorage.TTyped.Fillbyte(FDataPTR, FDataLen, $00)
    {$ENDIF}
  else
    raise EStorageEmpty.Create(CNT_ERR_BUFFER_EMPTY);
end;

procedure TQTXStorage.TStorageMemory.DoFillData(Start: Int64; FillLength: Int64; const Data; DataLen: integer);
var
  lSource:    Pbyte;
  lTarget:    Pbyte;
  lLongs:     integer;
  lSingles:   integer;
begin
  // Initialize pointers
  lSource := Addr(Data);
  lTarget := AddrOf(Start);

  // EVEN copy source into destination
  lLongs := FillLength div DataLen;
  While lLongs > 0 do
  begin
    Move(lSource^, lTarget^, DataLen);
    inc(lTarget, DataLen);
    dec(lLongs);
  end;

  // ODD copy of source into destination
  lSingles := FillLength mod DataLen;
  if lSingles > 0 then
  begin
    case lSingles of
    1: lTarget^ := lSource^;
    2: PWord(lTarget)^ := PWord(lSource)^;
    3: TTyped.PTripleByte(lTarget)^ := TTyped.PTripleByte(lSource)^;
    4: PCardinal(lTarget)^ := PCardinal(lSource)^;
    else
      Move(lSource^, lTarget^, lSingles);
    end;
  end;
end;

//##########################################################################
// TQTXStorage.TTyped
//##########################################################################


class function TQTXStorage.TTyped.CalcOffsetFor(Index, ItemSize, TotalItems: integer): integer;
begin
  if TotalItems < 1 then exit(-1);
  if Index < 0 then exit(-1);
  if Index >= TotalItems then exit(-1);
  if ItemSize < 0 then exit(-1);
  result := Index * ItemSize;
end;

class function TQTXStorage.TTyped.CalcTrailFor(Index, ItemSize, TotalItems: integer): integer;
begin
  if TotalItems < 1 then exit(-1);
  if Index < 0 then exit(-1);
  if Index >= TotalItems then exit(-1);
  if ItemSize < 0 then exit(-1);

  inc(index);
  dec(TotalItems, Index);
  result := TotalItems * ItemSize;
end;

class function TQTXStorage.TTyped.CalcOffsetFor(Index, ItemSize, TotalItems: int64): int64;
begin
  if TotalItems < 1 then exit(-1);
  if Index < 0 then exit(-1);
  if Index >= TotalItems then exit(-1);
  if ItemSize < 0 then exit(-1);
  result := Index * ItemSize;
end;

class function TQTXStorage.TTyped.CalcTrailFor(Index, ItemSize, TotalItems: int64): int64;
begin
  if TotalItems < 1 then exit(-1);
  if Index < 0 then exit(-1);
  if Index >= TotalItems then exit(-1);
  if ItemSize < 0 then exit(-1);

  inc(index);
  dec(TotalItems, Index);
  result := TotalItems * ItemSize;
end;

class function TQTXStorage.TTyped.GetTempFileName: string;
begin
  {$IFDEF FPC}
  result := sysutils.GetTempFileName();
  {$ELSE}
  result := TPath.GetTempFileName();
  {$ENDIF}
end;

class procedure TQTXStorage.TTyped.Fillbyte(Target: Pbyte; const FillSize: integer;
  const Value: byte);
var
  LBytesToFill: integer;
begin
  LBytesToFill := FillSize;
  While LBytesToFill > 0 do
  begin
    Target^ := Value;
    dec(LBytesToFill);
    inc(Target);
  end;
end;

class procedure TQTXStorage.TTyped.FillWord(Target: PWord;
          const FillSize: integer; const Value: word);
var
  FTemp:  cardinal;
  FLongs: integer;
begin
  FTemp := Value shl 16 or Value;
  FLongs := FillSize shr 3;
  while FLongs>0 do
  begin
    Pcardinal(Target)^:=FTemp; inc(Pcardinal(Target));
    Pcardinal(Target)^:=FTemp; inc(Pcardinal(Target));
    Pcardinal(Target)^:=FTemp; inc(Pcardinal(Target));
    Pcardinal(Target)^:=FTemp; inc(Pcardinal(Target));
    dec(FLongs);
  end;

  Case FillSize mod 8 of
  1:  Target^:=Value;
  2:  Pcardinal(Target)^:=FTemp;
  3:  begin
        Pcardinal(Target)^:=FTemp; inc(Pcardinal(Target));
        Target^:=Value;
      end;
  4:  begin
        Pcardinal(Target)^:=FTemp; inc(Pcardinal(Target));
        Pcardinal(Target)^:=FTemp;
      end;
  5:  begin
        Pcardinal(Target)^:=FTemp; inc(Pcardinal(Target));
        Pcardinal(Target)^:=FTemp; inc(Pcardinal(Target));
        Target^:=Value;
      end;
  6:  begin
        Pcardinal(Target)^:=FTemp; inc(Pcardinal(Target));
        Pcardinal(Target)^:=FTemp; inc(Pcardinal(Target));
        Pcardinal(Target)^:=FTemp;
      end;
  7:  begin
        Pcardinal(Target)^:=FTemp; inc(Pcardinal(Target));
        Pcardinal(Target)^:=FTemp; inc(Pcardinal(Target));
        Pcardinal(Target)^:=FTemp; inc(Pcardinal(Target));
        Target^:=Value;
      end;
  end;
end;

class procedure TQTXStorage.TTyped.FillTriple(dstAddr: PTripleByte;
          const inCount: integer;const Value: TTripleByte);
var
  FLongs: integer;
begin
  FLongs := inCount shr 3;
  While FLongs>0 do
  begin
    dstAddr^ := Value; inc(dstAddr);
    dstAddr^ := Value; inc(dstAddr);
    dstAddr^ := Value; inc(dstAddr);
    dstAddr^ := Value; inc(dstAddr);
    dstAddr^ := Value; inc(dstAddr);
    dstAddr^ := Value; inc(dstAddr);
    dstAddr^ := Value; inc(dstAddr);
    dstAddr^ := Value; inc(dstAddr);
    dec(FLongs);
  end;

  case (inCount mod 8) of
  1:  dstAddr^ := Value;
  2:  begin
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value;
      end;
  3:  begin
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value;
      end;
  4:  begin
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value;
      end;
  5:  begin
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value;
      end;
  6:  begin
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value;
      end;
  7:  begin
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value;
      end;
  end;
end;

class procedure TQTXStorage.TTyped.FillLong(dstAddr: PCardinal;
      const inCount: integer; const Value: Cardinal);
var
  lLongs: integer;
begin
  lLongs := inCount shr 3;
  while lLongs > 0 do
  begin
    dstAddr^ := Value; inc(dstAddr);
    dstAddr^ := Value; inc(dstAddr);
    dstAddr^ := Value; inc(dstAddr);
    dstAddr^ := Value; inc(dstAddr);
    dstAddr^ := Value; inc(dstAddr);
    dstAddr^ := Value; inc(dstAddr);
    dstAddr^ := Value; inc(dstAddr);
    dstAddr^ := Value; inc(dstAddr);
    dec(lLongs);
  end;

  case inCount mod 8 of
  1:  dstAddr^:=Value;
  2:  begin
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value;
      end;
  3:  begin
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value;
      end;
  4:  begin
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value;
      end;
  5:  begin
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value;
      end;
  6:  begin
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value;
      end;
  7:  begin
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value;
      end;
  end;
end;

// rounds a number to nearest factor of X
class function TQTXStorage.TTyped.ToNearest(const Value, Factor: integer): integer;
var
  lTemp: integer;
Begin
  result := Value;
  lTemp := Value mod Factor;
  if lTemp > 0 then
    inc(result, factor - lTemp);
end;

// Swaps the content of two variables
class procedure TQTXStorage.TTyped.Swap(var Primary, Secondary: integer);
var
  LTemp: integer;
Begin
  LTemp := Primary;
  Primary := Secondary;
  Secondary := LTemp;
end;

// Calculates difference between two numbers, counting zero
class function TQTXStorage.TTyped.Span(const Primary, Secondary: integer; const WithZero: boolean = false): integer;
Begin
  If Primary <> Secondary then
  begin
    If Primary > Secondary then
      result := (Primary - Secondary)
    else
      result := (Secondary - Primary);

    if WithZero then
    begin
      If (Primary < 1) or (Secondary < 1) then
        inc(result);
    end;

  end else
    result := 0;
end;

// Returns the number of bits in X number of bytes
class function TQTXStorage.TTyped.BitsOf(const FromByteCount: integer): integer;
Begin
  result := integer(FromByteCount * 8);
end;

// Returns the number of bytes from X number of bits
class function TQTXStorage.TTyped.BytesOf(FromBitCount: integer): integer;
Begin
  if FromBitCount > 0 then
  begin
    if fromBitCount > 8 then
    begin
      FromBitCount := TTyped.ToNearest(FromBitCount, 8);
      result := FromBitCount div 8;
      if (result * 8) < FromBitCount then
        inc(result);
    end else
    result := 8;
  end else
  result := 0;
end;

class function TQTXStorage.TTyped.BitsSetInByte(const Value: byte): integer;
begin
  Result := CNT_BiTStorage_ByteTable[Value];
end;

class Function TQTXStorage.TTyped.BitGet(const Index: integer; const Buffer): boolean;
var
  LValue: byte;
  BitOfs: 0 .. 255;
  {$IFNDEF USE_ARRAYS}
  lAddr: PByte;
  lByteIndex: integer;
  {$ENDIF}
begin
  BitOfs := Index mod 8;
  {$IFDEF USE_ARRAYS}
  lValue := PByteAccessArray(@Buffer)^[index shr 3];
  Result := (LValue and (1 shl (BitOfs mod 8))) <> 0;
  {$ELSE}
  lAddr := @Buffer;
  lByteIndex := integer(Index div 8);
  inc(lAddr, lByteIndex);

  lValue := lAddr^;
  Result := (LValue and (1 shl (BitOfs mod 8))) <> 0;
  {$ENDIF}
end;

class procedure TQTXStorage.TTyped.BitSet(const Index: integer; var Buffer; const Value: boolean);
var
  lByte: byte;
  lByteIndex: integer;
  BitOfs: 0 .. 255;
  lCurrent: boolean;
  {$IFNDEF USE_ARRAYS}
  lAddr: PByte;
  {$ENDIF}
begin
  lByteIndex := Index div 8;
  {$IFDEF USE_ARRAYS}
  lByte := PByteAccessArray(@Buffer)^[lByteIndex];
  {$ELSE}
  lAddr := @Buffer;
  inc(lAddr, lByteIndex);
  lByte := lAddr^;
  {$ENDIF}

  BitOfs := Index mod 8;
  lCurrent := (lByte and (1 shl (BitOfs mod 8))) <> 0;

  case Value of
  true:
    begin
      //set bit if not already set *)
      if not LCurrent then
        {$IFDEF USE_ARRAYS}
        PByteAccessArray(@Buffer)^[lByteIndex] := (lByte or (1 shl (BitOfs mod 8)));
        {$ELSE}
        lAddr^ := (LByte or (1 shl (BitOfs mod 8)));
        {$ENDIF}
    end;
  false:
    begin
      // clear bit if already set
      if lCurrent then
        {$IFDEF USE_ARRAYS}
        PByteAccessArray(@Buffer)^[lByteIndex] := lByte and not (1 shl (BitOfs mod 8));
        {$ELSE}
        lAddr^ := lByte and not (1 shl (BitOfs mod 8));
        {$ENDIF}
    end;
  end;
end;

//##########################################################################
// TStorage.THash
//##########################################################################

class function TQTXStorage.THash.AdlerHash(const Data; DataLength: integer):  cardinal;
var
  LAdler: Cardinal;
begin
  LAdler := $0;
  result := AdlerHash(LAdler, Data, DataLength);
end;

//Note: This version is reentrant, The Adler parameter is
//      typically set to zero on the first call, and then
//      the result for the next calls until the buffer has been hashed
class function TQTXStorage.THash.AdlerHash(const Adler: Cardinal;
  const Data; DataLength: integer):  cardinal;
var
  s1, s2: cardinal;
  i, n: integer;
  p: Pbyte;
begin
  if DataLength > 0 then
  begin
    s1 := LongRec(Adler).Lo;
    s2 := LongRec(Adler).Hi;
    p := @Data;
    while DataLength>0 do
    begin
      if DataLength<5552 then
      n := DataLength else
      n := 5552;

      for i := 1 to n do
      begin
        inc(s1,p^);
        inc(p);
        inc(s2,s1);
      end;

      s1 := s1 mod 65521;
      s2 := s2 mod 65521;
      dec(DataLength,n);
    end;
    result := word(s1) + cardinal(word(s2)) shl 16;
  end else
  result := Adler;
end;

class function TQTXStorage.THash.AdlerHash(const Text: THashString):  cardinal;
var
  LBytes: TBytes;
begin
  result := 0;
  if length(Text) > 0 then
  begin
    LBytes := TEncoding.Default.GetBytes(Text);
    result := KAndRHash(LBytes[0], Length(LBytes) );
  end;
end;

class function TQTXStorage.THash.BobJenkinsHash(const Data; DataLength: integer):  cardinal;
var
  x: integer;
  LHash: cardinal;
  P: PByte;
begin
  LHash := 0;
  if DataLength > 0 then
  begin
    P := @Data;
    for x:=1 to Datalength do
    begin
      LHash := LHash + p^;
      LHash := LHash shl 10;
      LHash := LHash shr 6;
      inc(p);
    end;

    LHash := LHash + (LHash shl 3);
    LHash := LHash + (LHash shr 11);
    LHash := LHash + (LHash shl 15);
  end;
  result := LHash;
end;

class function TQTXStorage.THash.BobJenkinsHash(const Text: THashString):  cardinal;
var
  LBytes: TBytes;
begin
  result := 0;
  if length(text) > 0 then
  begin
    LBytes := TEncoding.Default.GetBytes(Text);
    result := BorlandHash(LBytes[0], Length(LBytes) );
  end;
end;

class function TQTXStorage.THash.BorlandHash(const Data; DataLength: integer):  cardinal;
var
  I: integer;
  p: Pbyte;
begin
  result := 0;
  if DataLength > 0 then
  begin
    p := @Data;
    for I := 1 to DataLength do
    begin
      result := ((result shl 2) or (result shr ( SizeOf(result) * 8 - 2))) xor p^;
      inc(p);
    end;
  end;
end;

class function TQTXStorage.THash.BorlandHash(const Text: THashString):  cardinal;
var
  LBytes: TBytes;
begin
  result := 0;
  if length(Text) > 0 then
  begin
    LBytes := TEncoding.Default.GetBytes(Text);
    result := BorlandHash(LBytes[0], Length(LBytes) );
  end;
end;

class function TQTXStorage.THash.KAndRHash(const Data; DataLength: integer):  cardinal;
var
  x:  integer;
  LAddr: PByte;
  LCrc: cardinal;
begin
  LCrc := $0;
  if DataLength > 0 then
  begin
    LAddr := @Data;
    for x:=1 to datalength do
    begin
      LCrc := ( (LAddr^ + LCrc) * 31);
    end;
  end;
  result := LCrc;
end;

class function TQTXStorage.THash.KAndRHash(const Text: THashString):  cardinal;
var
  LBytes: TBytes;
begin
  result := 0;
  if length(Text) > 0 then
  begin
    LBytes := TEncoding.Default.GetBytes(Text);
    result := KAndRHash(LBytes[0], Length(LBytes) );
  end;
end;

class function TQTXStorage.THash.ElfHash(const Data; DataLength: integer): cardinal;
var
  i:    integer;
  x:    cardinal;
  LSrc: Pbyte;
begin
  result := 0;
  if DataLength > 0 then
  begin
    LSrc := @Data;
    for i := 1 to DataLength do
    begin
      result := (result shl 4) + LSrc^;
      x := result and $F0000000;
      if x <> 0 then
        result := result xor (x shr 24) ;
      result := result and (not x);
      inc(LSrc);
    end;
  end;
end;

class function TQTXStorage.THash.ElfHash(const Text: THashString): cardinal;
var
  LBytes: TBytes;
begin
  result := 0;
  if length(text) > 0 then
  begin
    LBytes := TEncoding.Default.GetBytes(Text);
    result := ElfHash(LBytes[0], Length(LBytes) );
  end;
end;

//##########################################################################
// TQTXStorage.TStorage
//##########################################################################

{procedure TQTXStorage.TStorage.Afterconstruction;
begin
  inherited;
  FCaps := DoGetCapabilities;
end;  }

procedure TQTXStorage.TStorage.BeforeDestruction;
begin
  Release();
  inherited;
end;

procedure TQTXStorage.TStorage.Assign(Source:TPersistent);
begin
  if Source<>NIl then
  begin
    if (Source is TStorage) then
    begin
      Release;
      Append(TStorage(source));
    end else
    Inherited;
  end else
  Inherited;
end;

function TQTXStorage.TStorage.GetEmpty: boolean;
begin
  result := GetSize() <= 0;
end;

// Note: This method is not the same as "GetEmpty()" !
// This functions is called when storing data to via the standard
// Delphi / LCL persistence system. When using a file, you might
// not want to push 5 gigabytes of data into a form or datamodule ..
function TQTXStorage.TStorage.ObjectHasData: boolean;
begin
  result := GetSize() > 0;
end;

procedure TQTXStorage.TStorage.BeforeReadObject;
begin
  if mcOwned in GetCapabilities() then
    Release()
  else
    raise EStorage.Create(CNT_ERR_BUFFER_RELEASENOTSUPPORTED);
end;

procedure TQTXStorage.TStorage.AfterReadObject;
begin
end;

procedure TQTXStorage.TStorage.BeforeWriteObject;
begin
end;

procedure TQTXStorage.TStorage.AfterWriteObject;
begin
end;

procedure TQTXStorage.TStorage.ReadObject(const Reader:TReader);
var
  lTotal: Int64;
begin
  lTotal := 0;
  Reader.Read(lTotal, SizeOf(lTotal));
  if lTotal > 0 then
    ImportFrom(0, lTotal, Reader);
end;

procedure TQTXStorage.TStorage.WriteObject(const Writer:TWriter);
var
  lSize:  Int64;
begin
  lSize := GetSize();
  Writer.write(lSize, SizeOf(lSize));
  if lSize > 0 then
    ExportTo(0, lSize, Writer);
end;

procedure TQTXStorage.TStorage.ReadObjBin(Stream: TStream);
var
  lReader:  TReader;
begin
  lReader := TReader.Create(Stream, 1024);
  try
    BeforeReadObject();
    ReadObject(lReader);
  finally
    lReader.free;
    AfterReadObject();
  end;
end;

procedure TQTXStorage.TStorage.WriteObjBin(Stream: TStream);
var
  lWriter:  TWriter;
begin
  lWriter := TWriter.Create(Stream, 1024);
  try
    BeforeWriteObject();
    WriteObject(lWriter);
  finally
    lWriter.FlushBuffer();
    lWriter.free;
    AfterWriteObject();
  end;
end;

procedure TQTXStorage.TStorage.DefineProperties(Filer:TFiler);
begin
  inherited;
  Filer.DefineBinaryproperty('IO_BIN', ReadObjBin, WriteObjBin, ObjectHasData);
end;

procedure TQTXStorage.TStorage.DoFillData(Start: Int64; FillLength: Int64;
          const Data; DataLen: integer);
var
  lToWrite: integer;
begin
  While FillLength > 0 do
  begin
    lToWrite := EnsureRange(Datalen, 1, FillLength);
    DoWriteData(Start, Data, lToWrite);
    FillLength := FillLength - lToWrite;
    Start := Start + lToWrite;
  end;
end;

function TQTXStorage.TStorage.FillWith(const ByteIndex: Int64;
         const BytesToFill: Int64;
         const Source; const SourceLength: integer): Int64;
var
  lTotal: Int64;
  lTemp:  Int64;
  lAddr:  pbyte;
begin
  // Initialize
  result:=0;

  // Are we empty?
  if not Empty then
  begin
    // Check write capabilities
    if mcWrite in GetCapabilities() then
    begin
      // Check length[s] of data
      if  (BytesToFill > 0) and (SourceLength > 0) then
      begin
        // check data-source
        lAddr := addr(Source);

        if lAddr <> nil then
        begin
          // Get total range
          lTotal := GetSize();

          // Check range entrypoint
          if (ByteIndex >= 0) and (ByteIndex < lTotal) then
          begin

            // Does fill exceed range?
            lTemp := ByteIndex + BytesToFill;
            if lTemp > lTotal then
              lTemp := (lTotal - ByteIndex)
            else
              lTemp :=  BytesToFill;

            // fill range
            DoFillData(ByteIndex, LTemp,LAddr^, SourceLength);

            // return size of region filled
            result := lTemp;

          end else
          raise EStorageIndexViolation.CreateFmt(CNT_ERR_BUFFER_BYTEINDEXVIOLATION, [0, lTotal-1, ByteIndex]);
        end else
        raise EStorageInvalidDataSource.Create(CNT_ERR_BUFFER_INVALIDDATASOURCE);
      end;
    end else
    raise EStorage.Create(CNT_ERR_BUFFER_WRITENOTSUPPORTED);
  end else
  raise EStorageEmpty.Create(CNT_ERR_BUFFER_EMPTY);
end;

{$IFDEF BE_SILENT}
  {$WARNINGS OFF}
  {$HINTS OFF}
{$ENDIF}
procedure TQTXStorage.TStorage.DoZeroData;
var
  lSize:  Int64;
  lAlign: Int64;
  {$IFDEF USE_LOCAL_CACHE}
  lCache: TStorageCache;
  {$ENDIF}
begin
  lSize := GetSize();
  lAlign := lSize div SizeOf(byte);

  {$IFDEF USE_LOCAL_CACHE}
    {$IFDEF USE_RTL_FILL}
    fillchar(lCache, lAlign, byte(0) );
    {$ELSE}
    TQTXStorage.TTyped.Fillbyte(@lCache, lAlign, 0);
    {$ENDIF}
  FillWith(0, lSize, lCache, SizeOf(lCache));
  {$ELSE}
    {$IFDEF USE_RTL_FILL}
    fillchar(FCache, lAlign, byte(0) );
    {$ELSE}
    TQTXStorage.TTyped.Fillbyte(@FCache, lAlign, 0);
    {$ENDIF}
  FillWith(0, lSize, FCache, SizeOf(FCache));
  {$ENDIF}
end;
{$IFDEF BE_SILENT}
  {$WARNINGS ON}
  {$HINTS ON}
{$ENDIF}

{$IFDEF BE_SILENT}
  {$WARNINGS OFF}
  {$HINTS OFF}
{$ENDIF}
function  TQTXStorage.TStorage.ToString(Boundary: integer = 16;
          const Options: TStorageTextOutOptions = [hdSign, hdZeroPad]): string;
var
  x, y:   integer;
  LCount: integer;
  LPad:   integer;
  LDump:  array of byte;
  LCache: byte;

const
  CNT_MASK  = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
            + 'abcdefghijklmnopqrstuvwxyz'
            + '0123456789'
            + ',;<>{}[]-_#$%&/() §^:,?';

  procedure AddToCache(const Value: byte);
  var
    _len: integer;
  begin
    _Len := length(LDump);
    SetLength(LDump, _Len + 1);
    LDump[_len] := Value;
  end;

begin
  lCache := 0;

  if not Empty then
  Begin
    Boundary := EnsureRange(Boundary, 2, 64);
    LCount:=0;
    result := '';

    for x := 0 to Size-1 do
    begin

      if Read(x, SizeOf(LCache), LCache) = SizeOf(LCache) then
        AddToCache(LCache)
      else
        break;

      if (hdSign in Options) then
        result := result + '$' + IntToHex(LCache,2)
      else
        result := result + IntToHex(LCache,2);

      inc(LCount);

      if LCount >= Boundary then
      begin
        if Length(LDump) > 0 then
        begin
          result := result + ' ';
          for y := 0 to length(LDump)-1 do
          begin
            if pos(ansichar(lDump[y]), CNT_MASK) > 0 then
              result := result + chr(LDump[y])
            else
              result := result + '_';
          end;
        end;
        setlength(LDump, 0);

        result := result + #13 + #10;
        LCount := 0;
      end else
      result := result + ' ';
    end;

    if (hdZeroPad in Options) and (LCount >0 ) then
    begin
      LPad := Boundary - lCount;
      for x:=1 to LPad do
      Begin
        result := result + '--';
        if (hdSign in Options) then
          result := result + '-';

        inc(LCount);
        if LCount >= Boundary then
        begin
          result := result + #13 + #10;
          LCount := 0;
        end else
        result := result + ' ';
      end;
    end;
  end;
end;
{$IFDEF BE_SILENT}
  {$WARNINGS ON}
  {$HINTS ON}
{$ENDIF}

procedure TQTXStorage.TStorage.FillAll(const Value: byte);
begin
  if not Empty then
  begin
    if mcWrite in GetCapabilities() then
      FillWith(0, Size, value, SizeOf(value) )
    else
      raise EStorage.Create(CNT_ERR_BUFFER_WRITENOTSUPPORTED);
  end else
  raise EStorageEmpty.Create(CNT_ERR_BUFFER_EMPTY);
end;

procedure TQTXStorage.TStorage.FillAll;
begin
  if not Empty then
  begin
    if mcWrite in GetCapabilities() then
      DoZeroData()
    else
      raise EStorage.Create(CNT_ERR_BUFFER_WRITENOTSUPPORTED);
  end else
  raise EStorageEmpty.Create(CNT_ERR_BUFFER_EMPTY);
end;

{$IFDEF BE_SILENT}
  {$WARNINGS OFF}
  {$HINTS OFF}
{$ENDIF}
procedure TQTXStorage.TStorage.Append(const Buffer: IStorage);
var
  lOffset:      Int64;
  lTotal:       Int64;
  lRead:        integer;
  lbytesToRead: integer;
  {$IFDEF USE_LOCAL_CACHE}
  lCache:       TStorageCache;
  {$ENDIF}
begin
  if mcScale in GetCapabilities() then
  begin
    if mcWrite in GetCapabilities() then
    begin
      if Buffer <> nil then
      begin
        // does the source support read caps?
        if mcRead in Buffer.GetCapabilities() then
        begin
          lOffset := 0;
          lTotal := Buffer.GetSize();

          repeat
            {$IFDEF USE_LOCAL_CACHE}
            lbytesToRead := EnsureRange(SizeOf(lCache), 0, lTotal);
            lRead := Buffer.Read(lOffset, lbytesToRead, lCache);
            if lRead > 0 then
            begin
              Append(lCache, lRead);
              lTotal := lTotal - lRead;
              lOffset := lOffset + lRead;
            end;
            {$ELSE}
            lbytesToRead := EnsureRange(SizeOf(FCache), 0, lTotal);
            lRead := Buffer.Read(lOffset, lbytesToRead, FCache);
            if lRead > 0 then
            begin
              Append(FCache, lRead);
              lTotal := lTotal - lRead;
              lOffset := lOffset + lRead;
            end;
            {$ENDIF}
          until (lbytesToRead < 1) or (lRead < 1);

        end else
        raise EStorage.Create(CNT_ERR_BUFFER_SOURCEREADNOTSUPPORTED);
      end else
      raise EStorageInvalidDataSource.Create(CNT_ERR_BUFFER_INVALIDDATASOURCE);
    end else
    raise EStorage.Create(CNT_ERR_BUFFER_WRITENOTSUPPORTED);
  end else
  raise EStorageScaleUnSupported.Create(CNT_ERR_BUFFER_SCALENOTSUPPORTED);
end;
{$IFDEF BE_SILENT}
  {$WARNINGS ON}
  {$HINTS ON}
{$ENDIF}

{$IFDEF BE_SILENT}
  {$WARNINGS OFF}
  {$HINTS OFF}
{$ENDIF}
procedure TQTXStorage.TStorage.Append(const Stream: TStream);
var
  lTotal:       Int64;
  lRead:        integer;
  lbytesToRead: integer;
  {$IFDEF USE_LOCAL_CACHE}
  lCache:       TStorageCache;
  {$ENDIF}
begin
  if mcScale in GetCapabilities() then
  begin
    if mcWrite in GetCapabilities() then
    begin
      if Stream <> nil then
      begin
        lTotal := (Stream.Size - Stream.Position);
        if lTotal > 0 then
        begin
          Repeat
            {$IFDEF USE_LOCAL_CACHE}
            lBytesToRead := EnsureRange(SizeOf(lCache), 0, lTotal);
            lRead := Stream.Read(lCache, lbytesToRead);
            if lRead > 0 then
            begin
              Append(lCache, lRead);
              lTotal := lTotal - lRead;
            end;
            {$ELSE}
            lBytesToRead := EnsureRange(SizeOf(FCache), 0, lTotal);
            lRead := Stream.Read(FCache, lbytesToRead);
            if lRead > 0 then
            begin
              Append(FCache, lRead);
              lTotal := lTotal - lRead;
            end;
            {$ENDIF}
          Until (lBytesToRead < 1) or (lRead < 1);
        end;
      end else
      raise EStorageInvalidDataSource.Create(CNT_ERR_BUFFER_INVALIDDATASOURCE);
    end else
    raise EStorage.Create(CNT_ERR_BUFFER_WRITENOTSUPPORTED);
  end else
  raise EStorageScaleUnSupported.Create(CNT_ERR_BUFFER_SCALENOTSUPPORTED);
end;
{$IFDEF BE_SILENT}
  {$WARNINGS ON}
  {$HINTS ON}
{$ENDIF}

procedure TQTXStorage.TStorage.Append(const Data;const DataLength:integer);
var
  lOffset: Int64;
begin
  if mcScale in GetCapabilities() then
  begin
    if mcWrite in GetCapabilities() then
    begin
      if DataLength > 0 then
      begin
        lOffset := GetSize();
        DoGrowDataBy(DataLength);
        DoWriteData(lOffset, Data, DataLength);
      end;
    end else
    raise EStorageWriteUnSupported.Create(CNT_ERR_BUFFER_WRITENOTSUPPORTED);
  end else
  raise EStorageScaleUnSupported.Create(CNT_ERR_BUFFER_SCALENOTSUPPORTED);
end;

procedure TQTXStorage.TStorage.Release;
begin
  // Is the content owned/managed by us?
  if mcOwned in GetCapabilities() then
    DoReleaseData()
  else
    raise EStorage.Create(CNT_ERR_BUFFER_RELEASENOTSUPPORTED);
end;

procedure TQTXStorage.TStorage.SetSize(const NewSize: Int64);
var
  mFactor:  Int64;
  mOldSize: Int64;
begin
  if mcScale in GetCapabilities() then
  begin
    if NewSize > 0 then
    begin
      // Get current size
      mOldSize := GetSize();

      // Get difference between current size & new size
      mFactor := abs(mOldSize - NewSize);

      // only act if we need to
      if mFactor > 0 then
      begin
        try
          // grow or shrink?
          if NewSize>mOldSize then
          DoGrowDataBy(mFactor) else

          if NewSize<mOldSize then
          DoShrinkDataBy(mFactor);
        except
          on e: exception do
          raise EStorageScaleFailed.CreateFmt(CNT_ERR_BUFFER_SCALEFAILED,[e.message]);
        end;
      end;
    end else
    Release;
  end else
  raise EStorageScaleUnSupported.Create(CNT_ERR_BUFFER_SCALENOTSUPPORTED);
end;

{$IFDEF BE_SILENT}
  {$WARNINGS OFF}
  {$HINTS OFF}
{$ENDIF}
procedure TQTXStorage.TStorage.Insert(ByteIndex: Int64; const Source: IStorage);
var
  lTotal:   Int64;
  lRead:    integer;
  lToRead:  integer;
  lEntry:   Int64;
  {$IFDEF USE_LOCAL_CACHE}
  lCache:       TStorageCache;
  {$ENDIF}
begin
  // Validate source PTR
  if Source <> nil then
  begin
    // Check that buffer supports scaling
    if mcScale in GetCapabilities() then
    begin
      // Check that buffer support write access
      if mcWrite in GetCapabilities() then
      begin
        // Check for read-access
        if mcRead in Source.GetCapabilities() then
        begin
          // Get size of source
          lTotal := Source.GetSize();

          // Validate entry index
          if ByteIndex >= 0  then
          begin

            // anything to work with?
            if lTotal > 0 then
            begin

              lEntry := 0;
              While lTotal > 0 do
              begin
                // Clip data to cache boundaries
                {$IFDEF USE_LOCAL_CACHE}
                lToRead := SizeOf(lCache);
                {$ELSE}
                lToRead := SizeOf(FCache);
                {$ENDIF}
                if lToRead > lTotal then
                  lToRead := lTotal;

                // Read data from source
                {$IFDEF USE_LOCAL_CACHE}
                lRead := Source.Read(lEntry, lToRead, lCache);
                {$ELSE}
                lRead := Source.Read(lEntry, lToRead, FCache);
                {$ENDIF}
                if lRead > 0 then
                begin
                  // Write data to our buffer layer
                  {$IFDEF USE_LOCAL_CACHE}
                  Insert(ByteIndex, lCache, lRead);
                  {$ELSE}
                  Insert(ByteIndex, FCache, lRead);
                  {$ENDIF}

                  // update positions
                  lEntry := lEntry + lRead;
                  ByteIndex := ByteIndex + lRead;
                  lTotal := lTotal - lRead;
                end else
                Break;
              end;
            end;

          end else
          raise EStorageIndexViolation.CreateFmt(CNT_ERR_BUFFER_BYTEINDEXVIOLATION,[0, lTotal-1, ByteIndex]);
        end else
        raise EStorageReadUnSupported.Create(CNT_ERR_BUFFER_READNOTSUPPORTED);
      end else
      raise EStorageWriteUnSupported.Create(CNT_ERR_BUFFER_WRITENOTSUPPORTED);
    end else
    raise EStorageScaleUnSupported.Create(CNT_ERR_BUFFER_SCALENOTSUPPORTED);
  end else
  raise EStorageInvalidDataSource.Create(CNT_ERR_BUFFER_INVALIDDATASOURCE);
end;
{$IFDEF BE_SILENT}
  {$WARNINGS ON}
  {$HINTS ON}
{$ENDIF}

{$IFDEF BE_SILENT}
  {$WARNINGS OFF}
  {$HINTS OFF}
{$ENDIF}
procedure TQTXStorage.TStorage.Insert(ByteIndex: int64;const Source; DataLength: integer);
var
  lTotal:       Int64;
  lbytesToPush: Int64;
  lbytesToRead: integer;
  lPosition:    Int64;
  lFrom:        Int64;
  lTo:          Int64;
  lData:        Pbyte;
  {$IFDEF USE_LOCAL_CACHE}
  lCache:       TStorageCache;
  {$ENDIF}
begin
  if mcScale in GetCapabilities() then
  begin
    if mcWrite in GetCapabilities() then
    begin
      if mcRead in GetCapabilities() then
      begin
        if DataLength > 0 then
        begin
          lData := @Source;
          if lData <> nil then
          begin
            lTotal := GetSize();
            if (ByteIndex >= 0) and (ByteIndex < lTotal) then
            begin
              lBytesToPush := lTotal - ByteIndex;
              if lBytesToPush > 0 then
              begin
                DoGrowDataBy(DataLength);
                lPosition := ByteIndex + lBytesToPush;

                While lBytesToPush > 0 do
                begin
                  {$IFDEF USE_LOCAL_CACHE}
                  // calculate how much data to read
                  lBytesToRead := EnsureRange(SizeOf(lCache), 0, lBytesToPush);

                  // calculate read & write positions
                  lFrom := lPosition - lBytesToRead;
                  lTo := lPosition - (lBytesToRead - DataLength);

                  // read data from the end
                  DoReadData(lFrom, lCache, lbytesToRead);

                  // write data upwards
                  DoWriteData(lTo, lCache, lbytesToRead);
                  {$ELSE}
                  // calculate how much data to read
                  lbytesToRead := EnsureRange(SizeOf(FCache), 0, lBytesToPush);

                  // calculate read & write positions
                  lFrom := lPosition - lbytesToRead;
                  lTo := lPosition - (lbytesToRead - DataLength);

                  // read data from the end
                  DoReadData(lFrom, FCache, lbytesToRead);

                  // write data upwards
                  DoWriteData(lTo, FCache, lbytesToRead);
                  {$ENDIF}

                  // update offset values
                  lPosition := lPosition - lbytesToRead;
                  lbytesToPush := lbytesToPush - lbytesToRead;
                end;

                // insert new data
                DoWriteData(lPosition, Source, DataLength);
              end else
              DoWriteData(lTotal, Source, DataLength);
            end else

            // if @ end, use append instead
            if ByteIndex = lTotal then
              Append(Source, DataLength)
            else
              raise EStorageIndexViolation.CreateFmt(CNT_ERR_BUFFER_BYTEINDEXVIOLATION,[0, lTotal-1, ByteIndex]);
          end else
          raise EStorageInvalidDataSource.Create(CNT_ERR_BUFFER_INVALIDDATASOURCE);
        end;
      end else
      raise EStorageReadUnSupported.Create(CNT_ERR_BUFFER_READNOTSUPPORTED);
    end else
    raise EStorageWriteUnSupported.Create(CNT_ERR_BUFFER_WRITENOTSUPPORTED);
  end else
  raise EStorageScaleUnSupported.Create(CNT_ERR_BUFFER_SCALENOTSUPPORTED);
end;
{$IFDEF BE_SILENT}
  {$WARNINGS ON}
  {$HINTS ON}
{$ENDIF}

{$IFDEF BE_SILENT}
  {$WARNINGS OFF}
  {$HINTS OFF}
{$ENDIF}
procedure TQTXStorage.TStorage.Remove(ByteIndex: int64; DataLength: integer);
var
  lTemp:      integer;
  lTop:       Int64;
  lBottom:    Int64;
  lToRead:    integer;
  lToPoll:    Int64;
  lPosition:  Int64;
  lTotal:     Int64;
  {$IFDEF USE_LOCAL_CACHE}
  lCache:       TStorageCache;
  {$ENDIF}
begin
  if mcScale in GetCapabilities() then
  begin
    if mcWrite in GetCapabilities() then
    begin
      if mcRead in GetCapabilities() then
      begin
        if DataLength > 0 then
        begin
          lTotal := GetSize();

          if (ByteIndex >= 0) and (ByteIndex < lTotal) then
          begin
            lTemp := ByteIndex + DataLength;
            if DataLength <> lTotal then
            begin
              if lTemp < lTotal then
              begin
                lToPoll := lTotal - (ByteIndex + DataLength);
                lTop := ByteIndex;
                lBottom := ByteIndex + DataLength;

                While lToPoll > 0 do
                begin
                  lPosition := lBottom;
                  {$IFDEF USE_LOCAL_CACHE}
                  lToRead := EnsureRange(SizeOf(lCache), 0, lToPoll);
                  DoReadData(lPosition, lCache, lToRead);
                  DoWriteData(lTop, lCache, lToRead);
                  {$ELSE}
                  lToRead := EnsureRange(SizeOf(FCache), 0, lToPoll);
                  DoReadData(lPosition, FCache, lToRead);
                  DoWriteData(lTop, FCache, lToRead);
                  {$ENDIF}

                  lTop := lTop + lToRead;
                  lBottom := lBottom + lToRead;
                  lToPoll := lToPoll - lToRead;
                end;
                DoShrinkDataBy(DataLength);
              end else
              Release;
            end else
            begin
              if lTemp > lTotal then
                Release()
              else
                DoShrinkDataBy(lTotal - DataLength);
            end;
          end else
          raise EStorageIndexViolation.CreateFmt
          (CNT_ERR_BUFFER_BYTEINDEXVIOLATION,[0, lTotal-1, ByteIndex]);
        end;
      end else
      raise EStorageReadUnSupported.Create(CNT_ERR_BUFFER_READNOTSUPPORTED);
    end else
    raise EStorageWriteUnSupported.Create(CNT_ERR_BUFFER_WRITENOTSUPPORTED);
  end else
  raise EStorageScaleUnSupported.Create(CNT_ERR_BUFFER_SCALENOTSUPPORTED);
end;
{$IFDEF BE_SILENT}
  {$WARNINGS ON}
  {$HINTS ON}
{$ENDIF}

function TQTXStorage.TStorage.Push(const Source; DataLength: integer; const Orientation: TStorageStackOrientation = soDownUp):  integer;
begin
  result := 0;

  case Orientation of
  soTopDown:
    begin
      // Data is inserted at the start of the buffer
      if not Empty then
        Insert(0, Source, DataLength)
      else
        Append(Source, DataLength);
      result := DataLength;
    end;
  soDownUp:
    begin
      // Data is appended at the bottom of the buffer
      Append(Source, DataLength);
      result := DataLength;
    end;
  end;
end;

function TQTXStorage.TStorage.Pop(var Target; DataLength: integer;
  const Orientation: TStorageStackOrientation = soDownUp):  integer;
var
  lTotal:   Int64;
  lRemains: Int64;
  lOffset:  int64;
begin
  result := 0;
  if mcScale in GetCapabilities() then
  begin
    if mcWrite in GetCapabilities() then
    begin
      if mcRead in GetCapabilities() then
      begin
        if DataLength > 0 then
        begin
          lTotal := GetSize();
          if lTotal > 0 then
          begin
            case Orientation of
            soTopDown:
              begin
                lRemains := lTotal - DataLength;
                if lRemains > 0 then
                begin
                  result := Read(0, DataLength, Target);
                  Remove(0, DataLength);
                end else
                begin
                  result := lTotal;
                  DoReadData(0, Target, lTotal);
                  Release;
                end;
              end;
            soDownUp:
              begin
                if DataLength > lTotal then
                  DataLength := lTotal;

                lOffset := lTotal - DataLength;
                if lOffset < 0 then
                  lOffset := 0;

                result := Read(lOffset, DataLength, Target);
                self.SetSize(lTotal - Result);
              end;
            end;
          end;
        end;
      end else
      raise EStorageReadUnSupported.Create(CNT_ERR_BUFFER_READNOTSUPPORTED);
    end else
    raise EStorageWriteUnSupported.Create(CNT_ERR_BUFFER_WRITENOTSUPPORTED);
  end else
  raise EStorageScaleUnSupported.Create(CNT_ERR_BUFFER_SCALENOTSUPPORTED);
end;

{$IFDEF BE_SILENT}
  {$WARNINGS OFF}
  {$HINTS OFF}
{$ENDIF}
function TQTXStorage.TStorage.HashCode: cardinal;
var
  i:        integer;
  x:        cardinal;
  lTotal:   Int64;
  lRead:    integer;
  lToRead:  integer;
  lIndex:   Int64;
  {$IFDEF USE_LOCAL_CACHE}
  lCache:   TStorageCache;
  {$ENDIF}
begin
  result := 0;
  if mcRead in GetCapabilities() then
  begin
    lTotal := GetSize();
    if lTotal > 0 then
    begin
      lIndex := 0;

      while lTotal > 0 do
      begin
        {$IFDEF USE_LOCAL_CACHE}
        lToRead := SizeOf(lCache);
        {$ELSE}
        lToRead := SizeOf(FCache);
        {$ENDIF}
        if lToRead > lTotal then
          lToRead := lTotal;

        {$IFDEF USE_LOCAL_CACHE}
        lRead := read(lIndex, lToRead, lCache);
        {$ELSE}
        lRead := read(lIndex, lToRead, FCache);
        {$ENDIF}

        if lRead > 0 then
        begin
          for i := 0 to lRead do
          begin
            {$IFDEF USE_LOCAL_CACHE}
            result := (result shl 4) + lCache[i];
            {$ELSE}
            result := (result shl 4) + FCache[i];
            {$ENDIF}

            x := result and $F0000000;
            if x <> 0 then
              result := result xor (x shr 24);

            result := result and (not x);
          end;

          lTotal := lTotal - lRead;
          lIndex := lIndex + lRead;
        end else
        Break;
      end;
    end else
    raise EStorageEmpty.Create(CNT_ERR_BUFFER_EMPTY);
  end else
  raise EStorageReadUnSupported.Create(CNT_ERR_BUFFER_READNOTSUPPORTED);
end;
{$IFDEF BE_SILENT}
  {$WARNINGS ON}
  {$HINTS ON}
{$ENDIF}

function TQTXStorage.TStorage.ToStream: TStream;
begin
  result := TMemoryStream.Create();
  SaveToStream(result);
  result.Seek(0, TSeekOrigin.soBeginning);
end;

procedure TQTXStorage.TStorage.FromStream(const Stream: TStream; const Disposable: boolean = false);
begin
  try
    LoadFromStream(Stream);
  finally
    if Disposable then
      Stream.free;
  end;
end;


function TQTXStorage.TStorage.ToStorage: TStorage;
begin
  // Attempt to create a disk based buffer first
  try
    result := TQTXStorage.TStorageFile.Create(TTyped.GetTempFileName(), fmCreate);
  except
    on e: exception do
    begin
      // Fall back on memory buffer
      result := TQTXStorage.TStorageMemory.Create();
      try
        result.Assign(self);
      except
        on e: exception do
        begin
          FreeAndNIL(result);
          raise;
        end;
      end;
    end;
  end;

  // Move data over quickly
  try
    result.Assign(self);
  except
    on e: exception do
    begin
      FreeAndNIL(result);
      raise;
    end;
  end;
end;

procedure TQTXStorage.TStorage.FromStorage(const Storage: IStorage);
begin
  Release();
  Append(Storage);
end;

procedure TQTXStorage.TStorage.FromStorage(const Storage: TStorage; const Disposable: boolean = false);
begin
  try
    Release();
    Append(Storage);
  finally
    if Disposable then
      Storage.free;
  end;
end;

procedure TQTXStorage.TStorage.LoadFromFile(Filename: string);
var
  mFile:  TFileStream;
begin
  mFile:=TFileStream.Create(filename,fmOpenRead or fmShareDenyNone);
  try
    LoadFromStream(mFile);
  finally
    mFile.free;
  end;
end;

procedure TQTXStorage.TStorage.SaveToFile(Filename: string);
var
  mFile:  TFileStream;
begin
  mFile:=TFileStream.Create(filename,fmCreate or fmShareDenyNone);
  try
    SaveToStream(mFile);
  finally
    mFile.free;
  end;
end;

{$IFDEF BE_SILENT}
  {$WARNINGS OFF}
  {$HINTS OFF}
{$ENDIF}
procedure TQTXStorage.TStorage.SaveToStream(const Stream: TStream);
var
  lWriter:  TWriter;
  lTotal:   Int64;
  lToRead:  integer;
  lRead:    integer;
  lOffset:  Int64;
  {$IFDEF USE_LOCAL_CACHE}
  lCache:   TStorageCache;
  {$ENDIF}
begin
  if mcRead in GetCapabilities() then
  begin
    if Stream <> nil then
    begin
      lWriter := TWriter.Create(Stream, 1024);
      try
        lTotal := GetSize();
        lOffset := 0;

        While lTotal > 0 do
        begin
          {$IFDEF USE_LOCAL_CACHE}
          lToRead := SizeOf(lCache);
          {$ELSE}
          lToRead := SizeOf(FCache);
          {$ENDIF}
          if lToRead > lTotal then
            lToRead := lTotal;

          {$IFDEF USE_LOCAL_CACHE}
          lRead := Read(lOffset, lToRead, lCache);
          {$ELSE}
          lRead := Read(lOffset, lToRead, FCache);
          {$ENDIF}
          if LRead > 0 then
          begin
            {$IFDEF USE_LOCAL_CACHE}
            lWriter.Write(lCache, lRead);
            {$ELSE}
            lWriter.Write(FCache, lRead);
            {$ENDIF}
            lOffset := lOffset + lRead;
            lTotal :=  lTotal - lRead;
          end else
          Break;
        end;
      finally
        lWriter.FlushBuffer;
        lWriter.free;
      end;
    end else
    raise EStorage.Create(CNT_ERR_BUFFER_INVALIDDATATARGET);
  end else
  raise EStorageReadUnSupported.Create(CNT_ERR_BUFFER_READNOTSUPPORTED);
end;
{$IFDEF BE_SILENT}
  {$WARNINGS ON}
  {$HINTS ON}
{$ENDIF}

{$IFDEF BE_SILENT}
  {$WARNINGS OFF}
  {$HINTS OFF}
{$ENDIF}
procedure TQTXStorage.TStorage.LoadFromStream(const Stream: TStream);
var
  lReader:  TReader;
  lTotal:   Int64;
  lToRead:  integer;
  {$IFDEF USE_LOCAL_CACHE}
  lCache:   TStorageCache;
  {$ENDIF}
begin
  if mcWrite in GetCapabilities() then
  begin
    if mcScale in GetCapabilities() then
    begin
      if Stream <> nil then
      begin
        Release;

        lReader := TReader.Create(Stream, 1024);
        try
          lTotal := (Stream.Size - Stream.Position);
          While lTotal > 0 do
          begin
            {$IFDEF USE_LOCAL_CACHE}
            lToRead := SizeOf(lCache);
            {$ELSE}
            lToRead := SizeOf(FCache);
            {$ENDIF}
            if lToRead > lTotal then
              lToRead := lTotal;

            {$IFDEF USE_LOCAL_CACHE}
            lReader.read(lCache, lToRead);
            self.Append(lCache,  lToRead);
            {$ELSE}
            lReader.read(FCache, lToRead);
            self.Append(FCache,  lToRead);
            {$ENDIF}
            lTotal := lTotal - lToRead;
          end;
        finally
          lReader.free;
        end;
      end else
      raise EStorageInvalidDataSource.Create(CNT_ERR_BUFFER_INVALIDDATASOURCE);
    end else
    raise EStorageScaleUnSupported.Create(CNT_ERR_BUFFER_SCALENOTSUPPORTED);
  end else
  raise EStorageWriteUnSupported.Create(CNT_ERR_BUFFER_WRITENOTSUPPORTED);
end;
{$IFDEF BE_SILENT}
  {$WARNINGS ON}
  {$HINTS ON}
{$ENDIF}

{$IFDEF BE_SILENT}
  {$WARNINGS OFF}
  {$HINTS OFF}
{$ENDIF}
function TQTXStorage.TStorage.ExportTo(ByteIndex: Int64; DataLength: integer;
  const Writer: TWriter):  integer;
var
  lToRead:  integer;
  lRead:    integer;
  {$IFDEF USE_LOCAL_CACHE}
  lCache:   TStorageCache;
  {$ENDIF}
begin
  result := 0;
  if mcRead in GetCapabilities() then
  begin
    if DataLength > 0 then
    begin
      if Writer <> nil then
      begin

        While DataLength > 0 do
        begin
          {$IFDEF USE_LOCAL_CACHE}
          lToRead := EnsureRange( SizeOf(lCache), 0, DataLength);
          lRead := Read(ByteIndex, lToRead, lCache);
          {$ELSE}
          lToRead := EnsureRange( SizeOf(FCache), 0, DataLength);
          lRead := Read(ByteIndex, lToRead, FCache);
          {$ENDIF}
          if lRead > 0 then
          begin
            {$IFDEF USE_LOCAL_CACHE}
            Writer.Write(lCache, lRead);
            {$ELSE}
            Writer.Write(FCache, lRead);
            {$ENDIF}
            ByteIndex := ByteIndex + lRead;
            DataLength := DataLength - lRead;
            result := result + lRead;
          end else
          Break;
        end;

        Writer.FlushBuffer();
      end else
      raise EStorage.Create(CNT_ERR_BUFFER_INVALIDDATATARGET);
    end else
    raise EStorageEmpty.Create(CNT_ERR_BUFFER_EMPTY);
  end else
  raise EStorageReadUnSupported.Create(CNT_ERR_BUFFER_READNOTSUPPORTED);
end;
{$IFDEF BE_SILENT}
  {$WARNINGS ON}
  {$HINTS ON}
{$ENDIF}

function TQTXStorage.TStorage.Read(const ByteIndex:Int64; DataLength: integer; var Data): integer;
var
  lTotal:   Int64;
  lRemains: Int64;
begin
  result := 0;
  if mcRead in GetCapabilities() then
  begin
    if DataLength > 0 then
    begin
      lTotal := GetSize();
      if lTotal > 0 then
      begin
        if (ByteIndex >= 0) and (ByteIndex < lTotal) then
        begin
          lRemains := lTotal - ByteIndex;
          if lRemains > 0 then
          begin
            if DataLength > lRemains then
              DataLength := lRemains;

            DoReadData(ByteIndex, Data, DataLength);
            result := DataLength;
          end;
        end;
      end else
      raise EStorageEmpty.Create(CNT_ERR_BUFFER_EMPTY);
    end;
  end else
  raise EStorageReadUnSupported.Create(CNT_ERR_BUFFER_READNOTSUPPORTED);
end;

{$IFDEF BE_SILENT}
  {$WARNINGS OFF}
  {$HINTS OFF}
{$ENDIF}
function  TQTXStorage.TStorage.ImportFrom(ByteIndex: Int64;DataLength: integer;const Reader: TReader):  integer;
var
  lToRead:  integer;
  {$IFDEF USE_LOCAL_CACHE}
  lCache:   TStorageCache;
  {$ENDIF}
begin
  // Initialize
  result:=0;
  if mcWrite in GetCapabilities() then
  begin
    if Reader <> nil then
    begin
      While DataLength > 0 do
      begin
        {$IFDEF USE_LOCAL_CACHE}
        lToRead := EnsureRange(SizeOf(lCache), 0, DataLength);
        {$ELSE}
        lToRead := EnsureRange(SizeOf(FCache), 0, DataLength);
        {$ENDIF}
        if lToRead > 0 then
        begin
          {$IFDEF USE_LOCAL_CACHE}
          Reader.Read(lCache, lToRead);
          Write(ByteIndex, lToRead, lCache);
          {$ELSE}
          Reader.Read(FCache, lToRead);
          Write(ByteIndex, lToRead, FCache);
          {$ENDIF}
          result := result + lToRead;
          ByteIndex := ByteIndex + lToRead;
          DataLength := DataLength - lToRead;
        end else
        Break;
      end;
    end else
    raise EStorageInvalidDataSource.Create(CNT_ERR_BUFFER_INVALIDDATASOURCE);
  end else
  raise EStorage.Create(CNT_ERR_BUFFER_WRITENOTSUPPORTED);
end;
{$IFDEF BE_SILENT}
  {$WARNINGS ON}
  {$HINTS ON}
{$ENDIF}

function TQTXStorage.TStorage.Write(const ByteIndex: Int64; DataLength: integer; const Data):  integer;
var
  lTotal:   Int64;
  lending:  Int64;
  lExtra:   Int64;
begin
  result := 0;

  if mcWrite in GetCapabilities() then
  begin
    if DataLength > 0 then
    begin
      lTotal := GetSize();
      if lTotal>0 then
      begin
        if (ByteIndex >= 0) and (ByteIndex < lTotal) then
        begin
          lEnding := ByteIndex + DataLength;
          if lEnding > lTotal then
          begin
            lExtra := lEnding - lTotal;
            if mcScale in GetCapabilities() then
              DoGrowDataBy(lExtra)
            else
              DataLength := EnsureRange(DataLength - lExtra, 0, MAXINT);
          end;
          if DataLength > 0 then
            DoWriteData(ByteIndex, Data, DataLength);
          result := DataLength;
        end else
        raise EStorageIndexViolation.CreateFmt(CNT_ERR_BUFFER_BYTEINDEXVIOLATION,[0, lTotal-1, ByteIndex]);
      end else
      begin
        if mcScale in GetCapabilities() then
        begin
          DoGrowDataBy(DataLength);
          DoWriteData(0, Data, DataLength);
          result := DataLength;
        end else
        raise EStorageScaleUnSupported.Create(CNT_ERR_BUFFER_SCALENOTSUPPORTED);
      end;
    end;
  end else
  raise EStorage.Create(CNT_ERR_BUFFER_WRITENOTSUPPORTED);
end;

//##########################################################################
// TQTXStreamAdapter
//##########################################################################

constructor TQTXStorage.TStreamAdapter.Create(const SourceBuffer:TStorage);
begin
  inherited Create;
  if SourceBuffer <> nil then
    FBufObj := SourceBuffer
  else
    raise EQTXStreamAdapter.Create(CNT_ERR_STREAMADAPTER_INVALIDBUFFER);
end;

function TQTXStorage.TStreamAdapter.GetSize: Int64;
begin
  result := FBufObj.Size;
end;

procedure TQTXStorage.TStreamAdapter.SetSize(const NewSize: Int64);
begin
  FBufObj.Size := EnsureRange(NewSize, 0, MaxLongInt);
end;

function TQTXStorage.TStreamAdapter.Read(var Buffer;Count: longint):  longint;
begin
  result := FBufObj.Read(FOffset, Count, Buffer);
  inc(FOffset, result);
end;

function TQTXStorage.TStreamAdapter.Write(const Buffer;Count:longint):  longint;
begin
  if FOffset = FBufObj.Size then
  begin
    FBufObj.Append(Buffer, Count);
    result := Count;
  end else
  result := FBufObj.Write(FOffset, Count, Buffer);
  inc(FOffset,  result);
end;

function TQTXStorage.TStreamAdapter.Seek(const Offset: Int64; Origin: TSeekOrigin): Int64;
begin
  Case Origin of
  sobeginning:
    begin
      if Offset >= 0 then
        FOffset := EnsureRange(Offset, 0, FBufObj.Size);
     end;
  soCurrent:
    begin
      FOffset := EnsureRange(FOffset + Offset, 0, FBufObj.Size);
    end;
  soEnd:
    begin
      if Offset > 0 then
        FOffset := FBufObj.Size-1
      else
        FOffset := EnsureRange(FOffset-(abs(Offset)), 0, FBufObj.Size);
    end;
  end;
  result := FOffset;
end;

//##########################################################################
//TDBLibBiTStorage
//##########################################################################

constructor TQTXStorage.TBitStorage.Create;
begin
  inherited;
  FData := nil;
  FDataLen := 0;
  FBitsMax := 0;
  FReadyByte := 0;
end;

destructor TQTXStorage.TBitStorage.Destroy;
Begin
  If not GetEmpty() then
    Release();

  inherited;
end;

function TQTXStorage.TBitStorage.ObjectHasData: boolean;
begin
  result := FDataLen > 0;
end;

procedure TQTXStorage.TBitStorage.WriteObjBin(Stream: TStream);
begin
  SaveToStream(Stream);
end;

procedure TQTXStorage.TBitStorage.ReadObjBin(Stream: TStream);
begin
  LoadFromStream(Stream);
end;

procedure TQTXStorage.TBitStorage.DefineProperties(Filer: TFiler);
begin
  inherited;
  Filer.DefineBinaryproperty('IO_BITDATA', ReadObjBin, WriteObjBin, ObjectHasData);
end;

procedure TQTXStorage.TBitStorage.Assign(Source: TPersistent);
var
  lSource: TQTXStorage.TBitStorage;
begin
  If not GetEmpty() then
    Release();

  if Source <> nil then
  begin
    if Source is TQTXStorage.TBitStorage then
    begin
      lSource := TQTXStorage.TBitStorage(Source);
      if not lSource.Empty then
      begin
        self.Allocate(lSource.Count);
        move(lSource.Data^, FData^, FDataLen);
        FReadyByte := 0;
      end;
    end else
    inherited Assign(Source);
  end else
  inherited Assign(Source);
end;

function TQTXStorage.TBitStorage.ToString(const Boundary: integer = 16): string;
const
  CNT_SYM: array [boolean] of string = ('0', '1');
var
  x: integer;
  LCount: integer;
begin
  result := '';
  lCount := Count;
  if lCount > 0 then
  begin
    lCount := TQTXStorage.TTyped.ToNearest(LCount, Boundary);
    x := 0;
    while x < lCount do
    begin
      if x < lCount then
      begin
        Result := Result + CNT_SYM[self[x]];
        if (x mod Boundary) = (Boundary - 1) then
          result := result + #13#10;
      end
      else
        result := result + '_';
      inc(x);
    end;
  end;
end;

function TQTXStorage.TBitStorage.GetFreeBits: integer;
var
  lAddr: pByte;
  llongs,
  lSingles: integer;
begin
  result := 0;

  if FData = nil then
    exit(0);

  lAddr := FData;

  llongs := FDataLen div 8;
  while llongs > 0 do
  begin
    inc(result, 8 - CNT_BiTStorage_ByteTable[lAddr^] ); inc(laddr);
    inc(result, 8 - CNT_BiTStorage_ByteTable[lAddr^] ); inc(laddr);
    inc(result, 8 - CNT_BiTStorage_ByteTable[lAddr^] ); inc(laddr);
    inc(result, 8 - CNT_BiTStorage_ByteTable[lAddr^] ); inc(laddr);
    inc(result, 8 - CNT_BiTStorage_ByteTable[lAddr^] ); inc(laddr);
    inc(result, 8 - CNT_BiTStorage_ByteTable[lAddr^] ); inc(laddr);
    inc(result, 8 - CNT_BiTStorage_ByteTable[lAddr^] ); inc(laddr);
    inc(result, 8 - CNT_BiTStorage_ByteTable[lAddr^] ); inc(laddr);
    dec(llongs);
  end;

  lsingles := FDatalen mod 8;
  while lSingles > 0 do
  begin
    inc(result, 8 - CNT_BiTStorage_ByteTable[lAddr^] ); inc(laddr);
    dec(lSingles);
  end;
end;

function TQTXStorage.TBitStorage.GetSizeOfHeader: integer;
begin
  result := SizeOf(Cardinal);     // Unique ID
  inc(result, SizeOf(integer) );  // Number of bytes
end;

procedure TQTXStorage.TBitStorage.WriteHeader(const Writer: TWriter);
var
  lTemp: cardinal;
begin
  // Write unique signature
  lTemp := CNT_BITStorage_SIGNATURE;
  Writer.Write(lTemp, SizeOf(lTemp));

  // write size of buffer in bytes
  Writer.WriteInteger(FDataLen);
end;

function TQTXStorage.TBitStorage.ReadHeader(const Reader: TReader): integer;
var
  lTemp: cardinal;
begin
  lTemp := $00000000;

  // Read unique signature
  Reader.Read(lTemp, SizeOf(lTemp));
  if lTemp <> CNT_BITStorage_SIGNATURE then
    raise EBitStorageHeaderInvalid.CreateFmt(CNT_BiTStorage_InvalidFileHeader,
    [IntToHex(CNT_BITStorage_SIGNATURE,8), IntToHex(lTemp, 8) ]);

  // Read & return number of bytes
  result := Reader.ReadInteger();
end;

function TQTXStorage.TBitStorage.GetFileSize: integer;
begin
  result := GetSizeOfHeader();
  if FData <> nil then
    inc(result, FDataLen);
end;

function TQTXStorage.TBitStorage.ToStream: TStream;
begin
  result := TMemoryStream.Create;
  SaveToStream(result);
  result.Position := 0;
end;

procedure TQTXStorage.TBitStorage.FromStream(const Stream: TStream; const Disposable: boolean = false);
begin
  try
    LoadFromStream(Stream);
  finally
    if Disposable then
      Stream.Free;
  end;
end;

procedure TQTXStorage.TBitStorage.SaveToStream(const Stream: TStream);
var
  LWriter: TWriter;
begin
  LWriter := TWriter.Create(stream, 1024 * 1024);
  try
    WriteHeader(lWriter);
    if FDataLen > 0 then
      LWriter.Write(FData^, FDataLen);
  finally
    // Flush to stream
    LWriter.FlushBuffer();
    LWriter.Free;
  end;
end;

procedure TQTXStorage.TBitStorage.LoadFromStream(const stream: TStream);
var
  LReader:  TReader;
  LLen:     integer;
Begin
  if not GetEmpty() then
    Release();

  LReader := TReader.Create(stream, 1024 * 1024);
  try
    lLen := ReadHeader(lReader);
    if LLen > 0 then
    begin
      Allocate( TQTXStorage.TTyped.BitsOf(LLen) );
      LReader.Read(FData^, LLen);
    end;
  finally
    LReader.Free;
  end;
end;

Function TQTXStorage.TBitStorage.GetEmpty: boolean;
Begin
  Result := FData = NIL;
end;

Function TQTXStorage.TBitStorage.GetByte(const Index: integer): byte;
{$IFNDEF USE_ARRAYS}
var
  lAddr: pByte;
{$ENDIF}
begin
  if FData = nil then
    raise EBitStorageEmpty.Create(CNT_BiTStorage_Empty);

  if Index > FDataLen then
    raise EBitStorageInvalidByteIndex.CreateFmt(CNT_BiTStorage_InvalidByteIndex, [Index, FDataLen-1]);

  {$IFDEF USE_ARRAYS}
  result := PByteAccessArray(FData)^[Index]
  {$ELSE}
  lAddr := FData;
  inc(lAddr, Index);
  result := lAddr^;
  {$ENDIF}
end;

procedure TQTXStorage.TBitStorage.SetByte(const Index: integer; const Value: byte);
{$IFNDEF USE_ARRAYS}
var
  lAddr: pByte;
{$ENDIF}
begin
  if FData = nil then
    raise EBitStorageEmpty.Create(CNT_BiTStorage_Empty);

  if Index > FDataLen then
    raise EBitStorageInvalidByteIndex.CreateFmt(CNT_BiTStorage_InvalidByteIndex, [Index, FDataLen-1]);

  {$IFDEF USE_ARRAYS}
  PByteAccessArray(FData)^[Index] := value;
  {$ELSE}
  lAddr := FData;
  inc(lAddr, index);
  lAddr^ := value;
  {$ENDIF}
end;

procedure TQTXStorage.TBitStorage.SetBitRange(First, Last: integer; const Bitvalue: boolean);
var
  x: integer;
  LLongs: integer;
  LSingles: integer;
  LCount: integer;

begin
  if FData = nil then
    raise EBitStorageEmpty.Create(CNT_BiTStorage_Empty);

  if First < FBitsMax then
  Begin
    If Last < FBitsMax then
    begin
      // Swap first|last if not in right order
      If First > Last then
        TQTXStorage.TTyped.Swap(First, Last);

      // get totals, take ZERO into account
      LCount := TQTXStorage.TTyped.Span(First, Last, true);

      // use refactoring & loop reduction
      LLongs := LCount div 8;

      x := First;

      while LLongs > 0 do
      Begin
        SetBit(x, Bitvalue); inc(x);
        SetBit(x, Bitvalue); inc(x);
        SetBit(x, Bitvalue); inc(x);
        SetBit(x, Bitvalue); inc(x);
        SetBit(x, Bitvalue); inc(x);
        SetBit(x, Bitvalue); inc(x);
        SetBit(x, Bitvalue); inc(x);
        SetBit(x, Bitvalue); inc(x);
        dec(LLongs);
      end;

      (* process singles *)
      LSingles := LCount mod 8;
      while (LSingles > 0) do
      Begin
        SetBit(x, Bitvalue); inc(x);
        dec(LSingles);
      end;

    end else
    begin
      if First = Last then
        SetBit(First, true)
      else;
        raise EBitStorageInvalidBitIndex.CreateFmt(CNT_BiTStorage_InvalidBitIndex, [FBitsMax-1, Last]);
    end;
  end else;
  raise EBitStorageInvalidBitIndex.CreateFmt(CNT_BiTStorage_InvalidBitIndex, [FBitsMax-1, First]);
end;

procedure TQTXStorage.TBitStorage.SetBits(const Values: TArray<integer>; const Bitvalue: boolean);
var
  llongs: integer;
  lSingles: integer;
  x: integer;
  lCount: integer;
begin
  if FData = nil then
    raise EBitStorageEmpty.Create(CNT_BiTStorage_Empty);

  lCount := length(Values);
  if lCount > 0 then
  begin
    x := low(Values);
    lLongs := lCount div 8;
    while (lLongs > 0) do
    begin
      SetBit(Values[x], Bitvalue); inc(x);
      SetBit(Values[x], Bitvalue); inc(x);
      SetBit(Values[x], Bitvalue); inc(x);
      SetBit(Values[x], Bitvalue); inc(x);
      SetBit(Values[x], Bitvalue); inc(x);
      SetBit(Values[x], Bitvalue); inc(x);
      SetBit(Values[x], Bitvalue); inc(x);
      SetBit(Values[x], Bitvalue); inc(x);
      dec(llongs);
    end;

    lSingles := lCount mod 8;
    while lSingles > 0 do
    begin
      SetBit(Values[x], Bitvalue); inc(x);
      dec(lSingles);
    end;
  end;
end;

function TQTXStorage.TBitStorage.FindIdleBit(var Value: integer; const FromStart: boolean = false): boolean;
var
  lOffset: integer;
  lBit: integer;
  lAddr: PByte;
  x: integer;
Begin
  result := false;

  if FData = nil then
  begin
    Value := -1;
    exit;
  end;

  // Initialize
  lAddr := FData;
  lOffset := 0;

  // Start from scratch or use ready-byte cursor?
  If FromStart then
    FReadyByte := 0
  else
  begin
    if FReadyByte > FDataLen then
      FReadyByte := 0
    else
    begin
      // If ready-byte at end of buffer, re-wind
      if FReadyByte = FDataLen then
      begin
        if GetByte(FReadyByte) = $FF then
          FReadyByte := 0;
      end;
    end;
  end;

  case (FReadyByte = 0) of
  true:
    begin
      // iterate through the buffer until we find a byte
      // that is not $FF, which means it has a free bit we can use
      While lOffset < FDataLen do
      Begin
        if lAddr^ < $FF then
        begin
          result := true;
          break;
        end;
        inc(lOffset);
        inc(lAddr);
      end;
    end;
  false:
    begin
      // Readybyte knows where to look, so just add the
      // index of the byte where bits are ready to the offset
      inc(lOffset, FReadyByte);
      inc(lAddr, FReadyByte);

      While lOffset < FDataLen do
      Begin
        if lAddr^ < $FF then
        begin
          result := true;
          break;
        end;
        inc(lOffset);
        inc(lAddr);
      end;

    end;
  end;

  // Did we indeed find a byte with a free bit?
  if result then
  begin
    // convert to bit index
    lBit := lOffset * 8;

    // Scan 8 iterations looking for the bit
    for x := 1 to 8 do
    Begin
      if GetBit(lBit) then
      begin
        inc(lBit);
        Continue;
      end;

      // This is the bit-index we found
      Value := lBit;

      if not (Value < FBitsMax) then
        raise EBitStorage.Create(CNT_BiTStorage_InternalError);

      // more than 1 bit available in byte? remember that
      FReadyByte := lOffset;
      break;
    end;

  end;
end;

Function TQTXStorage.TBitStorage.GetBit(Const Index: integer): boolean;
var
  BitOfs: 0 .. 255;
  {$IFNDEF USE_ARRAYS}
  lAddr: pByte;
  lByteIndex: integer;
  {$ENDIF}
begin
  if FData = nil then
    raise EBitStorageEmpty.Create(CNT_BiTStorage_Empty);

  if not (Index < FBitsMax) then
    raise EBitStorageInvalidBitIndex.CreateFmt(CNT_BiTStorage_InvalidBitIndex, [FBitsMax-1, Index]);

  BitOfs := Index mod 8;
  {$IFDEF USE_ARRAYS}
  result := (PByteAccessArray(FData)^[Index div 8] and (1 shl (BitOfs mod 8))) <> 0;
  {$ELSE}
  lAddr := FData;
  lByteIndex := integer(Index div 8);
  inc(lAddr, lByteIndex);
  result := lAddr^ and (1 shl (BitOfs mod 8)) <> 0;
  {$ENDIF}
end;

procedure TQTXStorage.TBitStorage.SetBit(Const Index: integer; const Value: boolean);
var
  {$IFNDEF USE_ARRAYS}
  lAddr: pByte;
  {$ENDIF}
  lByte: byte;
  lByteIndex: integer;
  BitOfs: 0 .. 255;
begin
  if FData = nil then
    raise EBitStorageEmpty.Create(CNT_BiTStorage_Empty);

  if Index > FBitsMax then
    raise EBitStorageInvalidBitIndex.CreateFmt(CNT_BiTStorage_InvalidBitIndex, [FBitsMax-1, Index]);

  BitOfs := Index mod 8;
  lByteIndex := Index div 8; //was div 8

  {$IFDEF USE_ARRAYS}
  lByte := PByteAccessArray(FData)^[lByteIndex];
  {$ELSE}
  lAddr := FData;
  inc(lAddr, lByteIndex);
  lByte := lAddr^;
  {$ENDIF}

  case Value of
  true:
    begin
      // set bit if not already set
      If (lByte and (1 shl (BitOfs mod 8))) = 0 then
      Begin
        lByte := (lByte or (1 shl (BitOfs mod 8)));
        {$IFDEF USE_ARRAYS}
        PByteAccessArray(FData)^[lByteIndex] := lByte;
        {$ELSE}
        lAddr^ := lByte;
        {$ENDIF}

        if (FReadyByte > 0) then
        begin
          if lByteIndex = FReadyByte then
          begin
            // No more free bits? Reset ready-byte
            if lByte = $FF then
              FReadyByte := 0
            else
            if TQTXStorage.TTyped.BitsSetInByte(lByte) > 7 then
              FReadyByte := 0;
          end;
        end;

      end;
    end;
  false:
    begin
      // clear bit if not already clear
      If (lByte and (1 shl (BitOfs mod 8))) <> 0 then
      Begin
        lByte := (lByte and not (1 shl (BitOfs mod 8)));
        {$IFDEF USE_ARRAYS}
        PByteAccessArray(FData)^[lByteIndex] := lByte;
        {$ELSE}
        lAddr^ := lByte;
        {$ENDIF}

        // remember this byte pos, because now we know that
        // there is at least 1 free bit there.
        // note: this stuff can be HEAVILY optimized, to make it
        // favour bytes with more available bits etc etc
        FReadyByte := lByteIndex;
      end;
    end;
  end;
end;

procedure TQTXStorage.TBitStorage.ReAllocate(NewMaxBits: integer);
var
  lTemp:  pByte;
  lSize: integer;
  lOldSize: integer;
begin
  // Nothing allocated? Just do a clean alloc & exit
  if FData = nil then
  begin
    Allocate(NewMaxBits);
    exit;
  end;

  // keep current size
  lOldSize := FDataLen;

  // Get # of bytes. NOTE: This is affected by the growth scheme (!)
  // Do not expect to get exact bytes, it will round up by factorial
  lSize := TQTXStorage.TTyped.BytesOf(NewMaxBits);

  try
    lTemp := Allocmem(LSize);
  except
    on e: exception do
    raise EBitStorage.Create('Failed to allocate new buffer error');
  end;

  try
    // zero out new memory
    {$IFDEF USE_RTL_FILL}
    Fillchar(lTemp^, lSize, byte(0));
    {$ELSE}
    TTyped.FillByte(lTemp, lSize, 0);
    {$ENDIF}

    // Copy existing bits over
    if lSize > FDataLen then
      move(FData^, LTemp^, FDataLen)
    else
      move(FData^, lTemp^, lSize);

  finally
    // Release old buffer
    FreeMem(FData);

    // new memory is now our buffer
    FData := lTemp;

    // If we are scaling up, then we want to place the lookup-cursor where
    // the old buffer ended, so that free bits are instantly available
    if lSize > lOldSize then
      FReadyByte := lOldSize-1
    else
      FReadyByte := 0;

    // Keep new buffer + info
    FDataLen := lSize;
    FBitsMax := TQTXStorage.TTyped.BitsOf(FDataLen);
  end;
end;

procedure TQTXStorage.TBitStorage.Allocate(MaxBits: integer);
Begin
  (* release buffer if not empty *)
  If FData <> NIL then
    Release();

  If MaxBits > 0 then
  Begin
    (* Allocate new buffer *)
    try
      FReadyByte := 0;
      FDataLen := TQTXStorage.TTyped.BytesOf(MaxBits);
      FData := AllocMem(FDataLen);
      FBitsMax := FDatalen * 8;

      // zero out new memory
      {$IFDEF USE_RTL_FILL}
      Fillchar(FData^, FDataLen, byte(0))
      {$ELSE}
      TTyped.FillByte(FData, FDataLen, 0);
      {$ENDIF}

    except
      on e: Exception do
      Begin
        FData := NIL;
        FDataLen := 0;
        FBitsMax := 0;
        raise;
      end;
    end;
  end;
end;

procedure TQTXStorage.TBitStorage.Release;
Begin
  If FData <> NIL then
  Begin
    try
      FreeMem(FData);
    finally
      FReadyByte := 0;
      FData := NIL;
      FDataLen := 0;
      FBitsMax := 0;
    end;
  end;
end;

procedure TQTXStorage.TBitStorage.Zero;
Begin
  If FData <> NIL then
    {$IFDEF USE_RTL_FILL}
    Fillchar(FData^, FDataLen, byte(0))
    {$ELSE}
    TTyped.FillByte(FData, FDataLen, 0);
    {$ENDIF}
  else
    raise EBitStorageEmpty.Create(CNT_BiTStorage_Empty);
end;

end.

