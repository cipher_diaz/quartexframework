unit mainform;

{$mode DELPHI}{$H+}

interface

uses
  qtx.vectors, generics.collections, Classes, SysUtils, Forms, Controls,
  Graphics, Dialogs, ExtCtrls, StdCtrls, ComCtrls, ActnList, Menus;

type
  TPersonalia = packed record
    FirstName:  shortstring;
    LastName:   shortstring;
    Gender:     boolean;
    Age:        integer;
    Birth:      TDateTime;
    Job:        shortstring;
    email:      shortstring;
  end;

  { TForm1 }

  TForm1 = class(TForm)
    acNewFile: TAction;
    acOpen: TAction;
    acSaveFile: TAction;
    acCloseFile: TAction;
    acView: TAction;
    ActionList1: TActionList;
    btnClose: TButton;
    btnNew: TButton;
    btnNew1: TButton;
    btnOpen: TButton;
    btnSave: TButton;
    cbDataType: TComboBox;
    Label1: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label5: TLabel;
    Label6: TLabel;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    lbViewBufferSize: TLabel;
    lbHex: TLabel;
    lbViewHex: TLabel;
    lbOffset: TLabel;
    lbSize: TLabel;
    lbTotal: TLabel;
    lstItems: TListView;
    TrackBar1: TTrackBar;
    ViewItems: TListView;
    MainMenu1: TMainMenu;
    MainMenu2: TMainMenu;
    MenuItem1: TMenuItem;
    MenuItem2: TMenuItem;
    MenuItem3: TMenuItem;
    MenuItem4: TMenuItem;
    MenuItem5: TMenuItem;
    OpenDialog: TOpenDialog;
    PageControl1: TPageControl;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    SaveDialog: TSaveDialog;
    ScrollBox1: TScrollBox;
    ViewScrollBox: TScrollBox;
    Splitter1: TSplitter;
    Status: TStatusBar;
    TabSheet1: TTabSheet;
    TabSheet2: TTabSheet;
    procedure acCloseFileExecute(Sender: TObject);
    procedure acCloseFileUpdate(Sender: TObject);
    procedure acNewFileExecute(Sender: TObject);
    procedure acOpenExecute(Sender: TObject);
    procedure acOpenUpdate(Sender: TObject);
    procedure acSaveFileExecute(Sender: TObject);
    procedure acSaveFileUpdate(Sender: TObject);
    procedure acViewExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure lstItemsClick(Sender: TObject);
    procedure TrackBar1Change(Sender: TObject);
  private
    FInfo:    TVector<TPersonalia>;
    procedure VectorToListView(const Vector: TVector<TPersonalia>);
  public

  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

procedure TForm1.FormCreate(Sender: TObject);
begin
  FInfo := nil;
end;

procedure TForm1.lstItemsClick(Sender: TObject);
var
  lRec: TPersonalia;
  idx: integer;
  lOffset: integer;
  lTemp: TQTXStorage.TStorageMemory;
begin
  if not (csDestroying in ComponentState) then
  begin
    if lstItems.Items.Count >0 then
    begin
      idx := lstItems.ItemIndex;
      if idx >= 0 then
      begin
        lRec := FInfo.Items[idx];
        lOffset := SizeOf(TPersonalia) * idx;
        lbOffset.Caption := '$' + IntToHex(lOffset, 8);
        lbSize.Caption := '$' + IntToHex(SizeOf(TPersonalia), 8);
        lbTotal.Caption := '$' + IntToHex(FInfo.Memory.GetDataSize, 8);

        lTemp := TQTXStorage.TStorageMemory.Create();
        try
          lTemp.Append(lRec, SizeOf(lRec));

          lbHex.AutoSize := false;
          lbHex.Caption := lTemp.ToString(16);
          application.ProcessMessages();
          lbHex.AutoSize := true;
          lbHex.Height := lbHex.Height + 20;

          ScrollBox1.UpdateScrollbars();
          ScrollBox1.Update();
          application.ProcessMessages();
        finally
          lTemp.free;
        end;
      end;
    end;
  end;
end;

procedure TForm1.TrackBar1Change(Sender: TObject);
begin
  if not (csDestroying in ComponentState) then
    lbViewBufferSize.Caption := TrackBar1.Position.ToString() + ' bytes';
end;

procedure TForm1.VectorToListView(const Vector: TVector<TPersonalia>);
const
  Gender: Array[boolean] of string = ('Male', 'Female');
var
  lRec: TPersonalia;
  lItem: TListItem;
begin
  screen.Cursor := crHourGlass;
  lstItems.Items.BeginUpdate();
  try
    lstItems.items.Clear();

    for lRec in Vector do
    begin
      lItem := lstItems.Items.Add();
      lItem.Caption := lRec.FirstName;
      lItem.SubItems.add(lRec.LastName);
      lItem.SubItems.add(Gender[lRec.Gender]);
      lItem.SubItems.add(lRec.Age.ToString());
      lItem.SubItems.Add(lRec.Job);
      lItem.SubItems.Add(lRec.email);
    end;
  finally
    lstItems.Items.EndUpdate();
    screen.Cursor := crDefault;
  end;
end;

{procedure TForm1.Button1Click(Sender: TObject);
var
  lBuffer:  TQTXStorage.TStorageMemory;
  lView:    TView<TBit>;
  lBit:     TBit;
  lText:  string;
begin
  lText := '';

  // Create a memory buffer
  lBuffer := TQTXStorage.TStorageMemory.Create();
  try
    // size the buffer
    lBuffer.Size := 5;

    // Create a dataview into the buffer
    lView := TView<TBit>.Create(lBuffer);
    try
      // Set some bits
      lView.Items[3] := 1;
      lView.Items[8] := 1;
      lView.Items[14] := 1;
      lView.Items[23] := 1;
      lView.Items[30] := 1;
      lView.Items[36] := 1;

      // Stringify the result
      for lbit in lView do
      begin
        lText += IntToStr(ord(lBit));
      end;
      showmessage('Count:' + lView.Count.ToString() + #13 + 'Enum:' + lText);
    finally
      lView.free;
    end;
  finally
    lBuffer := nil;
  end;
end;      }

procedure TForm1.acNewFileExecute(Sender: TObject);
const
  CNT_TOTAL = 1000;
var
  lRec: TPersonalia;
  x:  integer;
begin
  if FInfo <> nil then
    AcCloseFile.Execute();

  screen.Cursor := crHourGlass;
  Status.SimpleText := format('Adding %d record to vector ..', [CNT_TOTAL]);
  application.ProcessMessages();

  try
    FInfo := TVector<TPersonalia>.Create(TVectorAllocatorType.vaSequence);
    for x := 1 to CNT_TOTAL do
    begin
      lRec.Gender := (x mod 2) = 0;
      case lRec.Gender of
      true:
        begin
          lRec.FirstName := 'John' + x.ToString();
          lRec.email := 'john@doe.com';
        end;
      false:
        begin
          lRec.FirstName := 'Jane' + x.ToString();
          lRec.email := 'jane@doe.com';
        end;
      end;
      lRec.LastName := 'Doe' + x.ToString();
      lRec.Age := 24 + (x mod 20);
      lRec.job := 'Coder';
      FInfo.Add(lRec);
    end;

    Status.SimpleText := 'Vector complete. Populating listview ..';
    application.ProcessMessages();
    VectorToListView(FInfo);
  finally
    screen.Cursor := crDefault;
    Status.SimpleText := 'Ready.';
    application.ProcessMessages();
  end;

end;

procedure TForm1.acCloseFileExecute(Sender: TObject);
begin
  if FInfo <> nil then
  begin
    lstItems.Items.BeginUpdate();
    try
      lstItems.Clear();
    finally
      lstItems.Items.EndUpdate();
      FreeAndNIL(FInfo);
    end;
  end;
end;

procedure TForm1.acCloseFileUpdate(Sender: TObject);
begin
  if not (csLoading in ComponentState)
  and not (csDestroying in ComponentState) then
  begin
    TAction(Sender).Enabled := FInfo <> nil;
  end;
end;

procedure TForm1.acOpenExecute(Sender: TObject);
var
  lAlloc: TVector<TPersonalia>.TVectorAllocatorFile;
begin
  if FInfo <> nil then
    AcCloseFile.Execute();

  if OpenDialog.Execute() then
  begin
    screen.Cursor := crHourGlass;
    Status.SimpleText := 'Loading vector file ..';
    application.ProcessMessages();

    try
      lAlloc := TVector<TPersonalia>.TVectorAllocatorFile.Create(GetTempFileName(), fmCreate);
      FInfo := TVector<TPersonalia>.Create(lAlloc);
      lAlloc.Buffer.LoadFromFile(OpenDialog.FileName);

      Status.SimpleText := 'Vector loaded. Populating Listview ..';
      application.ProcessMessages();

      VectorToListView(FInfo);

    finally
      Screen.Cursor := crDefault;
      Status.SimpleText := 'Ready.';
      application.ProcessMessages();
    end;
  end;
end;

procedure TForm1.acOpenUpdate(Sender: TObject);
begin
  if not (csLoading in ComponentState)
  and not (csDestroying in ComponentState) then
  begin
    TAction(Sender).Enabled := FInfo = nil;
  end;
end;

procedure TForm1.acSaveFileExecute(Sender: TObject);
var
  lTemp:  TVector<TPersonalia>;
  lAlloc: TVector<TPersonalia>.TVectorAllocatorFile;
begin
  if FInfo <> nil then
  begin
    if SaveDialog.Execute() then
    begin
      // Create a new vector with a specified file location
      lAlloc := TVector<TPersonalia>.TVectorAllocatorFile.Create(SaveDialog.FileName, fmCreate);
      lTemp := TVector<TPersonalia>.Create(lAlloc);
      try
        // Assign the data over
        lTemp.Assign(FInfo);
      finally
        freeAndNil(lTemp);
      end;
    end;
  end;
end;

procedure TForm1.acSaveFileUpdate(Sender: TObject);
begin
  if not (csLoading in ComponentState)
  and not (csDestroying in ComponentState) then
  begin
    TAction(Sender).Enabled := FInfo <> nil;
  end;
end;

procedure TForm1.acViewExecute(Sender: TObject);
var
  lBuffer:  TQTXStorage.TStorageMemory;
  lView:    TView<TBit>;
  lView2:   TView<integer>;
  lView3:   TView<Double>;
  lBit:     TBit;
  x,y:      integer;
  xx:       double;
  lItem:    TListItem;
  lOffset:  integer;
const
    cText: array[boolean] of string = ('True', 'False');
begin

  Screen.Cursor := crHourGlass;
  try
    ViewItems.items.BeginUpdate();
    ViewItems.Items.Clear();

    lbViewBufferSize.Caption := TrackBar1.Position.ToString() + ' bytes';

    // Create a memory buffer
    lBuffer := TQTXStorage.TStorageMemory.Create();
    try
      // size the buffer
      lBuffer.Size := TrackBar1.Position;

      // Create a dataview into the buffer
      case cbDataType.ItemIndex of
      0:  lView := TView<TBit>.Create(lBuffer);
      1:  lView2 := TView<integer>.Create(lBuffer);
      2:  lView3 := TView<double>.Create(lBuffer);
      end;

      try
        case cbDataType.ItemIndex of
        0:
          begin
            for x :=0 to lView.Count-1 do
            begin
              lView[x] := x mod 2;
            end;

            x := 0;
            for lbit in lView do
            begin
              lItem := ViewItems.Items.Add();
              lItem.Caption := x.ToString();
              lItem.SubItems.add( cText[lBit = 0]);
              lItem.SubItems.add( IntToStr(lBit) );

              lOffset := x shr 3;
              lItem.SubItems.add( lOffset.ToString() );

              inc(x);
            end;
          end;
        1:
          begin
            randomize();
            for x :=0 to lView2.Count-1 do
            begin
              lView2[x] := x * random(120);
            end;

            y := 0;
            for x in lView2 do
            begin
              lItem := ViewItems.Items.Add();
              lItem.Caption := x.ToString();
              lItem.SubItems.add( IntToStr(x) );
              lItem.SubItems.add( IntToStr(x) );

              lOffset := SizeOf(integer) * y;
              lItem.SubItems.add( lOffset.ToString() );
              inc(y);
            end;
          end;
        2:
          begin
            randomize();
            for x :=0 to lView3.Count-1 do
            begin
              lView3[x] := (x * random(120) ) + 0.20;
            end;

            y := 0;
            for xx in lView3 do
            begin
              lItem := ViewItems.Items.Add();
              lItem.Caption := FloatToStr(xx);
              lItem.SubItems.add( lItem.Caption );
              lItem.SubItems.add( lItem.Caption );

              lOffset := SizeOf(double) * y;
              lItem.SubItems.add( lOffset.ToString() );
              inc(y);
            end;
          end;
        end;

        lbViewHex.AutoSize := false;
        lbViewHex.Caption := lBuffer.ToString(8);
        application.ProcessMessages();
        lbViewHex.AutoSize := true;
        lbViewHex.Height := lbViewHex.Height + 20;

        ViewScrollBox.UpdateScrollbars();
        ViewScrollBox.Update();
        application.ProcessMessages();
      finally
        lView.free;
      end;
    finally
      lBuffer := nil;
    end;

  finally
    ViewItems.items.EndUpdate();
    Screen.Cursor := crDefault;
  end;
end;

end.

