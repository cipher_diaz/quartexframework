object Form1: TForm1
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  Caption = 'TVector container class demo for Delphi'
  ClientHeight = 370
  ClientWidth = 452
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  DesignSize = (
    452
    370)
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 8
    Top = 65
    Width = 38
    Height = 13
    Caption = 'Output:'
  end
  object btnPopulate: TButton
    Left = 8
    Top = 8
    Width = 141
    Height = 49
    Caption = 'Populate'
    TabOrder = 0
    OnClick = btnPopulateClick
  end
  object btnRawAccess: TButton
    Left = 303
    Top = 8
    Width = 141
    Height = 49
    Anchors = [akTop, akRight]
    Caption = 'Raw Access'
    TabOrder = 1
    OnClick = btnRawAccessClick
  end
  object lstOuput: TListBox
    Left = 8
    Top = 84
    Width = 436
    Height = 257
    Anchors = [akLeft, akTop, akRight, akBottom]
    ItemHeight = 13
    TabOrder = 2
    OnClick = lstOuputClick
  end
  object statusbar: TStatusBar
    Left = 0
    Top = 351
    Width = 452
    Height = 19
    Panels = <>
    SimplePanel = True
    ExplicitLeft = 140
    ExplicitTop = 352
    ExplicitWidth = 0
  end
end
