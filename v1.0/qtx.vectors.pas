unit qtx.vectors;

// *** NOTE: This version to TVector is obsolete!
// It is meant purely for educational purposes in connection with the article on Embarcadero's community network:
// https://community.idera.com/developer-tools/b/blog/posts/vector-containers-for-delphi
//
// Please use the qtx.vectors.pas unit inside the "Latest" folder of this repository
//

{$IFDEF FPC}
  {$mode DELPHI}{$H+}
{$ENDIF}

interface

uses
  SysUtils, Classes, Generics.Collections;

type

  EQTXVector                    = class(Exception);
  EQTXVectorEmpty               = class(EQTXVector);
  EQTXVectorInvalidIndex        = class(EQTXVector);
  EQTXVectorAllocator           = class(Exception);
  EQTXVectorAllocatorLocked     = class(EQTXVectorAllocator);
  EQTXVectorAllocatorNotLocked  = class(EQTXVectorAllocator);

  TVectorAllocator = (
    vaSequence,
    vaDynamic,
    vaCustom
  );

  TVector<T> = class(TEnumerable<T>)
  public
    type
    IVectorMemory = interface
      ['{9A3A02CB-5A4B-409F-B999-4728B3636A1E}']
      procedure   Resize(ItemCount: integer);
      procedure   LockMemory(var Data);
      procedure   UnLockMemory;
      function    GetDataSize: integer;
      procedure   SetItem(const index: integer; Value: T);
      function    GetItem(const index: integer): T;
      function    GetCount: integer;
      function    Add(const Value: T): integer;
      procedure   Remove(const Index: integer);
      function    Insert(const Index: integer; Value: T): integer;
      procedure   Clear;
    end;

    TVectorEnumerator = class(TEnumerator<T>)
    private
      FIndex:     integer;
      FParent:    TVector<T>;
    protected
      function    DoGetCurrent: T; override;
      function    DoMoveNext: boolean; override;
    public
      constructor Create(Owner: TVector<T>); virtual;
    end;

    TVectorMemory = class(TInterfacedPersistent, IVectorMemory)
    public
      procedure   Resize(ItemCount: integer); virtual; abstract;
      procedure   LockMemory(var Data); virtual; abstract;
      procedure   UnLockMemory; virtual; abstract;
      function    GetDataSize: integer; virtual; abstract;
      procedure   SetItem(const index: integer; Value: T); virtual; abstract;
      function    GetItem(const index: integer): T; virtual; abstract;
      function    GetCount: integer;  virtual; abstract;
      function    Add(const Value: T): integer;  virtual; abstract;
      procedure   Remove(const Index: integer);  virtual; abstract;
      function    Insert(const Index: integer; Value: T): integer;  virtual; abstract;
      procedure   Clear;  virtual; abstract;
    end;
    TVectorMemoryClass = class of TVectorMemory;

    TVectorSequenceMemory = class(TVectorMemory)
    strict private
      FData:      PByte;
      FSize:      integer;
      FLock:      integer;
    strict protected
      function    OffsetTo(const Index: integer): integer; inline;
      function    CalcTrailFor(Index: integer): integer; inline;
    public
      procedure   Resize(ItemCount: integer); override;
      function    GetDataSize: integer; override;
      procedure   LockMemory(var Data); override;
      procedure   UnLockMemory; override;
      procedure   SetItem(const index: integer; Value: T); override;
      function    GetItem(const index: integer): T;  override;
      function    GetCount: integer;  override;
      function    Add(const Value: T): integer;  override;
      procedure   Remove(const Index: integer);  override;
      function    Insert(const Index: integer; Value: T): integer;  override;
      procedure   Clear;  override;
      destructor  Destroy; override;
    end;

    TVectorDynamicMemory = class(TVectorMemory)
    strict private
      FValues:    TArray<T>;
      FLock:      integer;
    public
      procedure   Resize(ItemCount: integer); override;
      function    GetDataSize: integer; override;
      procedure   LockMemory(var Data); override;
      procedure   UnLockMemory; override;
      procedure   SetItem(const index: integer; Value: T); override;
      function    GetItem(const index: integer): T;  override;
      function    GetCount: integer;  override;
      function    Add(const Value: T): integer;  override;
      procedure   Remove(const Index: integer);  override;
      function    Insert(const Index: integer; Value: T): integer;  override;
      procedure   Clear;  override;
      destructor  Destroy; override;
    end;

  strict private
    FMemory:    IVectorMemory;
    FType:      TVectorAllocator;

  strict protected
    function    GetCount: integer; inline;
    function    GetItem(const index: integer): T; inline;
    procedure   SetItem(const index: integer; Value: T); inline;
    function    GetFront: T; inline;
    function    GetBack: T; inline;

  protected
    function    DoGetEnumerator: TEnumerator<T>;  override;

    {$IFDEF FPC}
    function    GetPtrEnumerator: TEnumerator<PT>; override;
    {$ENDIF}
  public
    property    Memory: IVectorMemory read FMemory;
    property    AllocatorType: TVectorAllocator read FType;
    property    Items[const index: integer]:T read GetItem write SetItem; default;
    property    Count: integer read GetCount;

    property    Front: T read GetFront;
    property    Back: T read GetBack;

    procedure   Assign(const Source: TVector<T>); virtual;

    function    Add(const Value: T): integer; inline;
    procedure   Remove(const Index: integer); inline;
    function    Insert(const Index: integer; const Value: T): integer; inline;

    procedure   Clear; inline;

    procedure   PushBack(const Value: T); inline;
    procedure   PushFront(const Value: T); inline;
    procedure   Swap(const First, Second: integer);

    constructor Create; overload; virtual;
    constructor Create(const AllocatorType: TVectorAllocator = vaSequence); overload; virtual;
    constructor Create(const Allocator: IVectorMemory); overload; virtual;
    destructor  Destroy; override;
  end;


const
  CNT_ERR_VECTOR_InvalidIndex   = 'Invalid index, expected 0..0 not %d';
  CNT_ERR_VECTOR_Empty          = 'Invalid operation, vector list is empty';
  CNT_ERR_VECTOR_NoCustomAlloc  = 'Custom vector allocator cannot be created with this constructor';
  CNT_ERR_VECTOR_MemLocked      = 'Vector memory is locked';
  CNT_ERR_VECTOR_MemUnLocked    = 'Vector memory was unlocked';
  CNT_ERR_VECTOR_ResizeFailed   = 'Resize failed, invalid item count [%d]';

implementation

//#############################################################################
// TVector<T>
//#############################################################################

constructor TVector<T>.TVectorEnumerator.Create(Owner: TVector<T>);
begin
  inherited Create;
  FParent := Owner;
  FIndex := -1;
end;

function TVector<T>.TVectorEnumerator.DoGetCurrent: T;
begin
  if FIndex < 0 then
    inc(FIndex);
  result := FParent[FIndex];
end;

function TVector<T>.TVectorEnumerator.DoMoveNext: boolean;
begin
  result := (FIndex + 1) < FParent.GetCount();
  if result then
    inc(FIndex);
end;

//#############################################################################
// TVector<T>
//#############################################################################

constructor TVector<T>.Create;
begin
  inherited Create;
  FType := vaDynamic;
  FMemory := TVectorDynamicMemory.Create();
end;

constructor TVector<T>.Create(const Allocator: IVectorMemory);
begin
  inherited Create();
  FType := vaCustom;
  FMemory := Allocator;
end;

constructor TVector<T>.Create(const AllocatorType: TVectorAllocator = vaSequence);
var
  lTemp:  TVectorMemory;
begin
  inherited Create();
  FType := AllocatorType;

  case AllocatorType of
  vaSequence:
    begin
      lTemp := TVectorSequenceMemory.Create();
      FMemory := lTemp as IVectorMemory;
    end;

  vaDynamic:
    begin
      lTemp := TVectorDynamicMemory.Create();
      FMemory := lTemp as IVectorMemory;
    end;
  vaCustom:
    raise EQTXVector.Create(CNT_ERR_VECTOR_NoCustomAlloc);
  end;
end;

destructor TVector<T>.Destroy;
begin
  FMemory := nil;
  inherited;
end;

{$IFDEF FPC}
function TVector<T>.GetPtrEnumerator: TEnumerator<PT>;
begin
  Result := nil;
end;
{$ENDIF}

function TVector<T>.DoGetEnumerator: TEnumerator<T>;
begin
  Result := TVectorEnumerator.Create(self);
end;

procedure TVector<T>.Assign(const Source: TVector<T>);
var
  x: integer;
  lSrc: PByte;
  lDst: PByte;
begin
  FMemory.Clear();
  if Source <> nil then
  begin
    if Source.Count < 1 then
      exit;

    lSrc := nil;
    lDst := nil;

    if Source.AllocatorType = self.AllocatorType then
    begin
      if Source.AllocatorType = vaSequence then
      begin
        FMemory.Resize(Source.Count);
        FMemory.LockMemory(ldst);
        try
          Source.Memory.LockMemory(lsrc);
          try
            move(lSrc^, lDst^, FMemory.GetDataSize() );
          finally
            Source.Memory.UnLockMemory();
          end;
        finally
          FMemory.UnLockMemory();
        end;
        exit;
      end;
    end;

    FMemory.Resize(Source.Count);
    for x := 0 to Source.Count-1 do
    begin
      FMemory.SetItem(x, Source[x]);
    end;
  end;
end;

procedure TVector<T>.Clear;
begin
  FMemory.Clear();
end;

procedure TVector<T>.SetItem(const index: integer; Value: T);
begin
  FMemory.SetItem(Index, Value);
end;

procedure TVector<T>.Swap(const First, Second: integer);
var
  lTemp: T;
  lCount: integer;
begin
  lCount := FMemory.GetCount();
  if lCount > 0 then
  begin
    if (First >=0) and (First < lCount) then
    begin
      if (Second >= 0) and (Second < lCount) then
      begin
        lTemp := FMemory.GetItem(First);
        FMemory.SetItem(First, FMemory.GetItem(Second));
        FMemory.SetItem(Second, lTemp);
      end;
    end;
  end else
  raise EQTXVectorEmpty.Create(CNT_ERR_VECTOR_Empty);
end;

procedure TVector<T>.PushBack(const Value: T);
begin
  FMemory.Add(Value);
end;

procedure TVector<T>.PushFront(const Value: T);
begin
  if GetCount() > 0 then
    Insert(0, Value)
  else
    Add(Value);
end;

function TVector<T>.GetFront: T;
begin
  result := FMemory.GetItem(0);
end;

function TVector<T>.GetBack: T;
begin
  result := FMemory.GetItem( FMemory.GetCount() -1 );
end;

function TVector<T>.Add(const Value: T): integer;
begin
  result := FMemory.Add(Value);
end;

procedure TVector<T>.Remove(const Index: integer);
begin
  FMemory.Remove(Index);
end;

function TVector<T>.Insert(const Index: integer; const Value: T): integer;
begin
  result := FMemory.Insert(Index, Value);
end;

function TVector<T>.GetCount: integer;
begin
  result := FMemory.GetCount();
end;

function TVector<T>.GetItem(const index: integer): T;
begin
  result := FMemory.GetItem(Index);
end;

//#############################################################################
// TVectorSequenceMemory
//#############################################################################

destructor TVector<T>.TVectorSequenceMemory.Destroy;
begin
  if FData <> nil then
  begin
    Freemem(FData);
    FData := nil;
    FSize := 0;
  end;
  inherited;
end;

procedure TVector<T>.TVectorSequenceMemory.Resize(ItemCount: integer);
var
  lCount: integer;
begin
  lCount := GetCount();
  if ItemCount <> lCount then
  begin
    if (lCount > 0) and (ItemCount < 1) then
    begin
      FreeMem(FData);
      FData := nil;
      FSize := 0;
      exit;
    end;

    if ItemCount > 0 then
    begin
      FSize := SizeOf(T) * ItemCount;
      try
        FData := allocmem(FSize);
      except
        FSize := 0;
        FData := nil;
        raise;
      end;
    end else
    EQTXVectorAllocator.CreateFmt(CNT_ERR_VECTOR_ResizeFailed, [ItemCount]);
  end;
end;

function TVector<T>.TVectorSequenceMemory.GetDataSize: integer;
begin
  result := FSize;
end;

procedure TVector<T>.TVectorSequenceMemory.LockMemory(var Data);
begin
  if FLock < 1 then
  begin
    inc(FLock);
    pointer(Data) := FData;
  end else
  raise EQTXVectorAllocatorLocked.Create(CNT_ERR_VECTOR_MemLocked);
end;

procedure TVector<T>.TVectorSequenceMemory.UnLockMemory;
begin
  if FLock > 0 then
    dec(FLock)
  else
  raise EQTXVectorAllocatorNotLocked.Create(CNT_ERR_VECTOR_MemUnLocked);
end;

function TVector<T>.TVectorSequenceMemory.CalcTrailFor(Index: integer): integer;
var
  lCount: integer;
begin
  lCount := FSize div SizeOf(T);
  inc(index);
  dec(lCount, Index);
  result := lCount * SizeOf(T);
end;

function TVector<T>.TVectorSequenceMemory.OffsetTo(const Index: integer): integer;
begin
  if Index > 0 then
    result := SizeOf(T) * Index
  else
    result := -1;
end;

{$HINTS OFF}
function TVector<T>.TVectorSequenceMemory.GetItem(const index: integer): T;
var
  lSource: PByte;
  lCount: integer;
begin
  if FData <> nil then
  begin
    lCount := GetCount();
    if (index >= 0) and (index < lCount ) then
    begin
      lSource := FData;
      inc(lSource, Index * SizeOf(T) );
      move(lSource^, result, SizeOf(T) );
    end else
    raise EQTXVectorInvalidIndex.CreateFmt(CNT_ERR_VECTOR_InvalidIndex, [0, lCount-1, Index]);
  end else
  raise EQTXVectorEmpty.Create(CNT_ERR_VECTOR_Empty);
end;
{$HINTS ON}

procedure TVector<T>.TVectorSequenceMemory.SetItem(const index: integer; Value: T);
var
  lTarget: PByte;
  lCount: integer;
begin
  if FData <> nil then
  begin
    lCount := GetCount();
    if (index >= 0) and (index < lCount ) then
    begin
      lTarget := FData;
      inc(lTarget, Index * SizeOf(T) );
      move(Value, lTarget^, SizeOf(T));
    end else
    raise EQTXVectorInvalidIndex.CreateFmt(CNT_ERR_VECTOR_InvalidIndex, [0, lCount-1, Index]);
  end else
  raise EQTXVectorEmpty.Create(CNT_ERR_VECTOR_Empty);
end;

function TVector<T>.TVectorSequenceMemory.GetCount: integer;
begin
  if FSize > 0 then
    result := FSize div SizeOf(T)
  else
    result := 0;
end;

function TVector<T>.TVectorSequenceMemory.Add(const Value: T): integer;
var
  lNewSize: integer;
  lTarget: PByte;
begin
  lNewSize := FSize + SizeOf(T);
  result := GetCount();

  if FSize < 1 then
  begin
    FData := AllocMem(lNewSize);
    FSize := lNewSize;
    move(Value, FData^, SizeOf(T) );
  end else
  begin
    {$IFDEF FPC}
    FData := ReAllocmem(FData, lNewSize);
    {$ELSE}
    ReAllocmem(FData, lNewSize);
    {$ENDIF}

    lTarget := FData;
    inc(lTarget, FSize);

    FSize := lNewSize;
    move(Value, lTarget^, SizeOf(T) );
  end;
end;

procedure TVector<T>.TVectorSequenceMemory.Remove(const Index: integer);
var
  lCount: integer;
  lNewSize: integer;
  lSource: PByte;
  lTarget: PByte;
  lTrailSize: integer;
begin
  lCount := GetCount();
  if lCount > 0 then
  begin
    lNewSize := FSize - SizeOf(T);

    // Will the list be empty after this?
    if lNewSize < 1 then
    begin
      Freemem(FData);
      FData := nil;
      FSize := 0;
      exit;
    end;

    // Delete last item in longer list? Truncate
    if Index = lCount-1 then
    begin
      {$IFDEF FPC}
      FData := ReAllocMem(FData, lNewSize);
      {$ELSE}
      ReAllocMem(FData, lNewSize);
      {$ENDIF}
      FSize := lNewSize;
      exit;
    end;

    // Remove & Copy with trail
    lSource := FData;
    inc(lSource, SizeOf(T) * (Index + 1) );

    lTarget := FData;
    inc(lTarget, SizeOf(T) * Index);

    lTrailSize := CalcTrailFor(Index);
    move(lSource^, lTarget^, lTrailSize);

    {$IFDEF FPC}
    FData := ReAllocMem(FData, lNewSize);
    {$ELSE}
    ReAllocMem(FData, lNewSize);
    {$ENDIF}
    FSize := lNewSize;
  end else
  raise EQTXVectorEmpty.Create(CNT_ERR_VECTOR_Empty);
end;

function TVector<T>.TVectorSequenceMemory.Insert(const Index: integer; Value: T): integer;
var
  lCount: integer;
  lNewSize: integer;
  lTrailSize: integer;
  lSource: PByte;
  lTarget: PByte;
begin
  lCount := GetCount();

  // First item? Check that index is 0
  if FSize < 1 then
  begin
    if Index = 0 then
    begin
      result := Add(Value);
      exit;
    end else
    raise EQTXVectorInvalidIndex.CreateFmt(CNT_ERR_VECTOR_InvalidIndex, [0, 0, Index]);
  end;

  lNewSize := FSize + SizeOf(T);
  lTrailSize := CalcTrailFor(Index-1); //-1

  // Scale up buffer
  {$IFDEF FPC}
  FData := ReAllocMem(FData, lNewSize);
  {$ELSE}
  ReAllocMem(FData, lNewSize);
  {$ENDIF}
  FSize := lNewSize;

  // Move trailing data downward
  lSource := FData;
  inc(lSource, OffsetTo(Index) );

  lTarget := lSource;
  inc(lTarget, SizeOf(T) );

  move(lSource^, lTarget^, lTrailSize);

  // Write data to inserted space
  move(Value, lSource^, SizeOf(T) );

  result := lCount + 1;
end;

procedure TVector<T>.TVectorSequenceMemory.Clear;
begin
  if FData <> nil then
  begin
    Freemem(FData);
    FData := nil;
    FSize := 0;
  end;
end;

//#############################################################################
// TVector<T>.TVectorDynamicMemory
//#############################################################################

destructor TVector<T>.TVectorDynamicMemory.Destroy;
begin
  if length(FValues) > 0 then
    Clear();
  inherited;
end;

function TVector<T>.TVectorDynamicMemory.Add(const Value: T): integer;
begin
  result := length(FValues);
  SetLength(FValues, result + 1);
  FValues[result] := Value;
  inc(result);
end;

procedure TVector<T>.TVectorDynamicMemory.Resize(ItemCount: integer);
begin
  if ItemCount >= 0 then
    setlength(FValues, ItemCount)
  else
    EQTXVectorAllocator.CreateFmt(CNT_ERR_VECTOR_ResizeFailed, [ItemCount]);
end;

function TVector<T>.TVectorDynamicMemory.GetDataSize: integer;
begin
  result := SizeOf(FValues);
end;

procedure TVector<T>.TVectorDynamicMemory.LockMemory(var Data);
begin
  if FLock < 1 then
  begin
    inc(FLock);
    Pointer(Data) := @FValues[0];
  end else
  raise EQTXVectorAllocatorLocked.Create(CNT_ERR_VECTOR_MemLocked);
end;

procedure TVector<T>.TVectorDynamicMemory.UnLockMemory;
begin
  if FLock > 0 then
    dec(FLock)
  else
  raise EQTXVectorAllocatorNotLocked.Create(CNT_ERR_VECTOR_MemUnLocked);
end;

procedure TVector<T>.TVectorDynamicMemory.Clear;
begin
  setlength(FValues, 0);
end;

function TVector<T>.TVectorDynamicMemory.GetCount: integer;
begin
  result := length(FValues);
end;

function TVector<T>.TVectorDynamicMemory.GetItem(const index: integer): T;
begin
  result := FValues[Index];
end;

procedure TVector<T>.TVectorDynamicMemory.SetItem(const index: integer; Value: T);
begin
  FValues[Index] := Value;
end;

function TVector<T>.TVectorDynamicMemory.Insert(const Index: integer; Value: T): integer;
begin
  system.insert(Value, FValues, Index);
  result := length(FValues);
end;

procedure TVector<T>.TVectorDynamicMemory.Remove(const Index: integer);
begin
  system.delete(FValues, Index, 1);
end;

end.

