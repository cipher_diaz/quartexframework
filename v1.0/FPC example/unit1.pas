unit Unit1;

{$mode delphi}{$H+}

interface

uses
  Generics.Collections,
  qtx.vectors,
  Classes, SysUtils, Forms, Controls, Graphics, Dialogs, StdCtrls, ComCtrls;

type

  PTest = ^TTest;
  TTest = record
    id: integer;
    name: shortstring;
    class function Create(id: integer; name: shortstring): TTest; static;
  end;

  TMyVectorList = class(TVector<TTest>)
  end;



  { TForm1 }

  TForm1 = class(TForm)
    btnPopulate: TButton;
    btnRawAccess: TButton;
    Label1: TLabel;
    lstOuput: TListBox;
    StatusBar: TStatusBar;
    procedure btnPopulateClick(Sender: TObject);
    procedure btnRawAccessClick(Sender: TObject);
    procedure lstOuputClick(Sender: TObject);
  private
  public
  end;

var
  Form1: TForm1;

implementation

{$R *.lfm}

class function TTest.Create(id: integer; name: shortstring): TTest;
begin
  result.id := id;
  result.name := name;
end;


{ TForm1 }

procedure TForm1.btnPopulateClick(Sender: TObject);
var
  x: integer;
  lList: TMyVectorList;
  lRec: TTest;
begin
  statusbar.SimpleText := 'Creating vector list';
  application.ProcessMessages;
  try
    screen.Cursor := crDefault;
    lstOuput.Items.BeginUpdate;
    lstOuput.Items.Clear;

    lList := TMyVectorList.Create(TVectorAllocator.vaSequence);
    try
      // populate
      for x := 1 to 10 do
      begin
        lList.add( TTest.Create(x, 'name #' + IntToStr(x-1) ) );
      end;

      // do some inserts
      lList.insert(4, TTest.Create(4, 'first') );
      lList.insert(9, TTest.Create(9, 'second') );

      // Read back
      for lRec in lList do
      begin
        lstOuput.items.add( lRec.name );
      end;

    finally
      lList.Clear;
      lList.free;
    end;

  finally
    lstOuput.Items.EndUpdate;
    screen.Cursor := crDefault;

    statusbar.SimpleText := 'Click one of the items to examine';
  end;
end;

procedure TForm1.btnRawAccessClick(Sender: TObject);
var
  x: integer;
  lList: TMyVectorList;
  lRaw: PTest;
begin
  statusbar.SimpleText := 'Creating vector list for raw access';
  application.ProcessMessages;

  try
    screen.Cursor := crDefault;
    lstOuput.Items.BeginUpdate;
    lstOuput.Items.Clear;


    lList := TMyVectorList.Create(vaSequence);
    try
      // populate
      for x := 1 to 10 do
      begin
        lList.add( TTest.Create(x, 'name #' + IntToStr(x-1) ) );
      end;

      // Get a pointer to the managed memory
      lRaw := nil;
      llist.Memory.LockMemory(lRaw);
      try
        // manually read out the name field from each
        for x := 0 to lList.Count-1 do
        begin
          lstOuput.items.add( lRaw^.name );
          inc(lRaw);
        end;
      finally
        lList.Memory.UnLockMemory;
      end;

    finally
      lList.Clear;
      lList.free;
    end;

  finally
    lstOuput.Items.EndUpdate;
    screen.Cursor := crDefault;
    statusbar.SimpleText := 'Click one of the items to examine';
  end;
end;

procedure TForm1.lstOuputClick(Sender: TObject);
var
  lOffset:  integer;
  lTotal: integer;
const
  CNT_INFO = 'Record at offset %d. Record size is %d , total vector is %d bytes';
begin
  if lstOuput.Items.Count > 0 then
  begin
    if lstOuput.ItemIndex >= 0 then
    begin
      lOffset := SizeOf(TTest) * lstOuput.ItemIndex;
      lTotal := SizeOf(TTest) * lstOuput.Items.Count;
      statusbar.SimpleText := format(CNT_INFO, [lOffset, SizeOf(TTest), lTotal]);
    end else
    statusbar.SimpleText := 'Click one of the items';
  end else
  statusbar.SimpleText := 'Click one of the buttons';
end;

end.

