// ############################################################################
// #
// # Quartex code library
// #        version 1.0.1
// ############################################################################
// #
// # Written by Jon L. Aasenden @ Quartex Components
// #
// # This software is released under Mozilla Public License V2
// #   https://www.mozilla.org/en-US/MPL/2.0/
// #
// ############################################################################

unit quartex.bitbuffer;


{$IFDEF FPC}
  {$mode DELPHI}{$H+}
{$ENDIF}

{$I 'quartex.inc'}

interface

uses
  Classes, SysUtils, Generics.Collections,
  quartex.storage;

type

  TBitStorage = class(TPersistent)
  strict private
    FData:      PByte;
    FDataLen:   integer;
    FBitsMax:   integer;
    FReadyByte: integer;
  strict protected
    type
    EBitStorage = class(Exception);
    EBitStorageInvalidBitIndex  = class(EBitStorage);
    EBitStorageHeaderInvalid    = class(EBitStorage);
    EBitStorageInvalidByteIndex = class(EBitStorage);
    EBitStorageEmpty            = class(EBitStorage);

  strict protected
    function    GetByte(const Index: integer): byte; virtual;
    procedure   SetByte(const Index: integer; const Value: byte); virtual;

    function    GetBit(const Index: integer): boolean; virtual;
    procedure   SetBit(const Index: integer; const Value: boolean); virtual;

    function    GetSizeOfHeader: integer; virtual;
    procedure   WriteHeader(const Writer: TWriter); virtual;
    function    ReadHeader(const Reader: TReader): integer; virtual;

    function    GetEmpty: boolean; virtual;
    function    GetFileSize: integer; inline;
    function    GetFreeBits: integer; inline;

  protected
    // standard Delphi/LCL persistence methods
    function    ObjectHasData: boolean; virtual;
    procedure   ReadObjBin(Stream: TStream); virtual;
    procedure   WriteObjBin(Stream: TStream); virtual;
    procedure   DefineProperties(Filer: TFiler); override;
  public
    property    Empty: boolean read GetEmpty;
    property    Data: PByte read FData;
    property    ByteSize: integer read FDataLen;
    property    FileSize: integer read GetFileSize;
    property    BitsAvailable: integer read GetFreeBits;
    property    Count: integer read FBitsMax;
    property    Bytes[const Index: integer]: byte Read GetByte write SetByte;
    property    Bits[const Index: integer]: boolean Read GetBit write SetBit; default;

    procedure   Allocate(MaxBits: integer);
    procedure   ReAllocate(NewMaxBits: integer);
    procedure   Release;
    procedure   Zero; inline;

    procedure   Assign(Source: TPersistent); override;

    function    ToStream: TStream;
    procedure   FromStream(const Stream: TStream; const Disposable: boolean = false);
    function    ToString(const Boundary: integer = 16): string; reintroduce;

    procedure   SaveToStream(const Stream: TStream); virtual;
    procedure   LoadFromStream(const Stream: TStream); virtual;

    procedure   SetBitRange(First, Last: integer; const Bitvalue: boolean); inline;
    procedure   SetBits(const Values: TArray<integer>; const Bitvalue: boolean); inline;
    function    FindIdleBit(var Value: integer; const FromStart: boolean = false): boolean; inline;

    constructor Create; virtual;
    destructor  Destroy; override;
  end;


implementation


{$IFDEF USE_ARRAYS}
type
  // Delphi has a 2Gb limit on arrays
  TByteAccessArray = packed array[0..2147483648-2] of byte;
  PByteAccessArray = ^TByteAccessArray;
{$ENDIF}

//##########################################################################
//TDBLibBiTStorage
//##########################################################################

constructor TBitStorage.Create;
begin
  inherited;
  FData := nil;
  FDataLen := 0;
  FBitsMax := 0;
  FReadyByte := 0;
end;

destructor TBitStorage.Destroy;
Begin
  If not GetEmpty() then
    Release();

  inherited;
end;

function TBitStorage.ObjectHasData: boolean;
begin
  result := FDataLen > 0;
end;

procedure TBitStorage.WriteObjBin(Stream: TStream);
begin
  SaveToStream(Stream);
end;

procedure TBitStorage.ReadObjBin(Stream: TStream);
begin
  LoadFromStream(Stream);
end;

procedure TBitStorage.DefineProperties(Filer: TFiler);
begin
  inherited;
  Filer.DefineBinaryproperty('IO_BITDATA', ReadObjBin, WriteObjBin, ObjectHasData);
end;

procedure TBitStorage.Assign(Source: TPersistent);
var
  lSource: TBitStorage;
begin
  If not GetEmpty() then
    Release();

  if Source <> nil then
  begin
    if Source is TBitStorage then
    begin
      lSource := TBitStorage(Source);
      if not lSource.Empty then
      begin
        self.Allocate(lSource.Count);
        move(lSource.Data^, FData^, FDataLen);
        FReadyByte := 0;
      end;
    end else
    inherited Assign(Source);
  end else
  inherited Assign(Source);
end;

function TBitStorage.ToString(const Boundary: integer = 16): string;
const
  CNT_SYM: array [boolean] of string = ('0', '1');
var
  x: integer;
  LCount: integer;
begin
  result := '';
  lCount := Count;
  if lCount > 0 then
  begin
    lCount := TTyped.ToNearest(LCount, Boundary);
    x := 0;
    while x < lCount do
    begin
      if x < lCount then
      begin
        Result := Result + CNT_SYM[self[x]];
        if (x mod Boundary) = (Boundary - 1) then
          result := result + #13#10;
      end
      else
        result := result + '_';
      inc(x);
    end;
  end;
end;

function TBitStorage.GetFreeBits: integer;
var
  lAddr: pByte;
  llongs: integer;
begin
  result := 0;

  if FData = nil then
    exit(0);

  lAddr := FData;

  llongs := FDataLen div 8;
  while llongs > 0 do
  begin
    inc(result, 8 - CNT_BiTStorage_ByteTable[lAddr^] ); inc(laddr);
    inc(result, 8 - CNT_BiTStorage_ByteTable[lAddr^] ); inc(laddr);
    inc(result, 8 - CNT_BiTStorage_ByteTable[lAddr^] ); inc(laddr);
    inc(result, 8 - CNT_BiTStorage_ByteTable[lAddr^] ); inc(laddr);
    inc(result, 8 - CNT_BiTStorage_ByteTable[lAddr^] ); inc(laddr);
    inc(result, 8 - CNT_BiTStorage_ByteTable[lAddr^] ); inc(laddr);
    inc(result, 8 - CNT_BiTStorage_ByteTable[lAddr^] ); inc(laddr);
    inc(result, 8 - CNT_BiTStorage_ByteTable[lAddr^] ); inc(laddr);
    dec(llongs);
  end;

  case FDataLen mod 8 of
  1:  inc(result, 8 - CNT_BiTStorage_ByteTable[lAddr^] );
  2:  begin
        inc(result, 8 - CNT_BiTStorage_ByteTable[lAddr^] ); inc(laddr);
        inc(result, 8 - CNT_BiTStorage_ByteTable[lAddr^] );
      end;
  3:  begin
        inc(result, 8 - CNT_BiTStorage_ByteTable[lAddr^] ); inc(laddr);
        inc(result, 8 - CNT_BiTStorage_ByteTable[lAddr^] ); inc(laddr);
        inc(result, 8 - CNT_BiTStorage_ByteTable[lAddr^] );
      end;
  4:  begin
        inc(result, 8 - CNT_BiTStorage_ByteTable[lAddr^] ); inc(laddr);
        inc(result, 8 - CNT_BiTStorage_ByteTable[lAddr^] ); inc(laddr);
        inc(result, 8 - CNT_BiTStorage_ByteTable[lAddr^] ); inc(laddr);
        inc(result, 8 - CNT_BiTStorage_ByteTable[lAddr^] );
      end;
  5:  begin
        inc(result, 8 - CNT_BiTStorage_ByteTable[lAddr^] ); inc(laddr);
        inc(result, 8 - CNT_BiTStorage_ByteTable[lAddr^] ); inc(laddr);
        inc(result, 8 - CNT_BiTStorage_ByteTable[lAddr^] ); inc(laddr);
        inc(result, 8 - CNT_BiTStorage_ByteTable[lAddr^] ); inc(laddr);
        inc(result, 8 - CNT_BiTStorage_ByteTable[lAddr^] );
      end;
  6:  begin
        inc(result, 8 - CNT_BiTStorage_ByteTable[lAddr^] ); inc(laddr);
        inc(result, 8 - CNT_BiTStorage_ByteTable[lAddr^] ); inc(laddr);
        inc(result, 8 - CNT_BiTStorage_ByteTable[lAddr^] ); inc(laddr);
        inc(result, 8 - CNT_BiTStorage_ByteTable[lAddr^] ); inc(laddr);
        inc(result, 8 - CNT_BiTStorage_ByteTable[lAddr^] ); inc(laddr);
        inc(result, 8 - CNT_BiTStorage_ByteTable[lAddr^] );
      end;
  7:  begin
        inc(result, 8 - CNT_BiTStorage_ByteTable[lAddr^] ); inc(laddr);
        inc(result, 8 - CNT_BiTStorage_ByteTable[lAddr^] ); inc(laddr);
        inc(result, 8 - CNT_BiTStorage_ByteTable[lAddr^] ); inc(laddr);
        inc(result, 8 - CNT_BiTStorage_ByteTable[lAddr^] ); inc(laddr);
        inc(result, 8 - CNT_BiTStorage_ByteTable[lAddr^] ); inc(laddr);
        inc(result, 8 - CNT_BiTStorage_ByteTable[lAddr^] ); inc(laddr);
        inc(result, 8 - CNT_BiTStorage_ByteTable[lAddr^] );
      end;
  end;
end;

function TBitStorage.GetSizeOfHeader: integer;
begin
  result := SizeOf(Cardinal);     // Unique ID
  inc(result, SizeOf(integer) );  // Number of bytes
end;

procedure TBitStorage.WriteHeader(const Writer: TWriter);
var
  lTemp: cardinal;
begin
  // Write unique signature
  lTemp := CNT_BITStorage_SIGNATURE;
  Writer.Write(lTemp, SizeOf(lTemp));

  // write size of buffer in bytes
  Writer.WriteInteger(FDataLen);
end;

function TBitStorage.ReadHeader(const Reader: TReader): integer;
var
  lTemp: cardinal;
begin
  lTemp := $00000000;

  // Read unique signature
  Reader.Read(lTemp, SizeOf(lTemp));
  if lTemp <> CNT_BITStorage_SIGNATURE then
    raise EBitStorageHeaderInvalid.CreateFmt(CNT_BiTStorage_InvalidFileHeader,
    [IntToHex(CNT_BITStorage_SIGNATURE,8), IntToHex(lTemp, 8) ]);

  // Read & return number of bytes
  result := Reader.ReadInteger();
end;

function TBitStorage.GetFileSize: integer;
begin
  result := GetSizeOfHeader();
  if FData <> nil then
    inc(result, FDataLen);
end;

function TBitStorage.ToStream: TStream;
begin
  result := TMemoryStream.Create;
  SaveToStream(result);
  result.Position := 0;
end;

procedure TBitStorage.FromStream(const Stream: TStream; const Disposable: boolean = false);
begin
  try
    LoadFromStream(Stream);
  finally
    if Disposable then
      Stream.Free;
  end;
end;

procedure TBitStorage.SaveToStream(const Stream: TStream);
var
  LWriter: TWriter;
begin
  LWriter := TWriter.Create(stream, 1024 * 1024);
  try
    WriteHeader(lWriter);
    if FDataLen > 0 then
      LWriter.Write(FData^, FDataLen);
  finally
    // Flush to stream
    LWriter.FlushBuffer();
    LWriter.Free;
  end;
end;

procedure TBitStorage.LoadFromStream(const stream: TStream);
var
  LReader:  TReader;
  LLen:     integer;
Begin
  if not GetEmpty() then
    Release();

  LReader := TReader.Create(stream, 1024 * 1024);
  try
    lLen := ReadHeader(lReader);
    if LLen > 0 then
    begin
      Allocate( TTyped.BitsOf(LLen) );
      LReader.Read(FData^, LLen);
    end;
  finally
    LReader.Free;
  end;
end;

Function TBitStorage.GetEmpty: boolean;
Begin
  Result := FData = NIL;
end;

Function TBitStorage.GetByte(const Index: integer): byte;
{$IFNDEF USE_ARRAYS}
var
  lAddr: pByte;
{$ENDIF}
begin
  if FData = nil then
    raise EBitStorageEmpty.Create(CNT_BiTStorage_Empty);

  if Index > FDataLen then
    raise EBitStorageInvalidByteIndex.CreateFmt(CNT_BiTStorage_InvalidByteIndex, [Index, FDataLen-1]);

  {$IFDEF USE_ARRAYS}
  result := PByteAccessArray(FData)^[Index]
  {$ELSE}
  lAddr := FData;
  inc(lAddr, Index);
  result := lAddr^;
  {$ENDIF}
end;

procedure TBitStorage.SetByte(const Index: integer; const Value: byte);
{$IFNDEF USE_ARRAYS}
var
  lAddr: pByte;
{$ENDIF}
begin
  if FData = nil then
    raise EBitStorageEmpty.Create(CNT_BiTStorage_Empty);

  if Index > FDataLen then
    raise EBitStorageInvalidByteIndex.CreateFmt(CNT_BiTStorage_InvalidByteIndex, [Index, FDataLen-1]);

  {$IFDEF USE_ARRAYS}
  PByteAccessArray(FData)^[Index] := value;
  {$ELSE}
  lAddr := FData;
  inc(lAddr, index);
  lAddr^ := value;
  {$ENDIF}
end;

procedure TBitStorage.SetBitRange(First, Last: integer; const Bitvalue: boolean);
var
  x: integer;
  LLongs: integer;
  LSingles: integer;
  LCount: integer;

begin
  if FData = nil then
    raise EBitStorageEmpty.Create(CNT_BiTStorage_Empty);

  if First < FBitsMax then
  Begin
    If Last < FBitsMax then
    begin
      // Swap first|last if not in right order
      If First > Last then
        TTyped.Swap(First, Last);

      // get totals, take ZERO into account
      LCount := TTyped.Span(First, Last, true);

      // use refactoring & loop reduction
      LLongs := LCount div 8;

      x := First;

      while LLongs > 0 do
      Begin
        SetBit(x, Bitvalue); inc(x);
        SetBit(x, Bitvalue); inc(x);
        SetBit(x, Bitvalue); inc(x);
        SetBit(x, Bitvalue); inc(x);
        SetBit(x, Bitvalue); inc(x);
        SetBit(x, Bitvalue); inc(x);
        SetBit(x, Bitvalue); inc(x);
        SetBit(x, Bitvalue); inc(x);
        dec(LLongs);
      end;

      (* process singles *)
      LSingles := LCount mod 8;
      while (LSingles > 0) do
      Begin
        SetBit(x, Bitvalue); inc(x);
        dec(LSingles);
      end;

    end else
    begin
      if First = Last then
        SetBit(First, true)
      else;
        raise EBitStorageInvalidBitIndex.CreateFmt(CNT_BiTStorage_InvalidBitIndex, [FBitsMax-1, Last]);
    end;
  end else;
  raise EBitStorageInvalidBitIndex.CreateFmt(CNT_BiTStorage_InvalidBitIndex, [FBitsMax-1, First]);
end;

procedure TBitStorage.SetBits(const Values: TArray<integer>; const Bitvalue: boolean);
var
  llongs: integer;
  lSingles: integer;
  x: integer;
  lCount: integer;
begin
  if FData = nil then
    raise EBitStorageEmpty.Create(CNT_BiTStorage_Empty);

  lCount := length(Values);
  if lCount > 0 then
  begin
    x := low(Values);
    lLongs := lCount div 8;
    while (lLongs > 0) do
    begin
      SetBit(Values[x], Bitvalue); inc(x);
      SetBit(Values[x], Bitvalue); inc(x);
      SetBit(Values[x], Bitvalue); inc(x);
      SetBit(Values[x], Bitvalue); inc(x);
      SetBit(Values[x], Bitvalue); inc(x);
      SetBit(Values[x], Bitvalue); inc(x);
      SetBit(Values[x], Bitvalue); inc(x);
      SetBit(Values[x], Bitvalue); inc(x);
      dec(llongs);
    end;

    lSingles := lCount mod 8;
    while lSingles > 0 do
    begin
      SetBit(Values[x], Bitvalue); inc(x);
      dec(lSingles);
    end;
  end;
end;

function TBitStorage.FindIdleBit(var Value: integer; const FromStart: boolean = false): boolean;
var
  lOffset: integer;
  lBit: integer;
  lAddr: PByte;
  x: integer;
Begin
  result := false;

  if FData = nil then
  begin
    Value := -1;
    exit;
  end;

  // Initialize
  lAddr := FData;
  lOffset := 0;

  // Start from scratch or use ready-byte cursor?
  If FromStart then
    FReadyByte := 0
  else
  begin
    if FReadyByte > FDataLen then
      FReadyByte := 0
    else
    begin
      // If ready-byte at end of buffer, re-wind
      if FReadyByte = FDataLen then
      begin
        if GetByte(FReadyByte) = $FF then
          FReadyByte := 0;
      end;
    end;
  end;

  case (FReadyByte = 0) of
  true:
    begin
      // iterate through the buffer until we find a byte
      // that is not $FF, which means it has a free bit we can use
      While lOffset < FDataLen do
      Begin
        if lAddr^ < $FF then
        begin
          result := true;
          break;
        end;
        inc(lOffset);
        inc(lAddr);
      end;
    end;
  false:
    begin
      // Readybyte knows where to look, so just add the
      // index of the byte where bits are ready to the offset
      inc(lOffset, FReadyByte);
      inc(lAddr, FReadyByte);

      While lOffset < FDataLen do
      Begin
        if lAddr^ < $FF then
        begin
          result := true;
          break;
        end;
        inc(lOffset);
        inc(lAddr);
      end;

    end;
  end;

  // Did we indeed find a byte with a free bit?
  if result then
  begin
    // convert to bit index
    lBit := lOffset * 8;

    // Scan 8 iterations looking for the bit
    for x := 1 to 8 do
    Begin
      if GetBit(lBit) then
      begin
        inc(lBit);
        Continue;
      end;

      // This is the bit-index we found
      Value := lBit;

      if not (Value < FBitsMax) then
        raise EBitStorage.Create(CNT_BiTStorage_InternalError);

      // more than 1 bit available in byte? remember that
      FReadyByte := lOffset;
      break;
    end;

  end;
end;

Function TBitStorage.GetBit(Const Index: integer): boolean;
var
  BitOfs: 0 .. 255;
  {$IFNDEF USE_ARRAYS}
  lAddr: pByte;
  lByteIndex: integer;
  {$ENDIF}
begin
  if FData = nil then
    raise EBitStorageEmpty.Create(CNT_BiTStorage_Empty);

  if not (Index < FBitsMax) then
    raise EBitStorageInvalidBitIndex.CreateFmt(CNT_BiTStorage_InvalidBitIndex, [FBitsMax-1, Index]);

  BitOfs := Index mod 8;
  {$IFDEF USE_ARRAYS}
  result := (PByteAccessArray(FData)^[Index div 8] and (1 shl (BitOfs mod 8))) <> 0;
  {$ELSE}
  lAddr := FData;
  lByteIndex := integer(Index div 8);
  inc(lAddr, lByteIndex);
  result := lAddr^ and (1 shl (BitOfs mod 8)) <> 0;
  {$ENDIF}
end;

procedure TBitStorage.SetBit(Const Index: integer; const Value: boolean);
var
  {$IFNDEF USE_ARRAYS}
  lAddr: pByte;
  {$ENDIF}
  lByte: byte;
  lByteIndex: integer;
  BitOfs: 0 .. 255;
begin
  if FData = nil then
    raise EBitStorageEmpty.Create(CNT_BiTStorage_Empty);

  if Index > FBitsMax then
    raise EBitStorageInvalidBitIndex.CreateFmt(CNT_BiTStorage_InvalidBitIndex, [FBitsMax-1, Index]);

  BitOfs := Index mod 8;
  lByteIndex := Index div 8; //was div 8

  {$IFDEF USE_ARRAYS}
  lByte := PByteAccessArray(FData)^[lByteIndex];
  {$ELSE}
  lAddr := FData;
  inc(lAddr, lByteIndex);
  lByte := lAddr^;
  {$ENDIF}

  case Value of
  true:
    begin
      // set bit if not already set
      If (lByte and (1 shl (BitOfs mod 8))) = 0 then
      Begin
        lByte := (lByte or (1 shl (BitOfs mod 8)));
        {$IFDEF USE_ARRAYS}
        PByteAccessArray(FData)^[lByteIndex] := lByte;
        {$ELSE}
        lAddr^ := lByte;
        {$ENDIF}

        if (FReadyByte > 0) then
        begin
          if lByteIndex = FReadyByte then
          begin
            // No more free bits? Reset ready-byte
            if lByte = $FF then
              FReadyByte := 0
            else
            if TTyped.BitsSetInByte(lByte) > 7 then
              FReadyByte := 0;
          end;
        end;

      end;
    end;
  false:
    begin
      // clear bit if not already clear
      If (lByte and (1 shl (BitOfs mod 8))) <> 0 then
      Begin
        lByte := (lByte and not (1 shl (BitOfs mod 8)));
        {$IFDEF USE_ARRAYS}
        PByteAccessArray(FData)^[lByteIndex] := lByte;
        {$ELSE}
        lAddr^ := lByte;
        {$ENDIF}

        // remember this byte pos, because now we know that
        // there is at least 1 free bit there.
        // note: this stuff can be HEAVILY optimized, to make it
        // favour bytes with more available bits etc etc
        FReadyByte := lByteIndex;
      end;
    end;
  end;
end;

procedure TBitStorage.ReAllocate(NewMaxBits: integer);
var
  lTemp:  pByte;
  lSize: integer;
  lOldSize: integer;
begin
  // Nothing allocated? Just do a clean alloc & exit
  if FData = nil then
  begin
    Allocate(NewMaxBits);
    exit;
  end;

  // keep current size
  lOldSize := FDataLen;

  // Get # of bytes. NOTE: This is affected by the growth scheme (!)
  // Do not expect to get exact bytes, it will round up by factorial
  lSize := TTyped.BytesOf(NewMaxBits);

  try
    lTemp := Allocmem(LSize);
  except
    on e: exception do
    raise EBitStorage.Create('Failed to allocate new buffer error');
  end;

  try
    // zero out new memory
    {$IFDEF USE_RTL_FILL}
    Fillchar(lTemp^, lSize, byte(0));
    {$ELSE}
    TTyped.FillByte(lTemp, lSize, 0);
    {$ENDIF}

    // Copy existing bits over
    if lSize > FDataLen then
      move(FData^, LTemp^, FDataLen)
    else
      move(FData^, lTemp^, lSize);

  finally
    // Release old buffer
    FreeMem(FData);

    // new memory is now our buffer
    FData := lTemp;

    // If we are scaling up, then we want to place the lookup-cursor where
    // the old buffer ended, so that free bits are instantly available
    if lSize > lOldSize then
      FReadyByte := lOldSize-1
    else
      FReadyByte := 0;

    // Keep new buffer + info
    FDataLen := lSize;
    FBitsMax := TTyped.BitsOf(FDataLen);
  end;
end;

procedure TBitStorage.Allocate(MaxBits: integer);
Begin
  (* release buffer if not empty *)
  If FData <> NIL then
    Release();

  If MaxBits > 0 then
  Begin
    (* Allocate new buffer *)
    try
      FReadyByte := 0;
      FDataLen := TTyped.BytesOf(MaxBits);
      FData := AllocMem(FDataLen);
      FBitsMax := FDatalen * 8;

      // zero out new memory
      {$IFDEF USE_RTL_FILL}
      Fillchar(FData^, FDataLen, byte(0))
      {$ELSE}
      TTyped.FillByte(FData, FDataLen, 0);
      {$ENDIF}

    except
      on e: Exception do
      Begin
        FData := NIL;
        FDataLen := 0;
        FBitsMax := 0;
        raise;
      end;
    end;
  end;
end;

procedure TBitStorage.Release;
Begin
  If FData <> NIL then
  Begin
    try
      FreeMem(FData);
    finally
      FReadyByte := 0;
      FData := NIL;
      FDataLen := 0;
      FBitsMax := 0;
    end;
  end;
end;

procedure TBitStorage.Zero;
Begin
  If FData <> NIL then
    {$IFDEF USE_RTL_FILL}
    Fillchar(FData^, FDataLen, byte(0))
    {$ELSE}
    TTyped.FillByte(FData, FDataLen, 0)
    {$ENDIF}
  else
    raise EBitStorageEmpty.Create(CNT_BiTStorage_Empty);
end;


end.

