// ############################################################################
// #
// # Quartex code library
// #        version 1.0.1
// ############################################################################
// #
// # Written by Jon L. Aasenden @ Quartex Components
// #
// # This software is released under Mozilla Public License V2
// #   https://www.mozilla.org/en-US/MPL/2.0/
// #
// ############################################################################

unit quartex.views;

{$IFDEF FPC}
  {$mode DELPHI}{$H+}
{$ENDIF}

{$I 'quartex.inc'}

interface

uses
  Classes, SysUtils, Generics.Collections,
  quartex.storage;

type

  {$IFDEF USE_BITVIEW}
  {$Z1}
  {$MINENUMSIZE 1}
  TBit = 0..1;
  {$ENDIF}

  TView<T> = class(TEnumerable<T>)
  strict protected
    type
    TViewEnumerator = class(TEnumerator<T>)
    private
      FIndex:     integer;
      FParent:    TView<T>;
    protected
      function    DoGetCurrent: T; override;
      function    DoMoveNext: boolean; override;
    public
      constructor Create(Owner: TView<T>); virtual;
    end;

  strict private
    FBuffer:    IStorage;
    FReserved:  integer;
  strict protected
    function    GetCount: integer; virtual;
    function    GetItem(const Index: integer): T; virtual;
    procedure   SetItem(const Index: integer; const Value: T); virtual;
  protected
    function    DoGetEnumerator: TEnumerator<T>;  override;
    {$IFDEF FPC}
    function    GetPtrEnumerator: TEnumerator<PT>; override;
    {$ENDIF}
  public
    property    Reserved: integer read FReserved;
    property    Buffer: IStorage read FBuffer;
    property    Count: integer read GetCount;
    property    Items[const Index: integer]: T read GetItem write SetItem; default;

    procedure   ToArray(var Value: TArray<T>); overload;
    function    ToArray(const Index: integer; ItemCount: integer): TArray<T>; overload;

    function    ToList: TList<T>; overload;
    procedure   ToList(var List: TList<T>); overload;

    procedure   SetItems(const Index: integer; Values: TArray<T>);

    constructor Create(const Buffer: IStorage); overload; virtual;
    constructor Create(const Buffer: IStorage; const Reserve: integer); overload; virtual;
  end;

  // Views for common, intrinsic types
  TViewChar     = class(TView<char>);
  TViewUInt8    = class(TView<byte>);
  TViewUInt16   = class(TView<word>);
  TViewUInt32   = class(TView<cardinal>);
  TViewUInt64   = class(TView<Uint64>);
  TViewInt8     = class(TView<shortint>);
  TViewInt16    = class(TView<smallint>);
  TViewInt32    = class(TView<integer>);
  TViewInt64    = class(TView<int64>);
  TViewBool     = class(TView<bytebool>);
  TViewSingle   = class(TView<single>);
  TViewDouble   = class(TView<double>);
  TViewDateTime = class(TView<TDateTime>);
  TViewTime     = class(TView<TTime>);
  TViewDate     = class(TView<TDate>);


implementation


//#############################################################################
// TView<T>
//#############################################################################

constructor TView<T>.TViewEnumerator.Create(Owner: TView<T>);
begin
  inherited Create;
  FParent := Owner;
  FIndex := -1;
end;

function TView<T>.TViewEnumerator.DoGetCurrent: T;
begin
  { if FIndex < 0 then
    inc(FIndex); }
  result := FParent[FIndex];
end;

function TView<T>.TViewEnumerator.DoMoveNext: boolean;
begin
  result := (FIndex + 1) < FParent.GetCount();
  if result then
    inc(FIndex);
end;

//##########################################################################
// TView<T>
//##########################################################################

constructor TView<T>.Create(const Buffer: IStorage);
begin
  inherited Create();
  FBuffer := Buffer;
  FReserved := 0;
end;

constructor TView<T>.Create(const Buffer: IStorage; const Reserve: integer);
begin
  inherited Create();
  FBuffer := Buffer;
  FReserved := 0;
  if Reserve > 0 then
    FReserved := Reserve
  else
    raise Exception.CreateFmt('Invalid reservation, expected 1 or more bytes not %d', [reserve]);
end;

procedure TView<T>.SetItems(const Index: integer; Values: TArray<T>);
var
  dx, x:  integer;
  lCount: integer;
  lRightMost: integer;
begin
  lCount := GetCount();
  if lCount < 1 then
    raise Exception.Create('View is empty error');

  if Index < 0 then
    raise Exception.CreateFmt('Invalid index, expected 0..%d, not %d', [lCount-1, index]);

  lRightMost := ( index + length(Values) ) -1;
  if lRightMost > lCount-1 then
    raise Exception.CreateFmt('Invalid span, expected 0..%d, not %d', [lCount-1, lRightMost]);

  dx := index;
  for x := low(Values) to high(values) do
  begin
    SetItem(dx, Values[x]);
    inc(dx);
  end;
end;

function TView<T>.GetCount: integer;
var
  lSize:  integer;
begin
  lSize := FBuffer.GetSize();
  dec(lSize, FReserved);
  {$IFDEF USE_BITVIEW}
  if lSize < 1 then
    exit(0);
  case TypeInfo(T) = TypeInfo(TBit)  of
  true:   result := lSize * 8;
  false:  result := lSize div SizeOf(T);
  end;
  {$ELSE}
  if lSize > 0 then
    result := lSize div SizeOf(T)
  else
    result := 0;
  {$ENDIF}
end;

{$IFDEF BE_SILENT}
  {$WARNINGS OFF}
  {$HINTS OFF}
{$ENDIF}
function TView<T>.GetItem(const Index: integer): T;
var
  lOffset: integer;
{$IFDEF USE_BITVIEW}
  lValue: byte;
  BitOfs: 0 .. 255;
{$ENDIF}
begin
  {$IFDEF USE_BITVIEW}
  case TypeInfo(T) = TypeInfo(TBit)  of
  true:
    begin
      lValue := 0;
      lOffset := Index div 8;
      inc(lOffset, FReserved);
      FBuffer.Read(lOffset, SizeOf(lValue), lValue);

      BitOfs := Index mod 8;
      if (lValue and (1 shl (BitOfs mod 8))) <> 0 then
        PByte(@result)^ := TBit(1)
      else
        PByte(@result)^ := TBit(0);
    end;
  false:
    begin
      lOffset := Index * SizeOf(T);
      inc(lOffset, FReserved);
      FBuffer.Read(lOffset, SizeOf(T), result);
    end;
  end;
  {$ELSE}
  lOffset := Index * SizeOf(T);
  inc(lOffset, FReserved);
  FBuffer.Read(lOffset, SizeOf(T), result);
  {$ENDIF}
end;
{$IFDEF BE_SILENT}
  {$WARNINGS ON}
  {$HINTS ON}
{$ENDIF}

procedure TView<T>.SetItem(const Index: integer; const Value: T);
var
  lOffset: integer;
{$IFDEF USE_BITVIEW}
  lByte:  byte;
  BitOfs: 0 .. 255;
  lCurrent: boolean;
{$ENDIF}
begin
  {$IFDEF USE_BITVIEW}
  case TypeInfo(T) = TypeInfo(TBit)  of
  true:
    begin
      lByte := 0;
      lOffset := Index div 8;
      inc(lOffset, FReserved);
      FBuffer.Read(lOffset, SizeOf(lByte), lByte);

      BitOfs := Index mod 8;
      lCurrent := (lByte and (1 shl (BitOfs mod 8))) <> 0;

      case PByte(@Value)^ of
      1:
        begin
          // only set if needed
          if not LCurrent then
          begin
            lByte := lByte or (1 shl (BitOfs mod 8));
            FBuffer.Write(lOffset, SizeOf(lByte), lByte);
          end;
        end;
      0:
        begin
          // only clear if needed
          if lCurrent then
          begin
            lByte := lByte and not (1 shl (BitOfs mod 8));
            FBuffer.Write(lOffset, SizeOf(lByte), lByte);
          end;
        end;
      end;
    end;
  false:
    begin
      lOffset := Index * SizeOf(T);
      inc(lOffset, FReserved);
      FBuffer.Write(lOffset, SizeOf(T), Value);
    end;
  end;
  {$ELSE}
  lOffset := Index * SizeOf(T);
  inc(lOffset, FReserved);
  FBuffer.Write(lOffset, SizeOf(T), Value);
  {$ENDIF}
end;

function TView<T>.ToList: TList<T>;
var
  idx:  integer;
  lCount: integer;
  lFactor: integer;
begin
  result := TList<T>.Create();
  lCount := GetCount();
  if lCount < 1 then
    exit;

  idx := 0;
  lFactor := lCount shr 3;
  while lFactor > 0 do
  begin
    result.add( GetItem(idx) ); inc(idx);
    result.add( GetItem(idx) ); inc(idx);
    result.add( GetItem(idx) ); inc(idx);
    result.add( GetItem(idx) ); inc(idx);
    result.add( GetItem(idx) ); inc(idx);
    result.add( GetItem(idx) ); inc(idx);
    result.add( GetItem(idx) ); inc(idx);
    result.add( GetItem(idx) ); inc(idx);
    dec(lFactor);
  end;

  case lCount mod 8 of
  1:  result.add( GetItem(idx) );
  2:  begin
        result.add( GetItem(idx) ); inc(idx);
        result.add( GetItem(idx) );
      end;
  3:  begin
        result.add( GetItem(idx) ); inc(idx);
        result.add( GetItem(idx) ); inc(idx);
        result.add( GetItem(idx) );
      end;
  4:  begin
        result.add( GetItem(idx) ); inc(idx);
        result.add( GetItem(idx) ); inc(idx);
        result.add( GetItem(idx) ); inc(idx);
        result.add( GetItem(idx) );
      end;
  5:  begin
        result.add( GetItem(idx) ); inc(idx);
        result.add( GetItem(idx) ); inc(idx);
        result.add( GetItem(idx) ); inc(idx);
        result.add( GetItem(idx) ); inc(idx);
        result.add( GetItem(idx) );
      end;
  6:  begin
        result.add( GetItem(idx) ); inc(idx);
        result.add( GetItem(idx) ); inc(idx);
        result.add( GetItem(idx) ); inc(idx);
        result.add( GetItem(idx) ); inc(idx);
        result.add( GetItem(idx) ); inc(idx);
        result.add( GetItem(idx) );
      end;
  7:  begin
        result.add( GetItem(idx) ); inc(idx);
        result.add( GetItem(idx) ); inc(idx);
        result.add( GetItem(idx) ); inc(idx);
        result.add( GetItem(idx) ); inc(idx);
        result.add( GetItem(idx) ); inc(idx);
        result.add( GetItem(idx) ); inc(idx);
        result.add( GetItem(idx) );
      end;
  end;
end;

procedure TView<T>.ToList(var List: TList<T>);
var
  idx:  integer;
  lCount: integer;
  lFactor: integer;
begin
  lCount := GetCount();
  if lCount < 1 then
    exit;

  if List = nil then
    List := TList<T>.Create();

  idx := 0;
  lFactor := lCount shr 3;
  while lFactor > 0 do
  begin
    List.add( GetItem(idx) ); inc(idx);
    List.add( GetItem(idx) ); inc(idx);
    List.add( GetItem(idx) ); inc(idx);
    List.add( GetItem(idx) ); inc(idx);
    List.add( GetItem(idx) ); inc(idx);
    List.add( GetItem(idx) ); inc(idx);
    List.add( GetItem(idx) ); inc(idx);
    List.add( GetItem(idx) ); inc(idx);
    dec(lFactor);
  end;

  case lCount mod 8 of
  1:  List.add( GetItem(idx) );
  2:  begin
        List.add( GetItem(idx) ); inc(idx);
        List.add( GetItem(idx) );
      end;
  3:  begin
        List.add( GetItem(idx) ); inc(idx);
        List.add( GetItem(idx) ); inc(idx);
        List.add( GetItem(idx) );
      end;
  4:  begin
        List.add( GetItem(idx) ); inc(idx);
        List.add( GetItem(idx) ); inc(idx);
        List.add( GetItem(idx) ); inc(idx);
        List.add( GetItem(idx) );
      end;
  5:  begin
        List.add( GetItem(idx) ); inc(idx);
        List.add( GetItem(idx) ); inc(idx);
        List.add( GetItem(idx) ); inc(idx);
        List.add( GetItem(idx) ); inc(idx);
        List.add( GetItem(idx) );
      end;
  6:  begin
        List.add( GetItem(idx) ); inc(idx);
        List.add( GetItem(idx) ); inc(idx);
        List.add( GetItem(idx) ); inc(idx);
        List.add( GetItem(idx) ); inc(idx);
        List.add( GetItem(idx) ); inc(idx);
        List.add( GetItem(idx) );
      end;
  7:  begin
        List.add( GetItem(idx) ); inc(idx);
        List.add( GetItem(idx) ); inc(idx);
        List.add( GetItem(idx) ); inc(idx);
        List.add( GetItem(idx) ); inc(idx);
        List.add( GetItem(idx) ); inc(idx);
        List.add( GetItem(idx) ); inc(idx);
        List.add( GetItem(idx) );
      end;
  end;
end;

procedure TView<T>.ToArray(var Value: TArray<T>);
var
  idx, idy:  integer;
  lOld: integer;
  lCount: integer;
  lFactor: integer;
begin
  lCount := GetCount();
  if lCount < 1 then
    exit;

  lOld := length(Value);
  setlength(Value, lOld + lCount);

  idx := 0;
  idy := lOld;
  lFactor := lCount shr 3;
  while lFactor > 0 do
  begin
    Value[idy] := GetItem(idx); inc(idx); inc(idy);
    Value[idy] := GetItem(idx); inc(idx); inc(idy);
    Value[idy] := GetItem(idx); inc(idx); inc(idy);
    Value[idy] := GetItem(idx); inc(idx); inc(idy);
    Value[idy] := GetItem(idx); inc(idx); inc(idy);
    Value[idy] := GetItem(idx); inc(idx); inc(idy);
    Value[idy] := GetItem(idx); inc(idx); inc(idy);
    Value[idy] := GetItem(idx); inc(idx); inc(idy);
    dec(lFactor);
  end;

  case lCount mod 8 of
  1:  Value[idy] := GetItem(idx);
  2:  begin
        Value[idy] := GetItem(idx); inc(idx); inc(idy);
        Value[idy] := GetItem(idx);
      end;
  3:  begin
        Value[idy] := GetItem(idx); inc(idx); inc(idy);
        Value[idy] := GetItem(idx); inc(idx); inc(idy);
        Value[idy] := GetItem(idx);
      end;
  4:  begin
        Value[idy] := GetItem(idx); inc(idx); inc(idy);
        Value[idy] := GetItem(idx); inc(idx); inc(idy);
        Value[idy] := GetItem(idx); inc(idx); inc(idy);
        Value[idy] := GetItem(idx);
      end;
  5:  begin
        Value[idy] := GetItem(idx); inc(idx); inc(idy);
        Value[idy] := GetItem(idx); inc(idx); inc(idy);
        Value[idy] := GetItem(idx); inc(idx); inc(idy);
        Value[idy] := GetItem(idx); inc(idx); inc(idy);
        Value[idy] := GetItem(idx);
      end;
  6:  begin
        Value[idy] := GetItem(idx); inc(idx); inc(idy);
        Value[idy] := GetItem(idx); inc(idx); inc(idy);
        Value[idy] := GetItem(idx); inc(idx); inc(idy);
        Value[idy] := GetItem(idx); inc(idx); inc(idy);
        Value[idy] := GetItem(idx); inc(idx); inc(idy);
        Value[idy] := GetItem(idx);
      end;
  7:  begin
        Value[idy] := GetItem(idx); inc(idx); inc(idy);
        Value[idy] := GetItem(idx); inc(idx); inc(idy);
        Value[idy] := GetItem(idx); inc(idx); inc(idy);
        Value[idy] := GetItem(idx); inc(idx); inc(idy);
        Value[idy] := GetItem(idx); inc(idx); inc(idy);
        Value[idy] := GetItem(idx); inc(idx); inc(idy);
        Value[idy] := GetItem(idx);
      end;
  end;
end;

function TView<T>.ToArray(const Index: integer; ItemCount: integer):  TArray<T>;
var
  idx, idy:  integer;
  lCount: integer;
  lFactor: integer;
begin
  result := TArray<T>.Create();
  lCount := GetCount();
  if lCount < 1 then
    exit;

  if Index < 0 then
    raise Exception.CreateFmt('Invalid index, expected 0..%d, not %d', [lCount-1, index]);

  if Count < 1 then
    raise Exception.CreateFmt('Invalid count [%d] error', [ItemCount]);

  if (Index + ItemCount-1) > lCount-1 then
    raise Exception.CreateFmt('Invalid span, expected 0..%d, not %d', [lCount-1, Index + ItemCount-1]);

  setlength(result, ItemCount);

  idx := 0;
  idy := Index;
  lFactor := ItemCount shr 3;
  while lFactor > 0 do
  begin
    result[idx] := GetItem(idy); inc(idx); inc(idy);
    result[idx] := GetItem(idy); inc(idx); inc(idy);
    result[idx] := GetItem(idy); inc(idx); inc(idy);
    result[idx] := GetItem(idy); inc(idx); inc(idy);
    result[idx] := GetItem(idy); inc(idx); inc(idy);
    result[idx] := GetItem(idy); inc(idx); inc(idy);
    result[idx] := GetItem(idy); inc(idx); inc(idy);
    result[idx] := GetItem(idy); inc(idx); inc(idy);
    dec(lFactor);
  end;

  case ItemCount mod 8 of
  1:  result[idx] := GetItem(idy);
  2:  begin
        result[idx] := GetItem(idy); inc(idx); inc(idy);
        result[idx] := GetItem(idy);
      end;
  3:  begin
        result[idx] := GetItem(idy); inc(idx); inc(idy);
        result[idx] := GetItem(idy); inc(idx); inc(idy);
        result[idx] := GetItem(idy);
      end;
  4:  begin
        result[idx] := GetItem(idy); inc(idx); inc(idy);
        result[idx] := GetItem(idy); inc(idx); inc(idy);
        result[idx] := GetItem(idy); inc(idx); inc(idy);
        result[idx] := GetItem(idy);
      end;
  5:  begin
        result[idx] := GetItem(idy); inc(idx); inc(idy);
        result[idx] := GetItem(idy); inc(idx); inc(idy);
        result[idx] := GetItem(idy); inc(idx); inc(idy);
        result[idx] := GetItem(idy); inc(idx); inc(idy);
        result[idx] := GetItem(idy);
      end;
  6:  begin
        result[idx] := GetItem(idy); inc(idx); inc(idy);
        result[idx] := GetItem(idy); inc(idx); inc(idy);
        result[idx] := GetItem(idy); inc(idx); inc(idy);
        result[idx] := GetItem(idy); inc(idx); inc(idy);
        result[idx] := GetItem(idy); inc(idx); inc(idy);
        result[idx] := GetItem(idy);
      end;
  7:  begin
        result[idx] := GetItem(idy); inc(idx); inc(idy);
        result[idx] := GetItem(idy); inc(idx); inc(idy);
        result[idx] := GetItem(idy); inc(idx); inc(idy);
        result[idx] := GetItem(idy); inc(idx); inc(idy);
        result[idx] := GetItem(idy); inc(idx); inc(idy);
        result[idx] := GetItem(idy); inc(idx); inc(idy);
        result[idx] := GetItem(idy);
      end;
  end;
end;

function TView<T>.DoGetEnumerator: TEnumerator<T>;
begin
  result := TView<T>.TViewEnumerator.Create(self);
end;

{$IFDEF FPC}
function TView<T>.GetPtrEnumerator: TEnumerator<PT>;
begin
  Result := nil;
end;
{$ENDIF}

end.

