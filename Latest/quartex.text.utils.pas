// ############################################################################
// #
// # Quartex code library
// #
// ############################################################################
// #
// # Written by Jon L. Aasenden @ Quartex Components
// #
// ############################################################################

unit quartex.text.utils;

interface

uses
  System.Sysutils,
  System.Classes;

type

  // Exception types
  EConvertError               = class(Exception);
  EConvertBinaryStringInvalid = class(EConvertError);
  EConvertTextToNumber        = class(EConvertError);


  // Custom datatypes

  TValuePrefixType = (
    vpNone = 0,   // Unknown or no prefix
    vpHexPascal,  // $
    vpHexC,       // 0x
    vpBinPascal,  // %
    vpBinC,       // 0d
    vpString      // "
    );

  TQTXParsedDataTypes =
    (
      dtUnknown = 0,
      dtBoolean,
      dtByte,
      dtChar,
      dtWord,
      dtLong,
      dtInt16,
      dtInt32,
      dtFloat32,
      dtFloat64,
      dtString
    );

  TQTXStringUtils = class
  public
    ///<summary>
    ///ResolveDataType examines the Text parameter and attempts to recognize
    ///the datatype it represents. For example "yes" or "true" would be
    ///recognized as boolean, %10101010 is recognized as binary etc.
    ///See TQTXParsedDataTypes for the datatypes it distinguishes.
    ///The recognized datatype is returned in the "Determined" parameter.
    ///Returns false if no datatype could be recognized.
    ///</summary>
    class function ResolveDataType(Text: string; var Determined: TQTXParsedDataTypes): boolean; overload;

    ///<summary>
    ///This version of ResolveDataType does the exact same as it's previous
    ///overload, but without the datatype as var parameter. If the datatype
    ///could not be determined, dtUnknown is returned.
    ///</summary>
    class function ResolveDataType(Text: string): TQTXParsedDataTypes; overload;

    ///<summary>
    ///Attempts to locate and recognize any type prefixes, such as $ for a
    ///pascal hexadecimal, 0x for a C/C++ hexadecimal number, % for a pascal
    ///bit representation (or "0b" for C/C++ binary representation).
    ///Returns true if successfull.
    ///</summary>
    class function TryTypePrefix(const Text: string; var Prefix: TValuePrefixType): boolean;

    ///<summary>
    ///Attempts to convert a binary string (see supposed prefixes) into a
    ///32-bit signed integer. Returns true is successful.
    ///</summary>
    class function TryBinToInt32(Text: string; var Value: integer): boolean; overload;


    ///<summary>
    ///Attempts to convert a binary string (see supposed prefixes) into a
    ///32-bit unsigned cardinal (longword). Returns true is successful.
    ///</summary>
    class function TryBinToUInt32(Text: string; var Value: cardinal): boolean;

    ///<summary>
    ///Attempts to convert a floating-point string (ex: 12.5 or 39480.1200)
    ///into a typed double value. Returns true if successfull
    ///</summary>
    class function TryStrToFloat(Text: string; var Value: double): boolean;

    ///<summary>
    ///Attempts to convert a floating-point string (ex: 12.5 or 39480.1200)
    ///into a typed double value. Returns value as the result.
    ///</summary>
    class function StrToFloat(Text: string): double;


    ///<summary>
    ///Attempts to convert a string into a signed 32-bit integer.
    ///Returns true is successful.
    ///</summary>
    class function TryStrToInt32(Text: string; var Value: int32): boolean;

    ///<summary>
    ///Attempts to convert a string into a signed 32-bit integer.
    ///This is the same as TryStrToInt32() except it returns the value as
    ///the function result [immediate]
    ///</summary>
    class function StrToInt32(Text: string): int32;

    ///<summary>
    ///Attempts to convert a string into a boolean. Supports both standard
    ///Delphi types ("true" and "false") but also "yes" and "no".
    ///Returns true is successful.
    ///</summary>
    class function TryStrToBool(Text: string; var Value: boolean): boolean;

    ///<summary>
    ///Attempts to convert a string into a boolean. Supports both standard
    ///Delphi types ("true" and "false") but also "yes" and "no".
    ///Returns value as the immediate result
    ///</summary>
    class function StrToBool(Text: string): boolean;

    ///<summary>
    ///Performs a direct conversion of a string based on the assumption
    ///that the content represents a binary number (signed 32bit).
    ///Throws an exception if the conversion fails.
    ///</summary>
    class function BinStrToInt32(Text: string): integer; overload;

    ///<summary>
    ///Performs a direct conversion of a string based on the assumption
    ///that the content represents a binary number (un-signed 32bit).
    ///Throws an exception if the conversion fails.
    ///</summary>
    class function BinStrToUInt32(Text: string): cardinal;

    ///<summary>
    ///Examines the Text content and returns true if the string can be a hexadecimal number.
    ///</summary>
    class function ValidateHexChars(Text: string): boolean;

    ///<summary>
    ///Examines the Text content and returns true if the string can represents a number.
    ///</summary>
    class function ValidateDecChars(Text: string): boolean;

    ///<summary>
    ///Examines the Text content and returns true if the string can represent a binary number.
    ///</summary>
    class function ValidateBinChars(Text: string): boolean;

    ///<summary>
    ///Returns true if the string content is "quoted", as well as the un-quoted
    ///value in the var-param "Value".
    ///<notes>
    ///  Only " quotes are supported at this time
    ///</notes>
    ///</summary>
    class function ExamineQuotedString(Text: string; var Value: string): boolean;

    ///<summary>
    ///This is an alternative to ExamineQuotedString() that does not return
    ///the un-quoted value. See ExamineQuotedString() for more information.
    ///</summary>
    class function GetStringIsQuoted(const Text: string): boolean;

    ///<summary>
    ///Returns the text passed with leading and trailing quote (") chars.
    ///</summary>
    class function QuoteString(const Text: string): string;

    ///<summary>
    ///Returns the text passed, removing any quote marks.
    ///</summary>
    class function UnQuoteString(const Text: string): string;

    ///<summary>
    ///This function checks if the parameter "Value" exists within an array
    ///of strings. This is typically used much like CharInSet().
    ///For example: if TextIn('hello', ['hello', 'world']) then
    ///</summary>
    class function TextIn(Value: string; Alternatives: array of string): boolean;

    class function Right(const Text: string; Count: integer): string;
    class function Left(const Text: string; Count: integer): string;
    class function DeleteRight(const Text: string; Count: integer): string;
    class function DeleteLeft(const Text: string; Count: integer): string;
  end;

implementation

uses
  quartex.util.bits;

//#############################################################################
// TQTXStringUtils
//#############################################################################

class function TQTXStringUtils.TextIn(Value: string; Alternatives: array of string): boolean;
var
  el: string;
begin
  result := false;
  Value := Value.Trim();
  if Value.Length > 0 then
  begin
    for el in Alternatives do
    begin
      result := Value = el;
      if result then
        break;
    end;
  end;
end;

class function TQTXStringUtils.DeleteLeft(const Text: string; Count: integer): string;
var
  lLen: integer;
begin
  lLen := Length(Text);
  if Count >= lLen then
  begin
    result := '';
    exit;
  end;

  dec(lLen, Count);
  result := copy(Text, count+1, lLen);
end;

class function TQTXStringUtils.DeleteRight(const Text: string; Count: integer): string;
var
  lLen: integer;
begin
  lLen := Length(Text);
  if Count >= lLen then
  begin
    result := '';
    exit;
  end;

  dec(lLen, Count);
  result := copy(Text, 1, lLen);
end;

class function TQTXStringUtils.Left(const Text: string; Count: integer): string;
var
  lLength: integer;
begin
  lLength := Length(Text);

  if Count >= lLength then
  begin
    result := '';
    exit;
  end;

  result := Copy(Text, 1, Count);
end;

class function TQTXStringUtils.Right(const Text: string; Count: integer): string;
var
  lOffset: integer;
begin
  lOffset := Length(Text);

  if Count >= lOffset then
  begin
    result := '';
    exit;
  end;

  dec(lOffset, Count-1);

  result := copy(Text, lOffset, Count);
end;

class function TQTXStringUtils.TryTypePrefix(const Text: string; var Prefix: TValuePrefixType): boolean;
begin
  result := false;
  Prefix := vpNone;
  if Text.length > 0 then
  begin
    if Text.StartsWith('$' ) then Prefix := vpHexPascal else
    if Text.StartsWith('0x') then Prefix := vpHexC else
    if Text.StartsWith('%' ) then Prefix := vpBinPascal else
    if Text.StartsWith('0b') then Prefix := vpBinC else
    if Text.StartsWith('"' ) then Prefix := vpString;
    result := Prefix <> vpNone;
  end;
end;

class function TQTXStringUtils.ResolveDataType(Text: string): TQTXParsedDataTypes;
begin
  if not ResolveDataType(Text, result) then
    result := TQTXParsedDataTypes.dtUnknown;
end;

class function TQTXStringUtils.ResolveDataType(Text: string;
  var Determined: TQTXParsedDataTypes): boolean;
var
  DummyBoolean: boolean;
  DummyFloat: double;
  Prefix: TValuePrefixType;
begin
  result := false;
  Determined := dtUnknown;

  // check boolean first
  if TryStrToBool(Text, DummyBoolean) then
  begin
    Determined := TQTXParsedDataTypes.dtBoolean;
    result := true;
    exit;
  end;

  // Check float
  if TryStrToFloat(Text, DummyFloat) then
  begin
    Determined := TQTXParsedDataTypes.dtFloat64;
    result := true;
    exit;
  end;

  // Check non-prefixed int32
  if ValidateDecChars(Text) then
  begin
    Determined := TQTXParsedDataTypes.dtInt32;
    result := true;
    exit;
  end;

  if TryTypePrefix(Text, Prefix) then
  begin
    case Prefix of
      vpString:
        begin
          Determined := TQTXParsedDataTypes.dtString;
        end;
      vpHexPascal:
        begin
          // Remove $
          Text := TQTXStringUtils.Right(Text, Text.length-1);
          case Text.length of
          1:  Determined := TQTXParsedDataTypes.dtByte;  //A
          2:  Determined := TQTXParsedDataTypes.dtByte;  //AA
          4:  Determined := TQTXParsedDataTypes.dtWord;  //AAAA
          8:  Determined := TQTXParsedDataTypes.dtLong;  //AAAAAAAA
          end;
        end;
      vpHexC:
        begin
          // Remove 0x
          Text := TQTXStringUtils.Right(Text, Text.length-2);
          case Text.length of
          1:  Determined := TQTXParsedDataTypes.dtByte;  //A, AA
          2:  Determined := TQTXParsedDataTypes.dtByte;  //A, AA
          4:  Determined := TQTXParsedDataTypes.dtWord;  //AAAA
          8:  Determined := TQTXParsedDataTypes.dtLong;  //AAAAAAAA
          end;
        end;
      vpBinPascal:
        begin
          // Remove %
          Text := TQTXStringUtils.Right(Text, Text.length-1);

          // check that the string contains all binary (zeroes and ones)
          if TQTXStringUtils.ValidateBinChars(Text) then
          begin
            if (Text.length <= 8) then
              Determined := TQTXParsedDataTypes.dtByte
            else
            if ((Text.Length >8) and (Text.Length <=16)) then
              Determined :=  TQTXParsedDataTypes.dtWord
            else
            if ((Text.Length >16) or (Text.Length >=32)) then
              Determined :=  TQTXParsedDataTypes.dtLong;
          end;
        end;
      vpBinC:
        begin
          // Remove 0b
          Text := TQTXStringUtils.Right(Text, Text.length-2);
          if (Text.length <= 8) then
            Determined := TQTXParsedDataTypes.dtByte
          else
          if ((Text.Length >8) and (Text.Length <=16)) then
            Determined :=  TQTXParsedDataTypes.dtWord
          else
          if ((Text.Length >16) and (Text.Length <=32)) then
            Determined :=  TQTXParsedDataTypes.dtLong;
        end;
    end;

    result := Determined <> dtUnknown;
  end;
end;

class function TQTXStringUtils.ExamineQuotedString(Text: string; var Value: string): boolean;
var
  lLength: integer;
begin
  result := false;
  Text := Text.trim();
  lLength := Text.Length;
  if lLength > 0 then
  begin
    if Text.StartsWith('"') then
    begin
      if Text.EndsWith('"') then
      begin
        dec(lLength, 2);
        value := Copy(Text, 2, lLength);
        result := true;
      end;
    end;
  end;
end;

class function TQTXStringUtils.BinStrToUInt32(Text: string): cardinal;
begin
  if not TryBinToUInt32(Text, result) then
    raise EConvertBinaryStringInvalid.CreateFmt('Failed to convert binary string (%s)', [Text]);
end;

class function TQTXStringUtils.BinStrToInt32(Text: string): integer;
var
  lTemp: cardinal absolute result;
begin
  if not TryBinToUInt32(Text, lTemp) then
    raise EConvertBinaryStringInvalid.CreateFmt('Failed to convert binary string (%s)', [Text]);
end;

class function TQTXStringUtils.TryBinToInt32(Text: string; var Value: integer): boolean;
var
  lValue: cardinal absolute Value;
begin
  result := TryBinToUInt32(Text, lValue);
end;

class function TQTXStringUtils.TryBinToUInt32(Text: string; var Value: cardinal): boolean;
var
  x: integer;
  lBitIndex: integer;
begin
  result := false;
  Value := 0;

  // check pascal notation
  if text.StartsWith('%') then
    Text := TQTXStringUtils.DeleteLeft(Text, 1)
  else
  // check C/C++ notation
  if text.StartsWith('0b') then
    Text := TQTXStringUtils.DeleteLeft(Text, 2);

  // Validate that string contains only zeroes and ones.
  if not ValidateBinChars(Text) then
    exit;

  // Setup bit-index
  lBitIndex := 0;

  // process from LSB to MSB
  for x := high(Text) downto low(Text) do
  begin
    if  Text[x] = '1' then
      TBits.BitSet(lBitIndex, Value, true);

    inc(lBitIndex);
    if lBitIndex > 31 then
      break;
  end;

  result := true;
end;

class function TQTXStringUtils.StrToFloat(Text: string): double;
begin
  if not TryStrToFloat(Text, result) then
    raise EConvertTextToNumber.CreateFmt('Failed to convert "%s" to a float', [Text]);
end;

class function TQTXStringUtils.TryStrToFloat(Text: string; var Value: double): boolean;
var
  lTextLen: integer;
  lScan: boolean;
  lOffset: integer;
  character: char;
begin
  result := false;

  Text := text.trim();
  lTextLen := Text.Length;
  if lTextLen >= 1 then
  begin
    lScan := false;
    lOffset := 0;

    for character in Text do
    begin
      inc(loffset);

      // we only allow one "." per floating point string
      if character = '.' then
      begin
        // Dot as first char, but no more data?
        if (loffset = 1) and (lTextLen = 1) then
        begin
          break;
        end;

        // dot as first char, but with more text afterwards
        if (loffset = 1) and (lTextLen > 1) then
        begin
          lscan := true;
          continue;
        end;

        // dot somewhere in the middle, but not the last char!
        if (loffset > 1) and (loffset < lTextLen) then
        begin
          if not lscan then
          begin
            lscan := true;
            continue;
          end else
          break;
        end else

        // its a dot but it doesnt follow the rules
        // so just exit, this doesnt validate
        break;
      end;

      result := CharInSet(character, ['0'..'9']);
      if not result then
        break;
    end;

    if result then
      Value := StrToFloat(Text);
  end;
end;


class function TQTXStringUtils.ValidateHexChars(Text: string): boolean;
var
  x, lLength: integer;
begin
  result := false;
  Text := Text.Trim();
  lLength := Text.Length;
  if lLength > 0 then
  begin
    for x := 1 to lLength do
    begin
      result := CharInSet(Text[x], ['0'..'9', 'a'..'f', 'A'..'F']);
      if not result then
        break;
    end;
  end;
end;

class function TQTXStringUtils.ValidateDecChars(Text: string): boolean;
var
  x: integer;
  lLength: integer;
begin
  result := false;
  Text := Text.Trim();
  lLength := Text.Length;
  if lLength > 0 then
  begin
    for x := 1 to lLength do
    begin
      result := CharInSet(Text[x], ['0'..'9']);
      if not result then
        break;
    end;
  end;
end;

class function TQTXStringUtils.ValidateBinChars(Text: string): boolean;
var
  x: integer;
  lLength: integer;
begin
  result := false;
  Text := Text.Trim();
  lLength := Text.Length;
  if lLength > 0 then
  begin
    for x := 1 to lLength do
    begin
      result := CharInSet(Text[x], ['0', '1']);
      if not result then
        break;
    end;
  end;
end;

class function TQTXStringUtils.StrToInt32(Text: string): int32;
begin
  if not TryStrToInt32(Text, result) then
    raise EConvertTextToNumber.CreateFmt('Failed to convert "%s" to integer', [Text]);
end;

class function TQTXStringUtils.TryStrToInt32(Text: string; var Value: int32): boolean;
var
  TextLen: integer;
  Prefix:  TValuePrefixType;
begin
  result := false;

  Text := Text.trim();
  TextLen := Text.Length;

  if TextLen > 0 then
  begin
    Prefix := TValuePrefixType.vpNone;
    if TryTypePrefix(Text, Prefix) then
    begin
      case Prefix of
      vpHexPascal:
        begin
          dec(TextLen);
          Text := TQTXStringUtils.Right(Text, TextLen);
          result := ValidateHexChars(Text);
          if result then
            Value := StrToInt('$' + Text);
        end;
      vpHexC:
        begin
          dec(TextLen, 2);
          Text := TQTXStringUtils.Right(Text, TextLen);
          result := ValidateHexChars(Text);
          if result then
            Value := StrToInt('$' + Text);
        end;
      vpBinPascal:
        begin
          dec(TextLen);
          Text := TQTXStringUtils.Right(Text, TextLen);
          result := ValidateBinChars(Text);
          if result then
            Value := BinStrToInt32(Text);
        end;
      vpBinC:
        begin
          dec(TextLen, 2);
          Text := TQTXStringUtils.Right(Text, TextLen);
          result := ValidateBinChars(Text);

          if result then
            Value := BinStrToUInt32(Text);
        end;
      vpString:
        begin
          exit;
        end;
      else
        begin
          result := ValidateDecChars(Text);
          if result then
            Value := StrToInt(Text);
        end;
      end;
    end else
    begin
      // No prefixing, validate normal decimal int32
      result := ValidateDecChars(Text);
      if result then
        Value := StrToInt(Text);
    end;
  end;
end;

class function TQTXStringUtils.StrToBool(Text: string): boolean;
begin
  if not TryStrToBool(Text, result) then
    raise EConvertTextToNumber.CreateFmt('Failed to convert "%s" to boolean', [Text]);
end;

class function TQTXStringUtils.TryStrToBool(Text: string; var Value: boolean): boolean;
begin
  result := TextIn(Text.Trim().ToLower(), ['yes', 'true', 'no', 'false']);
end;

class function TQTXStringUtils.GetStringIsQuoted(const Text: string): boolean;
begin
  if Text.Length > 1 then
    result := Text.StartsWith('"') and Text.EndsWith('"')
  else
    result := false;
end;

class function TQTXStringUtils.QuoteString(const Text: string): string;
begin
  if not GetStringIsQuoted(Text) then
    result := '"' + Text + '"'
  else
    result := Text;
end;

class function TQTXStringUtils.UnQuoteString(const Text: string): string;
var
  lLength: integer;
begin
  lLength := Text.Length;
  if lLength > 0 then
  begin
    if Text.StartsWith('"') and Text.EndsWith('"') then
    begin
      dec(lLength, 2);
      result := copy(Text, 2, lLength);
    end else
      result := Text;
  end else
  Setlength(result, 0);
end;

end.
