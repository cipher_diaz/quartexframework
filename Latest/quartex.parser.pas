// ############################################################################
// #
// # Quartex code library
// #        version 1.0.1
// ############################################################################
// #
// # Written by Jon L. Aasenden @ Quartex Components
// #
// ############################################################################

unit quartex.parser;

{$IFDEF FPC}
  {$mode DELPHI}{$H+}
{$ENDIF}

{$I 'quartex.inc'}

{$DEFINE USE_INCLUSIVE}

interface

uses
  SysUtils, Classes, Generics.Collections,
  quartex.storage,
  quartex.views,
  quartex.vectors;


{.$DEFINE USE_OPTIMIZATION}

const
  TOK_ROUNDOPEN   = 1;  //  "("
  TOK_ROUNDCLOSE  = 2;  //  ")"
  TOK_CUROPEN     = 3;  //  "{"
  TOK_CURCLOSE    = 4;  //  "}"
  TOK_SPACE       = 5;  //  " "

  TOK_ADD         = 6;  //  "+"
  TOK_SUBTRACT    = 7;  //  "-"
  TOK_DIVIDE      = 8;  //  "/"
  TOK_POWER       = 9;  //  "^"

  TOK_COMMA       =10;  //  ","
  TOK_COLON       =11;  //  ";"

  Numeric_Operators: Array [0..3] of Integer = (
    TOK_ADD,
    TOK_SUBTRACT,
    TOK_DIVIDE,
    TOK_POWER
    );

type

  // Exception classes
  EParserError = Class(Exception);

  // Forward declarations
  TParserBuffer       = class;
  TParserContext      = class;
  TCustomParser       = class;
  TParserRegistry     = class;
  TParserModelObject  = class;
  TCustomParserClass  = class of TCustomParser;

  TParsePosition = record
    psOffset: integer;
    psColumn: integer;
    psRow:    integer;
    function ToString: string;
  end;

  {$IFDEF FPC}
  TParserValidator = function (Item: char): boolean;
  TParserWordValidator = function (Text: string): boolean;
  {$ELSE}
  TParserValidator = reference to function (Item: char): boolean;
  TParserWordValidator = reference to function (Text: string): boolean;
  {$ENDIF}

  TParserStack = class(TObject)
  strict private
    FStack:   TStack<TParsePosition>;
    FParent:  TParserBuffer;
  strict protected
    function  GetEmpty: boolean;
  public
    property  Buffer: TParserBuffer read FParent;
    property  Empty: boolean read GetEmpty;
    procedure Push; overload;
    procedure Push(const Value: TParsePosition); overload;
    function  Pop: TParsePosition;
    procedure Drop;
    procedure Clear;

    constructor Create(const Buffer: TParserBuffer); virtual;
    destructor  Destroy; override;
  end;

  TParserBuffer = class(TObject)
  strict private
    FData:    string;
    FIndex:   integer;
    FCol:     integer;
    FRow:     integer;
    FStacker: TParserStack;
    procedure CheckCRLF; {$IFDEF USE_OPTIMIZATION}inline;{$ENDIF}
    procedure SetCacheData(Value: string);
  strict protected
    function  GetCurrent: char; {$IFDEF USE_OPTIMIZATION}inline;{$ENDIF}
    function  GetTrail: string; {$IFDEF USE_OPTIMIZATION}inline;{$ENDIF}
    function  GetEmpty: boolean; {$IFDEF USE_OPTIMIZATION}inline;{$ENDIF}
  protected
    function  GetPosition: TParsePosition; {$IFDEF USE_OPTIMIZATION}inline;{$ENDIF}
    procedure SetPosition(const Value: TParsePosition); {$IFDEF USE_OPTIMIZATION}inline;{$ENDIF}
  public
    property  CacheData: string read FData write SetCacheData;
    property  Stack: TParserStack read FStacker;
    property  Position: TParsePosition read GetPosition;
    property  Empty: boolean read GetEmpty;
    property  Col: integer read FCol;
    property  Row: integer read FRow;
    property  Current: char read GetCurrent;

    procedure   ConsumeJunk;
    procedure   ConsumeCRLF;

    function  First: boolean; {$IFDEF USE_OPTIMIZATION}inline;{$ENDIF}
    function  Back: boolean;  {$IFDEF USE_OPTIMIZATION}inline;{$ENDIF}
    function  Next: boolean; {$IFDEF USE_OPTIMIZATION}inline;{$ENDIF}
    function  BOF: boolean; {$IFDEF USE_OPTIMIZATION}inline;{$ENDIF}
    function  EOF: boolean; {$IFDEF USE_OPTIMIZATION}inline;{$ENDIF}
    function  Numeric: boolean; {$IFDEF USE_OPTIMIZATION}inline;{$ENDIF}
    procedure SkipJunk; {$IFDEF USE_OPTIMIZATION}inline;{$ENDIF}

    function  ReadNumber(var Text: string): boolean; {$IFDEF USE_OPTIMIZATION}inline;{$ENDIF}

    function  Read: char; {$IFDEF USE_OPTIMIZATION}inline;{$ENDIF}
    function  ReadTo(const Validate: TParserValidator; var Text: string): boolean; overload;
    function  ReadTo(const Terminators: TSysCharSet; var Text: string): boolean; overload;
    function  ReadTo(Match: string): boolean; overload; {$IFDEF USE_OPTIMIZATION}inline;{$ENDIF}
    function  ReadTo(Match: string; var Inner: string): boolean; overload;   {$IFDEF USE_OPTIMIZATION}inline;{$ENDIF}

    function  ReadQuotedString(var Text: string): boolean; overload; {$IFDEF USE_OPTIMIZATION}inline;{$ENDIF}
    function  ReadQuotedString: string; overload; {$IFDEF USE_OPTIMIZATION}inline;{$ENDIF}

    function  ReadCommaList(var List: TArray<string>): boolean;  {$IFDEF USE_OPTIMIZATION}inline;{$ENDIF}

    function  ReadWord(var Text: string): boolean; {$IFDEF USE_OPTIMIZATION}inline;{$ENDIF}
    function  ReadToEOL(var text: string): boolean; overload; {$IFDEF USE_OPTIMIZATION}inline;{$ENDIF}
    function  ReadToEOL: boolean; overload; {$IFDEF USE_OPTIMIZATION}inline;{$ENDIF}

    function  CrLf: boolean; {$IFDEF USE_OPTIMIZATION}inline;{$ENDIF}
    function  Space: boolean; {$IFDEF USE_OPTIMIZATION}inline;{$ENDIF}
    function  Tab: boolean; {$IFDEF USE_OPTIMIZATION}inline;{$ENDIF}
    function  SemiColon: boolean; {$IFDEF USE_OPTIMIZATION}inline;{$ENDIF}
    function  Colon: boolean; {$IFDEF USE_OPTIMIZATION}inline;{$ENDIF}
    function  ConditionEnter: boolean; {$IFDEF USE_OPTIMIZATION}inline;{$ENDIF}
    function  ConditionLeave: boolean; {$IFDEF USE_OPTIMIZATION}inline;{$ENDIF}
    function  BracketEnter: boolean; {$IFDEF USE_OPTIMIZATION}inline;{$ENDIF}
    function  BracketLeave: boolean; {$IFDEF USE_OPTIMIZATION}inline;{$ENDIF}
    function  Ptr: boolean; {$IFDEF USE_OPTIMIZATION}inline;{$ENDIF}
    function  Punctum: boolean; {$IFDEF USE_OPTIMIZATION}inline;{$ENDIF}
    function  Question: boolean; {$IFDEF USE_OPTIMIZATION}inline;{$ENDIF}
    function  Less: boolean; {$IFDEF USE_OPTIMIZATION}inline;{$ENDIF}
    function  More: boolean; {$IFDEF USE_OPTIMIZATION}inline;{$ENDIF}
    function  Equal: boolean; {$IFDEF USE_OPTIMIZATION}inline;{$ENDIF}
    function  Pipe: boolean; {$IFDEF USE_OPTIMIZATION}inline;{$ENDIF}

    function  Peek: char; overload; {$IFDEF USE_OPTIMIZATION}inline;{$ENDIF}
    function  Peek(const Validate: TParserWordValidator): boolean; overload;
    function  Peek(CharCount: integer; var OutText: string): boolean; overload;

    function  Compare(const Value: string; const CaseSensitive: boolean = false): boolean; {$IFDEF USE_OPTIMIZATION}inline;{$ENDIF}

    property  Trail: string read GetTrail; {$IFDEF USE_OPTIMIZATION}inline;{$ENDIF}

    procedure LoadFromString(Text: string);
    procedure LoadFromStream(const Stream: TStream);
    procedure LoadFromStorage(const Storage: IStorage);

    procedure Clear;
    constructor Create; virtual;
    destructor  Destroy; override;
  End;

  TParserContext = class(TObject)
  strict private
    FBuffer:    TParserBuffer;
    FModel:     TParserModelObject;
  strict protected
    // Override this to provide a custom buffer
    function    CreateBuffer: TParserBuffer; virtual;
  public
    property    Buffer: TParserBuffer read FBuffer;
    property    Model: TParserModelObject read FModel write FModel;
    constructor Create; virtual;
    destructor  Destroy; override;
  end;

  TParserInfo = class(TObject)
  strict private
    FInstance: TCustomParser;
    FSymbolId: string;
  public
    property    Instance: TCustomParser read FInstance write FInstance;
    property    SymbolId: string read FSymbolid write FSymbolid;
    destructor  Destroy; override;
  end;

  TParserRegistry = class(TObject)
  strict private
    FParsers:   TObjectList<TParserInfo>;
  public
    function    GetParserIndexFor(SymbolId: string; var aIndex: integer): boolean;
    function    GetParserInstanceFor(SymbolId: string; var Parser: TCustomParser): boolean;
    procedure   RegisterParser(SymbolId: string; const Parser: TCustomParser);
    procedure   Clear;
    constructor Create; virtual;
    destructor  Destroy; override;
  end;

  TCustomParser = class(TObject)
  strict private
    FContext:   TParserContext;
  public
    property    Context: TParserContext read FContext;
    procedure   Parse; virtual;
    constructor Create(const ParseContext: TParserContext); virtual;
  end;

  TParserModelObject = class(TPersistent)
  strict private
    FParent:    TParserModelObject;
    FChildren:  TObjectList<TParserModelObject>;
  strict protected
    function    GetParent: TParserModelObject; virtual;
    function    ChildGetCount: integer; virtual;
    function    ChildGetItem(const Index: integer): TParserModelObject; virtual;
    function    ChildAdd(const Instance: TParserModelObject): TParserModelObject; virtual;
    function    ChildExport(const Index: integer): TParserModelObject; virtual;
  protected
    property    Parent: TParserModelObject read GetParent;
  public
    procedure   Clear; virtual;
    constructor Create(const AParent: TParserModelObject); virtual;
    destructor  Destroy; override;
  end;

  function ParserDictionary: TParserRegistry;


implementation

resourcestring
  CNT_ERR_BUFFER_EMPTY  = 'Buffer is empty error';
  CNT_ERR_OFFSET_BOF    = 'Offset at BOF error';
  CNT_ERR_OFFSET_EOF    = 'Offset at EOF error';
  CNT_ERR_COMMENT_NOTCLOSED = 'Comment not closed error';
  CNT_ERR_OFFSET_EXPECTED_EOF = 'Expected EOF error';
  CNT_ERR_LENGTH_INVALID = 'Invalid length error';


var
  TOK_SYM: Array[TOK_ROUNDOPEN..TOK_COLON] of string =
  ('(',')','{','}',' ','+','-','/','^',',',';');

var
ParserRegistry_: TParserRegistry;

function ParserDictionary: TParserRegistry;
begin
  if ParserRegistry_ = nil then
    ParserRegistry_ := TParserRegistry.Create;
  result := ParserRegistry_;
end;

//##########################################################################
// TParserModelObject
//##########################################################################

constructor TParserModelObject.Create(const AParent:TParserModelObject);
begin
  inherited Create;
  FParent := AParent;
  FChildren := TObjectList<TParserModelObject>.Create(true);
end;

destructor TParserModelObject.Destroy;
begin
  FChildren.Clear;
  FChildren.Free;
  inherited;
end;

function TParserModelObject.GetParent:TParserModelObject;
begin
  result := FParent;
end;

procedure TParserModelObject.Clear;
var
  LItem: TParserModelObject;
begin
  try
    for LItem in FChildren do
    begin
      if assigned(LItem) then
      begin
        LItem.free;
      end;
    end;
  finally
    FChildren.clear;
  end;
end;

function TParserModelObject.ChildGetCount: integer;
begin
  result := FChildren.count;
end;

function TParserModelObject.ChildExport(const Index: integer): TParserModelObject;
begin
  result := FChildren.Extract(FChildren[index]);
end;

function TParserModelObject.ChildGetItem(const Index: integer):TParserModelObject;
begin
  result := FChildren[index];
end;

function TParserModelObject.ChildAdd(const Instance: TParserModelObject): TParserModelObject;
begin
  if not FChildren.Contains(Instance) then
    FChildren.add(Instance);
  result := Instance;
end;
//##########################################################################
// TParserRegistry
//##########################################################################

destructor TParserInfo.Destroy;
begin
  if assigned(FInstance) then
    FreeAndNIL(FInstance);
  inherited;
end;

//##########################################################################
// TParserRegistry
//##########################################################################

constructor TParserRegistry.Create;
begin
  inherited;
  FParsers := TObjectList<TParserInfo>.Create(true);
end;

destructor TParserRegistry.Destroy;
begin
  FParsers.Free;
  inherited;
end;

procedure TParserRegistry.Clear;
begin
  FParsers.Clear;
end;

function TParserRegistry.GetParserIndexFor(SymbolId: string;
         var aIndex: integer): boolean;
var
  x:  Integer;
begin
  result := false;
  aIndex := -1;
  SymbolId := SymbolId.Trim.ToLower;
  if SymbolId.length>0 then
  begin
    for x:=0 to FParsers.count-1 do
    begin
      result :=SameText(FParsers[x].SymbolId,SymbolId);
      if result then
      begin
        aIndex := x;
        break;
      end;
    end;
  end;
end;

function TParserRegistry.getParserInstanceFor(SymbolId: string;
         var Parser: TCustomParser): boolean;
var
  x:  Integer;
begin
  result := false;
  Parser := NIL;
  SymbolId := SymbolId.Trim().ToLower();
  if SymbolId.length>0 then
  begin
    for x:=0 to FParsers.count-1 do
    begin
      result := sameText(FParsers[x].SymbolId,SymbolId);
      if result then
      begin
        Parser := FParsers[x].Instance;
        break;
      end;
    end;
  end;
end;

procedure TParserRegistry.RegisterParser(SymbolId: string; const Parser: TCustomParser);
var
  LIndex: Integer;
  LInfo:  TParserInfo;
begin
  SymbolId := SymbolId.Trim.ToLower;
  if SymbolId.length>0 then
  begin
    if (Parser <> NIL) then
    begin
      if not GetParserIndexFor(SymbolId, LIndex) then
      begin
        (* register sub parser *)
        LInfo:=TParserInfo.Create;
        LInfo.Instance := Parser;
        LInfo.SymbolId := SymbolId;
        FParsers.Add(LInfo);
      end else
      raise EParserError.createFmt
      ('Parser for keyword [%s] already registered',[SymbolId]);
    end else
    raise EParserError.CreateFmt
    ('Parser class for keyword [%s] was NIL error',[SymbolId]);
  end;
end;

//###########################################################################
// TCustomParser
//###########################################################################

constructor TCustomParser.Create(const ParseContext: TParserContext);
begin
  inherited Create;
  FContext := ParseContext;
end;

procedure TCustomParser.Parse;
begin
  raise EParserError.CreateFmt
  ('No parser implemented for class %s',[classname]);
end;

//###########################################################################
// TParserContext
//###########################################################################

constructor TParserContext.Create;
begin
  inherited Create;
  FBuffer := CreateBuffer();
end;

destructor TParserContext.destroy;
begin
  FBuffer.free;
  inherited;
end;

function TParserContext.CreateBuffer: TParserBuffer;
begin
  result := TParserBuffer.Create;
end;

//###########################################################################
// TParserBuffer
//###########################################################################

constructor TParserBuffer.Create;
begin
  inherited;
  FStacker := TParserStack.Create(self);
end;

destructor TParserBuffer.Destroy;
begin
  FStacker.Free;
  inherited;
end;

procedure TParserBuffer.Clear;
begin
  SetLength(FData, 0);
  FRow:=0;
  FCol:=0;
  FIndex:=0;
  FStacker.Clear;
end;

procedure TParserBuffer.ConsumeJunk;
begin
  if not Empty then
  begin

    if BOF() then
    begin
      if not First() then
        exit;
    end;

    if EOF() then
    begin
      raise EParserError.Create(CNT_ERR_OFFSET_EOF);
      exit;
    end;

    repeat
      case Current of
      ' ':
        begin
        end;
      '"':
        begin
          break;
        end;
      #8, #09:
        begin
        end;
      '/':
        begin
          (* Skip C style remark *)
          if Compare('/*', false) then
          begin
            if ReadTo('*/') then
            begin
              inc(FIndex, 2);
              Continue;
            end else
            raise EParserError.Create(CNT_ERR_COMMENT_NOTCLOSED);
          end else
          begin
            (* Skip Pascal style remark *)
            if Compare('//', false) then
            begin
              if ReadToEOL() then
              begin
                continue;
              end else
              raise EParserError.Create(CNT_ERR_OFFSET_EXPECTED_EOF);
            end;
          end;
        end;
      '(':
        begin
          (* Skip pascal style remark *)
          if Compare('(*', false)
            and not Compare('(*)', false) then
          begin
            if ReadTo('*)') then
            begin
              inc(FIndex, 2);
              continue;
            end else
            raise EParserError.Create(CNT_ERR_COMMENT_NOTCLOSED);
          end else
          break;
        end;
      #13:
        begin
          if FData[FIndex + 1] = #10 then
            inc(FIndex, 2)
          else
            inc(FIndex, 1);
          //if Peek = #10 then
          //  ConsumeCRLF;
          continue;
        end;
      #10:
        begin
          inc(FIndex);
          continue;
        end;
      else
        break;
      end;

      if not Next() then
        break;
    until EOF;

  end else
  raise EParserError.Create(CNT_ERR_BUFFER_EMPTY);
end;

procedure TParserBuffer.ConsumeCRLF;
begin
  if not Empty then
  begin

    if BOF() then
    begin
      if not First() then
        exit;
    end;

    if EOF() then
    begin
      raise EParserError.Create(CNT_ERR_OFFSET_EOF);
      exit;
    end;

    if  (FData[FIndex] = #13) then
    begin
      if FData[FIndex + 1] = #10 then
        inc(FIndex, 2)
      else
        inc(FIndex);

      inc(FRow);
      FCol := 0;
    end;

  end;
end;

function TParserBuffer.GetTrail: string;
begin
  If not Empty then
    result := Copy(FData,FIndex,length(FData)-FIndex)
  else
    setLength(result, 0);
end;

function TParserBuffer.First: boolean;
begin
  result := not Empty;
  If result then
  begin
    FRow := 0;
    FCol := 0;
    FIndex := 1;
  end;
end;

procedure TParserBuffer.SkipJunk;
var
  mTemp:  String;
begin
  if not Empty then
  begin
    if not EOF then
    begin
      repeat
        case Current of
        ' ':
          begin
          end;
        #09:
          begin
          end;
        '/':
          begin
            (* Skip C style remark *)
            if Compare('/*') then
            begin
              if readTo('*/') then
              begin
                Next;
                Next;
                continue;
              end else
              raise EParserError.Create('Comment not closed error');
            end else
            begin
              (* Skip Pascal style remark *)
              if Compare('//') then
              begin
                if ReadToEOL(mTemp) then
                begin
                  //next;
                  continue;
                end else
                raise EParserError.Create('Expected end of line error');
              end;
            end;
          end;
        '(':
          begin
            (* Skip pascal style remark *)
            if compare('(*')
            and not compare('(*)') then
            begin
              if readTo('*)') then
              begin
                Next();
                Next();
                continue;
              end else
              raise EParserError.Create('Comment not closed error');
            end else
            break;
          end;
        #13:
          begin
            if Peek = #10 then
            begin
              CheckCRLF;
              Continue;
            end;
          end;
        else
          break;
        end;

        if not Next() then
        break;
      until EOF;
    end;
  end;
end;

procedure TParserBuffer.SetPosition(const Value: TParsePosition);
begin
  if not Empty then
  begin
    FIndex := Value.psOffset;
    FCol := Value.psColumn;
    FRow := Value.psRow;
  end;
end;

function TParserBuffer.GetPosition: TParsePosition;
begin
  result.psOffset := FIndex;
  result.psColumn := FCol;
  result.psRow := FRow;
end;

procedure TParserBuffer.LoadFromStream(const Stream: TStream);
var
  lTemp: TStringList;
begin
  lTemp := TStringList.Create;
  try
    lTemp.LoadFromStream(Stream, TEncoding.Default);
    LoadFromString(lTemp.Text);
  finally
    lTemp.free;
  end;
end;

procedure TParserBuffer.LoadFromStorage(const Storage: IStorage);
var
  lAdapter: TStreamAdapter;
begin
  lAdapter := TStreamAdapter.Create(Storage);
  try
    LoadFromStream(lAdapter);
  finally
    lAdapter.free;
  end;
end;

procedure TParserBuffer.LoadFromString(Text: string);
begin
  FRow:=0;
  FCol:=0;
  FData  := Text.Trim();
  FIndex := 0;
end;

function TParserBuffer.GetEmpty: boolean;
begin
  result := FData.length < 1;
end;

function TParserBuffer.GetCurrent: char;
begin
  if  ( not Empty )
  and ( (FIndex >= 1 )
  and ( FIndex <= FData.length ) ) then
    result := FData[FIndex]
  else
    result := #0;
end;

function TParserBuffer.CrLf: boolean;
begin
  result := (not Empty)
        and ( (FIndex >= 1) and (FIndex <= FData.length ) )
        and ( (FData[FIndex] = #13)  and (FData[FIndex+1] = #10) );
end;

function TParserBuffer.Space: boolean;
begin
    result := (not Empty)
        and ( (FIndex >= 1) and (FIndex <= FData.length ) )
        and (Current = ' ');
end;

function  TParserBuffer.ConditionEnter: boolean;
begin
  result := (not Empty)
        and ( (FIndex >= 1) and (FIndex <= FData.length ) )
        and ( (FData[FIndex] = '(') );
end;

function  TParserBuffer.Tab: boolean;
begin
  result := (not Empty)
        and ( (FIndex >= 1) and (FIndex <= FData.length ) )
        and ( (FData[FIndex] = #09) );
end;

function TParserBuffer.Colon: boolean;
begin
  result := (not Empty)
        and ( (FIndex >= 1) and (FIndex <= FData.length ) )
        and ( (FData[FIndex] = ':') );
end;

function  TParserBuffer.SemiColon: boolean;
begin
  result := (not Empty)
        and ( (FIndex >= 1) and (FIndex <= FData.length ) )
        and ( (FData[FIndex] = ';') );
end;

function  TParserBuffer.BracketEnter: boolean;
begin
  result := (not Empty)
        and ( (FIndex >= 1) and (FIndex <= FData.length ) )
        and ( (FData[FIndex] = '[') );
end;

function  TParserBuffer.BracketLeave: boolean;
begin
  result := (not Empty)
        and ( (FIndex >= 1) and (FIndex <= FData.length ) )
        and ( (FData[FIndex] = ']') );
end;

function  TParserBuffer.ConditionLeave: boolean;
begin
  result := (not Empty)
        and ( (FIndex >= 1) and (FIndex <= FData.length ) )
        and ( (FData[FIndex] = ')') );
end;

function  TParserBuffer.Ptr: boolean;
begin
  result := (not Empty)
        and ( (FIndex >= 1) and (FIndex <= FData.length ) )
        and ( (FData[FIndex] = '^') );
end;

function  TParserBuffer.Punctum: boolean;
begin
  result := (not Empty)
        and ( (FIndex >= 1) and (FIndex <= FData.length ) )
        and ( (FData[FIndex] = '.') );
end;

function  TParserBuffer.Question: boolean;
begin
  result := (not Empty)
        and ( (FIndex >= 1) and (FIndex <= FData.length ) )
        and ( (FData[FIndex] = '?') );
end;

function  TParserBuffer.Less: boolean;
begin
  result := (not Empty)
        and ( (FIndex >= 1) and (FIndex <= FData.length ) )
        and ( (FData[FIndex] = '<') );
end;

function  TParserBuffer.More: boolean;
begin
  result := (not Empty)
        and ( (FIndex >= 1) and (FIndex <= FData.length ) )
        and ( (FData[FIndex] = '>') );
end;

function  TParserBuffer.Equal: boolean;
begin
  result := (not Empty)
        and ( (FIndex >= 1) and (FIndex <= FData.length ) )
        and ( (FData[FIndex] = '=') );
end;

function TParserBuffer.Pipe: boolean;
begin
  result := (not Empty)
        and ( (FIndex >= 1) and (FIndex <= FData.length ) )
        and ( (FData[FIndex] = '|') );
end;

function TParserBuffer.ReadCommaList(var List: TArray<string>): boolean;
var
  LTemp: string;
  LValue: string;
  LIndex: integer;

  procedure AppendItem;
  begin
    LIndex := length(list);
    SetLength(List, LIndex + 1);
    List[LIndex] := LTemp;
    LTemp := '';
  end;

begin
  setlength(List,0);
  result := false;

  if not Empty then
  begin

    if not EOF then
    begin
      SkipJunk();

      if EOF then
        exit;

      While true do
      begin

        case Current of
        #09:
          begin
            // tab, just skip
          end;
        #0:
          begin
            // Unexpected EOL
            break;
          end;
        ';':
          begin
            //Perfectly sound ending
            result := true;
            break;
          end;
        '"':
          begin
            if ReadQuotedString(LValue) then
            begin
              LIndex := length(list);
              SetLength(List, LIndex+1);
              List[LIndex] := LValue;
              LValue := '';
            end;
          end;
        ',':
          begin
            LTemp := LTemp.Trim();
            if LTemp.Length>0 then
              AppendItem;
          end;
        '}':
          begin
            break;
          end;
        else
          begin
            LTemp := LTemp + Current;
          end;
        end;

        if not Next() then
          break;
      end;

      if LTemp.Length > 0 then
      AppendItem;
    end;
  end;
end;

function TParserBuffer.ReadQuotedString: string;
begin
  setlength(result, 0);
  ReadQuotedString(result);
end;

function TParserBuffer.ReadQuotedString(var Text: string): boolean;
var
  LChar:  char;
begin
  result := false;
  SetLength(Text, 0);

  if not Empty then
  begin
    if not EOF then
    begin
      SkipJunk();

      if EOF then
      exit;

      if Current <> '"' then
      exit else
      begin
        if not Next() then
        exit;
      end;

      while not EOF do
      begin
        LChar := Read();
        if LChar = #0 then
        break;

        if LChar = '"' then
        begin
          result := true;
          break;
        end;

        Text := Text + LChar;
      end;
    end;
  end;
end;

function TParserBuffer.Peek(const Validate: TParserWordValidator): boolean;
var
  LText:  string;
begin
  if assigned(Validate) then
  begin

    if not Empty then
    begin
      result := true;

      While not EOF do
      begin
        SkipJunk();

        if EOF then
        begin
          result := false;
          break;
        end;

        Stack.Push();
        if ReadWord(LText) then
        begin
          if not Validate(LText) then
          begin
            Stack.Pop();
            break;
          end else
          Stack.Drop();
        end else
        begin
          Stack.Pop();
          break;
        end;
      end;
    end else
    result := false;
  end else
  result := false;
end;

function TParserBuffer.ReadWord(var Text: string): boolean;
begin
  result := false;
  Setlength(Text,0);

  if not Empty then
  begin
    if not EOF then
    begin

      repeat
        CheckCRLF();
        if CharInSet( Current, ['A'..'Z','a'..'z','0'..'9','_']) then
          Text := Text + Current
        else
          break;
      until not Next();

      result := Text.Length > 0;
    end;
  end;
end;

function TParserBuffer.Compare(const Value: string; const CaseSensitive: boolean = false): boolean;
var
  //LData:  String;
  //LLen: integer;
  LenToRead: integer;
  ReadData: string;
begin
  result := false;
  if not Empty then
  begin
    if BOF() then
    begin
      if not First() then
        exit;
    end;

    if EOF() then
    begin
      raise EParserError.Create(CNT_ERR_OFFSET_EOF);
      exit;
    end;

    LenToRead := Value.Length;
    if LenToRead > 0 then
    begin
      // Peek will set an error message if it
      // fails, so we dont need to set anything here
      ReadData := '';
      if Peek(LenToRead, ReadData) then
      begin
        case CaseSensitive of
        false: result := ReadData.ToLower() = Value.ToLower();
        true:  result := ReadData = Value;
        end;
      end;
    end else
    raise EParserError.Create(CNT_ERR_LENGTH_INVALID);

  end else
  raise EParserError.Create(CNT_ERR_BUFFER_EMPTY);
end;

function TParserBuffer.Peek(CharCount: integer; var OutText: string): boolean;
begin
  result := false;
  SetLength(OutText, 0);

  if not Empty then
  begin
    if not EOF then
    begin
      Stack.Push();
      try
        while (CharCount > 0) do
        begin
          OutText := OutText + Current;
          if not Next() then
            break;
          dec(CharCount);
        end;
        result := (OutText.Length > 0);
      finally
        Stack.Pop();
      end;
    end;
  end;
end;

function TParserBuffer.ReadToEOL: boolean;
var
  //LLen: integer;
  LStart: integer;
begin
  result := false;

  if not Empty then
  begin
    if BOF() then
    begin
      if not First() then
        exit;
    end;

    if EOF() then
    begin
      raise EParserError.Create(CNT_ERR_OFFSET_EOF);
      exit;
    end;

    // Keep start
    LStart := FIndex;

    // Enum until match of EOF
    {$IFDEF USE_INCLUSIVE}
    repeat
      if (FData[FIndex] = #13)
      and (FData[FIndex + 1] = #10) then
      begin
        result := true;
        break;
      end else
      begin
        inc(FIndex);
        inc(FCol);
      end;
    until EOF();
    {$ELSE}
    While FIndex < FData.Length do
    begin
      if (FData[FIndex] = #13)
      and (FData[FIndex + 1] = #10) then
      begin
        result := true;
        break;
      end else
      begin
        inc(FIndex);
        inc(FCol);
      end;
    end;
    {$ENDIF}

    // Last line in textfile might not have
    // a CR+LF, so we have to check for termination
    if not result then
    begin
      if EOF then
      begin
        if LStart < FIndex then
          result := true;
      end;
    end;

  end else
  raise EParserError.Create(CNT_ERR_BUFFER_EMPTY);
end;

function TParserBuffer.ReadToEOL(var Text: string): boolean;
var
  LLen: integer;
  LStart: integer;
begin
  Text := '';
  result := false;

  if not Empty then
  begin

    if BOF() then
    begin
      if not First() then
        exit;
    end;

    if EOF() then
    begin
      raise EParserError.Create(CNT_ERR_OFFSET_EOF);
      exit;
    end;

    // Keep start
    LStart := FIndex;

    {$IFDEF USE_INCLUSIVE}
    repeat
      if  (FData[FIndex    ] = #13)
      and (FData[FIndex + 1] = #10) then
      begin
        if FIndex > LStart then
        begin
          // Any text to return? Or did we start
          // directly on a CR+LF and have no text to give?
          LLen := FIndex - LStart;
          Text := copy(FData, LStart, LLen);
        end;

        // Either way, we exit because CR+LF has been found
        result := true;
        break;
      end;

      inc(FIndex);
      inc(FCol);
    until EOF();
    {$ELSE}
    While FOffset <= FData.Length do
    begin
      if  (FData[FIndex    ] = #13)
      and (FData[FIndex + 1] = #10) then
      begin
        if FIndex > LStart then
        begin
          // Any text to return? Or did we start
          // directly on a CR+LF and have no text to give?
          LLen := FIndex - LStart;
          Text := copy(FData, LStart, LLen);
        end;

        // Either way, we exit because CR+LF has been found
        result := true;
        break;
      end;

      inc(FIndex);
      inc(FCol);
    end;
    {$ENDIF}

    // Last line in textfile might not have
    // a CR+LF, so we have to check for EOF and treat
    // that as a terminator.
    if not result then
    begin
      if FIndex >= FData.Length then
      begin
        if LStart < FIndex then
        begin
          LLen := FIndex - LStart;
          if LLen > 0 then
          begin
            Text := copy(FData, LStart, LLen);
            result := true;
          end;
          exit;
        end;
      end;
    end;

  end;
end;

function TParserBuffer.Read: char;
begin
  if not Empty then
  begin
    result := Current;
    Next();
  end else
  result := #0;
end;

function TParserBuffer.ReadTo(Match: string; var Inner: string): boolean;
var
  LCache:  String;
begin
  result := false;
  SetLength(Inner, 0);

  if not Empty then
  begin
    Match := Match.ToLower();
    if Match.length>0 then
    begin

      repeat
        if Peek(Match.length,LCache) then
        begin
          LCache := LCache.ToLower();
          result := SameText(LCache, Match);
          if result then
          break else
          Inner := Inner + Current;
        end else
        Inner := Inner + Current;

        if not Next() then
        break;
      until EOF;
    end;
  end;
end;

function TParserBuffer.ReadTo(Match: string): boolean;
var
  LTemp:  String;
begin
  result := false;
  Match := Match.ToLower();
  if not Empty then
  begin
    if Match.length>0 then
    begin
      repeat
        if Peek(Match.length,LTemp) then
        begin
          LTemp := LTemp.ToLower();
          result := SameText(LTemp, Match);
          if result then
          break;
        end;

        if not Next then
        break;
      until EOF;
    end;
  end;
end;

function TParserBuffer.Peek: char;
begin
  if not Empty then
  begin
    if (FIndex < FData.Length) then
      result := FData[FIndex + 1]
    else
      result := #0;
  end else
  result := #0;
end;

function TParserBuffer.Numeric: boolean;
begin
  result := CharInSet(Current,['0'..'9']);
end;

function TParserBuffer.ReadNumber(var Text: string): boolean;
begin
  result := false;
  SetLength(Text,0);

  if not Empty then
  begin
    if not EOF then
    begin

      while Numeric do
      begin
        Text := Text + Current;

        if not Next() then
          break;
      end;

      result := Text.Length > 0;
    end;
  end;
end;

function TParserBuffer.ReadTo(const Validate: TParserValidator; var Text: string): boolean;
begin
  SetLength(Text, 0);
  if not Empty then
  begin
    if assigned(Validate) then
    begin
      while not EOF do
      begin
        if not Validate(Current) then
        break else
        Text := text + Current;

        if not next() then
        break;
      end;

      result := Text.Length > 0;
    end else
    result := false;
  end else
  result := false;
end;

function TParserBuffer.ReadTo(const Terminators: TSysCharSet;
         var Text: string): boolean;
begin
  SetLength(Text, 0);
  if not Empty then
  begin
    while not EOF do
    begin
      if not CharInSet(Current, Terminators) then
        Text := Text + Current
      else
        break;

      if not next() then
        break;
    end;

    result := Text.Length > 0;
  end else
  result := false;
end;

function TParserBuffer.Back: boolean;
begin
  if not Empty then
  begin
    result := (FIndex > low(FData));
    if result then
      dec(FIndex);
  end else
  result := false;
end;

procedure TParserBuffer.SetCacheData(Value: string);
begin
  Clear();
  FData := Value;
  FIndex := 0; // start at BOF
  FCol := 0;
  FRow := 0;
end;

procedure TParserBuffer.CheckCRLF;
begin
  if CharInSet(Current, [#13, #10]) then
  begin
    inc(FRow);
    FCol := 0;
    if (Peek = #10) then
      inc(FIndex);
    inc(FIndex);
  end;
end;

function TParserBuffer.Next: boolean;
begin
  if not Empty then
  begin
    result := (FIndex <= FData.Length);
    if result then
    begin
      inc(FIndex);
      inc(FCol);
      CheckCRLF;
    end;
  end else
  result := false;
end;

function TParserBuffer.BOF: boolean;
begin
  result := FIndex < 1;
end;

function TParserBuffer.EOF: boolean;
begin
  result := FIndex > FData.Length;
end;

//###########################################################################
// TParsePosition
//###########################################################################

function TParsePosition.ToString: string;
begin
  result := Format('Col: %d, Row: %d [Offset: %d]', [psColumn, psRow, psOffset]);
end;

//###########################################################################
// TParserStack
//###########################################################################

constructor TParserStack.Create(const Buffer: TParserBuffer);
begin
  inherited Create;
  FStack := TStack<TParsePosition>.Create;
  FParent := Buffer;
  if FParent = nil then
  raise Exception.Create
  ('Failed to create parser stack, parent cannot be nil error');
end;

destructor TParserStack.Destroy;
begin
  FStack.Free;
  inherited;
end;

procedure TParserStack.Clear;
begin
  FStack.Clear;
end;

procedure TParserStack.Drop;
begin
  FStack.Pop();
end;

function TParserStack.GetEmpty: boolean;
begin
  result := FStack.Count < 1;
end;

function TParserStack.Pop: TParsePosition;
begin
  result := FStack.Pop();
  FParent.SetPosition(result);
end;

procedure TParserStack.Push(const Value: TParsePosition);
begin
  FStack.Push(Value);
end;

procedure TParserStack.Push;
begin
  FStack.Push( FParent.Position );
end;

end.

