// ############################################################################
// #
// # Quartex code library
// #        version 1.0.1
// ############################################################################
// #
// # Written by Jon L. Aasenden @ Quartex Components
// #
// # This software is released under Mozilla Public License V2
// #   https://www.mozilla.org/en-US/MPL/2.0/
// #
// ############################################################################

unit quartex.storage.memory;

{$IFDEF FPC}
  {$mode DELPHI}{$H+}
{$ENDIF}

{$I 'quartex.inc'}

interface

uses
  SysUtils, Classes, Math, quartex.storage;

type

  TStorageMemory = class(TStorage)
  strict private
    FDataPTR:   Pbyte;
    FDataLen:   int64;
  strict protected
    function    BasePTR: Pbyte; inline;
    function    AddrOf(const byteIndex: Int64): Pbyte; {$IFDEF FPC} inline; {$ENDIF}

    function    GetCapabilities: TStorageCapabilities; override;
    function    GetSize: Int64; override;
    procedure   DoReleaseData; override;
    procedure   DoReadData(Start: Int64; var Buffer; BufLen: integer); override;
    procedure   DoWriteData(Start: Int64;const Buffer; BufLen: integer); override;
    procedure   DoFillData(Start: Int64; FillLength: Int64; const Data; DataLen: integer); override;

    procedure   DoGrowDataBy(const Value: integer); override;
    procedure   DoShrinkDataBy(const Value: integer); override;
    procedure   DoZeroData; override;
  public
    property    Data: Pbyte read FDataPTR;
  end;

implementation

//##########################################################################
// TStorageMemory
//##########################################################################

function TStorageMemory.BasePTR: Pbyte;
begin
  result := FDataPTR;
end;

function TStorageMemory.AddrOf(const ByteIndex: Int64): Pbyte;
begin
  // Are we within range of the buffer?
  if (ByteIndex >= 0) and (ByteIndex < FDataLen) then
  begin
    result := FDataPTR;
    inc(result, byteIndex);
  end else
  raise EStorageIndexViolation.CreateFmt(CNT_ERR_BUFFER_BYTEINDEXVIOLATION,  [0, FDataLEN, ByteIndex]);
end;

function TStorageMemory.GetCapabilities: TStorageCapabilities;
begin
  result := [mcScale, mcOwned, mcRead, mcWrite];
end;

function TStorageMemory.GetSize: Int64;
begin
  if FDataPTR <> nil then
    result := FDataLen
  else
    result := 0;
end;

procedure TStorageMemory.DoReleaseData;
begin
  try
    if FDataPTR <> NIL then
      Freemem(FDataPTR);
  finally
    FDataPTR := NIL;
    FDataLEN := 0;
  end;
end;

procedure TStorageMemory.DoReadData(Start: Int64; var Buffer; BufLen: integer);
begin
  move(AddrOf(Start)^, Addr(Buffer)^, BufLen);
end;

procedure TStorageMemory.DoWriteData(Start: Int64; const Buffer; BufLen: integer);
begin
  move(Addr(Buffer)^, AddrOf(start)^, BufLen);
end;

procedure TStorageMemory.DoGrowDataBy(const Value: integer);
var
  lNewSize: int64;
begin
  try
    if FDataPTR <> nil then
    begin
      // Re-scale current memory
      LNewSize := FDataLEN + Value;
      ReAllocMem(FDataPTR, LNewSize);
      FDataLen := LNewSize;
    end else
    begin
      // Allocate new memory
      FDataPTR := AllocMem(Value);
      FDataLen := Value;
    end;
  except
    on e: exception do
    begin
      FDataLen := 0;
      FDataPTR := nil;
      raise EStorageScaleFailed.CreateFmt(CNT_ERR_BUFFER_SCALEFAILED,[e.message]);
    end;
  end;
end;

procedure TStorageMemory.DoShrinkDataBy(const Value: integer);
var
  lNewSize: int64;
begin
  if FDataPTR <> nil then
  begin
    lNewSize := EnsureRange(FDataLEN - Value, 0, FDataLen);
    if lNewSize > 0 then
    begin
      if lNewSize <> FDataLen then
      begin
        try
          ReAllocMem(FDataPTR, lNewSize);
          FDataLen := lNewSize;
        except
          on e: exception do
          begin
            raise EStorageScaleFailed.CreateFmt(CNT_ERR_BUFFER_SCALEFAILED,[e.message]);
          end;
        end;
      end;
    end else
      DoReleaseData;
  end else
  raise EStorageEmpty.Create(CNT_ERR_BUFFER_EMPTY);
end;

procedure TStorageMemory.DoZeroData;
begin
  if FDataPTR <> nil then
    {$IFDEF USE_RTL_FILL}
    fillchar(FDataPTR^, FDataLen, byte(0))
    {$ELSE}
    TTyped.Fillbyte(FDataPTR, FDataLen, $00)
    {$ENDIF}
  else
    raise EStorageEmpty.Create(CNT_ERR_BUFFER_EMPTY);
end;

procedure TStorageMemory.DoFillData(Start: Int64; FillLength: Int64; const Data; DataLen: integer);
var
  lSource:    Pbyte;
  lTarget:    Pbyte;
  lLongs:     integer;
  lSingles:   integer;
begin
  // Initialize pointers
  lSource := Addr(Data);
  lTarget := AddrOf(Start);

  // EVEN copy source into destination
  lLongs := FillLength div DataLen;
  While lLongs > 0 do
  begin
    Move(lSource^, lTarget^, DataLen);
    inc(lTarget, DataLen);
    dec(lLongs);
  end;

  // ODD copy of source into destination
  lSingles := FillLength mod DataLen;
  if lSingles > 0 then
  begin
    case lSingles of
    1: lTarget^ := lSource^;
    2: PWord(lTarget)^ := PWord(lSource)^;
    3: TTyped.PTripleByte(lTarget)^ := TTyped.PTripleByte(lSource)^;
    4: PCardinal(lTarget)^ := PCardinal(lSource)^;
    else
      Move(lSource^, lTarget^, lSingles);
    end;
  end;
end;

end.

