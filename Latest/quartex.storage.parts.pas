// ############################################################################
// #
// # Quartex code library
// #        version 1.0.1
// ############################################################################
// #
// # Written by Jon L. Aasenden @ Quartex Components
// #
// # This software is released under Mozilla Public License V2
// #   https://www.mozilla.org/en-US/MPL/2.0/
// #
// ############################################################################

unit quartex.storage.parts;

{$IFDEF FPC}
  {$mode DELPHI}{$H+}
{$ENDIF}

{$I 'quartex.inc'}

interface

uses
  Classes, SysUtils,
  {$IFNDEF FPC}
  Generics.Collections,
  {$ENDIF}
  quartex.storage,
  quartex.vectors,
  quartex.views;

type

  EPartAccess = class(Exception);

  TPartAccessWriteReserveEvent = procedure (Sender: TObject; Storage: TStorage);
  TPartAccessReadReserveEvent = procedure (Sender: TObject; Storage: TStorage);

  TPartAccess<T> = class(TObject)
  private
    FStorage:   TStorage;
    FView:      TView<T>;
    FReserve:   integer;
    FOnRead:    TPartAccessReadReserveEvent;
    FOnWrite:   TPartAccessWriteReserveEvent;
  public
    property    Storage: TStorage read FStorage;
    property    Pages: TView<T> read FView;
    property    Reserve: integer read FReserve;

    procedure   ReadReserved; overload;
    procedure   WriteReserved; overload;

    function    ReadReserved(var Buffer): integer; overload;
    function    WriteReserved(const Buffer): integer overload;

    constructor Create(const Buffer: TStorage); overload; virtual;
    constructor Create(const Buffer: TStorage; const Reserve: integer); overload; virtual;
    destructor  Destroy; override;
  public
    property    OnReadReserve:  TPartAccessReadReserveEvent read FOnRead write FOnRead;
    property    OnWriteReserve: TPartAccessWriteReserveEvent read FOnWrite write FOnWrite;
  end;

implementation

//#############################################################################
// TPartAccess
//#############################################################################

constructor TPartAccess<T>.Create(const Buffer: TStorage);
begin
  inherited Create;
  FReserve := 0;
  FStorage := buffer;
  FView := TView<T>.Create(FStorage);
end;

constructor TPartAccess<T>.Create(const Buffer: TStorage; const Reserve: integer);
begin
  inherited Create;
  FReserve := Reserve;
  FStorage := buffer;
  FView := TView<T>.Create(FStorage, Reserve);
end;

destructor TPartAccess<T>.Destroy;
begin
  FView.free;
  FStorage := nil;
  inherited;
end;

procedure TPartAccess<T>.WriteReserved;
begin
  if assigned(FOnWrite) then
    FOnWrite(self, FStorage);
end;

procedure TPartAccess<T>.ReadReserved;
begin
  if assigned(FOnRead) then
    FOnRead(self, FStorage);
end;

function TPartAccess<T>.ReadReserved(var Buffer): integer;
begin
  if FReserve > 0 then
    result := FStorage.Read(0, FReserve, Buffer)
  else
    result := 0;
end;

function TPartAccess<T>.WriteReserved(const Buffer): integer;
begin
  if FReserve > 0 then
    result := FStorage.Write(0, FReserve, Buffer)
  else
    result := 0;
end;


end.

