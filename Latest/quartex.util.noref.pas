// ############################################################################
// #
// # Quartex code library
// #        version 1.0.1
// ############################################################################
// #
// # Written by Jon L. Aasenden @ Quartex Components
// #
// # This software is released under Mozilla Public License V2
// #   https://www.mozilla.org/en-US/MPL/2.0/
// #
// ############################################################################

unit quartex.util.noref;

{$IFDEF FPC}
  {$mode DELPHI}{$H+}
{$ENDIF}

{$I 'quartex.inc'}

interface

uses
  SysUtils, Classes;

type
  /// <summary>
  /// TNonReferenceCountedObject is an alternative to TInterfacedObject, but
  /// without reference counting. As such it behaves exactly as TObject,
  /// with the notable exception that it supports interfaces.
  /// Any calls to the standard reference counting mechanisms are simply ignored.
  /// This object must be manually released, just like any class
  /// inheriting from TObject or TPersistent.
  /// </summary>
  TNonReferenceCountedObject = class(TObject, IUnknown)
  protected
    {$IFDEF FPC}
    { implement methods of IUnknown }
    function QueryInterface({$IFDEF FPC_HAS_CONSTREF}constref{$ELSE}const{$ENDIF} iid : tguid;out obj) : longint;{$IFNDEF WINDOWS}cdecl{$ELSE}stdcall{$ENDIF};
    function _AddRef : longint;{$IFNDEF WINDOWS}cdecl{$ELSE}stdcall{$ENDIF};
    function _Release : longint;{$IFNDEF WINDOWS}cdecl{$ELSE}stdcall{$ENDIF};
    {$ELSE}
    function _AddRef: integer; stdcall;
    function _Release: integer; stdcall;
    function QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
    {$ENDIF}
  end;

  /// <summary>
  /// TNonReferenceCountedPersistent is an alternative to TInterfacedPersistent,
  /// but without reference counting. As such it behaves exactly as TPersistent,
  /// with the notable exception that it supports interfaces.
  /// Any calls to the standard reference counting mechanisms are simply ignored.
  /// This object must be manually released, just like any class
  /// inheriting from TPersistent.
  /// </summary>
  TNonReferenceCountedPersistent = class(TPersistent, IUnknown)
  protected
    {$IFDEF FPC}
    { implement methods of IUnknown }
    function QueryInterface({$IFDEF FPC_HAS_CONSTREF}constref{$ELSE}const{$ENDIF} iid : tguid;out obj) : longint;{$IFNDEF WINDOWS}cdecl{$ELSE}stdcall{$ENDIF};
    function _AddRef : longint;{$IFNDEF WINDOWS}cdecl{$ELSE}stdcall{$ENDIF};
    function _Release : longint;{$IFNDEF WINDOWS}cdecl{$ELSE}stdcall{$ENDIF};
    {$ELSE}
    function QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
    function _AddRef: integer; stdcall;
    function _Release: integer; stdcall;
    {$ENDIF}
  end;

implementation

//#############################################################################
// TNonReferenceCountedObject
//#############################################################################

{$IFDEF FPC}
function TNonReferenceCountedObject.QueryInterface({$IFDEF FPC_HAS_CONSTREF}constref{$ELSE}const{$ENDIF} IID: TGUID; out Obj): longint;
{$ELSE}
function TNonReferenceCountedObject.QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
{$ENDIF}
begin
  If GetInterface(IID, Obj) then
    result := S_OK
  else
    result := E_NOINTERFACE;
end;

function TNonReferenceCountedObject._AddRef: longint;
begin
  result := -1;
end;

function TNonReferenceCountedObject._Release: longint;
begin
  result := -1;
end;

//#############################################################################
// TCapiPersistent
//#############################################################################

{$IFDEF FPC}
function TNonReferenceCountedPersistent.QueryInterface({$IFDEF FPC_HAS_CONSTREF}constref{$ELSE}const{$ENDIF} IID: TGUID; out Obj): longint;
{$ELSE}
function TNonReferenceCountedPersistent.QueryInterface(const IID: TGUID; out Obj): HResult; stdcall;
{$ENDIF}
begin
  If GetInterface(IID, Obj) then
    result := S_OK
  else
    result := E_NOINTERFACE;
end;

function TNonReferenceCountedPersistent._AddRef: longint;
begin
  result := -1;
end;

function TNonReferenceCountedPersistent._Release: longint;
begin
  result := -1;
end;

end.

