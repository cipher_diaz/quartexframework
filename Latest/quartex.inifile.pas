unit quartex.inifile;

interface

uses
  System.Sysutils,
  System.Classes,
  System.Inifiles;

type

  TQTXInifile = class(TMemIniFile)
  public
    procedure LoadFromFile(Filename: string);
    procedure SaveToFile(Filename: string);
    procedure LoadFromStream(Stream: TStream);
    procedure SaveToStream(Stream: TStream);
  end;

implementation

//#############################################################################
// TQTXInifile
//#############################################################################

procedure TQTXInifile.LoadFromFile(Filename: string);
var
  lStream: TFileStream;
begin
  lStream := TFileStream.Create(Filename, fmOpenRead or fmShareDenyNone);
  try
    LoadFromStream(lStream);
  finally
    lStream.Free;
  end;
end;

procedure TQTXInifile.SaveToFile(Filename: string);
var
  lStream: TFileStream;
begin
  lStream := TFileStream.Create(Filename, fmCreate or fmShareDenyWrite);
  try
    SaveToStream(lStream);
  finally
    lStream.Free;
  end;
end;

procedure TQTXInifile.LoadFromStream(Stream: TStream);
var
  lData:  TStringList;
begin
  lData := TStringList.Create();
  try
    lData.LoadFromStream(Stream, TEncoding.UTF8);
    SetStrings(lData);
  finally
    lData.Free;
  end;
end;

procedure TQTXInifile.SaveToStream(Stream: TStream);
var
  lData:  TStringList;
begin
  lData := TStringList.Create;
  try
    GetStrings(lData);
    lData.SaveToStream(Stream, TEncoding.UTF8);
  finally
    lData.Free;
  end;
end;

end.
