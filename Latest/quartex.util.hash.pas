// ############################################################################
// #
// # Quartex code library
// #        version 1.0.1
// ############################################################################
// #
// # Written by Jon L. Aasenden @ Quartex Components
// #
// # This software is released under Mozilla Public License V2
// #   https://www.mozilla.org/en-US/MPL/2.0/
// #
// ############################################################################

unit quartex.util.hash;

{$IFDEF FPC}
  {$mode DELPHI}{$H+}
{$ENDIF}

{$I 'quartex.inc'}

interface

uses
  SysUtils, Classes;

type

  THash = class abstract
  public
    type
    {$IFDEF FPC}
    THashString = Unicodestring;
    {$ELSE}
    THashString = string;
    {$ENDIF}
  public
    class function ElfHash(const Data; DataLength: integer): cardinal; overload;
    class function ElfHash(const Text: THashString):  cardinal; overload;

    class function KAndRHash(const Data; DataLength: integer):  cardinal; overload;
    class function KAndRHash(const Text: THashString):  cardinal; overload;

    class function AdlerHash(const Adler: Cardinal; const Data; DataLength: integer):  cardinal; overload;
    class function AdlerHash(const Data; DataLength: integer):  cardinal; overload;
    class function AdlerHash(const Text: THashString):  cardinal; overload;

    class function BorlandHash(const Data; DataLength: integer):  cardinal; overload;
    class function BorlandHash(const Text: THashString):  cardinal; overload;

    class function BobJenkinsHash(const Data; DataLength: integer):  cardinal; overload;
    class function BobJenkinsHash(const Text: THashString):  cardinal; overload;
  end;


implementation

//##########################################################################
// TStorage.THash
//##########################################################################

class function THash.AdlerHash(const Data; DataLength: integer):  cardinal;
var
  LAdler: Cardinal;
begin
  LAdler := $0;
  result := AdlerHash(LAdler, Data, DataLength);
end;

//Note: This version is reentrant, The Adler parameter is
//      typically set to zero on the first call, and then
//      the result for the next calls until the buffer has been hashed
class function THash.AdlerHash(const Adler: Cardinal;
  const Data; DataLength: integer):  cardinal;
var
  s1, s2: cardinal;
  i, n: integer;
  p: Pbyte;
begin
  if DataLength > 0 then
  begin
    s1 := LongRec(Adler).Lo;
    s2 := LongRec(Adler).Hi;
    p := @Data;
    while DataLength>0 do
    begin
      if DataLength < 5552 then
        n := DataLength
      else
        n := 5552;

      for i := 1 to n do
      begin
        inc(s1,p^);
        inc(p);
        inc(s2,s1);
      end;

      s1 := s1 mod 65521;
      s2 := s2 mod 65521;
      dec(DataLength,n);
    end;
    result := word(s1) + cardinal(word(s2)) shl 16;
  end else
  result := Adler;
end;

class function THash.AdlerHash(const Text: THashString):  cardinal;
var
  LBytes: TBytes;
begin
  result := 0;
  if length(Text) > 0 then
  begin
    LBytes := TEncoding.Default.GetBytes(Text);
    result := KAndRHash(LBytes[0], Length(LBytes) );
  end;
end;

class function THash.BobJenkinsHash(const Data; DataLength: integer):  cardinal;
var
  x: integer;
  LHash: cardinal;
  P: PByte;
begin
  LHash := 0;
  if DataLength > 0 then
  begin
    P := @Data;
    for x:=1 to Datalength do
    begin
      LHash := LHash + p^;
      LHash := LHash shl 10;
      LHash := LHash shr 6;
      inc(p);
    end;

    LHash := LHash + (LHash shl 3);
    LHash := LHash + (LHash shr 11);
    LHash := LHash + (LHash shl 15);
  end;
  result := LHash;
end;

class function THash.BobJenkinsHash(const Text: THashString):  cardinal;
var
  LBytes: TBytes;
begin
  result := 0;
  if length(text) > 0 then
  begin
    LBytes := TEncoding.Default.GetBytes(Text);
    result := BorlandHash(LBytes[0], Length(LBytes) );
  end;
end;

class function THash.BorlandHash(const Data; DataLength: integer):  cardinal;
var
  I: integer;
  p: Pbyte;
begin
  result := 0;
  if DataLength > 0 then
  begin
    p := @Data;
    for I := 1 to DataLength do
    begin
      result := ((result shl 2) or (result shr ( SizeOf(result) * 8 - 2))) xor p^;
      inc(p);
    end;
  end;
end;

class function THash.BorlandHash(const Text: THashString):  cardinal;
var
  LBytes: TBytes;
begin
  result := 0;
  if length(Text) > 0 then
  begin
    LBytes := TEncoding.Default.GetBytes(Text);
    result := BorlandHash(LBytes[0], Length(LBytes) );
  end;
end;

class function THash.KAndRHash(const Data; DataLength: integer):  cardinal;
var
  x:  integer;
  LAddr: PByte;
  LCrc: cardinal;
begin
  LCrc := $0;
  if DataLength > 0 then
  begin
    LAddr := @Data;
    for x:=1 to datalength do
    begin
      LCrc := ( (LAddr^ + LCrc) * 31);
    end;
  end;
  result := LCrc;
end;

class function THash.KAndRHash(const Text: THashString):  cardinal;
var
  LBytes: TBytes;
begin
  result := 0;
  if length(Text) > 0 then
  begin
    LBytes := TEncoding.Default.GetBytes(Text);
    result := KAndRHash(LBytes[0], Length(LBytes) );
  end;
end;

class function THash.ElfHash(const Data; DataLength: integer): cardinal;
var
  i:    integer;
  x:    cardinal;
  LSrc: Pbyte;
begin
  result := 0;
  if DataLength > 0 then
  begin
    LSrc := @Data;
    for i := 1 to DataLength do
    begin
      result := (result shl 4) + LSrc^;
      x := result and $F0000000;
      if x <> 0 then
        result := result xor (x shr 24) ;
      result := result and (not x);
      inc(LSrc);
    end;
  end;
end;

class function THash.ElfHash(const Text: THashString): cardinal;
var
  LBytes: TBytes;
begin
  result := 0;
  if length(text) > 0 then
  begin
    LBytes := TEncoding.Default.GetBytes(Text);
    result := ElfHash(LBytes[0], Length(LBytes) );
  end;
end;

end.

