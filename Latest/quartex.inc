
//#############################################################################
// Switches for TStorage
//#############################################################################

{$DEFINE USE_SEEK}            // File buffer use seek() call or direct offset
{$DEFINE USE_LOCAL_CACHE}     // per-proc buffer allocation [heap intensive]
{$DEFINE USE_RAWFILE}         // Use file-api for disk IO, not stream [faster]

//#############################################################################
// Global and common switches
//#############################################################################

// Use fillchar() or TTyped.FillByte()
{$DEFINE USE_RTL_FILL}

// Mute hints and warnings
{$DEFINE BE_SILENT}

//#############################################################################
// Switches for TView
//#############################################################################

// Intercept TBit datatype and allow a
// storage buffer to be viewed as an array of bits
{$DEFINE USE_BITVIEW}

