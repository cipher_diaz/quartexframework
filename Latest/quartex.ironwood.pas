// ############################################################################
// #
// # Quartex code library
// #
// ############################################################################
// #
// # Written by Jon L. Aasenden @ Quartex Components
// #
// ############################################################################

unit quartex.ironwood;

interface

uses
  System.Sysutils,
  System.Classes,
  System.Math,
  quartex.text.utils;

type

  // Forward declarations
  THexRange                 = class;
  THexObfuscation           = class;
  THexNumberModulator       = class;
  THexSerialMatrix          = class;
  THexIronWoodPartition     = class;
  THexCustomSerialNumber    = class;
  THexIronWoodSerialNumber  = class;
  THexIronwoodGenerator     = class;

  // Type definitions
  THexIronwoodPartitions  = array of THexIronwoodPartition;
  THexKeyMatrix           = packed array[0..11] of byte;
  THexModulationTable     = array [0..255] of integer;
  THexObfuscationMatrix   = packed array [0..25] of integer;

  THexSerialGenerateMethod = (
    gmDispersed,
    gmCanonical
  );

  // Event declarations
  THexMintSerialNumberReadyEvent  = procedure(sender: TObject; SerialNumber: string; var Accept: boolean) of Object;
  THexMintBeforeEvent             = procedure (sender: TObject) of Object;
  THexMintAfterEvent              = procedure (sender: TObject) of Object;
  THexGetSerialMatrixEvent        = procedure (sender: TObject; var Matrix: THexKeyMatrix) of object;

  THexRange = class
  strict private
    FLow:       integer;
    FHigh:      integer;
  strict protected
    function    GetValid: boolean;
    function    GetTop: integer;
    function    GetBottom: integer;
    procedure   SetLow(const NewLow: integer);
    procedure   SetHigh(const NewHigh: integer);
  public
    property    Left: integer read FLow write SetLow;
    property    Right: integer read FHigh write SetHigh;
    property    Top: integer read GetTop;
    property    Bottom: integer read GetBottom;
    property    Valid: boolean read GetValid;

    procedure   Define(const FromValue, ToValue: integer);
    procedure   Reset;

    function    Next(Value: integer): integer;
    function    Within(const Value: integer): boolean;
    function    Inside(const Value: integer): boolean;

    constructor Create(FromValue, ToValue: integer); virtual;
  end;

  THexIronwoodPartition = class(TObject)
  strict private
    FFormula:   THexNumberModulator;
    FGates:     array of byte;
    FRange:     THexRange;
    FGateTotal: integer;
    FIndex:     integer;
  strict protected
    function    GetGateCount: integer; inline;
    function    AddGate(Value: integer): integer; inline;
    function    IndexOfGate(Value: integer): integer; inline;

    procedure   SetIndex(const index: integer);
    function    GetGate(const Index: integer): byte;
    procedure   SetGate(const Index: integer; const Value: byte);
  public
    property    Index: integer read FIndex write SetIndex;
    property    Modulator: THexNumberModulator read FFormula write FFormula;
    property    Gates[const Index: integer]: byte read GetGate write SetGate;
    property    GateCount: integer read GetGateCount;
    property    Range: THexRange read FRange;

    function    ToString: string; override;
    function    ToArray: TArray<integer>;

    function    Valid(Data: byte): boolean;
    procedure   Build(RootKeyValue: byte);

    constructor Create(PartitionGateCount: integer);virtual;
    destructor  Destroy;override;
  end;

  THexNumberModulator = class(TObject)
  strict private
    FCache:     THexModulationTable;
    function    GetItem(const index: integer): byte;
    procedure   SetItem(const index: integer; Value: byte);
    function    GetCount: integer;
  strict protected
    function    BuildNumberSeries: THexModulationTable; virtual; abstract;
  public
    property    Data[const index: integer]: byte read GetItem write SetItem; default;
    property    Count: integer read GetCount;

    function    ToString: string; override;
    function    ToNearest(const Value: integer): integer; virtual;

    constructor Create; virtual;
  end;

  THexLucasModulator = class(THexNumberModulator)
  strict protected
    function    BuildNumberSeries: THexModulationTable; override;
    function    DoFindNearest(const Value: integer): integer; virtual;
  public
    function    ToNearest(const Value: integer): integer; override;
  end;

  THexFibonacciModulator = class(THexNumberModulator)
  strict protected
    function    BuildNumberSeries: THexModulationTable; override;
  public
    function    ToNearest(const Value: integer): integer; override;
  end;

  THexLeonardoModulator = class(THexNumberModulator)
  strict protected
    function    BuildNumberSeries: THexModulationTable;override;
  public
    function    ToNearest(const Value: integer): integer; override;
  end;

  THexObfuscationTypes = class abstract
  public
    class function Sepsephos: THexObfuscation;
    class function Hebrew: THexObfuscation;
    class function Latin: THexObfuscation;
    class function Beatus: THexObfuscation;
    class function Amun: THexObfuscation;
  end;

  THexObfuscation = class(TObject)
  strict private
    FMatrix:    THexObfuscationMatrix;
  strict protected
    function    GetMatrix: THexObfuscationMatrix; virtual; abstract;
    function    FindInMatrix(const Value: integer): integer;
  public
    class function Types(): THexObfuscationTypes;
    function    Encode(const Data: uint8): uint8; overload; virtual;
    function    Encode(Text: string): string; overload; virtual;

    function    Decode(const Data: uint8): uint8; overload; virtual;
    function    Decode(Text: string): string; overload; virtual;

    procedure   Clear;
    function    Ready: boolean;
    procedure   Load;
    constructor Create; virtual;
  end;

  THexObfuscationSepsephos = class(THexObfuscation)
  strict protected
    function GetMatrix: THexObfuscationMatrix; override;
  end;

  THexObfuscationHebrew = class(THexObfuscation)
  strict protected
    function GetMatrix: THexObfuscationMatrix; override;
  end;

  THexObfuscationBeatus = class(THexObfuscation)
  strict protected
    function GetMatrix: THexObfuscationMatrix; override;
  end;

  THexObfuscationLatin = class(THexObfuscation)
  strict protected
    function GetMatrix: THexObfuscationMatrix; override;
  end;

  THexObfuscationAmun = class(THexObfuscation)
  strict protected
    function GetMatrix: THexObfuscationMatrix; override;
  end;

  THexCustomSerialNumber = class(TObject)
  public
    function Validate(SerialNumber: string): boolean; virtual; abstract;
  end;

  THexSerialMatrix = class(TObject)
  strict private
    FOnGetMatrix: THexGetSerialMatrixEvent;
  strict protected
    function    GetSerialMatrix(var Value: THexKeyMatrix): boolean;
  public
    property    OnGetMatrix: THexGetSerialMatrixEvent read FOnGetMatrix write FOnGetMatrix;
  end;

  THexIronWoodSerialNumber = class(THexCustomSerialNumber)
  strict private
    FGateCount:   integer;
    FRootData:    THexKeyMatrix;
    FModulator:   THexNumberModulator;
    FObfuscator:  THexObfuscation;
    FPartitions:  THexIronwoodPartitions;
  strict protected
    function      CanUseRootData: boolean;
    procedure     SetGateCount(const Value: integer); virtual;
    procedure     RebuildPartitionTable;
  public
    property      Modulator: THexNumberModulator read FModulator write FModulator;
    property      Obfuscaton: THexObfuscation read FObfuscator write FObfuscator;
    property      Partitions: THexIronwoodPartitions read FPartitions;

    procedure     Build(const Rootkey: THexKeyMatrix);
    function      Validate(SerialNumber: string): boolean; override;
    function      GetSignature: string;

    constructor   Create; virtual;
    destructor    Destroy; override;
  end;

  THexIronwoodGenerator = class(THexIronWoodSerialNumber)
  strict private
    FOnAcceptSerial:  THexMintSerialNumberReadyEvent;
    FOnBeforeMint:    THexMintBeforeEvent;
    FOnAfterMint:     THexMintAfterEvent;
    FMethod:          THexSerialGenerateMethod;
    FCancel:          boolean;
  strict protected
    function      IdxToHex(Value: byte): string; inline;

    function      Deliver(SerialNumber: string): boolean; virtual;
    procedure     DoDispersed(Count: Integer); virtual;
    procedure     DoCanonical(Count: Integer); virtual;
  public
    procedure     Mint(Count: Integer); overload; virtual;
    procedure     Mint(Count: Integer; &Method: THexSerialGenerateMethod);overload;
    procedure     Mint(Count: Integer; &Method: THexSerialGenerateMethod; CB: THexMintSerialNumberReadyEvent);overload;
    procedure     Cancel;

    property      MintMethod: THexSerialGenerateMethod read FMethod write FMethod;
    property      OnBeforeMinting: THexMintBeforeEvent read FOnBeforeMint write FOnBeforeMint;
    property      OnAfterMinting: THexMintAfterEvent read FOnAfterMint write FOnAfterMint;
    property      OnAcceptSerialNumber: THexMintSerialNumberReadyEvent read FOnAcceptSerial write FOnAcceptSerial;
  end;

implementation

//############################################################################
// THexRange
//############################################################################

constructor THexRange.Create(FromValue, ToValue: integer);
begin
  inherited Create;
  Define(FromValue, ToValue);
end;

procedure THexRange.Reset;
begin
  FLow := -1;
  FHigh := -1;
end;

procedure THexRange.Define(const FromValue, ToValue: integer);
begin
  if FromValue < ToValue then
  begin
    FLow := FromValue;
    FHigh := ToValue;
  end else
  if FromValue > ToValue then
  begin
    FLow := ToValue;
    FHigh := FromValue;
  end else
  begin
    FLow := FromValue;
    FHigh := ToValue;
  end;
end;

function THexRange.Inside(const Value: integer): boolean;
begin
  if GetValid() then
    result := (Value>FLow) and (Value<FHigh)
  else
    result := value = FLow;
end;

function THexRange.Next(Value: integer): integer;
begin
  result := (value + 1) mod Top;
end;

function THexRange.Within(const Value: integer): boolean;
begin
  if GetValid then
  begin
    result := (Value>=FLow) and (Value<=FHigh);
  end else
  result := value = FLow;
end;

procedure THexRange.SetLow(const NewLow: integer);
begin
  Define(NewLow, FHigh);
end;

procedure THexRange.SetHigh(const NewHigh: integer);
begin
  Define(FLow,NewHigh);
end;

function THexRange.GetValid: boolean;
begin
  result := (FLow < FHigh) or ( (FLow = FHigh) and (FLow>=0) );
end;

function THexRange.GetTop: integer;
begin
  result := FHigh + 1;
end;

function THexRange.GetBottom: integer;
begin
  result := FLow -1;
end;


//############################################################################
// THexIronWoodSerialNumber
//############################################################################

constructor THexIronWoodSerialNumber.Create;
begin
  inherited create;
  // Out of 256 gates, we allow 64 to be actually used
  FGateCount := 64;
end;

destructor THexIronWoodSerialNumber.Destroy;
var
  x: integer;
begin
  try
    for x := low(FPartitions) to high(FPartitions) do
    begin
      FPartitions[x].free;
      FPartitions[x] := nil;
    end;
  finally
    Setlength(FPartitions, 0);
  end;
  inherited;
end;

function THexIronWoodSerialNumber.CanUseRootData: boolean;
var
  lFilled: byte;
  x, lTop: integer;
begin
  lFilled := 0;
  lTop := length(FRootData);
  dec(LTop, 2);

  for x := low(FRootData) to high(FRootData) do
  begin
    if FRootData[x] <> 0 then
      inc(lFilled);
  end;

  result := lFilled >= lTop;
end;

procedure THexIronWoodSerialNumber.SetGateCount(const Value: integer);
begin
  FGateCount := System.Math.EnsureRange(Value, 16, 128);
end;

procedure THexIronWoodSerialNumber.RebuildPartitionTable;
var
  x: integer;
  lIndex: integer;
  lPartition: THexIronwoodPartition;
begin
  Setlength(FPartitions, 0);

  if CanUseRootData() then
  begin
    for x := 1 to 12 do
    begin
      lPartition := THexIronwoodPartition.Create(FGateCount);
      lPartition.Modulator := Modulator;

      lIndex := length(FPartitions);
      SetLength(FPartitions, lIndex + 1);
      FPartitions[lIndex] := lPartition;

      // Only issue a rebuild if a modulator has been assigned
      if (Modulator <> nil) then
      begin
        // Rebuild gate array
        lPartition.Build( FRootData[x-1] );
      end else
      raise Exception.Create('No modulator assigned, failed to rebuild partition-table error');
    end;
  end else
  raise Exception.Create('Unsuitable root-key, failed to rebuild partition-table error');
end;

procedure THexIronWoodSerialNumber.Build(const Rootkey: THexKeyMatrix);
begin
  FRootData := Rootkey;
  RebuildPartitionTable();
end;

function THexIronWoodSerialNumber.GetSignature: string;
var
  x: integer;
begin
  if length(FPartitions) > 0 then
  begin
    for x := low(FPartitions) to high(FPartitions) do
    begin
      result := result + FPartitions[x].ToString();
      if x < high(FPartitions) then
        result := result + #13;
    end;
  end;
end;

function THexIronWoodSerialNumber.Validate(SerialNumber: string): boolean;
var
  lVector, lLock: integer;
  lBlock: string;
  lRaw: byte;
begin
  result := false;

  SerialNumber := StringReplace(SerialNumber, '-', '', [rfReplaceAll]);
  if length(SerialNumber) = length(FPartitions) * 2 then
  begin
    lVector := 0;
    lLock   := 0;

    while serialnumber.length > 0 do
    begin
      lBlock := '$' + copy(Serialnumber, 1, 2);
      delete(serialnumber, 1, 2);

      //lRaw := System.Math.EnsureRange( StrToInt(LBlock), 0, 255); //HexToInt
      lRaw := System.Math.EnsureRange( TQTXStringUtils.StrToInt32(lBlock), 0, 255 );

      if FPartitions[LVector].Valid(LRaw) then
        inc(LLock);

      inc(LVector);
    end;

    result := LLock = length(FPartitions);
  end;
end;

//############################################################################
// THexIronwoodGenerator
//############################################################################

function THexIronwoodGenerator.Deliver(SerialNumber: string): boolean;
begin
  result := false;
  if assigned(OnAcceptSerialNumber) then
  begin
    OnAcceptSerialNumber(self,Serialnumber, result);
  end;
end;

procedure THexIronwoodGenerator.Mint(Count: Integer; &Method: THexSerialGenerateMethod);
var
  lCache: THexSerialGenerateMethod;
begin
  lCache := FMethod;
  try
    Mint(Count);
  finally
    FMethod := lCache;
  end;
end;

procedure THexIronwoodGenerator.Mint(Count: Integer; &Method: THexSerialGenerateMethod; CB: THexMintSerialNumberReadyEvent);
var
  lCache: THexMintSerialNumberReadyEvent;
begin
  lCache := OnAcceptSerialNumber;
  OnAcceptSerialNumber := CB;
  try
    Mint(Count, &Method);
  finally
    OnAcceptSerialNumber := lCache;
  end;
end;

procedure THexIronwoodGenerator.Cancel;
begin
  FCancel := true;
end;

procedure THexIronwoodGenerator.Mint(Count: Integer);
begin
  if assigned(OnBeforeMinting) then
    OnBeforeMinting(self);

  try
    FCancel := false;

    if MintMethod = gmDispersed then
      DoDispersed(Count)
    else
      DoCanonical(Count);

  finally
    if assigned(OnAfterMinting) then
      OnAfterMinting(self);
  end;
end;

function THexIronwoodGenerator.IdxToHex(Value: byte): string;
begin
  result := IntToHex(Value, 2);
end;

procedure THexIronwoodGenerator.DoDispersed(Count: Integer);
var
  lPartition: THexIronwoodPartition;
  lText: string;
  x, id: integer;
begin
  Randomize();

  while Count > 0 do
  begin
    if FCancel then
      break;

    setlength(lText, 0);
    for x := 0 to length(Partitions)-1 do
    begin
      lPartition := Partitions[x];
      id := random( LPartition.GateCount );

      lText := lText + IdxToHex( LPartition.Gates[id] );

      if (x mod 4 = 3) and (x < length(Partitions)-1) then
        lText := lText + '-';
    end;

    if Deliver( lText.ToUpper() ) then
      dec(count);
  end;
end;

procedure THexIronwoodGenerator.DoCanonical(Count: Integer);
var
  lPartition: THexIronwoodPartition;
  lText: string;
  x, id: integer;
begin
  while Count > 0 do
  begin
    lText := '';

    if FCancel then
      break;

    for x := 0 to length(Partitions)-1 do
    begin
      lPartition := Partitions[x];
      id := random( LPartition.GateCount );

      lText := lText + IdxToHex( LPartition.Gates[id] );

      if (x mod 4 = 3) and (x < length(Partitions)-1) then
        lText := lText + '-';
    end;

    if Deliver(LText.ToUpper()) then
      dec(count);
  end;
end;

//############################################################################
// THexLeonardoModulator
//############################################################################

function THexLeonardoModulator.BuildNumberSeries: THexModulationTable;
var
  x, a, b: integer;
begin
  result[low(result)] := 1;
  result[low(result)+1] := 1;
  for x := low(result) + 2 to high(result) do
  begin
    a:= result[x-2];
    b:= result[x-1];
    result[x] := a + b + 1;
  end;
end;

function THexLeonardoModulator.ToNearest(const Value: integer): integer;
begin
  if (value = 1) then
    result := 1
  else
  if (value = 2) then
    result := 1
  else
    result := ToNearest(value-1) + ToNearest(value-2) + 1;
end;

//############################################################################
// THexFibonacciModulator
//############################################################################

function THexFibonacciModulator.BuildNumberSeries: THexModulationTable;
var
  x, a, b: integer;
begin
  result[low(result)] := 0;
  result[low(result)+1] := 1;
  for x := low(result)+2 to high(result) do
  begin
    a:= result[x-2];
    b:= result[x-1];
    result[x] := a + b;
  end;
end;

function THexFibonacciModulator.ToNearest(const Value: integer): integer;
var
  LForward: integer;
  LBackward: integer;
  LForwardDistance: integer;
  LBackwardsDistance: integer;
begin
  (* Note: the round() function always rounds upwards to the closest
           whole number. Which in a formula can result in the routine
           returning the next number even though the previous number
           is closer.
           To remedy this we do a distance compare between "number" and
           "number-1", so make sure we pick the closest match in distance *)
  LForward := round( System.Math.Power( ( (1 + SQRT(5)) / 2), value) / SQRT(5) );
  LBackward := round( System.Math.Power( ( (1 + SQRT(5)) / 2), value-1) / SQRT(5) );

  if LForward <> LBackward then
  begin
    LForwardDistance := LForward - value;
    LBackwardsDistance := Value - LBackward;

    if (LForwardDistance < LBackwardsDistance) then
    result := LForward else
    result := LBackward;
  end else
  result := LForward;
end;

//############################################################################
// THexLucasModulator
//############################################################################

function THexLucasModulator.DoFindNearest(const Value: integer): integer;
var
  LBestDiff, a, b, c: integer;
  lMatch: integer;
  lDistance: integer;
begin
  lBestDiff := MAXINT;
  lMatch := -1;

  a := 2;
  b := 1;
  repeat
    c := a + b;
    LDistance := c - value;
    if lDistance >= 0 then
    begin
      if LDistance < LBestDiff then
      begin
        LBestDiff := LDistance;
        LMatch := c;
      end;
    end;

    a := b;
    b := c;
  until (c > value) or (LBestDiff = 0);

  if (LMatch > 0) then
    result := LMatch
  else
    result := value;
end;

function THexLucasModulator.ToNearest(const Value: integer): integer;
var
  LForward: integer;
  LBackward: integer;
  LForwardDistance: integer;
  LBackwardsDistance: integer;
begin
  (* Note: Lucas is a bit harder to work with than fibonacci. So instead
     of using a formula we have to actually search a bit.
     It is however important to search both ways, since the distance
     backwards can be closer to the number than forward *)
  LForward := DoFindNearest(Value);
  LBackward := DoFindNearest(value-1);

  if LForward <> LBackward then
  begin
    LForwardDistance := LForward - value;
    LBackwardsDistance := Value - LBackward;

    if (LForwardDistance < LBackwardsDistance) then
    result := LForward else
    result := LBackward;
  end else
  result := LForward;
end;

function THexLucasModulator.BuildNumberSeries: THexModulationTable;
var
  x: integer;
  a,b : integer;
begin
  result[low(result)] := 2;
  result[low(result)+1] := 1;
  for x:=low(result)+2 to high(result) do
  begin
    a:= result[x-2];
    b:= result[x-1];
    result[x] := a + b;
  end;
end;

//############################################################################
// THexNumberModulator
//############################################################################

constructor THexNumberModulator.Create;
begin
  inherited create;
  FCache := BuildNumberSeries();
end;

function  THexNumberModulator.GetItem(const index: integer): byte;
begin
  result := FCache[index];
end;

procedure THexNumberModulator.SetItem(const index: integer; Value: byte);
begin
  FCache[index] := Value;
end;

function THexNumberModulator.GetCount: integer;
begin
  result := Length(FCache);
end;

function THexNumberModulator.ToNearest(const Value: integer): integer;
begin
  if Value > 1 then
    result := ToNearest( Value - 1 ) + ToNearest( Value - 2 )
  else
  if Value = 0 then
    result := 2
  else
    result := 1;
end;

function THexNumberModulator.ToString: string;
var
  x: integer;
begin
  for x := low(FCache) to high(FCache) do
  begin
    if ( x < high(FCache) ) then
      result := result + FCache[x].toString() + ', '
    else
      result := result + FCache[x].toString();
  end;
end;

//############################################################################
// THexObfuscationTypes
//############################################################################

class function THexObfuscationTypes.Sepsephos: THexObfuscation;
begin
  result := THexObfuscationSepsephos.Create;
end;

class function THexObfuscationTypes.Hebrew: THexObfuscation;
begin
  result := THexObfuscationHebrew.Create;
end;

class function THexObfuscationTypes.Latin: THexObfuscation;
begin
  result := THexObfuscationLatin.Create;
end;

class function THexObfuscationTypes.Beatus: THexObfuscation;
begin
  result := THexObfuscationBeatus.Create;
end;

class function THexObfuscationTypes.Amun: THexObfuscation;
begin
  result := THexObfuscationAmun.Create;
end;

//############################################################################
// THexObfuscation
//############################################################################

constructor THexObfuscation.Create;
begin
  inherited Create;
  FMatrix := GetMatrix();
end;

class function THexObfuscation.Types(): THexObfuscationTypes;
begin
  result := THexObfuscationTypes.Create();
end;

function THexObfuscation.Ready: boolean;
var
  item: integer;
  LSum: integer;
begin
  lSum := 0;
  for Item in FMatrix do
  begin
    inc(LSum, Item);
  end;
  result := LSum > 0;
end;

procedure THexObfuscation.Load;
begin
  FMatrix := GetMatrix();
end;

procedure THexObfuscation.Clear;
begin
  fillchar(FMatrix, SizeOf(FMatrix), 0);
  //FMatrix := [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
end;

function THexObfuscation.FindInMatrix(const Value: integer): integer;
var
  x: integer;
begin
  result := -1;
  for x:=low(FMatrix) to high(FMatrix) do
  begin
    if FMatrix[x] = Value then
    begin
      result := x;
      break;
    end;
  end;
end;

function THexObfuscation.Encode(const Data: uint8): uint8;
const
  CNT_CHARSET = '0123456789abcdef';
var
  LText: string;
  LTemp: string;
  xpos: integer;
  lChar: char;
begin
  LText := IntToHex(Data,2);
  for LChar in LText do
  begin
    xpos := pos(LChar, CNT_CHARSET);
    LTemp := LTemp + IntToHex(FMatrix[xpos],1);
  end;

  //result := StrToInt('$' + lTemp);
  result := TQTXStringUtils.StrToInt32('$' + LTemp);
end;

function THexObfuscation.Encode(Text: string): string;
const
  CNT_CHARSET = 'abcdefghijklmnopqrstuvwxyz';
var
  xpos: integer;
  x: integer;
  LValue: integer;
  lChar: char;

  function CharCodeFor(Value: char): integer;
  begin
    result := ord(value);
    //result := byte( PByte(@value)^ );
  end;

begin
  Text := Text.trim().toLower();
  if Text.length > 0 then
  begin
    for x:=low(Text) to high(Text) do
    begin
      LChar := lowercase(Text)[x];
      xpos := pos(LChar, CNT_CHARSET);
      if xpos>0 then
      begin
        dec(xpos);
        LValue := FMatrix[xpos];
        result := result + IntToHex(LValue,4);
      end else
      begin
        result := result + ( 'C932' + IntToHex( CharCodeFor(LChar) ,2) );
      end;
    end;
  end;
end;

function THexObfuscation.Decode(Text: string): string;
begin
  result := '';
end;

function THexObfuscation.Decode(const Data: uint8): uint8;
const
  CNT_CHARSET = '0123456789abcdef';
var
  LText: string;
  LTemp: string;
  xpos: integer;
  lChar: char;
  lValue: integer;
begin
  LText := IntToHex(Data,2);
  for lChar in LText do
  begin
    //lValue := StrToInt('$' + LChar); // HexToInt()
    lValue := TQTXStringUtils.StrToInt32('$' + lChar);
    xpos := FindInMatrix(LValue);

    // If the value is not in the gematria matrix, the value
    // is either not encoded, or encoded using a different gematria matrix
    // Exit stage right ..
    if xpos < 0 then
    begin
      result := Data;
      exit;
    end;

    LTemp := LTemp + CNT_CHARSET[xpos];
  end;
  //result := StrToInt('$' + lTemp);
  result := TQTXStringUtils.StrToInt32('$' + LTemp);
end;

//############################################################################
// THexObfuscationSepsephos
//############################################################################

function THexObfuscationSepsephos.GetMatrix: THexObfuscationMatrix;
begin
  result[0] := 1;
  result[1] := 2;
  result[2] := 3;
  result[3] := 4;
  result[4] := 5;
  result[5] := 6;
  result[6] := 7;
  result[7] := 8;
  result[8] := 10;
  result[9] := 100;
  result[10] := 10;
  result[11] := 20;
  result[12] := 30;
  result[13] := 40;
  result[14] := 50;
  result[15] := 3;
  result[16] := 70;
  result[17] := 80;
  result[18] := 200;
  result[19] := 300;
  result[20] := 400;
  result[21] := 6;
  result[22] := 80;
  result[23] := 60;
  result[24] := 10;
  result[25] := 800;
  //result := ([1,2,3,4,5,6,7,8,10,100,10,20,30,40,50,3,70,80,200,300,400,6,80,60,10, 800]);
end;

//############################################################################
// THexObfuscationHebrew
//############################################################################

function THexObfuscationHebrew.GetMatrix: THexObfuscationMatrix;
begin
  result[0] := 0;
  result[1] := 2;
  result[2] := 100;
  result[3] := 4;
  result[4] := 0;
  result[5] := 80;
  result[6] := 3;
  result[7] := 5;
  result[8] := 10;
  result[9] := 10;
  result[10] := 20;
  result[11] := 30;
  result[12] := 40;
  result[13] := 50;
  result[14] := 0;
  result[15] := 80;
  result[16] := 100;
  result[17] := 200;
  result[18] := 300;
  result[19] := 9;
  result[20] := 6;
  result[21] := 6;
  result[22] := 6;
  result[23] := 60;
  result[24] := 10;
  result[25] := 7;
  //result := [0,2,100,4,0,80,3,5,10,10,20,30,40,50,0,80,100,200,300,9,6,6,6,60,10,7];
end;

//############################################################################
// THexObfuscationBeatus
//############################################################################

function THexObfuscationBeatus.GetMatrix: THexObfuscationMatrix;
begin
  result[0] := 1;
  result[1] := 2;
  result[2] := 90;
  result[3] := 4;
  result[4] := 5;
  result[5] := 6;
  result[6] := 7;
  result[7] := 8;
  result[8] := 10;
  result[9] := 10;
  result[10] := 20;
  result[11] := 30;
  result[12] := 40;
  result[13] := 50;
  result[14] := 70;
  result[15] := 80;
  result[16] := 100;
  result[17] := 200;
  result[18] := 300;
  result[19] := 400;
  result[20] := 6;
  result[21] := 6;
  result[22] := 6;
  result[23] := 60;
  result[24] := 10;
  result[25] := 7;
  //result := [1,2,90,4,5,6,7,8,10,10,20,30,40,50,70,80,100,200,300,400,6,6,6,60,10,7];
end;

//############################################################################
// THexObfuscationLatin
//############################################################################

function THexObfuscationLatin.GetMatrix: THexObfuscationMatrix;
begin
  result[0] := 1;
  result[1] := 2;
  result[2] := 700;
  result[3] := 4;
  result[4] := 5;
  result[5] := 500;
  result[6] := 3;
  result[7] := 8;
  result[8] := 10;
  result[9] := 10;
  result[10] := 20;
  result[11] := 30;
  result[12] := 40;
  result[13] := 50;
  result[14] := 70;
  result[15] := 80;
  result[16] := 600;
  result[17] := 100;
  result[18] := 200;
  result[19] := 300;
  result[20] := 400;
  result[21] := 6;
  result[22] := 800;
  result[23] := 60;
  result[24] := 10;
  result[25] := 7;
  //result := [1,2,700,4,5,500,3,8,10,10,20,30,40,50,70,80,600,100,200,300,400,6,800,60,10,7];
end;

//############################################################################
// THexObfuscationAmun
//############################################################################

function THexObfuscationAmun.GetMatrix: THexObfuscationMatrix;
begin
  result[0] := 1;
  result[1] := 19;
  result[2] := 46;
  result[3] := 21;
  result[4] := 09;
  result[5] := 19;
  result[6] := 73;
  result[7] := 31;
  result[8] := 18;
  result[9] := 60;
  result[10] := 12;
  result[11] := 17;
  result[12] := 37;
  result[13] := 4;
  result[14] := 7;
  result[15] := 8;
  result[16] := 17;
  result[17] := 13;
  result[18] := 244;
  result[19] := 364;
  result[20] := 496;
  result[21] := 512;
  result[22] := 122;
  result[23] := 196;
  result[24] := 291;
  result[25] := 600;
  //result := [1,19,46,21,09,19,73,31,18,60,12,17,37,4,7,8,17,13,244,364,496,512,122,196,291,600];
end;


//############################################################################
// THexSerialMatrix
//############################################################################

function THexSerialMatrix.GetSerialMatrix(var Value: THexKeyMatrix): boolean;
var
  lTemp: THexKeyMatrix;
begin
  fillchar(value, SizeOf(Value), 0);
  result := assigned(OnGetMatrix);
  if result then
  begin
    OnGetMatrix(self, lTemp);
    Value := LTemp;
  end;
end;

//############################################################################
// THexIronwoodPartition
//############################################################################

constructor THexIronwoodPartition.Create(PartitionGateCount: integer);
begin
  inherited create;
  FRange := THexRange.Create(0,255);

  // 64 is the optimal number of gates per partition, but we allow for
  // a minimal of 16 gates to be used - and a maximum of 128.
  // This means that half of the combinations [0..255] are invalid to
  // begin with, so this makes it much harder to figure out the non-linear
  // gate-numbers a partition contains
  FGateTotal := System.Math.EnsureRange(PartitionGateCount, 16, 128); //64;
end;

destructor THexIronwoodPartition.Destroy;
begin
  FRange.free;
  inherited;
end;

function THexIronwoodPartition.GetGateCount: integer;
begin
  result := length(FGates);
end;

function THexIronwoodPartition.ToArray: TArray<integer>;
var
  index: integer;
begin
  result := TArray<integer>.Create();

  SetLength(result, Length(FGates) );
  for index := 0 to length(FGates)-1 do
  begin
    result[index] := FGates[index];
  end;

  { for lItem in FGates do
  begin
    result.add(LItem);
  end; }
end;

procedure THexIronwoodPartition.SetIndex(const index: integer);
begin
  FIndex := System.Math.EnsureRange(Index, 0, 255);
end;

function THexIronwoodPartition.IndexOfGate(Value: integer): integer;
var
  x: integer;
begin
  result := -1;
  if length(FGates) > 0 then
  begin
    for x := low(FGates) to high(FGates) do
    begin
      if FGates[x] = Value then
      begin
        result := x;
        break;
      end;
    end;
  end;
end;

function THexIronwoodPartition.Valid(Data: byte): boolean;
begin
  if Length(FGates) > 0 then
    result := IndexOfGate(Data) >= 0
  else
    result := false;
end;

function THexIronwoodPartition.GetGate(const Index: integer): byte;
begin
  result := FGates[index]
end;

procedure THexIronwoodPartition.SetGate(const Index: integer; const Value: byte);
begin
  FGates[index] := value;
end;

function THexIronwoodPartition.ToString: string;
var
  x: integer;
  lText: string;
begin
  for x := low(FGates) to high(FGates) do
  begin
    ltext := string( FGates[x].ToHexString(2) ).ToLower();
    result := result + lText;
    if x < high(FGates) then
      result := result + '-';
  end;
end;

function THexIronwoodPartition.AddGate(Value: integer): integer;
begin
  result := GetGateCount();
  SetLength(FGates, result + 1 );
  FGates[result] := Value;
end;

procedure THexIronwoodPartition.Build(RootKeyValue: byte);
var
  x, lValue: integer;
begin
  Setlength(FGates, 0);
  if FFormula <> nil then
  begin
    x := 1;

    while ( GetGateCount() < FGateTotal) do
    begin
      LValue :=  (RootKeyValue + FFormula.Data[x] -x) mod FRange.Top;
      if IndexOfGate(lValue) < 0 then
        AddGate(lValue);

      inc(x);
      if x > 255 then
        x := 1;
    end;

  end;
end;


end.

