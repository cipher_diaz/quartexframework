// ############################################################################
// #
// # Quartex code library
// #        version 1.0.1
// ############################################################################
// #
// # Written by Jon L. Aasenden @ Quartex Components
// #
// # This software is released under Mozilla Public License V2
// #   https://www.mozilla.org/en-US/MPL/2.0/
// #
// ############################################################################

unit quartex.storage.disk;


{$IFDEF FPC}
  {$mode DELPHI}{$H+}
{$ENDIF}

{$I 'quartex.inc'}

interface

uses
  Classes, SysUtils, quartex.storage;

type

  TStorageFile = class(TStorage)
  {$IFDEF USE_RAWFILE}
  strict private
    type
    TStorageFileIO = file of byte;
  {$ENDIF}
  strict private
    FFilename:  string;
    FClosed:    boolean;
    {$IFDEF USE_RAWFILE}
    FFile:      TStorageFileIO;
    {$ELSE}
    FFile:      TStream;
    {$ENDIF}
  strict protected
    function    GetEmpty: boolean; override;

    function    GetCapabilities: TStorageCapabilities; override;
    function    GetSize: Int64; override;
    procedure   DoReleaseData; override;
    procedure   DoReadData(Start: Int64; var Buffer; BufLen: integer); override;
    procedure   DoWriteData(Start: Int64;const Buffer; BufLen:integer); override;
    procedure   DoGrowDataBy(const Value:integer); override;
    procedure   DoShrinkDataBy(const Value:integer); override;
  public
    property    Filename: string read FFilename;

    constructor Create(AFilename: string; StreamFlags: Word); reintroduce; virtual;
    destructor  Destroy; override;
  end;

implementation

//##########################################################################
// TStorageFile
//##########################################################################

constructor TStorageFile.Create(AFilename: string; StreamFlags: Word);
begin
  inherited Create();

  AFileName := AFileName.trim();
  if AFileName.Length < 1 then
    AFileName := TTyped.GetTempFileName();

  {$IFDEF USE_RAWFILE}
  AssignFile(FFile, AFileName);
  if StreamFlags = fmCreate then
    ReWrite(FFile)
  else
    Reset(FFile);

  FFileName := AFileName;
  {$ELSE}
  AFileName := AFileName.trim();
  if AFileName.length > 0 then
  begin
    try
      FFile := TFileStream.Create(AFilename, StreamFlags);
    except
      on e: exception do
        raise EStorage.CreateFmt(CNT_ERR_BUFFER_BASE,['Open', e.ClassName, e.message]);
    end;

    FFileName := AFilename;
  end else
  raise EStorage.CreateFmt(CNT_ERR_BUFFER_INVALIDFILENAME, [AFileName]);
  {$ENDIF}
end;

destructor TStorageFile.Destroy;
begin
  // Our ancestor will check if the storage is
  // empty, and if not - call Release().
  // Since we close the file here, we need a way
  // to avoid that call by the ancestor
  FClosed := true;

  {$IFDEF USE_RAWFILE}
  system.Close(FFile);
  {$ELSE}
  FreeAndNil(FFile);
  {$ENDIF}

  if GetDisposable() then
  begin
    if FileExists(FFileName) then
    begin
      try
        DeleteFile(FFileName);
      except
        // sink exception
      end;
    end;
  end;

  inherited;
end;

function TStorageFile.GetCapabilities: TStorageCapabilities;
begin
  result := [mcScale, mcOwned, mcRead, mcWrite];
end;

function TStorageFile.GetEmpty: boolean;
begin
  if FClosed then
    exit(true);
  {$IFDEF USE_RAWFILE}
  result := FileSize(FFile) = 0
  {$ELSE}
  result := (FFile.Size = 0)
  {$ENDIF}
end;

function TStorageFile.GetSize: Int64;
begin
  if FClosed then
    exit(0);
  {$IFDEF USE_RAWFILE}
  result := FileSize(FFile)
  {$ELSE}
  result := FFile.Size
  {$ENDIF}
end;

procedure TStorageFile.DoReleaseData;
begin
  if FClosed then
    exit;
  {$IFDEF USE_RAWFILE}
  Seek(FFile, 0);
  system.Truncate(FFile);
  {$ELSE}
  FFile.Size := 0;
  {$ENDIF}
end;

procedure TStorageFile.DoReadData(Start: Int64; var Buffer; BufLen: integer);
begin
  {$IFDEF USE_RAWFILE}
  Seek(FFile, Start);
  BlockRead(FFile, Buffer, BufLen);
  {$ELSE}
  if FFile.Position <> Start then
    {$IFDEF USE_SEEK}
    FFile.Seek(Start, TSeekOrigin.soBeginning);
    {$ELSE}
    FFile.Position := Start;
    {$ENDIF}
  FFile.ReadBuffer(Buffer, BufLen);
{$ENDIF}
end;

procedure TStorageFile.DoWriteData(Start: Int64; const Buffer; BufLen: integer);
begin
  {$IFDEF USE_RAWFILE}
    Seek(FFile, Start);
    BlockWrite(FFile, Buffer, BufLen);
  {$ELSE}
    if FFile.Position <> Start then
      {$IFDEF USE_SEEK}
      FFile.Seek(Start, TSeekOrigin.soBeginning);
      {$ELSE}
      FFile.Position := Start;
      {$ENDIF}
    FFile.WriteBuffer(Buffer, BufLen);
  {$ENDIF}
end;

{$IFDEF BE_SILENT}
  {$WARNINGS OFF}
  {$HINTS OFF}
{$ENDIF}
procedure TStorageFile.DoGrowDataBy(const Value: integer);
{$IFDEF USE_RAWFILE}
var
  {$IFDEF USE_LOCAL_CACHE}
  lCache: array [1..10240] of byte;
  {$ENDIF}
  lToGo:  integer;
  lToWrite: integer;
{$ENDIF}
begin
  {$IFDEF USE_RAWFILE}
  {$IFDEF USE_LOCAL_CACHE}
  Fillchar(lCache, SizeOf(lCache), byte(0) );
  {$ENDIF}
  lToGo := Value;
  while lToGo > 0 do
  begin
    {$IFDEF USE_LOCAL_CACHE}
    lToWrite := SizeOf(lCache);
    {$ELSE}
    lToWrite := SizeOf(FCache);
    {$ENDIF}
    if lToWrite > lToGo then
      lToWrite := lToGo;

    if lToWrite < 1 then
      break;
    Seek(FFile, FileSize(FFile) );
    {$IFDEF USE_LOCAL_CACHE}
    BlockWrite(FFile, lCache, lToWrite);
    {$ELSE}
    BlockWrite(FFile, FCache, lToWrite);
    {$ENDIF}
    lToGo := lToGo - lToWrite;
  end;
  {$ELSE}
  FFile.Size := FFile.Size + Value
  {$ENDIF}
end;
{$IFDEF BE_SILENT}
  {$WARNINGS ON}
  {$HINTS ON}
{$ENDIF}

procedure TStorageFile.DoShrinkDataBy(const Value: integer);
var
  lNewSize: Int64;
begin
  {$IFDEF USE_RAWFILE}
    lNewSize := FileSize(FFile) - Value;
    if lNewSize <= 0 then
      DoReleaseData()
    else
    begin
      Seek(FFile, lNewSize);
      truncate(FFile);
    end;
  {$ELSE}
    lNewSize := FFile.Size - Value;
    if lNewSize > 0 then
      FFile.Size := lNewSize
    else
      DoReleaseData();
  {$ENDIF}
end;


end.

