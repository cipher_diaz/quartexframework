// ############################################################################
// #
// # Quartex code library
// #        version 1.0.1
// ############################################################################
// #
// # Written by Jon L. Aasenden @ Quartex Components
// #
// # This software is released under Mozilla Public License V2
// #   https://www.mozilla.org/en-US/MPL/2.0/
// #
// ############################################################################

unit quartex.util.propertybag;

{$IFDEF FPC}
  {$mode DELPHI}{$H+}
{$ENDIF}

{$I 'quartex.inc'}

interface

uses
  SysUtils, Classes, Generics.Collections,
  quartex.storage;

type

  IPropertyElement = interface
    ['{C6C937DF-50FA-4984-BA6F-EBB0B367D3F3}']
    function  GetAsInt: integer;
    procedure SetAsInt(const Value: integer);

    function  GetAsString: string;
    procedure SetAsString(const Value: string);

    function  GetAsBool: boolean;
    procedure SetAsBool(const Value: boolean);

    function  GetAsFloat: double;
    procedure SetAsFloat(const Value: double);

    function  GetEmpty: boolean;

    property Empty: boolean read GetEmpty;
    property AsFloat: double read GetAsFloat write SetAsFloat;
    property AsBoolean: boolean read GetAsBool write SetAsBool;
    property AsInteger: integer read GetAsInt write SetAsInt;
    property AsString: string read GetAsString write SetAsString;
  end;

  IPropertyBag = interface
    ['{EE457334-2FB9-4BC2-9A6B-4A28EB30223C}']
    function    Read(Name: string): IPropertyElement;
    function    Write(Name: string; Value: string): IPropertyElement;

    procedure   SaveToStream(const Stream: TStream);
    procedure   LoadFromStream(const Stream: TStream);

    procedure   LoadFromStorage(const Storage: IStorage);
    procedure   SaveToStorage(const Storage: IStorage);

    function    ToString: string;
    procedure   Clear;
  end;

  TPropertyBag = Class(TInterfacedObject, IPropertyBag)
  public
    type
    EPropertybag           = class(exception);
    EPropertybagReadError  = class(EPropertybag);
    EPropertybagWriteError = class(EPropertybag);
    EPropertybagParseError = class(EPropertybag);

  strict private
    FLUT: TDictionary<string, string>;
  strict protected
    procedure   Parse(NameValuePairs: string);

  protected
    type
    TPropertyElement = class(TInterfacedObject, IPropertyElement)
    strict private
      FName:      string;
      FData:      string;
      FStorage:   TDictionary<string, string>;
    strict protected
      function    GetEmpty: boolean; inline;

      function    GetAsInt: integer; inline;
      procedure   SetAsInt(const Value: integer); inline;

      function    GetAsString: string; inline;
      procedure   SetAsString(const Value: string); inline;

      function    GetAsBool: boolean; inline;
      procedure   SetAsBool(const Value: boolean); inline;

      function    GetAsFloat: double; inline;
      procedure   SetAsFloat(const Value: double); inline;

    public
      property    AsFloat: double read GetAsFloat write SetAsFloat;
      property    AsBoolean: boolean read GetAsBool write SetAsBool;
      property    AsInteger: integer read GetAsInt write SetAsInt;
      property    AsString: string read GetAsString write SetAsString;
      property    Empty: boolean read GetEmpty;

      constructor Create(const Storage: TDictionary<string, string>; Name: string; Data: string); overload; virtual;
      constructor Create(Data: string); overload; virtual;
    end;

  public
    function    Read(Name: string): IPropertyElement;
    function    Write(Name: string; Value: string): IPropertyElement;

    procedure   SaveToStream(const Stream: TStream);
    procedure   LoadFromStream(const Stream: TStream);

    procedure   LoadFromStorage(const Storage: IStorage);
    procedure   SaveToStorage(const Storage: IStorage);

    function    ToString: string; override;
    procedure   Clear; virtual;

    constructor Create(NameValuePairs: string); virtual;
    destructor  Destroy; override;
  end;

implementation

resourcestring
  cnt_err_sourceparameters_parse =
  'Failed to parse input, invalid or damaged text error [%s]';

  cnt_err_sourceparameters_write_id =
  'Write failed, invalid or empty identifier error';

  cnt_err_sourceparameters_read_id =
  'Read failed, invalid or empty identifier error';

//#############################################################################
// TPropertyBag.TPropertyElement
//#############################################################################

constructor TPropertyBag.TPropertyElement.Create(Data: string);
begin
  inherited Create;
  FData := Data.Trim();
end;

constructor TPropertyBag.TPropertyElement.Create(const Storage: TDictionary<string, string>;
  Name: string; Data: string);
begin
  inherited Create;
  FStorage := Storage;
  FName := Name.Trim().ToLower();
  FData := Data.Trim();
end;

function TPropertyBag.TPropertyElement.GetEmpty: boolean;
begin
  result := FData.Length < 1;
end;

function TPropertyBag.TPropertyElement.GetAsString: string;
begin
  result := FData;
end;

procedure TPropertyBag.TPropertyElement.SetAsString(const Value: string);
begin
  if Value <> FData then
  begin
    FData := Value;
    if FName.Length > 0 then
    begin
      if FStorage <> nil then
        FStorage.AddOrSetValue(FName, Value);
    end;
  end;
end;

function TPropertyBag.TPropertyElement.GetAsBool: boolean;
begin
  TryStrToBool(FData, result);
end;

procedure TPropertyBag.TPropertyElement.SetAsBool(const Value: boolean);
begin
  FData := BoolToStr(Value, true);

  if FName.Length > 0 then
  begin
    if FStorage <> nil then
      FStorage.AddOrSetValue(FName, FData);
  end;
end;

function TPropertyBag.TPropertyElement.GetAsFloat: double;
begin
  TryStrToFloat(FData, result);
end;

procedure TPropertyBag.TPropertyElement.SetAsFloat(const Value: double);
begin
  FData := FloatToStr(Value);
  if FName.Length > 0 then
  begin
    if FStorage <> nil then
      FStorage.AddOrSetValue(FName, FData);
  end;
end;

function TPropertyBag.TPropertyElement.GetAsInt: integer;
begin
  TryStrToInt(FData, Result);
end;

procedure TPropertyBag.TPropertyElement.SetAsInt(const Value: integer);
begin
  FData := IntToStr(Value);
  if FName.Length > 0 then
  begin
    if FStorage <> nil then
      FStorage.AddOrSetValue(FName, FData);
  end;
end;

//#############################################################################
// TPropertyBag
//#############################################################################

constructor TPropertyBag.Create(NameValuePairs: string);

begin
  inherited Create;
  FLut := TDictionary<string, string>.Create();

  NameValuePairs := NameValuePairs.Trim();
  if NameValuePairs.Length > 0 then
    Parse(NameValuePairs);
end;

destructor TPropertyBag.Destroy;
begin
  FLut.Free;
  inherited;
end;

procedure TPropertyBag.Clear;
begin
  FLut.Clear;
end;

procedure TPropertyBag.Parse(NameValuePairs: string);
var
  lList:      TStringList;
  x:          integer;
  lId:        string;
  lValue:     string;
  lOriginal:  string;
begin
  // Reset content
  FLut.Clear();

  // Make a copy of the original text
  lOriginal := NameValuePairs;

  // Trim and prepare
  NameValuePairs := NameValuePairs.Trim();

  // Anything to work with?
  if NameValuePairs.Length > 0 then
  begin
    (* Populate our lookup table *)
    lList := TStringList.Create;
    try
      lList.Delimiter := ';';
      lList.StrictDelimiter := true;
      lList.DelimitedText := NameValuePairs;

      if lList.Count = 0 then
        raise EPropertybagParseError.CreateFmt(cnt_err_sourceparameters_parse, [LOriginal]);

      try
        for x := 0 to lList.Count-1 do
        begin
          lId := lList.Names[x].Trim().ToLower();
          if lId.Length > 0 then
          begin
            lValue := lList.ValueFromIndex[x].Trim();
            Write(lId, LValue);
          end;
        end;
      except
        on e: exception do
        raise EPropertybagParseError.CreateFmt(cnt_err_sourceparameters_parse, [LOriginal]);
      end;
    finally
      LList.Free;
    end;
  end;
end;

function TPropertyBag.ToString: string;
var
  lItem: TPair<string, string>;
begin
  setlength(result, 0);
  for lItem in FLut do
  begin
    if lItem.Key.Trim().Length > 0 then
    begin
      result := result + Format('%s=%s;', [lItem.Key, lItem.Value]);
    end;
  end;
end;

procedure TPropertyBag.LoadFromStorage(const Storage: IStorage);
var
  lTemp: TStream;
begin
  lTemp := Storage.ToStream();
  try
    lTemp.Seek(0, TSeekOrigin.soBeginning);
    LoadFromStream(lTemp);
  finally
    lTemp.free;
  end;
end;

procedure TPropertyBag.SaveToStorage(const Storage: IStorage);
var
  lTemp: TStream;
begin
  lTemp := TMemoryStream.Create();
  try
    SaveToStream(lTemp);
    lTemp.Seek(0, TSeekOrigin.soBeginning);
    Storage.Append(lTemp);
  finally
    lTemp.free;
  end;
end;

procedure TPropertyBag.SaveToStream(const Stream: TStream);
var
  lData: TStringStream;
begin
  lData := TStringStream.Create( ToString(), TEncoding.UTF8);
  try
    lData.SaveToStream(Stream);
  finally
    lData.Free;
  end;
end;

procedure TPropertyBag.LoadFromStream(const Stream: TStream);
var
  lData: TStringStream;
begin
  lData := TStringStream.Create('', TEncoding.UTF8);
  try
    lData.LoadFromStream(Stream);
    Parse(LData.DataString);
  finally
    lData.Free;
  end;
end;

function TPropertyBag.Write(Name: string; Value: string): IPropertyElement;
begin
  Name := Name.Trim().ToLower();
  if Name.Length > 0 then
  begin
    if not FLUT.ContainsKey(Name) then
      FLut.Add(Name, Value);

    result := TPropertyElement.Create(FLut, Name, Value) as IPropertyElement;
  end else
  raise EPropertybagWriteError.Create(cnt_err_sourceparameters_write_id);
end;

function TPropertyBag.Read(Name: string): IPropertyElement;
var
  lData:  String;
begin
  Name := Name.Trim().ToLower();
  if Name.Length > 0  then
  begin
    if FLut.TryGetValue(Name, LData) then
      result := TPropertyElement.Create(LData) as IPropertyElement
    else
      raise EPropertybagReadError.Create(cnt_err_sourceparameters_read_id);
  end else
  raise EPropertybagReadError.Create(cnt_err_sourceparameters_read_id);
end;

end.

