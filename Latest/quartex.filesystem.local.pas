// ############################################################################
// #
// # Quartex code library
// #        version 1.0.1
// ############################################################################
// #
// # Written by Jon L. Aasenden @ Quartex Components
// #
// # This software is released under Mozilla Public License V2
// #   https://www.mozilla.org/en-US/MPL/2.0/
// #
// ############################################################################

unit quartex.filesystem.local;

{$IFDEF FPC}
  {$mode DELPHI}{$H+}
{$ENDIF}

{$I 'quartex.inc'}

interface

uses
  System.SysUtils,
  System.Classes,
  quartex.vectors,
  quartex.filesystem,
  quartex.storage,
  quartex.storage.memory,
  quartex.storage.disk;

type

  TLocalFileSource = class(TFileSource)
  strict private
    FProxy:     TFileSystemFolder;
  protected
    procedure   UpdateFolderContent(const Folder: TFileSystemFolder); override;
    function    GetProxy: TFileSystemFolder; override;
    procedure   MountSource(Location: string); override;
    procedure   UnMountSource(FailedToMount: boolean); override;

    function    GetFileObjectStream(const Item: TFileSystemFile): TStream; override;
    function    GetFileObjectStorage(const Item: TFileSystemFile): IStorage; override;
    function    ReadFromFileObject(const Item: TFileSystemFile; Offset: int64; Count: integer; var Data): integer; override;
    function    WriteToFileObject(const Item: TFileSystemFile; Offset: int64; Count: integer; const Data): integer; override;
  end;


implementation

{$IFDEF MSWINDOWS}
uses
  windows;
{$ENDIF}

function _FileTimeToDateTime(ft: FILETIME) : TDateTime; inline;
var
  st : SYSTEMTIME;
  lt : FILETIME;
begin
  FillChar(st, SizeOf(st), byte(0) );
  FillChar(lt, SizeOf(lt), byte(0) );
  FileTimeToLocalFileTime(ft, lt);
  FileTimeToSystemTime(lt, st);
  result := SystemTimeToDateTime(st);
end;


//#############################################################################
// TLocalFileSource
//#############################################################################

function TLocalFileSource.GetProxy: TFileSystemFolder;
begin
  result := FProxy;
end;

procedure TLocalFileSource.MountSource(Location: string);
var
  lRec:     TSearchRec;
  lDir:     string;
  lAccess:  IFileSystemObject;
begin
  if not DirectoryExists(Location) then
    raise EFileSource.CreateFmt('Mount failed, location [%s]  does not exist error', [Location]);

  lDir := IncludeTrailingPathDelimiter(Location) + '*.*';
  if System.Sysutils.FindFirst(lDir, faAnyFile, lRec) = 0 then
  begin

    // Setup proxy [root] object
    FProxy := TFileSystemFolder.Create(self, nil);
    lAccess := IFileSystemObject(FProxy);
    lAccess.SetName( ExtractFileName(Location) );
    {$IFDEF FPC}
    lAccess.SetCreated( _FileTimeToDateTime(lRec.FindData.ftCreationTime) );
    lAccess.SetAltered( _FileTimeToDateTime(lRec.FindData.ftLastWriteTime) );
    {$ENDIF}

    // Mark as active
    SetActive(True);


    try
      UpdateFolderContent(FProxy);
    finally
      if assigned(OnSourceMounted) then
        OnSourceMounted(self);
    end;

  end;
end;

procedure TLocalFileSource.UnMountSource(FailedToMount: boolean);
begin
  if FProxy <> nil then
  begin
    try
      FreeAndNIL(FProxy);
    finally
      if assigned(OnSourceUnMounted) then
        OnSourceUnMounted(self);
    end;
  end;
end;

procedure TLocalFileSource.UpdateFolderContent(const Folder: TFileSystemFolder);
var
  lRec:   TSearchRec;
  lDir:   string;
  lPath:  string;
  lItem:  TFileSystemObject;
  lAccess:  IFileSystemObject;
  lFile:    IFileSystemFile;
  lFolder:  IFileSystemFolder;
  lExt: string;
  lTemp: string;
  lInclude: boolean;

  x: integer;
  idx: integer;
  lRemove: boolean;
  lAlreadyKnown: boolean;
  lRemoveList: TVector<TFileSystemObject>;

  lSysTime: TSystemTime;
  lLocaltime: TSystemTime;
begin
  lFolder := Folder as IFileSystemFolder;
  //lFolder.Clear();

  if Folder.Items.Count > 0 then
  begin
    lRemoveList := TVector<TFileSystemObject>.Create(TVectorAllocatorType.vaDynamic);
    try
      // Collect items that have been deleted
      for x := 0 to folder.Items.Count-1 do
      begin
        lRemove := false;
        lPath := IncludeTrailingPathDelimiter(Folder.Items[x].Source.Location) + folder.Items[x].BuildPath();


        if (folder.Items[x] is TFileSystemFolder) then
          lRemove := not DirectoryExists(lPath, true)
        else
        if (Folder.Items[x] is TFileSystemFile) then
          lRemove := not FileExists(lPath, true);
        if lRemove then
          lRemovelist.Add( Folder.Items[x] );
      end;

      // Remove items that no longer are valid
      for x := 0 to lRemoveList.Count-1 do
      begin
        idx := Folder.Items.IndexOf( lRemoveList.Items[x] );
        Folder.Items.Remove(idx);
        lRemoveList.Items[x].Free;
      end;

    finally
      lRemovelist.Free;
    end;
  end;

  lPath := IncludeTrailingPathDelimiter(Location);
  lPath := lPath + Folder.BuildPath;

  lDir := IncludeTrailingPathDelimiter(lPath) + '*.*';
  if System.Sysutils.FindFirst(lDir, faAnyFile, lRec) = 0 then
  begin

    try
      repeat
        If  (lRec.Name<>'.') and (lRec.Name<>'..') then
        begin

          if Filter.Enabled then
          begin
            lExt := LowerCase( ExtractFileExt(lRec.Name) );
            lInclude := false;
            for lTemp in Filter.Items do
            begin
              lInclude := lTemp.ToLower() = lExt;
              if lInclude then
                break;
            end;
            if not lInclude then
              continue;
          end;

          // Check if the item is already known
          lAlreadyKnown := false;
          for x := 0 to Folder.Items.Count-1 do
          begin
            lAlreadyKnown := SameText(Folder.Items[x].Name, lRec.Name);
            if lAlreadyKnown then
            begin
              // Already known? OK update details before we skip it
              if Folder.Items[x] is TFileSystemFile then
              begin
                lFile := Folder.Items[x] as IFileSystemFile;
                {$IFNDEF FPC}
                lFile.SetSize(lRec.Size);
                {$ELSE}
                lFile.SetSize( int64(lRec.FindData.nFileSizeHigh) shl int64(32) + int64(lRec.FindData.nFileSizeLow) );
                {$ENDIF}

                lAccess := Folder.Items[x] as IFileSystemObject;
                {$IFNDEF FPC}
                if FileTimeToSystemTime(lRec.FindData.ftCreationTime, lSysTime) then
                begin
                  if SystemTimeToTzSpecificLocalTime(nil, lSysTime, llocalTime) then
                    lAccess.SetCreated( SystemTimeToDateTime(lLocalTime) );
                end;

                if FileTimeToSystemTime(lRec.FindData.ftLastWriteTime, lSysTime) then
                begin
                  if SystemTimeToTzSpecificLocalTime(nil, lSysTime, llocalTime) then
                    lAccess.SetAltered( SystemTimeToDateTime(lLocalTime) );
                end;
                {$ELSE}
                lAccess.SetCreated( _FileTimeToDateTime(lRec.FindData.ftCreationTime) );
                lAccess.SetAltered( _FileTimeToDateTime(lRec.FindData.ftLastWriteTime) );
                {$ENDIF}

              end;
              break;
            end;
          end;
          // Do we already have it? Just continue
          if lAlreadyKnown then
            continue;

          //lItem := nil;
          lPath := IncludeTrailingPathDelimiter(lPath) + lRec.Name;

          case ((lRec.Attr and faDirectory) = faDirectory) of
          true:   lItem := TFileSystemFolder.Create(self, Folder);
          false:  lItem := TFileSystemFile.Create(self, Folder);
          end;

          lAccess := IFileSystemObject(lItem);
          lAccess.SetName(lRec.Name);
          Folder.Items.Add(lItem);

          // Register with linear vector
          DoRegisterItem(lItem);

          {$IFNDEF FPC}
          // Get created time
          if FileTimeToSystemTime(lRec.FindData.ftCreationTime, lSysTime) then
          begin
            if SystemTimeToTzSpecificLocalTime(nil, lSysTime, llocalTime) then
              lAccess.SetCreated( SystemTimeToDateTime(lLocalTime) );
          end;

          // get updated time
          if FileTimeToSystemTime(lRec.FindData.ftLastWriteTime, lSysTime) then
          begin
            if SystemTimeToTzSpecificLocalTime(nil, lSysTime, llocalTime) then
              lAccess.SetAltered( SystemTimeToDateTime(lLocalTime) );
          end;
          {$ELSE}
          lAccess.SetCreated( _FileTimeToDateTime(lRec.FindData.ftCreationTime) );
          lAccess.SetAltered( _FileTimeToDateTime(lRec.FindData.ftLastWriteTime) );
          {$ENDIF}

          case (lItem is TFileSystemFile) of
          true:
            begin
              lFile := lItem as IFileSystemFile;
              {$IFNDEF FPC}
              lFile.SetSize(lRec.Size);
              {$ELSE}
              lFile.SetSize( int64(lRec.FindData.nFileSizeHigh) shl int64(32) + int64(lRec.FindData.nFileSizeLow) );
              {$ENDIF}
            end;
          false:
            begin
              TFileSystemFolder(lItem).Update();
            end;
          end;

        end;
      until System.Sysutils.FindNext(lRec) <> 0;
    finally
      System.Sysutils.FindClose(lRec);
    end;
  end;
end;

function TLocalFileSource.ReadFromFileObject(const Item: TFileSystemFile; Offset: int64; Count: integer; var Data): integer;
var
  lFileName: string;
  lStream: TFileStream;
begin
  lFileName := IncludeTrailingPathDelimiter(GetLocation);
  lFileName := lFileName + Item.BuildPath();
  lStream := TFileStream.Create(lFileName, fmOpenRead or fmShareDenyNone);
  try
    lStream.Seek(Offset, TSeekOrigin.soBeginning);
    result := lStream.Read(Data, Count);
  finally
    lStream.free;
  end;
end;

function TLocalFileSource.WriteToFileObject(const Item: TFileSystemFile; Offset: int64; Count: integer; const Data): integer;
var
  lFileName: string;
  lStream: TFileStream;
begin
  lFileName := IncludeTrailingPathDelimiter( GetLocation() );
  lFileName := lFileName + Item.BuildPath();
  lStream := TFileStream.Create(lFileName, fmOpenWrite or fmShareDenyNone);
  try
    lStream.Seek(Offset, TSeekOrigin.soBeginning);
    result := lStream.Write(Data, Count);
  finally
    lStream.free;
  end;
end;

function TLocalFileSource.GetFileObjectStream(const Item: TFileSystemFile): TStream;
var
  lFileName: string;
begin
  lFileName := IncludeTrailingPathDelimiter(GetLocation);
  lFileName := lFileName + Item.BuildPath();
  result := TFileStream.Create(lFileName, fmOpenReadWrite);
end;

function TLocalFileSource.GetFileObjectStorage(const Item: TFileSystemFile): IStorage;
var
  lFileName: string;
begin
  lFileName := IncludeTrailingPathDelimiter(GetLocation);
  lFileName := lFileName + Item.BuildPath();
  result := TStorageFile.Create(lFileName, fmOpenReadWrite);
end;

end.

