// ############################################################################
// #
// # Quartex code library
// #        version 1.0.1
// ############################################################################
// #
// # Written by Jon L. Aasenden @ Quartex Components
// #
// # This software is released under Mozilla Public License V2
// #   https://www.mozilla.org/en-US/MPL/2.0/
// #
// ############################################################################

unit quartex.filesystem;

{$IFDEF FPC}
  {$mode DELPHI}{$H+}
{$ENDIF}

{$I 'quartex.inc'}

interface

uses
  {$IFDEF FPC}
  SysUtils, Classes,
  LazFileUtils,
  {$ELSE}
  //IdHashMessageDigest,
  //idHash,
  System.SysUtils,
  System.Classes,
  System.Hash,
  Generics.Collections,
  System.IOUtils,
  {$ENDIF}
  quartex.vectors,
  quartex.views,
  quartex.util.noref,
  quartex.storage,
  quartex.storage.memory,
  quartex.storage.disk;

type

  // Forward declarations
  TFileSource       = class;
  TFileSystemFolder = class;
  TFileSystemFile   = class;

  IFileSystemObject = interface
    ['{874503CF-FDAF-4951-A69A-1C40AB8B352C}']
    function  GetCreated: TDateTime;
    procedure SetCreated(Value: TDateTime);

    function  GetAltered: TDateTime;
    procedure SetAltered(Value: TDateTime);

    function  GetName: string;
    procedure SetName(const Value: string);
  end;

  IFileSystemFolder = interface(IFileSystemObject)
    ['{78D6B835-2C56-4C1A-89FF-61A64959D34F}']
    procedure Clear;
    procedure Update;
  end;

  IFileSystemFile = interface(IFileSystemObject)
    ['{8B0A4AA5-0846-43D5-8944-2AB16A7EDAB8}']
    function  GetSize: int64;
    procedure SetSize(const Value: int64);
  end;

  TFileSystemObject = class(TNonReferenceCountedObject, IFileSystemObject)
  public
    type
    EFileSystemObject = class(Exception);
  strict private
    FSource:    TFileSource;
    FOwner:     TFileSystemFolder;
    FName:      string;
    FCreated:   TDateTime;
    FAltered:   TDateTime;
    FIdentifier: string;
  strict protected
    function    GetCreated: TDateTime;
    procedure   SetCreated(Value: TDateTime);

    function    GetAltered: TDateTime;
    procedure   SetAltered(Value: TDateTime);

    function    GetName: string;
    procedure   SetName(const Value: string);

    procedure   SetIdentifier(const Value: string);

  public
    property    Parent: TFileSystemFolder read FOwner;
    property    Source: TFileSource read FSource;
    function    BuildPath: string; virtual;
    constructor Create(Source: TFileSource; Owner: TFileSystemFolder); virtual;
  published
    property    Name: string read GetName;
    property    Identifier: string read FIdentifier;
    property    DateCreated: TDateTime read GetCreated;
    property    DateAltered: TDateTime read GetAltered;
  end;

  TFileSourceSearch = class(TObject)
  private
    FItems:     TVector<TFileSystemObject>;
    FName:      string;
  public
    property    FileName: string read FName write FName;
    property    Items: TVector<TFileSystemObject> read FItems;
    constructor Create(const AFileName: string);
    destructor  Destroy; override;
  end;

  TFileSystemFolder = class(TFileSystemObject, IFileSystemFolder)
  protected
    FChildren:  TVector<TFileSystemObject>;
    procedure   Clear;
  public
    property    Items: TVector<TFileSystemObject> read FChildren;
    procedure   Update; virtual;

    function    ObjectByName(Name: string; out Item: TFileSystemObject; const Recursive: boolean = false): boolean;
    function    Contains(Name: string): boolean;

    function    Enter(Name: string; out Item: TFileSystemFolder): boolean;
    function    Leave(out Item: TFileSystemFolder): boolean;

    function    Find(ThisFileName: string; var Item: TFileSystemObject): boolean; overload;
    function    Find(ThisFileName: string; var Results: TFileSourceSearch): boolean; overload;

    constructor Create(Source: TFileSource; Owner: TFileSystemFolder); override;
    destructor  Destroy; override;
  end;

  TFileSystemFile = class(TFileSystemObject, IFileSystemFile)
  protected
    FSize:    int64;
  protected
    function  GetSize: int64;
    procedure SetSize(const Value: int64);
  public

    function  ToString: string; override;

    function  ToStream: TStream; virtual;
    function  ToStorage: IStorage; virtual;
    function  ToStringList: TStringList; virtual;

    function  ToViewUint8:  TView<byte>;
    function  ToViewUint16: TView<word>;
    function  ToViewUint32: TView<cardinal>;
    function  ToViewDouble: TView<double>;
    function  ToViewChar:   TView<char>;

    function  Read(Offset: int64; Count: integer; var Data): integer; virtual;
    function  Write(Offset: int64; Count: integer; const Data): integer; virtual;
  published
    property  Size: int64 read GetSize;
  end;

  IFileSource = interface
    ['{32CB22D1-D2E5-4E3A-91FA-BEE96A79B535}']
    function  GetActive: boolean;
    function  GetLocation: string;
    function  GetId: string;
    procedure UpdateFolderContent(const Folder: TFileSystemFolder);
    function  GetFileObjectStream(const Item: TFileSystemFile): TStream;
    function  GetFileObjectStorage(const Item: TFileSystemFile): IStorage;

    function  ReadFromFileObject(const Item: TFileSystemFile; Offset: int64; Count: integer; var Data): integer;
    function  WriteToFileObject(const Item: TFileSystemFile; Offset: int64; Count: integer; const Data): integer;
  end;

  TFileSource = class(TNonReferenceCountedObject, IFileSource)
  public
    type
    EFileSource = class(exception);
    EFileSourceInactive = class(EFileSource);

    TFileSourceFilter = class(TObject)
    strict private
      FItems:     TVector<string>;
      FOwner:     TFileSource;
      FEnabled:   boolean;
    strict protected
      procedure   SetEnabled(Value: boolean); virtual;
    public
      property    Owner: TFileSource read FOwner;
      property    Enabled: boolean read FEnabled write SetEnabled;
      property    Items: TVector<string> read FItems;
      constructor Create(const AOwner: TFileSource); reintroduce; virtual;
      destructor  Destroy; override;
    end;

  strict private
    FLinear:    TVector<TFileSystemObject>;
    //FProxy:     TFileSystemFolder;
    FName:      string;
    FId:        string;
    FLocation:  string;
    FActive:    boolean;
    FFilter:    TFileSourceFilter;

    FOnMounted:   TNotifyEvent;
    FOnUnMounted: TNotifyEvent;

  strict protected
    procedure   SetActive(const Value: boolean); virtual;
    function    GetActive: boolean; virtual;

    function    GetLocation: string; virtual;
    procedure   SetLocation (const Value: string); virtual;

    function    GetId: string; virtual;
    procedure   SetId(const Value: string); virtual;

    function    GetFilterInstance: TFileSourceFilter;

  protected
    procedure   DoRegisterItem(const Item: TFileSystemObject); inline;

    function    GetFileObjectStream(const Item: TFileSystemFile): TStream; virtual; abstract;
    function    GetFileObjectStorage(const Item: TFileSystemFile): IStorage; virtual; abstract;
    function    ReadFromFileObject(const Item: TFileSystemFile; Offset: int64; Count: integer; var Data): integer; virtual; abstract;
    function    WriteToFileObject(const Item: TFileSystemFile; Offset: int64; Count: integer; const Data): integer; virtual; abstract;

    function    GetProxy: TFileSystemFolder; virtual; abstract;
    procedure   UpdateFolderContent(const Folder: TFileSystemFolder); virtual; abstract;
    procedure   MountSource(Location: string); virtual; abstract;
    procedure   UnMountSource(FailedToMount: boolean); virtual; abstract;

  public
    property    Name: string read FName;
    property    Id: string read FId;
    property    Location: string read FLocation;
    property    Files: TFileSystemFolder read GetProxy;
    property    Active: boolean read GetActive;
    property    Items: TVector<TFileSystemObject> read FLinear;
    property    Filter: TFileSourceFilter read FFilter;

    function    GetRelativeObject(RelPath: string; var obj: TFileSystemObject): boolean;

    procedure   Mount(SourceLocation: string);
    procedure   UnMount;

    constructor Create(SourceName: string); virtual;
    destructor  Destroy; override;

  published
    property    OnSourceMounted: TNotifyEvent read FOnMounted write FOnMounted;
    property    OnSourceUnMounted: TNotifyEvent read FOnUnMounted write FOnUnMounted;
  end;

  TFileSourceManager = class(TObject)
  private
    FSources:   TVector<TFileSource>;
  protected
    procedure   RegisterSource(const Source: TFileSource);
    procedure   UnRegisterSource(const Source: TFileSource);
  public
    property    Sources: TVector<TFileSource> read FSources;

    function    GetSourceByName(Name: string; out Source: TFileSource): boolean;
    function    Find(FileName: string; out Results: TFileSourceSearch): boolean;

    constructor Create; virtual;
    destructor  Destroy; override;
  end;


function  FileSources: TFileSourceManager;

resourcestring
  CNT_ERR_FILESRC_NotActive = 'Invalid operation, filesource is not active error';

implementation

var
__SourceManager: TFileSourceManager = nil;

function  FileSources: TFileSourceManager;
begin
  if __SourceManager = nil then
    __SourceManager := TFileSourceManager.Create();
  result := __SourceManager;
end;

function FileNameWithoutExt(Name: string): string;
var
  lSample: string;
begin
  lSample := ExtractFileExt(Name);
  if lSample.length > 0 then
  begin
    if lSample.length > 4 then
      exit(Name);
  end;

  {$IFDEF FPC}
  result := ExtractFileNameWithoutExt(Name);
  {$ELSE}
  result := TPath.GetFileNameWithoutExtension(Name);
  {$endif}
end;

//#############################################################################
// TFileSource.TFileSourceFilter
//#############################################################################

constructor TFileSource.TFileSourceFilter.Create(const AOwner: TFileSource);
begin
  inherited Create;
  FItems := TVector<string>.Create(TVectorAllocatorType.vaDynamic);
  FOwner := AOwner;
  FEnabled := false;
end;

destructor TFileSource.TFileSourceFilter.Destroy;
begin
  FItems.clear();
  FItems.free;
  inherited;
end;

procedure TFileSource.TFileSourceFilter.SetEnabled(Value: boolean);
begin
  FEnabled := Value;
end;

//#############################################################################
// TFileSourceSearch
//#############################################################################

constructor TFileSourceSearch.Create(const AFileName: string);
begin
  inherited Create();
  FItems := TVector<TFileSystemObject>.Create(TVectorAllocatorType.vaDynamic);
  FName := AFileName.trim();
end;

destructor  TFileSourceSearch.Destroy;
begin
  FItems.free;
  inherited;
end;

//#############################################################################
// TFileSourceManager
//#############################################################################

constructor TFileSourceManager.Create;
begin
  inherited Create();
  FSources := TVector<TFileSource>.Create(TVectorAllocatorType.vaDynamic);
end;

destructor TFileSourceManager.Destroy;
begin
  FSources.free;
  inherited;
end;

function TFileSourceManager.GetSourceByName(Name: string; out Source: TFileSource): boolean;
var
  el: TFileSource;
begin
  result := false;
  Source := nil;
  Name := Name.trim().ToLower();
  if name.length > 0 then
  begin
    for el in FSources do
    begin
      result  := el.name.ToLower() = Name;
      if result then
      begin
        Source := el;
        break;
      end;
    end;
  end;
end;

procedure TFileSourceManager.RegisterSource(const Source: TFileSource);
begin
  if source <> nil then
  begin
    if FSources.IndexOf(Source) < 0 then
      FSources.add(Source);
  end;
end;

procedure TFileSourceManager.UnRegisterSource(const Source: TFileSource);
var
  lIndex: integer;
begin
  if Source <> nil then
  begin
    lIndex := FSources.IndexOf(Source);
    if lIndex >= 0 then
    begin
      FSources.remove(lIndex);
    end;
  end;
end;

function TFileSourceManager.Find(FileName: string; out Results: TFileSourceSearch): boolean;
var
  src: TFileSource;
begin
  results := TFileSourceSearch.Create(FileName);

  for src in FSources do
  begin
    if src.Active then
      src.Files.Find(Filename, results);
  end;

  result := results.Items.Count > 0;
end;

//#############################################################################
// TFileSource
//#############################################################################

constructor TFileSource.Create(SourceName: string);
begin
  inherited Create;
  FFilter := GetFilterInstance();
  FLinear := TVector<TFileSystemObject>.Create(TVectorAllocatorType.vaFile);
  FName := SourceName.trim();
  SetId( TTyped.CreateGuidString() );
  FileSources.RegisterSource(self);
end;

destructor TFileSource.Destroy;
begin
  try
    FFilter.free;
    try
      if GetActive() then
        UnMount();
    finally
      if FLinear.Count > 0 then
        FLinear.Clear();
      FreeAndNil(FLinear);
    end;
  finally
    FileSources.UnRegisterSOurce(self);
  end;
  inherited;
end;

function TFileSource.GetFilterInstance: TFileSourceFilter;
begin
  result := TFileSourceFilter.Create(self);
end;

procedure TFileSource.DoRegisterItem(const Item: TFileSystemObject);
begin
  FLinear.Add(Item);
end;

function TFileSource.GetLocation: string;
begin
  result := FLocation;
end;

procedure TFileSource.SetLocation (const Value: string);
begin
  FLocation := Value.trim();
end;

function TFileSource.GetId: string;
begin
  result := FID;
end;

procedure TFileSource.SetId(const Value: string);
begin
  FId := Value;
end;

{function TFileSource.FindByName(Name: string; var item: TFileSystemObject): boolean;
begin
  self.Files.Find
end;   }

function TFileSource.GetRelativeObject(RelPath: string; var obj: TFileSystemObject): boolean;
var
  lList: TArray<string>;
  y, x:  integer;
  lNode: TFileSystemFolder;
  lfound: TFileSystemObject;
begin
  obj := nil;
  result := false;

  // trim the text
  RelPath := RelPath.Trim();
  if RelPath.Length < 1 then
  begin
    obj := Files;
    exit(true);
  end;

  // Get rid of leading path delimiter
  While RelPath.StartsWith( System.IOUtils.TPath.DirectorySeparatorChar) do
  begin
    Delete(RelPath, 1, 1);
  end;
  if RelPath.Length < 1 then
  begin
    obj := Files;
    exit(true);
  end;

  // Get rid of leading path delimiter
  While RelPath.EndsWith( System.IOUtils.TPath.DirectorySeparatorChar) do
  begin
    Delete(RelPath, length(RelPath), 1);
  end;
  if RelPath.Length < 1 then
  begin
    obj := Files;
    exit(true);
  end;

  // trim the text again
  RelPath := RelPath.Trim();
  if RelPath.Length < 1 then
  begin
    obj := Files;
    exit(true);
  end;

  lList := RelPath.Split([System.IOUtils.TPath.DirectorySeparatorChar]);
  if length(lList) < 1 then
  begin
    obj := Files;
    exit(true);
  end;

  lNode := Files;

  for y := low(lList) to high(lList) do
  begin

    lFound := nil;
    for x := 0 to lNode.Items.Count-1 do
    begin
      if lNode.Items[x].Name.ToLower() = lList[y].ToLower() then
      begin
        lFound := lNode.Items[x];
        break;
      end;
    end;

    if lFound <> nil then
    begin
      if y = high(lList) then
      begin
        obj := lFound;
        exit(true);
      end;

      if lFound is TFileSystemFolder then
      begin
        lNode := TFileSystemFolder(lFound);
        Continue;
      end else
      exit(false);
    end else
    exit(false);
  end;

end;

procedure TFileSource.Mount(SourceLocation: string);
begin
  if GetActive() then
    UnMount();

  SourceLocation := SourceLocation.trim();

  SetLocation(SourceLocation);
  try
    MountSource(Location);
  except
    SetActive(False);
    SetLocation('');

    // The proxy object is managed by the decendant
    // so we call unmount with True to signal that
    // something went wrong while mounting, so just clean up
    // whatever resources you have so far and exit
    UnMountSource(true);
  end;
end;

procedure TFileSource.UnMount;
begin
  if GetActive() then
  begin
    try
      FLinear.Clear();
      SetLocation('');
      SetActive(False);
    finally
      UnMountSource(false);
    end;
  end;
end;

procedure TFileSource.SetActive(const Value: boolean);
begin
  FActive := Value;
end;

function TFileSource.GetActive: boolean;
begin
  result := FActive;
end;

//#############################################################################
// TFileSystemObject
//#############################################################################

constructor TFileSystemObject.Create(Source: TFileSource; Owner: TFileSystemFolder);
begin
  inherited Create;
  FSource := Source;
  FOwner := Owner;
  FIdentifier := TTyped.CreateGuidString();
end;

procedure TFileSystemObject.SetIdentifier(const Value: string);
begin
  FIdentifier := Value.trim();
end;

function TFileSystemObject.BuildPath: string;
var
  lObj: TFileSystemObject;
begin
  result := '';
  if not (self = FSource.Files) then
  begin
    lObj := self;

    repeat
      result := ( IncludeTrailingPathDelimiter(lObj.Name) + result);
      lObj := lObj.FOwner;
      if lObj = FSource.Files then
        break;
    until lObj = nil;

    //if (self is TFileSystemFile) then
    result := ExcludeTrailingPathDelimiter(result);
  end;
end;

function  TFileSystemObject.GetCreated: TDateTime;
begin
  result := FCreated;
end;

procedure TFileSystemObject.SetCreated(Value: TDateTime);
begin
  FCreated := Value;
end;

function  TFileSystemObject.GetAltered: TDateTime;
begin
  result := FAltered;
end;

procedure TFileSystemObject.SetAltered(Value: TDateTime);
begin
  FAltered := Value;
end;

function  TFileSystemObject.GetName: string;
begin
  result := FName;
end;

procedure TFileSystemObject.SetName(const Value: string);

  function GetStrHashMD5(Str: String): String;
  var
    HashMD5: THashMD5;
  begin
    // Function, no need to release [returns record]
    HashMD5 := THashMD5.Create();
    HashMD5.GetHashString(Str);
    result := HashMD5.GetHashString(Str);
  end;

  { function MD5File(const FileName: string): string;
  var
    IdMD5: TIdHashMessageDigest5;
    FS: TFileStream;
  begin
    IdMD5 := TIdHashMessageDigest5.Create;
    try
      FS := TFileStream.Create(FileName, fmOpenRead or fmShareDenyWrite);
      try
        Result := IdMD5.HashStreamAsHex(FS)
      finally
        FS.Free;
      end;
    finally
      IdMD5.Free;
    end;
  end;  }


var
  lPath: string;
begin
  FName := Value;
  if FName.Length > 0 then
  begin
    lPath := IncludeTrailingPathDelimiter( Source.Location ) + BuildPath();
    SetIdentifier( GetStrHashMD5(lPath) )
  end else
  SetIdentifier( TTyped.CreateGuidString() );
end;

//#############################################################################
// TFileSystemFile
//#############################################################################

function TFileSystemFile.GetSize: int64;
begin
  result := FSize;
end;

procedure TFileSystemFile.SetSize(const Value: int64);
begin
  FSize := Value;
end;

function TFileSystemFile.ToStream: TStream;
begin
  result := Source.GetFileObjectStream(self);
end;

function TFileSystemFile.ToStorage: IStorage;
begin
  result := Source.GetFileObjectStorage(self);
end;

function TFileSystemFile.ToStringList: TStringList;
var
  lData: TStream;
begin
  lData := ToStream();
  try
    result := TStringList.Create();
    result.LoadFromStream(lData);
  finally
    lData.free;
  end;
end;

function TFileSystemFile.ToString: string;
var
  lData: TStringList;
begin
  lData := ToStringList();
  if lData <> nil then
  begin
    try
      result := lData.Text;
    finally
      lData.free;
    end;
  end;
end;

function TFileSystemFile.Read(Offset: int64; count: integer; var Data): integer;
begin
  result := Source.ReadFromFileObject(self, offset, count, data);
end;

function TFileSystemFile.Write(Offset: int64; count: integer; const Data): integer;
begin
  result := Source.WriteToFileObject(self, offset, count, data);
end;

function TFileSystemFile.ToViewDouble: TView<double>;
var
  lStorage: IStorage;
begin
  lStorage := Source.GetFileObjectStorage(self);
  result := TView<double>.Create(lStorage);
end;

function TFileSystemFile.ToViewUint8: TView<byte>;
var
  lStorage: IStorage;
begin
  lStorage := Source.GetFileObjectStorage(self);
  result := TView<byte>.Create(lStorage);
end;

function TFileSystemFile.ToViewUint16: TView<word>;
var
  lStorage: IStorage;
begin
  lStorage := Source.GetFileObjectStorage(self);
  result := TView<word>.Create(lStorage);
end;

function TFileSystemFile.ToViewUint32: TView<cardinal>;
var
  lStorage: IStorage;
begin
  lStorage := Source.GetFileObjectStorage(self);
  result := TView<cardinal>.Create(lStorage);
end;

function TFileSystemFile.ToViewChar: TView<char>;
var
  lStorage: IStorage;
begin
  lStorage := Source.GetFileObjectStorage(self);
  result := TView<char>.Create(lStorage);
end;

//#############################################################################
// TFileSystemFolder
//#############################################################################

constructor TFileSystemFolder.Create(Source: TFileSource; Owner: TFileSystemFolder);
begin
  inherited Create(Source, Owner);
  FChildren := TVector<TFileSystemObject>.Create(TVectorAllocatorType.vaSequence);
end;

destructor TFileSystemFolder.Destroy;
begin
  Clear();
  FChildren.free;
  inherited;
end;

function TFileSystemFolder.Enter(Name: string; out Item: TFileSystemFolder): boolean;
var
  lTemp:  TFileSystemObject;
begin
  result := false;
  Item := nil;

  if Find(Name, lTemp) then
  begin
    result := lTemp is TFileSystemFolder;
    if result then
      Item := TFileSystemFolder(lTemp);
  end;
end;

function TFileSystemFolder.Leave(out Item: TFileSystemFolder): boolean;
begin
  item := Parent;
  result := item <> nil;
end;

function TFileSystemFolder.Find(ThisFileName: string; var Item: TFileSystemObject): boolean;
var
  el: TFileSystemObject;
begin
  result := false;
  Item := nil;

  ThisFileName := ThisFileName.ToLower();

  for el in FChildren do
  begin
    //if FileNameWithoutExt(Name.ToLower()) = FileNameWithoutExt(el.Name.ToLower()) then
    if el.Name.ToLower() = ThisFileName then
    begin
      Item := el;
      result := true;
      break;
    end else
    begin
      if (el is TFileSystemFolder) then
      begin
        result := TFileSystemFolder(el).Find(ThisFileName, Item);
        if result then
          break;
      end;
    end;
  end;
end;

function TFileSystemFolder.Find(ThisFileName: string; var Results: TFileSourceSearch): boolean;
var
  el: TFileSystemObject;
begin
  if results = nil then
    results := TFileSourceSearch.Create(ThisFileName);

  ThisFileName := ThisFileName.ToLower();

  for el in FChildren do
  begin
    if el is TFileSystemFile then
    begin
      if FileNameWithoutExt(ThisFileName) = FileNameWithoutExt(el.Name.ToLower()) then
      begin
        if results.Items.IndexOf(el) < 0 then
          results.Items.Add(el);
      end;
    end else
    begin
      if (el is TFileSystemFolder) then
        TFileSystemFolder(el).Find(ThisFileName, Results);
    end;
  end;

  result := results.Items.Count > 0;
end;

function TFileSystemFolder.Contains(Name: string): boolean;
var
  el: TFileSystemObject;
  lTempName: string;
begin
  result := false;
  Name := Name.trim();
  if Name.length < 1 then
    exit(false);

  lTempName := Name.ToLower();

  // Search our collection first
  for el in FChildren do
  begin
    result :=el.Name.ToLower() = lTempName;
    if result then
      break;
  end;
end;


function TFileSystemFolder.ObjectByName(Name: string;
  out Item: TFileSystemObject; const Recursive: boolean = false): boolean;
var
  el: TFileSystemObject;
  lTempName: string;
begin
  result := false;
  Name := Name.trim();
  if Name.length < 1 then
    exit(false);

  lTempName := Name.ToLower();

  // Search our collection first
  for el in FChildren do
  begin
    result :=el.Name.ToLower() = lTempName;
    if result then
    begin
      Item := el;
      break;
    end;
  end;

  // If not found, do a recursive search [if wanted] of folders
  if not result then
  begin
    if Recursive then
    begin
      for el in FChildren do
      begin
        if (el is TFileSystemFolder) then
        begin
          result := TFileSystemFolder(el).ObjectByName(Name, Item, Recursive);
          if result then
            break;
        end;
      end;
    end;
  end;
end;

procedure TFileSystemFolder.Clear;
var
  x:  integer;
begin
  if FChildren.Count > 0 then
  begin
    try
      for x := 0 to FChildren.Count-1 do
      begin
        FChildren[x].free;
      end;
    finally
      FChildren.Clear();
    end;
  end;
end;

procedure TFileSystemFolder.Update;
begin
  Source.UpdateFolderContent(self);
end;

{finalization
begin
  if __SourceManager <> nil then
    freeAndNil(__SourceManager);
end; }

end.

