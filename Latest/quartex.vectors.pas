// ############################################################################
// #
// # Quartex code library
// #        version 1.0.1
// ############################################################################
// #
// # Written by Jon L. Aasenden @ Quartex Components
// #
// # This software is released under Mozilla Public License V2
// #   https://www.mozilla.org/en-US/MPL/2.0/
// #
// ############################################################################

unit quartex.vectors;

{$IFDEF FPC}
  {$mode DELPHI}{$H+}
{$ENDIF}

{$I 'quartex.inc'}

interface

uses
  SysUtils, Classes, Generics.Collections,
  {$IFNDEF FPC} System.IOUtils,{$ENDIF}
  quartex.storage,
  quartex.storage.memory,
  quartex.storage.disk;

type

  /// <summary>
  ///   This type determines what kind of allocator will be created
  ///   by the vector container. This datatype is only valid for one of
  ///   the constructors. If you create the allocator manually and pass
  ///   it to the vector constructor, the type is automatically set to
  ///   vaCustom internally.
  /// </summary>
  TVectorAllocatorType = (
    vaSequence,   // sequenced in memory
    vaDynamic,    // dynamic in memory
    vaFile,       // sequenced in file
    vaMapped,     // Mapped to a buffer
    vaCustom      // Custom user-type (see constructors)
    );

  TVector<T> = class(TEnumerable<T>)
  public
    type
    EVector                    = class(Exception);
    EVectorEmpty               = class(EVector);
    EVectorInvalidIndex        = class(EVector);
    EVectorAllocator           = class(Exception);
    EVectorAllocatorLocked     = class(EVectorAllocator);
    EVectorAllocatorNotLocked  = class(EVectorAllocator);
    EVectorUnSupported         = class(EVectorAllocator);
    EVectorReadOnly            = class(EVectorAllocator);

    IVectorAllocator = interface
      ['{9A3A02CB-5A4B-409F-B999-4728B3636A1E}']
      procedure   Resize(ItemCount: integer);
      procedure   LockMemory(var Data);
      procedure   UnLockMemory;
      function    GetDataSize: int64;
      procedure   SetItem(const index: integer; Value: T);
      function    GetItem(const index: integer): T;
      function    GetCount: integer;
      function    Add(const Value: T): integer;
      procedure   Remove(const Index: integer);
      function    Insert(const Index: integer; Value: T): integer;
      procedure   Clear;
    end;

  strict protected
    type
    TVectorEnumerator = class(TEnumerator<T>)
    private
      FIndex:     integer;
      FParent:    TVector<T>;
    protected
      function    DoGetCurrent: T; override;
      function    DoMoveNext: boolean; override;
    public
      constructor Create(Owner: TVector<T>); virtual;
    end;

  public

    type
    TVectorAllocator = class(TInterfacedObject, IVectorAllocator)
    public
      procedure   Resize(ItemCount: integer); virtual; abstract;
      procedure   LockMemory(var Data); virtual; abstract;
      procedure   UnLockMemory; virtual; abstract;
      function    GetDataSize: int64; virtual; abstract;
      procedure   SetItem(const index: integer; Value: T); virtual; abstract;
      function    GetItem(const index: integer): T; virtual; abstract;
      function    GetCount: integer;  virtual; abstract;
      function    Add(const Value: T): integer;  virtual; abstract;
      procedure   Remove(const Index: integer);  virtual; abstract;
      function    Insert(const Index: integer; Value: T): integer;  virtual; abstract;
      procedure   Clear;  virtual; abstract;
    end;
    TVectorAllocatorClass = class of TVectorAllocator;


    // Ancestor for allocators that use buffers internally to deal
    // with storage management
    TVectorAllocatorBuffered = class(TVectorAllocator)
    strict protected
      function    GetStorageReference: TStorage; virtual; abstract;
    public
      property    Buffer: TStorage read GetStorageReference;
      procedure   Resize(ItemCount: integer); override;
      function    GetDataSize: int64; override;
      procedure   SetItem(const index: integer; Value: T); override;
      function    GetItem(const index: integer): T;  override;
      function    GetCount: integer;  override;
      function    Add(const Value: T): integer;  override;
      procedure   Remove(const Index: integer);  override;
      function    Insert(const Index: integer; Value: T): integer;  override;
      procedure   Clear;  override;
    end;

    // Sequenced memory management
    TVectorAllocatorSequence = class(TVectorAllocatorBuffered)
    strict private
      FLock:      integer;
      FStorage:   TStorageMemory;
    strict protected
      function    GetStorageReference: TStorage; override;
    public
      procedure   LockMemory(var Data); override;
      procedure   UnLockMemory; override;

      constructor Create; virtual;
      destructor  Destroy; override;
    end;

    // Sequential file management
    TVectorAllocatorFile = class(TVectorAllocatorBuffered)
    strict private
      FMode:      word;
      FName:      string;
      FStorage:   TStorageFile;
    strict protected
      function    GetStorageReference: TStorage; override;

    public
      property    Filename: string read FName;
      property    AccessMode: word read FMode;

      procedure   LockMemory(var Data); override;
      procedure   UnLockMemory; override;
      constructor Create(Filename: string; AccessFlags: word); virtual;
      destructor  Destroy; override;
    end;

    // External management (map to a buffer, buffer can read/write to anywhere)
    TVectorAllocatorMapped = class(TVectorAllocatorBuffered)
    strict private
      FStorage:   TStorage;

    strict protected
      function    GetStorageReference: TStorage; override;

    public
      procedure   Resize(ItemCount: integer); override;
      function    GetDataSize: int64; override;
      procedure   LockMemory(var Data); override;
      procedure   UnLockMemory; override;
      procedure   SetItem(const index: integer; Value: T); override;
      function    GetItem(const index: integer): T;  override;
      function    GetCount: integer;  override;
      function    Add(const Value: T): integer;  override;
      procedure   Remove(const Index: integer);  override;
      function    Insert(const Index: integer; Value: T): integer;  override;
      procedure   Clear;  override;

      constructor Create(const Buffer: TStorage); virtual;
      destructor  Destroy; override;
    end;

    // Dynamic memory management
    TVectorAllocatorDynamic = class(TVectorAllocator)
    strict private
      FValues:    TArray<T>;
      FLock:      integer;
    public
      procedure   Resize(ItemCount: integer); override;
      function    GetDataSize: int64; override;
      procedure   LockMemory(var Data); override;
      procedure   UnLockMemory; override;
      procedure   SetItem(const index: integer; Value: T); override;
      function    GetItem(const index: integer): T;  override;
      function    GetCount: integer;  override;
      function    Add(const Value: T): integer;  override;
      procedure   Remove(const Index: integer);  override;
      function    Insert(const Index: integer; Value: T): integer;  override;
      procedure   Clear;  override;
      destructor  Destroy; override;
    end;

  strict private
    FMemory:    IVectorAllocator;
    FType:      TVectorAllocatorType;

  strict protected
    function    GetCount: integer; inline;
    function    GetItem(const index: integer): T; inline;
    procedure   SetItem(const index: integer; Value: T); inline;
    function    GetFront: T; inline;
    function    GetBack: T; inline;

  protected
    function    DoGetEnumerator: TEnumerator<T>;  override;
    {$IFDEF FPC}
    function    GetPtrEnumerator: TEnumerator<PT>; override;
    {$ENDIF}
  public
    property    Memory: IVectorAllocator read FMemory;
    property    AllocatorType: TVectorAllocatorType read FType;
    property    Items[const index: integer]:T read GetItem write SetItem; default;
    property    Count: integer read GetCount;

    property    Front: T read GetFront;
    property    Back: T read GetBack;

    procedure   Assign(const Source: TVector<T>); virtual;

    function    Add(const Value: T): integer; inline;
    procedure   Remove(const Index: integer); inline;
    function    Insert(const Index: integer; const Value: T): integer; inline;
    function    IndexOf(const Value: T): integer;
    procedure   Clear; inline;

    procedure   PushBack(const Value: T); inline;
    procedure   PushFront(const Value: T); inline;
    procedure   Swap(const First, Second: integer);

    constructor Create; overload; virtual;
    constructor Create(const Allocator: TVectorAllocatorType = vaSequence); overload; virtual;
    constructor Create(const FileName: string; const Access: word); overload; virtual;
    constructor Create(const Buffer: TStorage); overload; virtual;
    constructor Create(const Allocator: IVectorAllocator); overload; virtual;

    destructor  Destroy; override;
  end;

// Note: The way generics work under FPC, references must be within the
// global symbol-table. So these cant be put in the implementation section
resourcestring
  CNT_ERR_VECTOR_InvalidIndex   = 'Invalid index, expected 0..0 not %d';
  CNT_ERR_VECTOR_Empty          = 'Invalid operation, vector list is empty';
  CNT_ERR_VECTOR_NoCustomAlloc  = 'Custom vector allocator cannot be created with this constructor';
  CNT_ERR_VECTOR_MemLocked      = 'Vector memory is locked';
  CNT_ERR_VECTOR_MemUnLocked    = 'Vector memory was unlocked';
  CNT_ERR_VECTOR_LockNotSupported = 'Vector does not support memory lock';
  CNT_ERR_VECTOR_ResizeFailed   = 'Resize failed, invalid item count [%d]';
  CNT_ERR_VECTOR_ReadOnly       = 'Vector is mapped or read only';


implementation

//#############################################################################
// TVector<T>
//#############################################################################

constructor TVector<T>.Create;
begin
  inherited Create;
  FType := vaDynamic;
  FMemory := TVectorAllocatorDynamic.Create();
end;

constructor TVector<T>.Create(const Allocator: IVectorAllocator);
begin
  inherited Create();
  FType := vaCustom;
  FMemory := Allocator;
end;

constructor TVector<T>.Create(const FileName: string; const Access: word);
var
  lTemp: TVectorAllocatorFile;
begin
  inherited Create();
  FType := TVectorAllocatorType.vaFile;
  lTemp := TVectorAllocatorFile.Create(FileName, Access);
  FMemory := lTemp as IVectorAllocator;
end;

constructor TVector<T>.Create(const Buffer: TStorage);
var
  lTemp: TVectorAllocator;
begin
  inherited Create();
  FType := TVectorAllocatorType.vaMapped;
  lTemp := TVectorAllocatorMapped.Create(Buffer);
  FMemory := lTemp as IVectorAllocator;
end;

constructor TVector<T>.Create(const Allocator: TVectorAllocatorType = vaSequence);
var
  lTemp:  TVectorAllocator;
  lFile:  TStorageFile;
  lAccess: IStorageDisposable;
begin
  inherited Create();
  FType := Allocator;

  case FType of
  vaSequence: lTemp := TVectorAllocatorSequence.Create();
  vaDynamic:  lTemp := TVectorAllocatorDynamic.Create();
  vaFile:
    begin
      // Create file-based vector
      lTemp := TVectorAllocatorFile.Create('', fmCreate);

      // Get a reference to the storage-object
      lFile := TStorageFile( TVectorAllocatorFile(lTemp).Buffer );

      // Mark the storage-object as disposable.
      // This means the file is deleted after use
      lAccess := IStorageDisposable(lFile);
      lAccess.SetDisposable(true);
    end
  else
    raise EVector.Create(CNT_ERR_VECTOR_NoCustomAlloc);
  end;

  FMemory := IVectorAllocator(lTemp);
end;

destructor TVector<T>.Destroy;
begin
  try
    FMemory.Clear();
  finally
    FMemory := nil;
  end;
  inherited;
end;

function TVector<T>.IndexOf(const Value: T): integer;
var
  x:  integer;
  lCount: integer;
  lSource:  T;
begin
  result := -1;
  lCount := GetCount();
  if lCount > 0 then
  begin
    for x := 0 to lCount-1 do
    begin
      lSource := GetItem(x);
      if CompareMem(@lSource, @Value, SizeOf(T)) then
      begin
        result := x;
        break;
      end;
    end;
  end;
end;

{$IFDEF FPC}
function TVector<T>.GetPtrEnumerator: TEnumerator<PT>;
begin
  Result := nil;
end;
{$ENDIF}

function TVector<T>.DoGetEnumerator: TEnumerator<T>;
begin
  Result := TVectorEnumerator.Create(self);
end;

procedure TVector<T>.Assign(const Source: TVector<T>);
var
  x: integer;
  lFactor: integer;
begin
  FMemory.Clear();
  if Source <> nil then
  begin
    if Source.Count < 1 then
      exit;

    // Prepare alloator
    FMemory.Resize(Source.Count);

    // Use loop expansion to move data over in batches of 8
    x:= 0;
    lFactor := Source.Count shr 3;
    while lFactor > 0 do
    begin
      SetItem(x, Source[x]); inc(x);
      SetItem(x, Source[x]); inc(x);
      SetItem(x, Source[x]); inc(x);
      SetItem(x, Source[x]); inc(x);
      SetItem(x, Source[x]); inc(x);
      SetItem(x, Source[x]); inc(x);
      SetItem(x, Source[x]); inc(x);
      SetItem(x, Source[x]); inc(x);
      dec(lFactor);
    end;

    case lFactor mod 8 of
    1:  FMemory.SetItem(x, Source[x]);
    2:  begin
          FMemory.SetItem(x, Source[x]); inc(x);
          FMemory.SetItem(x, Source[x]);
        end;
    3:  begin
          FMemory.SetItem(x, Source[x]); inc(x);
          FMemory.SetItem(x, Source[x]); inc(x);
          FMemory.SetItem(x, Source[x]);
        end;
    4:  begin
          FMemory.SetItem(x, Source[x]); inc(x);
          FMemory.SetItem(x, Source[x]); inc(x);
          FMemory.SetItem(x, Source[x]); inc(x);
          FMemory.SetItem(x, Source[x]);
        end;
    5:  begin
          FMemory.SetItem(x, Source[x]); inc(x);
          FMemory.SetItem(x, Source[x]); inc(x);
          FMemory.SetItem(x, Source[x]); inc(x);
          FMemory.SetItem(x, Source[x]); inc(x);
          FMemory.SetItem(x, Source[x]);
        end;
    6:  begin
          FMemory.SetItem(x, Source[x]); inc(x);
          FMemory.SetItem(x, Source[x]); inc(x);
          FMemory.SetItem(x, Source[x]); inc(x);
          FMemory.SetItem(x, Source[x]); inc(x);
          FMemory.SetItem(x, Source[x]); inc(x);
          FMemory.SetItem(x, Source[x]);
        end;
    7:  begin
          FMemory.SetItem(x, Source[x]); inc(x);
          FMemory.SetItem(x, Source[x]); inc(x);
          FMemory.SetItem(x, Source[x]); inc(x);
          FMemory.SetItem(x, Source[x]); inc(x);
          FMemory.SetItem(x, Source[x]); inc(x);
          FMemory.SetItem(x, Source[x]); inc(x);
          FMemory.SetItem(x, Source[x]);
        end;
    end;

  end;
end;

procedure TVector<T>.Clear;
begin
  FMemory.Clear();
end;

procedure TVector<T>.SetItem(const index: integer; Value: T);
begin
  FMemory.SetItem(Index, Value);
end;

procedure TVector<T>.Swap(const First, Second: integer);
var
  lTemp: T;
  lCount: integer;
begin
  lCount := FMemory.GetCount();
  if lCount > 0 then
  begin
    if (First >=0) and (First < lCount) then
    begin
      if (Second >= 0) and (Second < lCount) then
      begin
        lTemp := FMemory.GetItem(First);
        FMemory.SetItem(First, FMemory.GetItem(Second));
        FMemory.SetItem(Second, lTemp);
      end;
    end;
  end else
  raise EVectorEmpty.Create(CNT_ERR_VECTOR_Empty);
end;

procedure TVector<T>.PushBack(const Value: T);
begin
  FMemory.Add(Value);
end;

procedure TVector<T>.PushFront(const Value: T);
begin
  if GetCount() > 0 then
    Insert(0, Value)
  else
    Add(Value);
end;

function TVector<T>.GetFront: T;
begin
  result := FMemory.GetItem(0);
end;

function TVector<T>.GetBack: T;
begin
  result := FMemory.GetItem( FMemory.GetCount() -1 );
end;

function TVector<T>.Add(const Value: T): integer;
begin
  result := FMemory.Add(Value);
end;

procedure TVector<T>.Remove(const Index: integer);
begin
  FMemory.Remove(Index);
end;

function TVector<T>.Insert(const Index: integer; const Value: T): integer;
begin
  result := FMemory.Insert(Index, Value);
end;

function TVector<T>.GetCount: integer;
begin
  result := FMemory.GetCount();
end;

function TVector<T>.GetItem(const index: integer): T;
begin
  result := FMemory.GetItem(Index);
end;

//#############################################################################
// TVectorAllocatorBuffered
//#############################################################################

procedure TVector<T>.TVectorAllocatorBuffered.Clear;
begin
  GetStorageReference().Release();
end;

procedure TVector<T>.TVectorAllocatorBuffered.Resize(ItemCount: integer);
var
  lCount: integer;
  lBuffer: TStorage;
begin
  lBuffer := GetStorageReference();

  lCount := GetCount();
  if ItemCount <> lCount then
  begin
    if (lCount > 0) and (itemCount < 1) then
    begin
      lBuffer.Release();
      exit;
    end;

    if ItemCount > 0 then
    begin
      try
        lBuffer.Size := SizeOf(T) * ItemCount;
      except
        on e: exception do
        raise EVectorAllocator.CreateFmt(CNT_ERR_VECTOR_ResizeFailed, [ItemCount]);
      end;
    end;

  end;
end;

function TVector<T>.TVectorAllocatorBuffered.GetDataSize: int64;
begin
  result := GetStorageReference().Size;
end;

{$IFDEF BE_SILENT}
  {$WARNINGS OFF}
  {$HINTS OFF}
{$ENDIF}
function TVector<T>.TVectorAllocatorBuffered.GetItem(const index: integer): T;
var
  lCount: integer;
  lBuffer: TStorage;
begin
  lBuffer := GetStorageReference();
  if not lBuffer.Empty then
  begin
    lCount := GetCount();
    if (index >= 0) and (index < lCount ) then
      lBuffer.Read(Index * SizeOf(T), SizeOf(T), result)
    else
    raise EVectorInvalidIndex.CreateFmt(CNT_ERR_VECTOR_InvalidIndex, [0, lCount-1, Index]);
  end else
  raise EVectorEmpty.Create(CNT_ERR_VECTOR_Empty);
end;
{$IFDEF BE_SILENT}
  {$WARNINGS ON}
  {$HINTS ON}
{$ENDIF}

procedure TVector<T>.TVectorAllocatorBuffered.SetItem(const index: integer; Value: T);
var
  lCount: integer;
  lBuffer: TStorage;
begin
  lBuffer := GetStorageReference();
  if not lBuffer.Empty then
  begin
    lCount := GetCount();
    if (index >= 0) and (index < lCount ) then
      lBuffer.Write(Index * SizeOf(T), SizeOf(T), Value)
    else
    raise EVectorInvalidIndex.CreateFmt(CNT_ERR_VECTOR_InvalidIndex, [0, lCount-1, Index]);
  end else
  raise EVectorEmpty.Create(CNT_ERR_VECTOR_Empty);
end;

function TVector<T>.TVectorAllocatorBuffered.GetCount: integer;
var
  lSize:  int64;
begin
  lSize := GetStorageReference().Size;
  if lSize > 0 then
    result := lSize div SizeOf(T)
  else
    result := 0;
end;

function TVector<T>.TVectorAllocatorBuffered.Add(const Value: T): integer;
var
  lBuffer: TStorage;
begin
  lBuffer := GetStorageReference();
  lBuffer.Append(Value, SizeOf(T));
  result := lBuffer.Size div SizeOf(T);
end;

procedure TVector<T>.TVectorAllocatorBuffered.Remove(const Index: integer);
var
  lCount: integer;
  lNewSize: int64;
  lBuffer: TStorage;
begin
  lBuffer := GetStorageReference();
  lCount := GetCount();

  if lCount < 1 then
    raise EVectorEmpty.Create(CNT_ERR_VECTOR_Empty);

  if Index < 0 then
    raise EVectorInvalidIndex.CreateFmt(CNT_ERR_VECTOR_InvalidIndex, [0, lCount-1, Index]);

  if Index > (lCount-1) then
    raise EVectorInvalidIndex.CreateFmt(CNT_ERR_VECTOR_InvalidIndex, [0, lCount-1, Index]);

  lNewSize := lBuffer.Size - SizeOf(T);

  // Will the list be empty after this?
  if lNewSize < 1 then
  begin
    lBuffer.Release();
    exit;
  end;

  // Truncate or scale?
  if Index = lCount-1 then
    lBuffer.Size := lNewSize
  else
    lBuffer.Remove(Index * SizeOf(T), SizeOf(T));
end;

function TVector<T>.TVectorAllocatorBuffered.Insert(const Index: integer; Value: T): integer;
var
  lCount: integer;
  lNewSize: integer;
  lBuffer: TStorage;
begin
  lBuffer := GetStorageReference();
  lCount := GetCount();

  if lCount < 1 then
    raise EVectorEmpty.Create(CNT_ERR_VECTOR_Empty);

  if Index < 0 then
    raise EVectorInvalidIndex.CreateFmt(CNT_ERR_VECTOR_InvalidIndex, [0, lCount-1, Index]);

  // First item? Check that index is 0
  if lBuffer.Size < 1 then
  begin
    if Index = 0 then
    begin
      result := Add(Value);
      exit;
    end else
    raise EVectorInvalidIndex.CreateFmt(CNT_ERR_VECTOR_InvalidIndex, [0, 0, Index]);
  end;

  lBuffer.Insert(Index * SizeOf(T), Value, SizeOf(T));
end;

//#############################################################################
// TVectorAllocatorSequence
//#############################################################################

constructor TVector<T>.TVectorAllocatorSequence.Create;
begin
  inherited Create();
  FStorage := TStorageMemory.Create();
end;

destructor TVector<T>.TVectorAllocatorSequence.Destroy;
begin
  FStorage.Free;
  inherited;
end;

function TVector<T>.TVectorAllocatorSequence.GetStorageReference: TStorage;
begin
  result := FStorage;
end;

procedure TVector<T>.TVectorAllocatorSequence.LockMemory(var Data);
begin
  if FLock < 1 then
  begin
    inc(FLock);
    pointer(Data) := TStorageMemory(FStorage).Data;
  end else
  raise EVectorAllocatorLocked.Create(CNT_ERR_VECTOR_MemLocked);
end;

procedure TVector<T>.TVectorAllocatorSequence.UnLockMemory;
begin
  if FLock > 0 then
    dec(FLock)
  else
  raise EVectorAllocatorNotLocked.Create(CNT_ERR_VECTOR_MemUnLocked);
end;

//#############################################################################
// TVector<T>.TVectorAllocatorFile
//#############################################################################

constructor TVector<T>.TVectorAllocatorFile.Create(Filename: string; AccessFlags: word);
begin
  inherited Create;
  FName := Filename;
  FMode := AccessFlags;
  FStorage := TStorageFile.Create(FName, AccessFlags);
end;

destructor TVector<T>.TVectorAllocatorFile.Destroy;
begin
  FStorage.free;
  inherited;
end;

function TVector<T>.TVectorAllocatorFile.GetStorageReference: TStorage;
begin
  result := FStorage;
end;

procedure TVector<T>.TVectorAllocatorFile.LockMemory(var Data);
begin
  raise EVectorUnSupported.Create(CNT_ERR_VECTOR_LockNotSupported);
end;

procedure TVector<T>.TVectorAllocatorFile.UnLockMemory;
begin
  raise EVectorUnSupported.Create(CNT_ERR_VECTOR_LockNotSupported);
end;

//#############################################################################
// TVector<T>.TVectorAllocatorDynamic
//#############################################################################

destructor TVector<T>.TVectorAllocatorDynamic.Destroy;
begin
  if length(FValues) > 0 then
    Clear();
  inherited;
end;

function TVector<T>.TVectorAllocatorDynamic.Add(const Value: T): integer;
begin
  result := length(FValues);
  SetLength(FValues, result + 1);
  FValues[result] := Value;
  inc(result);
end;

procedure TVector<T>.TVectorAllocatorDynamic.Resize(ItemCount: integer);
begin
  if ItemCount >= 0 then
    setlength(FValues, ItemCount)
  else
    EVectorAllocator.CreateFmt(CNT_ERR_VECTOR_ResizeFailed, [ItemCount]);
end;

function TVector<T>.TVectorAllocatorDynamic.GetDataSize: int64;
begin
  result := SizeOf(FValues);
end;

procedure TVector<T>.TVectorAllocatorDynamic.LockMemory(var Data);
begin
  if FLock < 1 then
  begin
    inc(FLock);
    Pointer(Data) := @FValues[0];
  end else
  raise EVectorAllocatorLocked.Create(CNT_ERR_VECTOR_MemLocked);
end;

procedure TVector<T>.TVectorAllocatorDynamic.UnLockMemory;
begin
  if FLock > 0 then
    dec(FLock)
  else
  raise EVectorAllocatorNotLocked.Create(CNT_ERR_VECTOR_MemUnLocked);
end;

procedure TVector<T>.TVectorAllocatorDynamic.Clear;
begin
  setlength(FValues, 0);
end;

function TVector<T>.TVectorAllocatorDynamic.GetCount: integer;
begin
  result := length(FValues);
end;

function TVector<T>.TVectorAllocatorDynamic.GetItem(const index: integer): T;
begin
  result := FValues[Index];
end;

procedure TVector<T>.TVectorAllocatorDynamic.SetItem(const index: integer; Value: T);
begin
  FValues[Index] := Value;
end;

function TVector<T>.TVectorAllocatorDynamic.Insert(const Index: integer; Value: T): integer;
begin
  system.insert(Value, FValues, Index);
  result := length(FValues);
end;

procedure TVector<T>.TVectorAllocatorDynamic.Remove(const Index: integer);
begin
  system.delete(FValues, Index, 1);
end;

//#############################################################################
// TVector<T>.TVectorAllocatorMapped
//#############################################################################

constructor TVector<T>.TVectorAllocatorMapped.Create(const Buffer: TStorage);
begin
  inherited Create();
  FStorage := Buffer;
end;

destructor TVector<T>.TVectorAllocatorMapped.Destroy;
begin
  FStorage := nil;
  inherited;
end;

function TVector<T>.TVectorAllocatorMapped.GetStorageReference: TStorage;
begin
  result := FStorage;
end;

function TVector<T>.TVectorAllocatorMapped.Add(const Value: T): integer;
begin
  raise EVectorReadOnly.Create(CNT_ERR_VECTOR_ReadOnly);
end;

procedure TVector<T>.TVectorAllocatorMapped.Resize(ItemCount: integer);
begin
  raise EVectorReadOnly.Create(CNT_ERR_VECTOR_ReadOnly);
end;

function TVector<T>.TVectorAllocatorMapped.GetDataSize: int64;
begin
  result := FStorage.Size;
end;

procedure TVector<T>.TVectorAllocatorMapped.LockMemory(var Data);
begin
  raise EVectorReadOnly.Create(CNT_ERR_VECTOR_ReadOnly);
end;

procedure TVector<T>.TVectorAllocatorMapped.UnLockMemory;
begin
  raise EVectorReadOnly.Create(CNT_ERR_VECTOR_ReadOnly);
end;

procedure TVector<T>.TVectorAllocatorMapped.Clear;
begin
  raise EVectorReadOnly.Create(CNT_ERR_VECTOR_ReadOnly);
end;

function TVector<T>.TVectorAllocatorMapped.GetCount: integer;
begin
  if FStorage.Size > 0 then
    result := FStorage.Size div SizeOf(T)
  else
    result := 0;
end;

function TVector<T>.TVectorAllocatorMapped.GetItem(const index: integer): T;
begin
  FStorage.Read(SizeOf(T) * Index, SizeOf(T), result);
end;

procedure TVector<T>.TVectorAllocatorMapped.SetItem(const index: integer; Value: T);
begin
  raise EVectorReadOnly.Create(CNT_ERR_VECTOR_ReadOnly);
end;

function TVector<T>.TVectorAllocatorMapped.Insert(const Index: integer; Value: T): integer;
begin
  raise EVectorReadOnly.Create(CNT_ERR_VECTOR_ReadOnly);
end;

procedure TVector<T>.TVectorAllocatorMapped.Remove(const Index: integer);
begin
  raise EVectorReadOnly.Create(CNT_ERR_VECTOR_ReadOnly);
end;

//#############################################################################
// TVector<T>.TVectorEnumerator
//#############################################################################

constructor TVector<T>.TVectorEnumerator.Create(Owner: TVector<T>);
begin
  inherited Create;
  FParent := Owner;
  FIndex := -1;
end;

function TVector<T>.TVectorEnumerator.DoGetCurrent: T;
begin
  result := FParent[FIndex];
end;

function TVector<T>.TVectorEnumerator.DoMoveNext: boolean;
begin
  result := (FIndex + 1) < FParent.GetCount();
  if result then
    inc(FIndex);
end;


end.

