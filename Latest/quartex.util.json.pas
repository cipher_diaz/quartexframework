unit quartex.util.json;

interface

uses
  System.Rtti, 
  System.TypInfo,
  System.Sysutils,
  System.Classes,
  System.Generics.Collections, 
  System.Json,
  REST.Json, 
  REST.JSon.Types,
  REST.JsonReflect,
  quartex.util.register;

type

  EQTXASTJSONError = class(Exception);

  [TQTXAutoRegister]
  TQTXJSONPersistent = class(TPersistent)
  protected
    procedure WriteObjectJSon(const Target: TJSONObject); virtual;
    procedure ReadObjectJSon(const Source: TJSONObject); virtual;
  public
    procedure Assign(Source: TPersistent); override;
    function  Serialize: string; virtual;
    procedure Parse(ObjectData: string); virtual;
    class function JSONToObject(MetaData: string; var obj: TObject): boolean;
  end;

  EQTXGUIDError = class(Exception);

  TQTXGUID = class
  public
    class function  GUIDToStringA(GUID: TGUID): AnsiString; static;
    class function  StringToGUIDA(Value: AnsiString): TGUID; static;

    class function  GUIDToString(GUID: TGUID): string; static;
    class function  StringToGUID(Value: string): TGUID; static;

    class function  CreateGUID: string; static;
    class function  CreateGUIDA: AnsiString; static;
  end;

implementation


//##########################################################################
// TQTXJSONPersistent
//##########################################################################

class function TQTXGUID.CreateGUID: string;
var
  lGUID: TGUID;
begin
  System.Sysutils.CreateGUID(lGUID);
  result := GUIDToString(lGUID);
end;

class function TQTXGUID.CreateGUIDA: AnsiString;
var
  lGUID: TGUID;
begin
  System.Sysutils.CreateGUID(lGUID);
  result := GUIDToStringA(lGUID);
end;

class function TQTXGUID.GUIDToString(GUID: TGUID): string;
begin
  result := string( GUIDToStringA(GUID) );
end;

class function TQTXGUID.StringToGUID(Value: string): TGUID;
begin
  result := StringToGUIDA( AnsiString(Value) );
end;

class function TQTXGUID.GUIDToStringA(GUID: TGUID): AnsiString;
begin
  SetLength(result, 38);
  StrLFmt(@result[1], 38, '{%.8x-%.4x-%.4x-%.2x%.2x-%.2x%.2x%.2x%.2x%.2x%.2x}',
    [GUID.D1, GUID.D2, GUID.D3, GUID.D4[0], GUID.D4[1], GUID.D4[2], GUID.D4[3],
    GUID.D4[4], GUID.D4[5], GUID.D4[6], GUID.D4[7]]);
end;

class function TQTXGUID.StringToGUIDA(Value: AnsiString): TGUID;
Resourcestring
  ERR_InvalidGUID = '[%s] is not a valid GUID value';
var
  i: integer;
  src, dest: PAnsiChar;

  function _HexChar(const c: AnsiChar): byte;
  begin
    case c of
      '0' .. '9': result := byte(c) - byte('0');
      'a' .. 'f': result := (byte(c) - byte('a')) + 10;
      'A' .. 'F': result := (byte(c) - byte('A')) + 10;
      else
        raise EQTXGUIDError.CreateFmt(ERR_InvalidGUID, [Value]);
    end;
  end;

  function _Hexbyte(const P: PAnsiChar): AnsiChar;
  begin
    result := AnsiChar((_HexChar(P[0]) shl 4) + _HexChar(P[1]));
  end;

begin
  if length(Value) = 38 then
  begin
    dest := @result;
    src := PAnsiChar(Value);
    Inc(src);

    for i := 0 to 3 do
      dest[i] := _Hexbyte(src + (3 - i) * 2);

    Inc(src, 8);
    Inc(dest, 4);
    if src[0] <> '-' then
      raise EQTXGUIDError.CreateFmt(ERR_InvalidGUID, [Value]);

    Inc(src);
    for i := 0 to 1 do
    begin
      dest^ := _Hexbyte(src + 2);
      Inc(dest);
      dest^ := _Hexbyte(src);
      Inc(dest);
      Inc(src, 4);
      if src[0] <> '-' then
        raise EQTXGUIDError.CreateFmt(ERR_InvalidGUID, [Value]);
      Inc(src);
    end;

    dest^ := _Hexbyte(src);
    Inc(dest);
    Inc(src, 2);
    dest^ := _Hexbyte(src);
    Inc(dest);
    Inc(src, 2);
    if src[0] <> '-' then
      raise EQTXGUIDError.CreateFmt(ERR_InvalidGUID, [Value]);

    Inc(src);
    for i := 0 to 5 do
    begin
      dest^ := _Hexbyte(src);
      Inc(dest);
      Inc(src, 2);
    end;
  end else
  raise EQTXGUIDError.CreateFmt(ERR_InvalidGUID, [Value]);
end;

//##########################################################################
// TQTXJSONPersistent
//##########################################################################
 
procedure TQTXJSONPersistent.Assign(Source: TPersistent);
begin
  if assigned(source) then
  begin
    if ( (source is Classtype) or (source.InheritsFrom(ClassType)) ) then
      Parse( TQTXJSONPersistent(Source).Serialize )
    else
      inherited Assign(Source);
  end else
  inherited Assign(Source);
end;
 
function TQTXJSONPersistent.Serialize: string;
var
  lData: TJSonObject;
begin
  lData := TJsonObject.Create;
  try
    WriteObjectJSon(lData);
  finally
    result := TJSon.Format(lData);
  end;
end;
 
procedure TQTXJSONPersistent.Parse(ObjectData: string);
var
  lSchema: TJsonObject;
  lObjData: TJsonObject;
  lEntry: TJSonObject;
  lId: string;

resourcestring
  err_json_parse_failed_general = 'Serialization failed, system threw exception %s with message "%s"';
  err_json_parse_failed_data = 'Serialization failed, unable to find section ["%s\$data"] in JSON document';
  err_json_parse_failed_signature = 'Serialization failed, invalid signature, expected %s not %s';
  err_json_parse_failed_section = 'Serialization failed, unable to find section ["%s"] in JSON document';

begin
  setlength(lId, 0);
  lSchema := TJSonObject( TJSonObject.ParseJSONValue(ObjectData, true) );
  try

    if lSchema.Values[QualifiedClassName] <> nil then
    begin
      // Find object entrypoint
      lEntry := TJsonObject( lSchema.GetValue(QualifiedClassName) );

      // attempt to get the identifier
      if lEntry.Values['$identifier'] <> nil then
        lId := LEntry.GetValue('$identifier').Value;

      // validate identifier
      if lId.Equals(Classname) then
      begin
        // Get a reference to our data chunk
        lObjData := TJSonObject( LEntry.GetValue('$data') );
        if lObjData <> nil then
        begin
          // Map values into our instance
          try
            ReadObjectJSon( lObjData );
          except
            on e: exception do
            raise EQTXASTJSONError.CreateFmt(err_json_parse_failed_general, [e.ClassName, e.Message]);
          end;
        end else
        raise EQTXASTJSONError.CreateFmt(err_json_parse_failed_data,[QualifiedClassName]);
      end else
      raise EQTXASTJSONError.CreateFmt(err_json_parse_failed_signature, [classname, LId]);
    end else
    raise EQTXASTJSONError.CreateFmt(err_json_parse_failed_section, [QualifiedClassName]);
  finally
    LSchema.Free;
  end;
end;
 
procedure TQTXJSONPersistent.WriteObjectJSon(const Target: TJSONObject);
begin
  //
end;
 
procedure TQTXJSONPersistent.ReadObjectJSon(const Source: TJSONObject);
begin
  //TJSon.JsonToObject(self, Source);
end;
 
// This function will create any registered class based on name.
// The class must be registered first.
class function TQTXJSONPersistent.JSONToObject(MetaData: string; var obj: TObject): boolean;
var
  LSchema: TJsonObject;
  LClassName: string;
  LType: TClass;
  LNameNode: TJSONValue;
  LObj: TQTXJSONPersistent;
begin
  result := false;
  obj := nil;
 
  // Parse whole schema
  LSchema := TJSonObject( TJSonObject.ParseJSONValue(MetaData, true) );
  try
    LNameNode := LSchema.Values['$classname$'];
    if LNameNode <> nil then
    begin
      LClassName := LNameNode.Value;
      LType := GetClass(LClassName);
      if LType <> nil then
      begin
        LObj := TQTXJSONPersistent( LType.Create );
        LObj.Parse(MetaData);
        obj := LObj;
        result := true;
      end;
    end;
  finally
    LSchema.Free;
  end;
end;

end.