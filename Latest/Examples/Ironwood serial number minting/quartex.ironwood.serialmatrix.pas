unit quartex.ironwood.serialmatrix;

interface

uses
  System.Sysutils,
  System.Classes,
  System.Math,
  quartex.util.noref,
  quartex.ironwood.types;

type

  IHexSerialMatrix = interface
    ['{D562EC1E-6254-45E6-87C7-18CF16CF84B3}']
    function GetSerialMatrix(var Value: THexKeyMatrix): boolean;
  end;

  THexGetSerialMatrixEvent = procedure (sender: TObject; var Matrix: THexKeyMatrix) of object;

  THexSerialMatrix = class(TNonReferenceCountedObject, IHexSerialMatrix)
  strict private
    FOnGetMatrix: THexGetSerialMatrixEvent;
  strict protected
    function  GetSerialMatrix(var Value: THexKeyMatrix): boolean;
  public
    property  OnGetMatrix: THexGetSerialMatrixEvent read FOnGetMatrix write FOnGetMatrix;
  end;


implementation

//############################################################################
// THexSerialMatrix
//############################################################################

function THexSerialMatrix.GetSerialMatrix(var Value: THexKeyMatrix): boolean;
var
  lTemp: THexKeyMatrix;
begin
  fillchar(value, SizeOf(Value), 0);
  result := assigned(OnGetMatrix);
  if result then
  begin
    OnGetMatrix(self, lTemp);
    Value := LTemp;
  end;
end;



end.

