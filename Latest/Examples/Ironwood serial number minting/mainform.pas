unit mainform;

interface

uses
  Winapi.Windows, Winapi.Messages,

  System.SysUtils,
  System.Variants,
  System.Classes,

  quartex.util.noref,
  quartex.ironwood,

  Vcl.Graphics,
  Vcl.Controls,
  Vcl.Forms,
  Vcl.Dialogs,
  Vcl.StdCtrls,
  Vcl.ComCtrls, Vcl.ExtCtrls;

type
  TForm1 = class(TForm)
    ListView1: TListView;
    Panel1: TPanel;
    Label1: TLabel;
    TrackBar1: TTrackBar;
    lbCount: TLabel;
    Button1: TButton;
    StatusBar1: TStatusBar;
    procedure Button1Click(Sender: TObject);
    procedure TrackBar1Change(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
    procedure AcceptSerial(sender: TObject; SerialNumber: string; var Accept: boolean);
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.FormCreate(Sender: TObject);
begin
  TrackBar1Change(TrackBar1);
end;

procedure TForm1.AcceptSerial(sender: TObject; SerialNumber: string; var Accept: boolean);

  function SerialNumberExists(Value: string): boolean;
  var
    x: integer;
  begin
    result := false;
    for x := 0 to ListView1.Items.Count-1 do
    begin
      result := ListView1.Items[x].Caption = Value;
      if result then
        break;
    end;
  end;

begin
  Accept := not SerialNumberExists(SerialNumber);
  if Accept then
  begin
    if THexIronwoodGenerator(Sender).Validate(SerialNumber) then
    begin
      ListView1.Items.Add().Caption := SerialNumber;
      if ListView1.Items.Count mod 100 = 99 then
      begin
        Statusbar1.SimpleText := IntToStr(ListView1.Items.Count) + ' items minted';
        application.ProcessMessages();
      end;
    end else
      Accept := false;
  end;
end;

procedure TForm1.Button1Click(Sender: TObject);
const
  CNT_ROOTKEY: THexKeyMatrix = ($32,$63,$6F,$35,$2A,$A5,$EF,$34,$4D,$A0,$0B,$95);
var
  lGenerator: THexIronwoodGenerator;
begin
  screen.Cursor := crHourGlass;
  listview1.Items.BeginUpdate();
  try
    listview1.Items.Clear();

    lGenerator := THexIronwoodGenerator.Create();
    try
      lGenerator.Modulator := THexFibonacciModulator.Create();
      lGenerator.Obfuscaton := THexObfuscationTypes.Sepsephos();
      lGenerator.MintMethod := THexSerialGenerateMethod.gmCanonical;
      lGenerator.Build(CNT_ROOTKEY);
      lGenerator.OnAcceptSerialNumber := AcceptSerial;

      lGenerator.Mint(TrackBar1.Position);
    finally
      lGenerator.Free;
    end;

  finally
    listview1.items.EndUpdate();
    screen.Cursor := crDefault;
    Statusbar1.SimpleText := IntToStr(ListView1.Items.Count) + ' items minted';
  end;
end;

procedure TForm1.TrackBar1Change(Sender: TObject);
begin
  lbCount.Caption := IntToStr(TrackBar1.Position) + ' Serial numbers';
end;

end.
