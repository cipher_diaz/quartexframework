unit quartex.ironwood.obfuscation;

interface

uses
  System.Sysutils, System.Classes;

type

  THexObfuscationMatrix = packed array [0..25] of integer;

  THexObfuscation = class;

  THexObfuscationTypes = class
  public
    class function Sepsephos: THexObfuscation;
    class function Hebrew: THexObfuscation;
    class function Latin: THexObfuscation;
    class function Beatus: THexObfuscation;
    class function Amun: THexObfuscation;
  end;

  THexObfuscation = class(TObject)
  strict private
    FMatrix:    THexObfuscationMatrix;
  strict protected
    function    GetMatrix: THexObfuscationMatrix; virtual; abstract;
    function    FindInMatrix(const Value: integer): integer;
  public
    class function Types(): THexObfuscationTypes;
    function    Encode(const Data: uint8): uint8; overload; virtual;
    function    Encode(Text: string): string; overload; virtual;

    function    Decode(const Data: uint8): uint8; overload; virtual;
    function    Decode(Text: string): string; overload; virtual;

    procedure   Clear;
    function    Ready: boolean;
    procedure   Load;
    constructor Create; virtual;
  end;

  THexObfuscationSepsephos = class(THexObfuscation)
  strict protected
    function GetMatrix: THexObfuscationMatrix; override;
  end;

  THexObfuscationHebrew = class(THexObfuscation)
  strict protected
    function GetMatrix: THexObfuscationMatrix; override;
  end;

  THexObfuscationBeatus = class(THexObfuscation)
  strict protected
    function GetMatrix: THexObfuscationMatrix; override;
  end;

  THexObfuscationLatin = class(THexObfuscation)
  strict protected
    function GetMatrix: THexObfuscationMatrix; override;
  end;

  THexObfuscationAmun = class(THexObfuscation)
  strict protected
    function GetMatrix: THexObfuscationMatrix; override;
  end;

implementation

//############################################################################
// THexObfuscationTypes
//############################################################################

class function THexObfuscationTypes.Sepsephos: THexObfuscation;
begin
  result := THexObfuscationSepsephos.Create;
end;

class function THexObfuscationTypes.Hebrew: THexObfuscation;
begin
  result := THexObfuscationHebrew.Create;
end;

class function THexObfuscationTypes.Latin: THexObfuscation;
begin
  result := THexObfuscationLatin.Create;
end;

class function THexObfuscationTypes.Beatus: THexObfuscation;
begin
  result := THexObfuscationBeatus.Create;
end;

class function THexObfuscationTypes.Amun: THexObfuscation;
begin
  result := THexObfuscationAmun.Create;
end;

//############################################################################
// THexObfuscation
//############################################################################

constructor THexObfuscation.Create;
begin
  inherited Create;
  FMatrix := GetMatrix();
end;

class function THexObfuscation.Types(): THexObfuscationTypes;
begin
  result := THexObfuscationTypes.Create();
end;

function THexObfuscation.Ready: boolean;
var
  item: integer;
  LSum: integer;
begin
  lSum := 0;
  for Item in FMatrix do
  begin
    inc(LSum, Item);
  end;
  result := LSum > 0;
end;

procedure THexObfuscation.Load;
begin
  FMatrix := GetMatrix();
end;

procedure THexObfuscation.Clear;
begin
  fillchar(FMatrix, SizeOf(FMatrix), 0);
  //FMatrix := [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0];
end;

function THexObfuscation.FindInMatrix(const Value: integer): integer;
var
  x: integer;
begin
  result := -1;
  for x:=low(FMatrix) to high(FMatrix) do
  begin
    if FMatrix[x] = Value then
    begin
      result := x;
      break;
    end;
  end;
end;

function THexObfuscation.Encode(const Data: uint8): uint8;
const
  CNT_CHARSET = '0123456789abcdef';
var
  LText: string;
  LTemp: string;
  xpos: integer;
  lChar: char;
begin
  LText := IntToHex(Data,2);
  for LChar in LText do
  begin
    xpos := pos(LChar, CNT_CHARSET);
    LTemp := LTemp + IntToHex(FMatrix[xpos],1);
  end;
  //result := HexToInt('$' + LTemp);
  result := StrToInt('$' + lTemp);
end;

function THexObfuscation.Encode(Text: string): string;
const
  CNT_CHARSET = 'abcdefghijklmnopqrstuvwxyz';
var
  xpos: integer;
  x: integer;
  LValue: integer;
  lChar: char;

  function CharCodeFor(Value: char): integer;
  begin
    result := ord(value);
    //result := byte( PByte(@value)^ );
  end;

begin
  Text := Text.trim().toLower();
  if Text.length > 0 then
  begin
    for x:=low(Text) to high(Text) do
    begin
      LChar := lowercase(Text)[x];
      xpos := pos(LChar, CNT_CHARSET);
      if xpos>0 then
      begin
        dec(xpos);
        LValue := FMatrix[xpos];
        result := result + IntToHex(LValue,4);
      end else
      begin
        result := result + ( 'C932' + IntToHex( CharCodeFor(LChar) ,2) );
      end;
    end;
  end;
end;

function THexObfuscation.Decode(Text: string): string;
begin
  result := '';
end;

function THexObfuscation.Decode(const Data: uint8): uint8;
const
  CNT_CHARSET = '0123456789abcdef';
var
  LText: string;
  LTemp: string;
  xpos: integer;
  lChar: char;
  lValue: integer;
begin
  LText := IntToHex(Data,2);
  for lChar in LText do
  begin
    lValue := StrToInt('$' + LChar); // HexToInt()
    xpos := FindInMatrix(LValue);

    // If the value is not in the gematria matrix, the value
    // is either not encoded, or encoded using a different gematria matrix
    // Exit stage right ..
    if xpos < 0 then
    begin
      result := Data;
      exit;
    end;

    LTemp := LTemp + CNT_CHARSET[xpos];
  end;
  result := StrToInt('$' + lTemp);
  //result := HexToInt('$' + LTemp);
end;

//############################################################################
// THexObfuscationSepsephos
//############################################################################

function THexObfuscationSepsephos.GetMatrix: THexObfuscationMatrix;
begin
  result[0] := 1;
  result[1] := 2;
  result[2] := 3;
  result[3] := 4;
  result[4] := 5;
  result[5] := 6;
  result[6] := 7;
  result[7] := 8;
  result[8] := 10;
  result[9] := 100;
  result[10] := 10;
  result[11] := 20;
  result[12] := 30;
  result[13] := 40;
  result[14] := 50;
  result[15] := 3;
  result[16] := 70;
  result[17] := 80;
  result[18] := 200;
  result[19] := 300;
  result[20] := 400;
  result[21] := 6;
  result[22] := 80;
  result[23] := 60;
  result[24] := 10;
  result[25] := 800;
  //result := ([1,2,3,4,5,6,7,8,10,100,10,20,30,40,50,3,70,80,200,300,400,6,80,60,10, 800]);
end;

//############################################################################
// THexObfuscationHebrew
//############################################################################

function THexObfuscationHebrew.GetMatrix: THexObfuscationMatrix;
begin
  result[0] := 0;
  result[1] := 2;
  result[2] := 100;
  result[3] := 4;
  result[4] := 0;
  result[5] := 80;
  result[6] := 3;
  result[7] := 5;
  result[8] := 10;
  result[9] := 10;
  result[10] := 20;
  result[11] := 30;
  result[12] := 40;
  result[13] := 50;
  result[14] := 0;
  result[15] := 80;
  result[16] := 100;
  result[17] := 200;
  result[18] := 300;
  result[19] := 9;
  result[20] := 6;
  result[21] := 6;
  result[22] := 6;
  result[23] := 60;
  result[24] := 10;
  result[25] := 7;
  //result := [0,2,100,4,0,80,3,5,10,10,20,30,40,50,0,80,100,200,300,9,6,6,6,60,10,7];
end;

//############################################################################
// THexObfuscationBeatus
//############################################################################

function THexObfuscationBeatus.GetMatrix: THexObfuscationMatrix;
begin
  result[0] := 1;
  result[1] := 2;
  result[2] := 90;
  result[3] := 4;
  result[4] := 5;
  result[5] := 6;
  result[6] := 7;
  result[7] := 8;
  result[8] := 10;
  result[9] := 10;
  result[10] := 20;
  result[11] := 30;
  result[12] := 40;
  result[13] := 50;
  result[14] := 70;
  result[15] := 80;
  result[16] := 100;
  result[17] := 200;
  result[18] := 300;
  result[19] := 400;
  result[20] := 6;
  result[21] := 6;
  result[22] := 6;
  result[23] := 60;
  result[24] := 10;
  result[25] := 7;
  //result := [1,2,90,4,5,6,7,8,10,10,20,30,40,50,70,80,100,200,300,400,6,6,6,60,10,7];
end;

//############################################################################
// THexObfuscationLatin
//############################################################################

function THexObfuscationLatin.GetMatrix: THexObfuscationMatrix;
begin
  result[0] := 1;
  result[1] := 2;
  result[2] := 700;
  result[3] := 4;
  result[4] := 5;
  result[5] := 500;
  result[6] := 3;
  result[7] := 8;
  result[8] := 10;
  result[9] := 10;
  result[10] := 20;
  result[11] := 30;
  result[12] := 40;
  result[13] := 50;
  result[14] := 70;
  result[15] := 80;
  result[16] := 600;
  result[17] := 100;
  result[18] := 200;
  result[19] := 300;
  result[20] := 400;
  result[21] := 6;
  result[22] := 800;
  result[23] := 60;
  result[24] := 10;
  result[25] := 7;
  //result := [1,2,700,4,5,500,3,8,10,10,20,30,40,50,70,80,600,100,200,300,400,6,800,60,10,7];
end;

//############################################################################
// THexObfuscationAmun
//############################################################################

function THexObfuscationAmun.GetMatrix: THexObfuscationMatrix;
begin
  result[0] := 1;
  result[1] := 19;
  result[2] := 46;
  result[3] := 21;
  result[4] := 09;
  result[5] := 19;
  result[6] := 73;
  result[7] := 31;
  result[8] := 18;
  result[9] := 60;
  result[10] := 12;
  result[11] := 17;
  result[12] := 37;
  result[13] := 4;
  result[14] := 7;
  result[15] := 8;
  result[16] := 17;
  result[17] := 13;
  result[18] := 244;
  result[19] := 364;
  result[20] := 496;
  result[21] := 512;
  result[22] := 122;
  result[23] := 196;
  result[24] := 291;
  result[25] := 600;
  //result := [1,19,46,21,09,19,73,31,18,60,12,17,37,4,7,8,17,13,244,364,496,512,122,196,291,600];
end;

end.

