unit quartex.ironwood.partition;

interface

uses
  System.Sysutils,
  System.Classes,
  System.Math,
  quartex.ironwood.types,
  quartex.ironwood.modulators;

type

  THexIronwoodPartition = class(TObject)
  strict private
    FFormula:   THexNumberModulator;
    FGates:     array of byte;
    FRange:     THexRange;
    FGateTotal: integer;
    FIndex:     integer;
  strict protected
    function    GetGateCount: integer; inline;
    function    AddGate(Value: integer): integer; inline;
    function    IndexOfGate(Value: integer): integer; inline;

    procedure   SetIndex(const index: integer);
    function    GetGate(const Index: integer): byte;
    procedure   SetGate(const Index: integer; const Value: byte);
  public
    property    Index: integer read FIndex write SetIndex;
    property    Modulator: THexNumberModulator read FFormula write FFormula;
    property    Gates[const Index: integer]: byte read GetGate write SetGate;
    property    GateCount: integer read GetGateCount;
    property    Range: THexRange read FRange;

    function    ToString: string; override;
    function    ToArray: TArray<integer>;

    function    Valid(const Data: byte): boolean;
    procedure   Build(const RootKeyValue: byte);

    constructor Create(const PartitionGateCount: integer);virtual;
    destructor  Destroy;override;
  end;

  THexIronwoodPartitions = array of THexIronwoodPartition;

implementation


//############################################################################
// THexIronwoodPartition
//############################################################################

constructor THexIronwoodPartition.Create(const PartitionGateCount: integer);
begin
  inherited create;
  FRange := THexRange.Create(0,255);

  // 64 is the optimal number of gates per partition, but we allow for
  // a minimal of 16 gates to be used - and a maximum of 128.
  // This means that half of the combinations [0..255] are invalid to
  // begin with, so this makes it much harder to figure out the non-linear
  // gate-numbers a partition contains
  FGateTotal := System.Math.EnsureRange(PartitionGateCount, 16, 128); //64;
end;

destructor THexIronwoodPartition.Destroy;
begin
  FRange.free;
  inherited;
end;

function THexIronwoodPartition.GetGateCount: integer;
begin
  result := length(FGates);
end;

function THexIronwoodPartition.ToArray: TArray<integer>;
var
  index: integer;
begin
  result := TArray<integer>.Create();

  SetLength(result, Length(FGates) );
  for index := 0 to length(FGates)-1 do
  begin
    result[index] := FGates[index];
  end;

  { for lItem in FGates do
  begin
    result.add(LItem);
  end; }
end;

procedure THexIronwoodPartition.SetIndex(const index: integer);
begin
  FIndex := System.Math.EnsureRange(Index, 0, 255);
end;

function THexIronwoodPartition.IndexOfGate(Value: integer): integer;
var
  x: integer;
begin
  result := -1;
  if length(FGates) > 0 then
  begin
    for x := low(FGates) to high(FGates) do
    begin
      if FGates[x] = Value then
      begin
        result := x;
        break;
      end;
    end;
  end;
end;

function THexIronwoodPartition.Valid(const Data: byte): boolean;
begin
  if Length(FGates) > 0 then
    result := IndexOfGate(Data) >= 0
  else
    result := false;
end;

function THexIronwoodPartition.GetGate(const Index: integer): byte;
begin
  result := FGates[index]
end;

procedure THexIronwoodPartition.SetGate(const Index: integer; const Value: byte);
begin
  FGates[index] := value;
end;

function THexIronwoodPartition.ToString: string;
var
  x: integer;
  lText: string;
begin
  for x := low(FGates) to high(FGates) do
  begin
    ltext := string( FGates[x].ToHexString(2) ).ToLower();
    result := result + lText;
    if x < high(FGates) then
      result := result + '-';
  end;
end;

function THexIronwoodPartition.AddGate(Value: integer): integer;
begin
  result := GetGateCount();
  SetLength(FGates, result + 1 );
  FGates[result] := Value;
end;

procedure THexIronwoodPartition.Build(const RootKeyValue: byte);
var
  x:  integer;
  lValue: integer;
begin
  Setlength(FGates, 0);
  if FFormula <> nil then
  begin
    x := 1;

    while ( GetGateCount() < FGateTotal) do
    begin
      LValue :=  (RootKeyValue + FFormula.Data[x] -x) mod FRange.Top;
      if IndexOfGate(lValue) < 0 then
        AddGate(lValue);

      inc(x);
      if x > 255 then
        x := 1;
    end;

  end;
end;


end.

