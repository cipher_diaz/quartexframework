unit quartex.ironwood.serialnumber;

interface

uses
  System.Sysutils,
  System.Classes,
  System.Math,
  quartex.util.noref,

  quartex.ironwood.types,
  quartex.ironwood.serialmatrix,
  quartex.ironwood.obfuscation,
  quartex.ironwood.modulators,
  quartex.ironwood.partition;

type


  THexCustomSerialNumber = class(TObject)
  public
    function Validate(SerialNumber: string): boolean; virtual; abstract;
  end;

  THexIronWoodSerialNumber = class(THexCustomSerialNumber)
  strict private
    FGateCount:   integer;
    FRootData:    THexKeyMatrix;
    FModulator:   THexNumberModulator;
    FObfuscator:  THexObfuscation;
    FPartitions:  THexIronwoodPartitions;
  strict protected
    function    CanUseRootData: boolean;
    procedure   SetGateCount(const Value: integer); virtual;
    procedure   RebuildPartitionTable;
  public
    property    Modulator: THexNumberModulator read FModulator write FModulator;
    property    Obfuscaton: THexObfuscation read FObfuscator write FObfuscator;
    property    Partitions: THexIronwoodPartitions read FPartitions;
    procedure   Build(const Rootkey: THexKeyMatrix);
    function    GetSignature: string;

    function    Validate(SerialNumber: string): boolean; override;

    constructor Create; virtual;
    destructor  Destroy; override;
  end;

implementation

//############################################################################
// THexIronWoodSerialNumber
//############################################################################

constructor THexIronWoodSerialNumber.Create;
begin
  inherited create;
  // Out of 256 gates, we allow 64 to be actually used
  FGateCount := 64;
end;

destructor THexIronWoodSerialNumber.Destroy;
var
  x: integer;
begin
  try
    for x := low(FPartitions) to high(FPartitions) do
    begin
      FPartitions[x].free;
      FPartitions[x] := nil;
    end;
  finally
    Setlength(FPartitions, 0);
  end;
  inherited;
end;

function THexIronWoodSerialNumber.CanUseRootData: boolean;
var
  lFilled: byte;
  x, lTop: integer;
begin
  lFilled := 0;
  lTop := length(FRootData);
  dec(LTop, 2);

  for x := low(FRootData) to high(FRootData) do
  begin
    if FRootData[x] <> 0 then
      inc(lFilled);
  end;

  result := lFilled >= lTop;
end;

procedure THexIronWoodSerialNumber.SetGateCount(const Value: integer);
begin
  FGateCount := System.Math.EnsureRange(Value, 16, 128);
end;

procedure THexIronWoodSerialNumber.RebuildPartitionTable;
var
  x: integer;
  lIndex: integer;
  lPartition: THexIronwoodPartition;
begin
  Setlength(FPartitions, 0);

  if CanUseRootData() then
  begin
    for x := 1 to 12 do
    begin
      lPartition := THexIronwoodPartition.Create(FGateCount);
      lPartition.Modulator := Modulator;

      lIndex := length(FPartitions);
      SetLength(FPartitions, lIndex + 1);
      FPartitions[lIndex] := lPartition;

      // Only issue a rebuild if a modulator has been assigned
      if (Modulator <> nil) then
      begin
        // Rebuild gate array
        lPartition.Build( FRootData[x-1] );
      end else
      raise Exception.Create('No modulator assigned, failed to rebuild partition-table error');
    end;
  end else
  raise Exception.Create('Unsuitable root-key, failed to rebuild partition-table error');
end;

procedure THexIronWoodSerialNumber.Build(const Rootkey: THexKeyMatrix);
begin
  FRootData := Rootkey;
  RebuildPartitionTable();
end;

function THexIronWoodSerialNumber.GetSignature: string;
var
  x: integer;
begin
  if length(FPartitions) > 0 then
  begin
    for x := low(FPartitions) to high(FPartitions) do
    begin
      result := result + FPartitions[x].ToString();
      if x < high(FPartitions) then
        result := result + #13;
    end;
  end;
end;

function THexIronWoodSerialNumber.Validate(SerialNumber: string): boolean;
var
  lVector, lLock: integer;
  lBlock: string;
  lRaw: byte;
begin
  result := false;

  SerialNumber := StringReplace(SerialNumber, '-', '', [rfReplaceAll]);
  if length(SerialNumber) = length(FPartitions) * 2 then
  begin
    lVector := 0;
    lLock   := 0;

    while serialnumber.length > 0 do
    begin
      lBlock := '$' + copy(Serialnumber, 1, 2);
      delete(serialnumber, 1, 2);

      lRaw := System.Math.EnsureRange( StrToInt(LBlock), 0, 255); //HexToInt

      if FPartitions[LVector].Valid(LRaw) then
        inc(LLock);

      inc(LVector);
    end;

    result := LLock = length(FPartitions);
  end;
end;


end.

