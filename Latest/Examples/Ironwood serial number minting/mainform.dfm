object Form1: TForm1
  Left = 0
  Top = 0
  Caption = 'Quartex Framework ~ License serial number minting'
  ClientHeight = 532
  ClientWidth = 594
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object ListView1: TListView
    AlignWithMargins = True
    Left = 3
    Top = 50
    Width = 588
    Height = 460
    Align = alClient
    Columns = <
      item
        AutoSize = True
      end>
    TabOrder = 0
    ViewStyle = vsList
    ExplicitLeft = 8
    ExplicitTop = 51
    ExplicitWidth = 578
    ExplicitHeight = 302
  end
  object Panel1: TPanel
    AlignWithMargins = True
    Left = 3
    Top = 3
    Width = 588
    Height = 41
    Align = alTop
    BevelOuter = bvNone
    TabOrder = 1
    ExplicitLeft = 56
    ExplicitTop = 160
    ExplicitWidth = 405
    object Label1: TLabel
      AlignWithMargins = True
      Left = 3
      Top = 3
      Width = 129
      Height = 35
      Align = alLeft
      Caption = 'License numbers to mint = '
      Layout = tlCenter
      ExplicitHeight = 13
    end
    object lbCount: TLabel
      AlignWithMargins = True
      Left = 294
      Top = 3
      Width = 12
      Height = 35
      Align = alLeft
      Caption = '__'
      Layout = tlCenter
      ExplicitLeft = 268
      ExplicitTop = 28
      ExplicitHeight = 13
    end
    object TrackBar1: TTrackBar
      AlignWithMargins = True
      Left = 138
      Top = 3
      Width = 150
      Height = 35
      Align = alLeft
      Max = 10000
      Min = 100
      Position = 100
      TabOrder = 0
      OnChange = TrackBar1Change
      ExplicitLeft = 112
      ExplicitTop = 20
      ExplicitHeight = 21
    end
    object Button1: TButton
      AlignWithMargins = True
      Left = 455
      Top = 3
      Width = 130
      Height = 35
      Align = alRight
      Caption = 'Mint serial numbers'
      TabOrder = 1
      OnClick = Button1Click
      ExplicitLeft = 368
      ExplicitTop = 0
      ExplicitHeight = 41
    end
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 513
    Width = 594
    Height = 19
    Panels = <>
    SimplePanel = True
    ExplicitLeft = 252
    ExplicitTop = 408
    ExplicitWidth = 0
  end
end
