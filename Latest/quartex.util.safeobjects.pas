// ############################################################################
// #
// # Quartex code library
// #        version 1.0.1
// ############################################################################
// #
// # Written by Jon L. Aasenden @ Quartex Components
// #
// # This software is released under Mozilla Public License V2
// #   https://www.mozilla.org/en-US/MPL/2.0/
// #
// ############################################################################

unit quartex.util.safeobjects;

{$IFDEF FPC}
  {$mode DELPHI}{$H+}
{$ENDIF}

{$I 'quartex.inc'}

interface

uses
  SysUtils,
  SyncObjs,
  Classes,
  Generics.Collections,
  quartex.util.noref;

type
  // exception types
  EProtectedObject              = class(exception);
  EProtectedObjectWrite         = class(EProtectedObject);
  EProtectedObjectRead          = class(EProtectedObject);
  EProtectedObjectIncompatible  = class(EProtectedObject);

  EProtectedValue               = class(exception);
  EProtectedValueWrite          = class(EProtectedValue);
  EProtectedValueRead           = class(EProtectedValue);
  EProtectedValueIncompatible   = class(EProtectedValue);

  /// <summary>
  /// A protected object or value allows by default both read
  /// and write access. But the type of access you allow can
  /// be defined through this type (passed to the classes
  /// via their constructors).
  /// </summary>
  TInstanceAccess = set of (
    arRead,
    arWrite
  );

  /// <summary>
  /// TProtectedValue<T> simplifies the task of making a variable
  /// thread-safe. It is a generics based class,  which means you can
  /// use it to protect more or less any variable of any datatype.
  /// Note: TProtectedValue<T> does not create or maintain object
  /// instances, it merely restricts access to it. If you need full
  /// instance lifecycle management, use TProtectedObject<T> instead.
  /// </summary>
  TProtectedValue<T> = class(TObject)
  public
    type
    {$IFDEF FPC}
    TProtectedValueEntry = procedure (var Data: T) of object;
    {$ELSE}
    TProtectedValueEntry = reference to procedure (var Data: T);
    {$ENDIF}
  strict private
    FData:      T;
    FOptions:   TInstanceAccess;
    FLock:      TCriticalSection;

  strict protected
    function    GetValue: T;virtual;
    procedure   SetValue(Value: T);virtual;

    function    GetInstanceAccess: TInstanceAccess; virtual;
    procedure   SetInstanceAccess(Rights: TInstanceAccess); virtual;

  public
    property    Access: TInstanceAccess read GetInstanceAccess;
    property    Value: T read GetValue write SetValue;

    procedure   Synchronize(const Entry: TProtectedValueEntry);

    constructor Create(Value: T); overload; virtual;
    constructor Create(Value: T; const Access: TInstanceAccess = [arRead, arWrite]); overload; virtual;
    constructor Create(const Access: TInstanceAccess = [arRead, arWrite]); overload; virtual;
    destructor  Destroy; override;
  end;

  /// <summary>
  /// TProtectedIValue<T> is identical to TProtectedValue<T>, except
  /// that it derives from TNonReferenceCountedObject, allowing
  /// decendants to expose interfaces
  /// </summary>
  TProtectedIValue<T> = class(TNonReferenceCountedObject)
  public
    type
    {$IFDEF FPC}
    TProtectedIValueEntry = procedure (var Data: T) of object;
    {$ELSE}
    TProtectedIValueEntry = reference to procedure (var Data: T);
    {$ENDIF}
  strict private
    FData:      T;
    FOptions:   TInstanceAccess;
    FLock:      TCriticalSection;

  strict protected
    function    GetValue: T;virtual;
    procedure   SetValue(Value: T);virtual;

    function    GetInstanceAccess: TInstanceAccess; virtual;
    procedure   SetInstanceAccess(Rights: TInstanceAccess); virtual;

  public
    property    Access: TInstanceAccess read GeTInstanceAccess;
    property    Value: T read GetValue write SetValue;

    procedure   Synchronize(const Entry: TProtectedIValueEntry);

    constructor Create(Value: T); overload; virtual;
    constructor Create(Value: T; const Access: TInstanceAccess= [arRead, arWrite]); overload; virtual;
    constructor Create(const Access: TInstanceAccess = [arRead, arWrite]); overload; virtual;
    destructor  Destroy; override;
  end;

  /// <summary>
  /// TProtectedObject<T> is a class for creating, maintaining a secondary
  /// object instance. It also restricts access to this secondary object
  /// through the defined methods. Please keep in mind that this
  /// class will free the instance during its destructor.
  /// Note: SetValue() does not overwrite the instance, but rather calls
  /// assign if the instance inherits from TPersistent
  /// </summary>
  TProtectedObject<T: class, constructor> = class(TObject)
  public
    type
    {$IFDEF FPC}
    TProtectedObjectEntry = procedure (const Data: T) of object;
    {$ELSE}
    TProtectedObjectEntry = reference to procedure (const Data: T);
    {$ENDIF}
  strict private
    FData:      T;
    FOptions:   TInstanceAccess;
    FLock:      TCriticalSection;
  strict protected
    function    GetValue: T;virtual;
    procedure   SetValue(Value: T);virtual;
    function    GeTInstanceAccess: TInstanceAccess; virtual;
    procedure   SeTInstanceAccess(Rights: TInstanceAccess); virtual;

  public
    property    Value: T read GetValue write SetValue;
    property    AccessRights: TInstanceAccess read GeTInstanceAccess;

    function    Lock: T;
    procedure   Unlock;
    procedure   Synchronize(const Entry: TProtectedObjectEntry);

    Constructor Create(const AOptions: TInstanceAccess = [arRead,arWrite]);virtual;
    Destructor  Destroy; override;
  end;

  /// <summary>
  /// TProtectedIObject<T> is identical to TProtectedObject<T>, except
  /// that it derives from TNonReferenceCountedObject, allowing
  /// decendants to expose interfaces
  /// </summary>
  TProtectedIObject<T: class, constructor> = class(TNonReferenceCountedObject)
  public
    type
    {$IFDEF FPC}
    TProtectedIObjectEntry = procedure (const Data: T) of object;
    {$ELSE}
    TProtectedIObjectEntry = reference to procedure (const Data: T);
    {$ENDIF}
  strict private
    FData:      T;
    FOptions:   TInstanceAccess;
    FLock:      TCriticalSection;
  strict protected
    function    GetValue: T;virtual;
    procedure   SetValue(Value: T);virtual;

    function    GetInstanceAccess: TInstanceAccess; virtual;
    procedure   SetInstanceAccess(Rights: TInstanceAccess); virtual;
  public
    property    Value: T read GetValue write SetValue;
    property    AccessRights: TInstanceAccess read GeTInstanceAccess;

    function    Lock: T;
    procedure   Unlock;
    procedure   Synchronize(const Entry: TProtectedIObjectEntry);

    Constructor Create(const AOptions: TInstanceAccess = [arRead,arWrite]);virtual;
    Destructor  Destroy; override;
  end;


resourcestring
  CNT_ERR_Access_Read = 'Object does not allow read';
  CNT_ERR_Access_Write = 'Object does not allow write';
  CNT_ERR_Access_Incompatible = 'Incompatible type, assign failed';

implementation

//############################################################################
//  TProtectedObject
//############################################################################

constructor TProtectedIObject<T>.Create(const AOptions: TInstanceAccess = [arRead, arWrite]);
begin
  inherited Create;
  FLock := TCriticalSection.Create();
  FLock.Enter();
  try
    FOptions:=AOptions;
    FData := T.create;
  finally
    FLock.Leave();
  end;
end;

destructor TProtectedIObject<T>.Destroy;
begin
  FLock.Enter();
  try
    FData.free;
  finally
    FLock.Leave();
  end;
  inherited;
end;

function TProtectedIObject<T>.GeTInstanceAccess: TInstanceAccess;
begin
  FLock.Enter();
  try
    result := FOptions;
  finally
    FLock.Leave();
  end;
end;

procedure TProtectedIObject<T>.SeTInstanceAccess(Rights: TInstanceAccess);
begin
  FLock.Enter();
  try
    FOptions := Rights;
  finally
    FLock.Leave();
  end;
end;

function TProtectedIObject<T>.Lock: T;
begin
  FLock.Enter();
  result := FData;
end;

procedure TProtectedIObject<T>.Unlock;
begin
  FLock.Leave();
end;

procedure TProtectedIObject<T>.Synchronize(const Entry: TProtectedIObjectEntry);
begin
  if assigned(Entry) then
  begin
    FLock.Enter();
    try
      Entry(FData);
    finally
      FLock.Leave();
    end;
  end;
end;

function TProtectedIObject<T>.GetValue: T;
begin
  FLock.Enter();
  try
    if (arRead in FOptions) then
      result := FData
    else
      raise EProtectedObjectRead.Create(CNT_ERR_Access_Read);
  finally
    FLock.Leave();
  end;
end;

procedure TProtectedIObject<T>.SetValue(Value: T);
begin
  FLock.Enter();
  try
    if (arWrite in FOptions) then
    begin
      if (TObject(FData) is TPersistent)
      or (TObject(FData).InheritsFrom(TPersistent)) then
        TPersistent(FData).Assign(TPersistent(Value))
      else
        raise EProtectedObjectIncompatible.Create(CNT_ERR_Access_Incompatible)
    end else
    raise EProtectedObjectWrite.Create(CNT_ERR_Access_Write);
  finally
    FLock.Leave();
  end;
end;

//############################################################################
//  TProtectedObject
//############################################################################

constructor TProtectedObject<T>.Create(const AOptions: TInstanceAccess = [arRead, arWrite]);
begin
  inherited Create;
  FLock := TCriticalSection.Create();
  FLock.Enter();
  try
    FOptions:=AOptions;
    FData := T.create;
  finally
    FLock.Leave();
  end;
end;

destructor TProtectedObject<T>.Destroy;
begin
  FLock.Enter();
  try
    FData.free;
  finally
    FLock.Leave();
  end;
  inherited;
end;

function TProtectedObject<T>.GeTInstanceAccess: TInstanceAccess;
begin
  FLock.Enter();
  try
    result := FOptions;
  finally
    FLock.Leave();
  end;
end;

procedure TProtectedObject<T>.SeTInstanceAccess(Rights: TInstanceAccess);
begin
  FLock.Enter();
  try
    FOptions := Rights;
  finally
    FLock.Leave();
  end;
end;

function TProtectedObject<T>.Lock: T;
begin
  FLock.Enter();
  result := FData;
end;

procedure TProtectedObject<T>.Unlock;
begin
  FLock.Leave();
end;

procedure TProtectedObject<T>.Synchronize(const Entry: TProtectedObjectEntry);
begin
  if assigned(Entry) then
  begin
    FLock.Enter();
    try
      Entry(FData);
    finally
      FLock.Leave();
    end;
  end;
end;

function TProtectedObject<T>.GetValue: T;
begin
  FLock.Enter();
  try
    if (arRead in FOptions) then
      result := FData
    else
      raise EProtectedObjectRead.Create(CNT_ERR_Access_Read);
  finally
    FLock.Leave();
  end;
end;

procedure TProtectedObject<T>.SetValue(Value: T);
begin
  FLock.Enter();
  try
    if (arWrite in FOptions) then
    begin
      if (TObject(FData) is TPersistent)
      or (TObject(FData).InheritsFrom(TPersistent)) then
        TPersistent(FData).Assign(TPersistent(Value))
      else
        raise EProtectedObjectIncompatible.Create(CNT_ERR_Access_Incompatible)
    end else
    raise EProtectedObjectWrite.Create(CNT_ERR_Access_Write);
  finally
    FLock.Leave();
  end;
end;

//############################################################################
//  TProtectedValue
//############################################################################

constructor TProtectedValue<T>.Create(Value: T);
begin
  inherited Create;
  FLock := TCriticalSection.Create();
  FLock.Enter();
  try
    FOptions := [arRead, arWrite];
    FData := Value;
  finally
    FLock.Leave();
  end;
end;

constructor TProtectedValue<T>.Create(Value: T; const Access: TInstanceAccess = [arRead, arWrite]);
begin
  inherited Create;
  FLock := TCriticalSection.Create();
  FLock.Enter();
  try
    FOptions := [arRead, arWrite];
    FData := Value;
  finally
    FLock.Leave();
  end;
end;

Constructor TProtectedValue<T>.Create(const Access: TInstanceAccess = [arRead,arWrite]);
begin
  inherited Create;
  FLock := TCriticalSection.Create();
  FLock.Enter();
  try
    FOptions := Access;
  finally
    FLock.Leave();
  end;
end;

destructor TProtectedValue<T>.destroy;
begin
  FLock.free;
  inherited;
end;

function TProtectedValue<T>.GeTInstanceAccess: TInstanceAccess;
begin
  FLock.Enter();
  try
    result := FOptions;
  finally
    FLock.Leave();
  end;
end;

procedure TProtectedValue<T>.SeTInstanceAccess(Rights: TInstanceAccess);
begin
  FLock.Enter();
  try
    FOptions := Rights;
  finally
    FLock.Leave();
  end;
end;

procedure TProtectedValue<T>.Synchronize(const Entry: TProtectedValueEntry);
begin
  if assigned(Entry) then
  Begin
    FLock.Enter();
    try
      Entry(FData);
    finally
      FLock.Leave();
    end;
  end;
end;

function TProtectedValue<T>.GetValue: T;
begin
  FLock.Enter();
  try
    if (arRead in FOptions) then
      result := FData
    else
      raise EProtectedValueRead.Create(CNT_ERR_Access_Read);
  finally
    FLock.Leave();
  end;
end;

procedure TProtectedValue<T>.SetValue(Value: T);
begin
  FLock.Enter();
  try
    if (arWrite in FOptions) then
      FData := Value
    else
      raise EProtectedValueWrite.Create(CNT_ERR_Access_Write);
  finally
    FLock.Leave();
  end;
end;

//############################################################################
//  TProtectedIValue ~ same as TProtectedValue, but supports interfaces
//############################################################################

constructor TProtectedIValue<T>.Create(Value: T);
begin
  inherited Create;
  FLock := TCriticalSection.Create();
  FLock.Enter();
  try
    FOptions := [arRead, arWrite];
    FData := Value;
  finally
    FLock.Leave();
  end;
end;

constructor TProtectedIValue<T>.Create(Value: T; const Access: TInstanceAccess = [arRead, arWrite]);
begin
  inherited Create;
  FLock := TCriticalSection.Create();
  FLock.Enter();
  try
    FOptions := [arRead, arWrite];
    FData := Value;
  finally
    FLock.Leave();
  end;
end;

constructor TProtectedIValue<T>.Create(const Access: TInstanceAccess = [arRead,arWrite]);
begin
  inherited Create;
  FLock := TCriticalSection.Create();
  FLock.Enter();
  try
    FOptions := Access;
  finally
    FLock.Leave();
  end;
end;

destructor TProtectedIValue<T>.destroy;
begin
  FLock.free;
  inherited;
end;

function TProtectedIValue<T>.GeTInstanceAccess: TInstanceAccess;
begin
  FLock.Enter();
  try
    result := FOptions;
  finally
    FLock.Leave();
  end;
end;

procedure TProtectedIValue<T>.SeTInstanceAccess(Rights: TInstanceAccess);
begin
  FLock.Enter();
  try
    FOptions := Rights;
  finally
    FLock.Leave();
  end;
end;

procedure TProtectedIValue<T>.Synchronize(const Entry: TProtectedIValueEntry);
begin
  if assigned(Entry) then
  Begin
    FLock.Enter();
    try
      Entry(FData);
    finally
      FLock.Leave();
    end;
  end;
end;

function TProtectedIValue<T>.GetValue: T;
begin
  FLock.Enter();
  try
    if (arRead in FOptions) then
      result := FData
    else
      raise EProtectedValueRead.Create(CNT_ERR_Access_Read);
  finally
    FLock.Leave();
  end;
end;

procedure TProtectedIValue<T>.SetValue(Value: T);
begin
  FLock.Enter();
  try
    if (arWrite in FOptions) then
      FData := Value
    else
      raise EProtectedValueWrite.Create(CNT_ERR_Access_Write);
  finally
    FLock.Leave();
  end;
end;

end.




