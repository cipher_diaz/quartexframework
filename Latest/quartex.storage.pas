// ############################################################################
// #
// # Quartex code library
// #        version 1.0.1
// ############################################################################
// #
// # Written by Jon L. Aasenden @ Quartex Components
// #
// # This software is released under Mozilla Public License V2
// #   https://www.mozilla.org/en-US/MPL/2.0/
// #
// ############################################################################

unit quartex.storage;

{$IFDEF FPC}
  {$mode DELPHI}{$H+}
{$ENDIF}

{$I 'quartex.inc'}

interface

uses
  SysUtils
  , Classes
  , Math
  , quartex.util.noref
  {$IFNDEF FPC}
  , System.IOUtils
  , Generics.Collections
  {$ENDIF}
  ;

type

  /// <summary>
  /// A storage buffer can have different capabilities depending on the
  /// media it targets. Scale defines if the content can grown and shrink,
  /// owned determine if the content is owned by another entity (and can
  /// thus not be released). Read and Write determines if the storage allows
  /// read or write access.
  /// </summary>
  TStorageCapabilities      = set of (mcScale, mcOwned, mcRead, mcWrite);


  /// <summary>
  /// When doing a hex dump from a storage buffer, the bytes can be
  /// signed (prefixed with $), and zero-padded (e.g $01 rather that $1)
  /// </summary>
  TStorageTextOutOptions    = set of (hdSign, hdZeroPad);

  /// <summary>
  /// When using a storage buffer as a stack (push, pull methods), this
  /// datatype determines if a push is appended at the bottom of the
  /// storage, or at the top. A real stack would allocate (push) top-down,
  /// while a typical list based stack would append. Use down-up if you
  /// have no particular criteria for behavior
  /// </summary>
  TStorageStackOrientation  = (soTopDown, soDownUp);

  /// <summary>
  /// This array represents the size of the internal cache a storage
  /// buffer will operate with during move, copy, insert etc. operations
  /// </summary>
  TStorageCache           = array [1..10240] of byte;

  TProcessBeginsEvent = procedure (const Sender: TObject; const Primary: integer) of object;
  TProcessUpdateEvent = procedure (const Sender: TObject; const Value, Primary: integer) of object;
  TProcessEndsEvent   = procedure (const Sender: TObject; const Primary, Secondary: integer) of object;

  IBinarySearch = interface
    ['{10A75BDF-9FCB-422D-82D3-46ACB61F41FA}']
    function Search(const Data; const DataLength: integer; var FoundbyteIndex: int64):  boolean;
  end;

  TStorage = class;

  IStorage = interface
    ['{37C2EC42-1B7F-48CE-91C9-14D42F3FDFA7}']
    procedure   LoadFromFile(Filename: string);
    procedure   SaveToFile(Filename: string);

    procedure   LoadFromStream(const Stream: TStream);
    procedure   SaveToStream(const Stream: TStream);

    function    ToStream: TStream;
    procedure   FromStream(const Stream: TStream; const Disposable: boolean = false);

    function    ToStorage: IStorage;
    procedure   FromStorage(const Storage: TStorage; const Disposable: boolean = false); overload;
    procedure   FromStorage(const Storage: IStorage); overload;

    function    ImportFrom(ByteIndex: Int64; DataLength: integer; const Reader: TReader):  integer;
    function    ExportTo(ByteIndex: Int64; DataLength: integer; const Writer: TWriter):  integer;

    function    GetCapabilities: TStorageCapabilities;
    function    GetEmpty: boolean;

    function    GetSize: int64;
    procedure   SetSize(const NewSize: Int64);

    function    Read(const ByteIndex: int64; DataLength: integer; var Data):  integer; overload;
    function    Write(const ByteIndex: int64; DataLength: integer; const Data):  integer; overload;

    procedure   Append(const Buffer: IStorage); overload;
    procedure   Append(const Stream: TStream); overload;
    procedure   Append(const Data; const DataLength:integer); overload;

    procedure   Insert(ByteIndex: int64;const Source; DataLength: integer); overload;
    procedure   Insert(ByteIndex: int64; const Source: IStorage); overload;
    procedure   Remove(ByteIndex: int64; DataLength: integer);

    function    Push(const Source; DataLength: integer;const Orientation: TStorageStackOrientation = soDownUp):  integer;
    function    Pop(var Target; DataLength: integer; const Orientation: TStorageStackOrientation = soDownUp):  integer;

    function    FillWith(const ByteIndex: int64; const BytesToFill: int64; const Source; const SourceLength: integer):  int64;
    procedure   FillAll(const Value: byte); overload;
    procedure   FillAll; overload;

    function    ToString(Boundary: integer = 16; const Options: TStorageTextOutOptions = [hdSign, hdZeroPad]) : string;

    function    HashCode: cardinal;
  end;

  IStorageDisposable = interface
    ['{9ADD9616-C60D-4646-B3C1-2768F6EAE8F4}']
    function    GetDisposable: boolean;
    procedure   SetDisposable(const Value: boolean);
  end;

  TStorageSearch = class(TInterfacedObject, IBinarySearch)
  private
    FStorage:   IStorage;
  public
    function    Search(const Data; const DataLength: integer; var FoundbyteIndex: int64):  boolean;
    constructor Create(Storage: IStorage);
  end;

  TStorage = class(TNonReferenceCountedObject, IStorage, IStorageDisposable)
  public
    type
    EStorage = class(Exception);
    EStorageInvalidReference  = class(EStorage);
    EStorageInvalidDataSource = class(EStorage);
    EStorageInvalidDataTarget = class(EStorage);
    EStorageInvalidDataSize   = class(EStorage);
    EStorageReadUnSupported   = class(EStorage);
    EStorageWriteUnSupported  = class(EStorage);
    EStorageScaleUnSupported  = class(EStorage);
    EStorageEmpty             = class(EStorage);
    EStorageIndexViolation    = class(EStorage);
    EStorageScaleFailed       = class(EStorage);

  strict private
    FDisposable:  boolean;

  strict protected
    {$IFNDEF USE_LOCAL_CACHE}
    FCache:     TStorageCache;
    {$ENDIF}
    procedure   SetSize(const NewSize: Int64);

    // IStorageDisposable
    function    GetDisposable: boolean; virtual;
    procedure   SetDisposable(const Value: boolean); virtual;

  strict protected

    procedure   BeforeReadObject; virtual;
    procedure   BeforeWriteObject; virtual;

    procedure   AfterReadObject; virtual;
    procedure   AfterWriteObject; virtual;

    function    GetEmpty: boolean; virtual;

    function    GetCapabilities: TStorageCapabilities; virtual; abstract;
    function    GetSize: Int64; virtual; abstract;

    procedure   DoReleaseData; virtual;abstract;
    procedure   DoGrowDataBy(const Value: integer); virtual;abstract;
    procedure   DoShrinkDataBy(const Value: integer); virtual;abstract;
    procedure   DoReadData(Start: Int64; var Buffer; BufLen: integer); virtual;abstract;
    procedure   DoWriteData(Start: int64; const Buffer; BufLen: integer); virtual;abstract;
    procedure   DoFillData(Start: Int64; FillLength: Int64; const Data; DataLen: integer); virtual;
    procedure   DoZeroData; virtual;
  public
    property    Empty: boolean read GetEmpty;
    property    Capabilities: TStorageCapabilities read GetCapabilities;

    procedure   Assign(Source: IStorage); virtual;

    function    Read(const ByteIndex: int64; DataLength: integer; var Data):  integer; overload;
    function    Write(const ByteIndex: int64; DataLength: integer; const Data):  integer; overload;

    procedure   Append(const Buffer: IStorage); overload;
    procedure   Append(const Stream: TStream); overload;
    procedure   Append(const Data; const DataLength:integer); overload;

    procedure   Insert(ByteIndex: int64;const Source; DataLength: integer); overload;
    procedure   Insert(ByteIndex: int64; const Source: IStorage); overload;
    procedure   Remove(ByteIndex: int64; DataLength: integer);

    function    Push(const Source; DataLength: integer;const Orientation: TStorageStackOrientation = soDownUp):  integer;
    function    Pop(Var Target; DataLength: integer; const Orientation: TStorageStackOrientation = soDownUp):  integer;

    function    FillWith(const ByteIndex: int64; const BytesToFill: int64; const Source; const SourceLength: integer):  int64;
    procedure   FillAll(const Value: byte); overload;
    procedure   FillAll; overload;

    function    HashCode: cardinal; virtual;

    procedure   LoadFromFile(Filename: string);
    procedure   SaveToFile(Filename: string);
    procedure   SaveToStream(const Stream: TStream);
    procedure   LoadFromStream(const Stream: TStream);

    function    ToStream: TStream;
    procedure   FromStream(const Stream: TStream; const Disposable: boolean = false);

    function    ToStorage: IStorage;
    procedure   FromStorage(const Storage: TStorage; const Disposable: boolean = false); overload;
    procedure   FromStorage(const Storage: IStorage); overload;

    function    ExportTo(ByteIndex: Int64; DataLength: integer; const Writer: TWriter):  integer;
    function    ImportFrom(ByteIndex: Int64; DataLength: integer; const Reader: TReader):  integer;

    procedure   Release;

    function    ToString(Boundary: integer = 16; const Options: TStorageTextOutOptions = [hdSign, hdZeroPad]) : string; reintroduce; virtual;

    property Size: int64 read GetSize write SetSize;

    constructor Create; virtual;
    destructor  Destroy; override;
  end;

  TTyped = class abstract
  public
    type
    PTripleByte = ^TTripleByte;
    TTripleByte = packed record
      a:  byte;
      b:  byte;
      c:  byte;
    end;

    class function  GetTempFileName: string;

    // Note: FPC has similar routines in System (!)
    // You might want to use those instead, and fall back on these under Delphi
    class procedure FillByte(Target: Pbyte; const FillSize: integer; const Value: byte); inline; static;
    class procedure FillWord(Target: PWord; const FillSize: integer; const Value: word); inline; static;
    class procedure FillTriple(dstAddr: PTripleByte; const InCount: integer; const Value: TTripleByte); inline; static;
    class procedure FillLong(dstAddr: PCardinal; const InCount: integer; const Value: cardinal); inline; static;

    class function  ToNearest(const Value, Factor: integer): integer; inline; static;
    class procedure Swap(var Primary, Secondary: integer); inline; static;
    class function  Span(const Primary, Secondary: integer; const WithZero: boolean = false): integer; inline; static;

    class function  CalcTrailFor(Index, ItemSize, TotalItems: integer): integer; overload; inline; static;
    class function  CalcTrailFor(Index, ItemSize, TotalItems: int64): int64; overload; inline; static;

    class function  CalcOffsetFor(Index, ItemSize, TotalItems: integer): integer; overload; inline; static;
    class function  CalcOffsetFor(Index, ItemSize, TotalItems: int64): int64; overload; inline; static;

    class function  CreateGuidString: string; inline; static;
  end;

  TStreamAdapter = class(TStream)
  strict private
    FBufObj:    IStorage;
    FOffset:    int64;
  strict protected
    type
    EQTXStreamAdapter = class(Exception);
  protected
    function    GetSize: Int64; override;
    procedure   SetSize(const NewSize: Int64); override;
  public
    property    Memory: IStorage read FBufObj;
    function    Read(var Buffer; Count: longint):  longint; override;
    function    Write(const Buffer; Count: longint):  longint; override;
    function    Seek(const Offset: Int64; Origin: TSeekOrigin): Int64; override;
    constructor Create(const SourceBuffer: IStorage); reintroduce;
  end;

function  CreateBinarySearch(const Storage: TStorage): IBinarySearch;
function  CreateTempFileStorage: TStorage;
function  CreateFileStorage(FileName: string; StreamFlags: word): TStorage;
function  CreateMemoryStorage: TStorage;

resourcestring
  //CNT_ERR_BUFFER_BASE  = 'Method %s threw exception %s with %s';
  CNT_ERR_BUFFER_RELEASENOTSUPPORTED  = 'Storage capabillities does not allow release';
  CNT_ERR_BUFFER_SCALENOTSUPPORTED    = 'Storage capabillities does not allow scaling';
  CNT_ERR_BUFFER_READNOTSUPPORTED     = 'Storage capabillities does not allow read';
  CNT_ERR_BUFFER_WRITENOTSUPPORTED    = 'Storage capabillities does not allow write';
  CNT_ERR_BUFFER_SOURCEREADNOTSUPPORTED = 'data-source does not allow read';
  CNT_ERR_BUFFER_SCALEFAILED          = 'Storage scale operation failed: %s';
  CNT_ERR_BUFFER_BYTEINDEXVIOLATION   = 'Storage byte index violation, expected %d..%d not %d';
  CNT_ERR_BUFFER_INVALIDDATASOURCE    = 'Invalid data-source for operation';
  CNT_ERR_BUFFER_INVALIDDATATARGET    = 'Invalid data-target for operation';
  CNT_ERR_BUFFER_EMPTY                = 'Storage resource contains no data error';
  CNT_ERR_STREAMADAPTER_INVALIDBUFFER  = 'Invalid buffer error, buffer is NIL';
  CNT_ERR_BUFFER_InvalidReference     = 'Storage reference of instance was nil error';
  CNT_ERR_BUFFER_InvalidDataSize      = 'Invalid size for operatin error';

implementation

uses
  quartex.storage.memory,
  quartex.storage.disk;

//##########################################################################
// TQTXStorage
//##########################################################################

function CreateBinarySearch(const Storage: TStorage): IBinarySearch;
begin
  result := TStorageSearch.Create(Storage);
end;

function CreateMemoryStorage: TStorage;
var
  lTemp: TStorageMemory;
begin
  lTemp := TStorageMemory.Create();
  result := TStorage(lTemp);
end;

function CreateTempFileStorage: TStorage;
var
  lTemp: TStorageFile;
  lAccess: IStorageDisposable;
begin
  lTemp := TStorageFile.Create('', fmCreate);
  lAccess := IStorageDisposable(lTemp);
  lAccess.SetDisposable(true);
  result := lTemp;
end;

function CreateFileStorage(FileName: string; StreamFlags: word): TStorage;
var
  lTemp: TStorageFile;
begin
  lTemp := TStorageFile.Create(FileName, StreamFlags);
  result := TStorage(lTemp);
end;

//##########################################################################
// TStorageSearch
//##########################################################################

constructor TStorageSearch.Create(Storage: IStorage);
begin
  inherited Create();
  FStorage := Storage;
end;

function TStorageSearch.Search(const Data; const DataLength: integer; var FoundbyteIndex: int64):  boolean;
var
  lTotal:   Int64;
  lToScan:  Int64;
  src:      Pbyte;
  lbyte:    byte;
  lOffset:  Int64;
  x,  y:    Int64;
  lBasePtr: Pbyte;
begin
  // Initialize
  result := false;
  FoundbyteIndex := -1;
  lByte := 0;

  // Check reference
  if FStorage = nil then
    raise TStorage.EStorageInvalidReference.Create(CNT_ERR_BUFFER_InvalidReference);

  // Check read capability
  if not ( mcRead in FStorage.GetCapabilities() ) then
    raise TStorage.EStorageReadUnSupported.Create(CNT_ERR_BUFFER_READNOTSUPPORTED);

  // Check source PTR
  lBasePtr := addr(Data);
  if lBasePtr = nil then
    raise TStorage.EStorageInvalidDataSource.Create(CNT_ERR_BUFFER_INVALIDDATASOURCE);

  // Check search length
  if DataLength < 1 then
    raise TStorage.EStorageInvalidDataSize(CNT_ERR_BUFFER_InvalidDataSize);

  // Check buffer size
  lTotal := FStorage.GetSize();
  if lTotal < 1 then
    raise TStorage.EStorageEmpty(CNT_ERR_BUFFER_EMPTY);

  // Make sure we never scan beyond the boundary of the buffer
  // We have to subtract the length of the data
  lToScan := lTotal - DataLength;

  x := 0;
  While (x <= lToScan) do
  begin
    // setup source PTR
    src := lBasePtr;

    // setup target offset
    lOffset := x;

    // Check memory by step-sampling
    // We check each byte for a match with the source, at the same index.
    // if any byte does not match, we abort, inc our offset into the buffer
    // and keep on going until a match is found
    y := 1;
    while y < DataLength do
    begin
      // break if not equal
      FStorage.Read(lOffset, 1, lbyte);
      result := src^ = lbyte;
      if not result then
        break;

      inc(src);
      lOffset := lOffset + 1;
      Y := Y + 1;
    end;

    // success?
    if result then
    begin
      FoundbyteIndex := x;
      break;
    end;

    x := x + 1;
  end;
end;

//##########################################################################
// TTyped
//##########################################################################

class function TTyped.CreateGuidString: string;
var
  lGuid: TGuid;
begin
  CreateGUID(lGUID);
  result := GuidToString(lGUID);
end;

class function TTyped.CalcOffsetFor(Index, ItemSize, TotalItems: integer): integer;
begin
  if TotalItems < 1 then exit(-1);
  if Index < 0 then exit(-1);
  if Index >= TotalItems then exit(-1);
  if ItemSize < 0 then exit(-1);
  result := Index * ItemSize;
end;

class function TTyped.CalcTrailFor(Index, ItemSize, TotalItems: integer): integer;
begin
  if TotalItems < 1 then exit(-1);
  if Index < 0 then exit(-1);
  if Index >= TotalItems then exit(-1);
  if ItemSize < 0 then exit(-1);

  inc(index);
  dec(TotalItems, Index);
  result := TotalItems * ItemSize;
end;

class function TTyped.CalcOffsetFor(Index, ItemSize, TotalItems: int64): int64;
begin
  if TotalItems < 1 then exit(-1);
  if Index < 0 then exit(-1);
  if Index >= TotalItems then exit(-1);
  if ItemSize < 0 then exit(-1);
  result := Index * ItemSize;
end;

class function TTyped.CalcTrailFor(Index, ItemSize, TotalItems: int64): int64;
begin
  if TotalItems < 1 then exit(-1);
  if Index < 0 then exit(-1);
  if Index >= TotalItems then exit(-1);
  if ItemSize < 0 then exit(-1);

  inc(index);
  dec(TotalItems, Index);
  result := TotalItems * ItemSize;
end;

class function TTyped.GetTempFileName: string;
begin
  {$IFDEF FPC}
  result := sysutils.GetTempFileName();
  {$ELSE}
  result := TPath.GetTempFileName();
  {$ENDIF}
end;

class procedure TTyped.Fillbyte(Target: Pbyte; const FillSize: integer;
  const Value: byte);
var
  LBytesToFill: integer;
begin
  LBytesToFill := FillSize;
  While LBytesToFill > 0 do
  begin
    Target^ := Value;
    dec(LBytesToFill);
    inc(Target);
  end;
end;

class procedure TTyped.FillWord(Target: PWord;
          const FillSize: integer; const Value: word);
var
  FTemp:  cardinal;
  FLongs: integer;
begin
  FTemp := Value shl 16 or Value;
  FLongs := FillSize shr 3;
  while FLongs>0 do
  begin
    Pcardinal(Target)^:=FTemp; inc(Pcardinal(Target));
    Pcardinal(Target)^:=FTemp; inc(Pcardinal(Target));
    Pcardinal(Target)^:=FTemp; inc(Pcardinal(Target));
    Pcardinal(Target)^:=FTemp; inc(Pcardinal(Target));
    dec(FLongs);
  end;

  Case FillSize mod 8 of
  1:  Target^:=Value;
  2:  Pcardinal(Target)^:=FTemp;
  3:  begin
        Pcardinal(Target)^:=FTemp; inc(Pcardinal(Target));
        Target^:=Value;
      end;
  4:  begin
        Pcardinal(Target)^:=FTemp; inc(Pcardinal(Target));
        Pcardinal(Target)^:=FTemp;
      end;
  5:  begin
        Pcardinal(Target)^:=FTemp; inc(Pcardinal(Target));
        Pcardinal(Target)^:=FTemp; inc(Pcardinal(Target));
        Target^:=Value;
      end;
  6:  begin
        Pcardinal(Target)^:=FTemp; inc(Pcardinal(Target));
        Pcardinal(Target)^:=FTemp; inc(Pcardinal(Target));
        Pcardinal(Target)^:=FTemp;
      end;
  7:  begin
        Pcardinal(Target)^:=FTemp; inc(Pcardinal(Target));
        Pcardinal(Target)^:=FTemp; inc(Pcardinal(Target));
        Pcardinal(Target)^:=FTemp; inc(Pcardinal(Target));
        Target^:=Value;
      end;
  end;
end;

class procedure TTyped.FillTriple(dstAddr: PTripleByte;
          const inCount: integer;const Value: TTripleByte);
var
  FLongs: integer;
begin
  FLongs := inCount shr 3;
  While FLongs>0 do
  begin
    dstAddr^ := Value; inc(dstAddr);
    dstAddr^ := Value; inc(dstAddr);
    dstAddr^ := Value; inc(dstAddr);
    dstAddr^ := Value; inc(dstAddr);
    dstAddr^ := Value; inc(dstAddr);
    dstAddr^ := Value; inc(dstAddr);
    dstAddr^ := Value; inc(dstAddr);
    dstAddr^ := Value; inc(dstAddr);
    dec(FLongs);
  end;

  case (inCount mod 8) of
  1:  dstAddr^ := Value;
  2:  begin
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value;
      end;
  3:  begin
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value;
      end;
  4:  begin
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value;
      end;
  5:  begin
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value;
      end;
  6:  begin
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value;
      end;
  7:  begin
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value;
      end;
  end;
end;

class procedure TTyped.FillLong(dstAddr: PCardinal;
      const inCount: integer; const Value: Cardinal);
var
  lLongs: integer;
begin
  lLongs := inCount shr 3;
  while lLongs > 0 do
  begin
    dstAddr^ := Value; inc(dstAddr);
    dstAddr^ := Value; inc(dstAddr);
    dstAddr^ := Value; inc(dstAddr);
    dstAddr^ := Value; inc(dstAddr);
    dstAddr^ := Value; inc(dstAddr);
    dstAddr^ := Value; inc(dstAddr);
    dstAddr^ := Value; inc(dstAddr);
    dstAddr^ := Value; inc(dstAddr);
    dec(lLongs);
  end;

  case inCount mod 8 of
  1:  dstAddr^:=Value;
  2:  begin
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value;
      end;
  3:  begin
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value;
      end;
  4:  begin
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value;
      end;
  5:  begin
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value;
      end;
  6:  begin
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value;
      end;
  7:  begin
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value; inc(dstAddr);
        dstAddr^ := Value;
      end;
  end;
end;

// rounds a number to nearest factor of X
class function TTyped.ToNearest(const Value, Factor: integer): integer;
var
  lTemp: integer;
Begin
  result := Value;
  lTemp := Value mod Factor;
  if lTemp > 0 then
    inc(result, factor - lTemp);
end;

// Swaps the content of two variables
class procedure TTyped.Swap(var Primary, Secondary: integer);
var
  LTemp: integer;
Begin
  LTemp := Primary;
  Primary := Secondary;
  Secondary := LTemp;
end;

// Calculates difference between two numbers, counting zero
class function TTyped.Span(const Primary, Secondary: integer; const WithZero: boolean = false): integer;
Begin
  If Primary <> Secondary then
  begin
    If Primary > Secondary then
      result := (Primary - Secondary)
    else
      result := (Secondary - Primary);

    if WithZero then
    begin
      If (Primary < 1) or (Secondary < 1) then
        inc(result);
    end;

  end else
    result := 0;
end;

//##########################################################################
// TStorage
//##########################################################################

constructor TStorage.Create;
begin
  inherited;
  FDisposable := false;
end;

destructor TStorage.Destroy;
begin
  if not Empty then
    Release();
  inherited;
end;

procedure TStorage.SetDisposable(const Value: boolean);
begin
  FDisposable := Value;
end;

function TStorage.GetDisposable: boolean;
begin
  result := FDisposable;
end;

procedure TStorage.Assign(Source: IStorage);
begin
  Release();
  Append(Source);
end;

function TStorage.GetEmpty: boolean;
begin
  result := GetSize() <= 0;
end;

procedure TStorage.BeforeReadObject;
begin
  if mcOwned in GetCapabilities() then
    Release()
  else
    raise EStorage.Create(CNT_ERR_BUFFER_RELEASENOTSUPPORTED);
end;

procedure TStorage.AfterReadObject;
begin
end;

procedure TStorage.BeforeWriteObject;
begin
end;

procedure TStorage.AfterWriteObject;
begin
end;

procedure TStorage.DoFillData(Start: Int64; FillLength: Int64;
          const Data; DataLen: integer);
var
  lToWrite: integer;
begin
  While FillLength > 0 do
  begin
    lToWrite := EnsureRange(Datalen, 1, FillLength);
    DoWriteData(Start, Data, lToWrite);
    FillLength := FillLength - lToWrite;
    Start := Start + lToWrite;
  end;
end;

function TStorage.FillWith(const ByteIndex: Int64;
         const BytesToFill: Int64;
         const Source; const SourceLength: integer): Int64;
var
  lTotal: Int64;
  lTemp:  Int64;
  lAddr:  pbyte;
begin
  // Initialize
  result:=0;

  // Are we empty?
  if not Empty then
  begin
    // Check write capabilities
    if mcWrite in GetCapabilities() then
    begin
      // Check length[s] of data
      if  (BytesToFill > 0) and (SourceLength > 0) then
      begin
        // check data-source
        lAddr := addr(Source);

        if lAddr <> nil then
        begin
          // Get total range
          lTotal := GetSize();

          // Check range entrypoint
          if (ByteIndex >= 0) and (ByteIndex < lTotal) then
          begin

            // Does fill exceed range?
            lTemp := ByteIndex + BytesToFill;
            if lTemp > lTotal then
              lTemp := (lTotal - ByteIndex)
            else
              lTemp :=  BytesToFill;

            // fill range
            DoFillData(ByteIndex, LTemp,LAddr^, SourceLength);

            // return size of region filled
            result := lTemp;

          end else
          raise EStorageIndexViolation.CreateFmt(CNT_ERR_BUFFER_BYTEINDEXVIOLATION, [0, lTotal-1, ByteIndex]);
        end else
        raise EStorageInvalidDataSource.Create(CNT_ERR_BUFFER_INVALIDDATASOURCE);
      end;
    end else
    raise EStorage.Create(CNT_ERR_BUFFER_WRITENOTSUPPORTED);
  end else
  raise EStorageEmpty.Create(CNT_ERR_BUFFER_EMPTY);
end;

{$IFDEF BE_SILENT}
  {$WARNINGS OFF}
  {$HINTS OFF}
{$ENDIF}
procedure TStorage.DoZeroData;
var
  lSize:  Int64;
  lAlign: Int64;
  {$IFDEF USE_LOCAL_CACHE}
  lCache: TStorageCache;
  {$ENDIF}
begin
  lSize := GetSize();
  lAlign := lSize div SizeOf(byte);

  {$IFDEF USE_LOCAL_CACHE}
    {$IFDEF USE_RTL_FILL}
    fillchar(lCache, lAlign, byte(0) );
    {$ELSE}
    TTyped.Fillbyte(@lCache, lAlign, 0);
    {$ENDIF}
  FillWith(0, lSize, lCache, SizeOf(lCache));
  {$ELSE}
    {$IFDEF USE_RTL_FILL}
    fillchar(FCache, lAlign, byte(0) );
    {$ELSE}
    TTyped.Fillbyte(@FCache, lAlign, 0);
    {$ENDIF}
  FillWith(0, lSize, FCache, SizeOf(FCache));
  {$ENDIF}
end;
{$IFDEF BE_SILENT}
  {$WARNINGS ON}
  {$HINTS ON}
{$ENDIF}

{$IFDEF BE_SILENT}
  {$WARNINGS OFF}
  {$HINTS OFF}
{$ENDIF}
function  TStorage.ToString(Boundary: integer = 16;
          const Options: TStorageTextOutOptions = [hdSign, hdZeroPad]): string;
var
  x, y:   integer;
  LCount: integer;
  LPad:   integer;
  LDump:  array of byte;
  LCache: byte;

const
  CNT_MASK  = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
            + 'abcdefghijklmnopqrstuvwxyz'
            + '0123456789'
            + ',;<>{}[]-_#$%&/() §^:,?';

  procedure AddToCache(const Value: byte);
  var
    _len: integer;
  begin
    _Len := length(LDump);
    SetLength(LDump, _Len + 1);
    LDump[_len] := Value;
  end;

begin
  lCache := 0;

  if not Empty then
  Begin
    Boundary := EnsureRange(Boundary, 2, 64);
    LCount:=0;
    result := '';

    for x := 0 to Size-1 do
    begin

      if Read(x, SizeOf(LCache), LCache) = SizeOf(LCache) then
        AddToCache(LCache)
      else
        break;

      if (hdSign in Options) then
        result := result + '$' + IntToHex(LCache,2)
      else
        result := result + IntToHex(LCache,2);

      inc(LCount);

      if LCount >= Boundary then
      begin
        if Length(LDump) > 0 then
        begin
          result := result + ' ';
          for y := 0 to length(LDump)-1 do
          begin
            if pos(ansichar(lDump[y]), CNT_MASK) > 0 then
              result := result + chr(LDump[y])
            else
              result := result + '_';
          end;
        end;
        setlength(LDump, 0);

        result := result + #13 + #10;
        LCount := 0;
      end else
      result := result + ' ';
    end;

    if (hdZeroPad in Options) and (LCount >0 ) then
    begin
      LPad := Boundary - lCount;
      for x:=1 to LPad do
      Begin
        result := result + '--';
        if (hdSign in Options) then
          result := result + '-';

        inc(LCount);
        if LCount >= Boundary then
        begin
          result := result + #13 + #10;
          LCount := 0;
        end else
        result := result + ' ';
      end;
    end;
  end;
end;
{$IFDEF BE_SILENT}
  {$WARNINGS ON}
  {$HINTS ON}
{$ENDIF}

procedure TStorage.FillAll(const Value: byte);
begin
  if not Empty then
  begin
    if mcWrite in GetCapabilities() then
      FillWith(0, Size, value, SizeOf(value) )
    else
      raise EStorage.Create(CNT_ERR_BUFFER_WRITENOTSUPPORTED);
  end else
  raise EStorageEmpty.Create(CNT_ERR_BUFFER_EMPTY);
end;

procedure TStorage.FillAll;
begin
  if not Empty then
  begin
    if mcWrite in GetCapabilities() then
      DoZeroData()
    else
      raise EStorage.Create(CNT_ERR_BUFFER_WRITENOTSUPPORTED);
  end else
  raise EStorageEmpty.Create(CNT_ERR_BUFFER_EMPTY);
end;

{$IFDEF BE_SILENT}
  {$WARNINGS OFF}
  {$HINTS OFF}
{$ENDIF}
procedure TStorage.Append(const Buffer: IStorage);
var
  lOffset:      Int64;
  lTotal:       Int64;
  lRead:        integer;
  lbytesToRead: integer;
  {$IFDEF USE_LOCAL_CACHE}
  lCache:       TStorageCache;
  {$ENDIF}
begin
  if mcScale in GetCapabilities() then
  begin
    if mcWrite in GetCapabilities() then
    begin
      if Buffer <> nil then
      begin
        // does the source support read caps?
        if mcRead in Buffer.GetCapabilities() then
        begin
          lOffset := 0;
          lTotal := Buffer.GetSize();

          repeat
            {$IFDEF USE_LOCAL_CACHE}
            lbytesToRead := EnsureRange(SizeOf(lCache), 0, lTotal);
            lRead := Buffer.Read(lOffset, lbytesToRead, lCache);
            if lRead > 0 then
            begin
              Append(lCache, lRead);
              lTotal := lTotal - lRead;
              lOffset := lOffset + lRead;
            end;
            {$ELSE}
            lbytesToRead := EnsureRange(SizeOf(FCache), 0, lTotal);
            lRead := Buffer.Read(lOffset, lbytesToRead, FCache);
            if lRead > 0 then
            begin
              Append(FCache, lRead);
              lTotal := lTotal - lRead;
              lOffset := lOffset + lRead;
            end;
            {$ENDIF}
          until (lbytesToRead < 1) or (lRead < 1);

        end else
        raise EStorage.Create(CNT_ERR_BUFFER_SOURCEREADNOTSUPPORTED);
      end else
      raise EStorageInvalidDataSource.Create(CNT_ERR_BUFFER_INVALIDDATASOURCE);
    end else
    raise EStorage.Create(CNT_ERR_BUFFER_WRITENOTSUPPORTED);
  end else
  raise EStorageScaleUnSupported.Create(CNT_ERR_BUFFER_SCALENOTSUPPORTED);
end;
{$IFDEF BE_SILENT}
  {$WARNINGS ON}
  {$HINTS ON}
{$ENDIF}

{$IFDEF BE_SILENT}
  {$WARNINGS OFF}
  {$HINTS OFF}
{$ENDIF}
procedure TStorage.Append(const Stream: TStream);
var
  lTotal:       Int64;
  lRead:        integer;
  lbytesToRead: integer;
  {$IFDEF USE_LOCAL_CACHE}
  lCache:       TStorageCache;
  {$ENDIF}
begin
  if mcScale in GetCapabilities() then
  begin
    if mcWrite in GetCapabilities() then
    begin
      if Stream <> nil then
      begin
        lTotal := (Stream.Size - Stream.Position);
        if lTotal > 0 then
        begin
          Repeat
            {$IFDEF USE_LOCAL_CACHE}
            lBytesToRead := EnsureRange(SizeOf(lCache), 0, lTotal);
            lRead := Stream.Read(lCache, lbytesToRead);
            if lRead > 0 then
            begin
              Append(lCache, lRead);
              lTotal := lTotal - lRead;
            end;
            {$ELSE}
            lBytesToRead := EnsureRange(SizeOf(FCache), 0, lTotal);
            lRead := Stream.Read(FCache, lbytesToRead);
            if lRead > 0 then
            begin
              Append(FCache, lRead);
              lTotal := lTotal - lRead;
            end;
            {$ENDIF}
          Until (lBytesToRead < 1) or (lRead < 1);
        end;
      end else
      raise EStorageInvalidDataSource.Create(CNT_ERR_BUFFER_INVALIDDATASOURCE);
    end else
    raise EStorage.Create(CNT_ERR_BUFFER_WRITENOTSUPPORTED);
  end else
  raise EStorageScaleUnSupported.Create(CNT_ERR_BUFFER_SCALENOTSUPPORTED);
end;
{$IFDEF BE_SILENT}
  {$WARNINGS ON}
  {$HINTS ON}
{$ENDIF}

procedure TStorage.Append(const Data;const DataLength:integer);
var
  lOffset: Int64;
begin
  if mcScale in GetCapabilities() then
  begin
    if mcWrite in GetCapabilities() then
    begin
      if DataLength > 0 then
      begin
        lOffset := GetSize();
        DoGrowDataBy(DataLength);
        DoWriteData(lOffset, Data, DataLength);
      end;
    end else
    raise EStorageWriteUnSupported.Create(CNT_ERR_BUFFER_WRITENOTSUPPORTED);
  end else
  raise EStorageScaleUnSupported.Create(CNT_ERR_BUFFER_SCALENOTSUPPORTED);
end;

procedure TStorage.Release;
begin
  // Is the content owned/managed by us?
  if mcOwned in GetCapabilities() then
    DoReleaseData()
  else
    raise EStorage.Create(CNT_ERR_BUFFER_RELEASENOTSUPPORTED);
end;

procedure TStorage.SetSize(const NewSize: Int64);
var
  mFactor:  Int64;
  mOldSize: Int64;
begin
  if mcScale in GetCapabilities() then
  begin
    if NewSize > 0 then
    begin
      // Get current size
      mOldSize := GetSize();

      // Get difference between current size & new size
      mFactor := abs(mOldSize - NewSize);

      // only act if we need to
      if mFactor > 0 then
      begin
        try
          // grow or shrink?
          if NewSize>mOldSize then
          DoGrowDataBy(mFactor) else

          if NewSize<mOldSize then
          DoShrinkDataBy(mFactor);
        except
          on e: exception do
          raise EStorageScaleFailed.CreateFmt(CNT_ERR_BUFFER_SCALEFAILED,[e.message]);
        end;
      end;
    end else
    Release;
  end else
  raise EStorageScaleUnSupported.Create(CNT_ERR_BUFFER_SCALENOTSUPPORTED);
end;

{$IFDEF BE_SILENT}
  {$WARNINGS OFF}
  {$HINTS OFF}
{$ENDIF}
procedure TStorage.Insert(ByteIndex: Int64; const Source: IStorage);
var
  lTotal:   Int64;
  lRead:    integer;
  lToRead:  integer;
  lEntry:   Int64;
  {$IFDEF USE_LOCAL_CACHE}
  lCache:       TStorageCache;
  {$ENDIF}
begin
  // Validate source PTR
  if Source <> nil then
  begin
    // Check that buffer supports scaling
    if mcScale in GetCapabilities() then
    begin
      // Check that buffer support write access
      if mcWrite in GetCapabilities() then
      begin
        // Check for read-access
        if mcRead in Source.GetCapabilities() then
        begin
          // Get size of source
          lTotal := Source.GetSize();

          // Validate entry index
          if ByteIndex >= 0  then
          begin

            // anything to work with?
            if lTotal > 0 then
            begin

              lEntry := 0;
              While lTotal > 0 do
              begin
                // Clip data to cache boundaries
                {$IFDEF USE_LOCAL_CACHE}
                lToRead := SizeOf(lCache);
                {$ELSE}
                lToRead := SizeOf(FCache);
                {$ENDIF}
                if lToRead > lTotal then
                  lToRead := lTotal;

                // Read data from source
                {$IFDEF USE_LOCAL_CACHE}
                lRead := Source.Read(lEntry, lToRead, lCache);
                {$ELSE}
                lRead := Source.Read(lEntry, lToRead, FCache);
                {$ENDIF}
                if lRead > 0 then
                begin
                  // Write data to our buffer layer
                  {$IFDEF USE_LOCAL_CACHE}
                  Insert(ByteIndex, lCache, lRead);
                  {$ELSE}
                  Insert(ByteIndex, FCache, lRead);
                  {$ENDIF}

                  // update positions
                  lEntry := lEntry + lRead;
                  ByteIndex := ByteIndex + lRead;
                  lTotal := lTotal - lRead;
                end else
                Break;
              end;
            end;

          end else
          raise EStorageIndexViolation.CreateFmt(CNT_ERR_BUFFER_BYTEINDEXVIOLATION,[0, lTotal-1, ByteIndex]);
        end else
        raise EStorageReadUnSupported.Create(CNT_ERR_BUFFER_READNOTSUPPORTED);
      end else
      raise EStorageWriteUnSupported.Create(CNT_ERR_BUFFER_WRITENOTSUPPORTED);
    end else
    raise EStorageScaleUnSupported.Create(CNT_ERR_BUFFER_SCALENOTSUPPORTED);
  end else
  raise EStorageInvalidDataSource.Create(CNT_ERR_BUFFER_INVALIDDATASOURCE);
end;
{$IFDEF BE_SILENT}
  {$WARNINGS ON}
  {$HINTS ON}
{$ENDIF}

{$IFDEF BE_SILENT}
  {$WARNINGS OFF}
  {$HINTS OFF}
{$ENDIF}
procedure TStorage.Insert(ByteIndex: int64;const Source; DataLength: integer);
var
  lTotal:       Int64;
  lbytesToPush: Int64;
  lbytesToRead: integer;
  lPosition:    Int64;
  lFrom:        Int64;
  lTo:          Int64;
  lData:        Pbyte;
  {$IFDEF USE_LOCAL_CACHE}
  lCache:       TStorageCache;
  {$ENDIF}
begin
  if mcScale in GetCapabilities() then
  begin
    if mcWrite in GetCapabilities() then
    begin
      if mcRead in GetCapabilities() then
      begin
        if DataLength > 0 then
        begin
          lData := @Source;
          if lData <> nil then
          begin
            lTotal := GetSize();
            if (ByteIndex >= 0) and (ByteIndex < lTotal) then
            begin
              lBytesToPush := lTotal - ByteIndex;
              if lBytesToPush > 0 then
              begin
                DoGrowDataBy(DataLength);
                lPosition := ByteIndex + lBytesToPush;

                While lBytesToPush > 0 do
                begin
                  {$IFDEF USE_LOCAL_CACHE}
                  // calculate how much data to read
                  lBytesToRead := EnsureRange(SizeOf(lCache), 0, lBytesToPush);

                  // calculate read & write positions
                  lFrom := lPosition - lBytesToRead;
                  lTo := lPosition - (lBytesToRead - DataLength);

                  // read data from the end
                  DoReadData(lFrom, lCache, lbytesToRead);

                  // write data upwards
                  DoWriteData(lTo, lCache, lbytesToRead);
                  {$ELSE}
                  // calculate how much data to read
                  lbytesToRead := EnsureRange(SizeOf(FCache), 0, lBytesToPush);

                  // calculate read & write positions
                  lFrom := lPosition - lbytesToRead;
                  lTo := lPosition - (lbytesToRead - DataLength);

                  // read data from the end
                  DoReadData(lFrom, FCache, lbytesToRead);

                  // write data upwards
                  DoWriteData(lTo, FCache, lbytesToRead);
                  {$ENDIF}

                  // update offset values
                  lPosition := lPosition - lbytesToRead;
                  lbytesToPush := lbytesToPush - lbytesToRead;
                end;

                // insert new data
                DoWriteData(lPosition, Source, DataLength);
              end else
              DoWriteData(lTotal, Source, DataLength);
            end else

            // if @ end, use append instead
            if ByteIndex = lTotal then
              Append(Source, DataLength)
            else
              raise EStorageIndexViolation.CreateFmt(CNT_ERR_BUFFER_BYTEINDEXVIOLATION,[0, lTotal-1, ByteIndex]);
          end else
          raise EStorageInvalidDataSource.Create(CNT_ERR_BUFFER_INVALIDDATASOURCE);
        end;
      end else
      raise EStorageReadUnSupported.Create(CNT_ERR_BUFFER_READNOTSUPPORTED);
    end else
    raise EStorageWriteUnSupported.Create(CNT_ERR_BUFFER_WRITENOTSUPPORTED);
  end else
  raise EStorageScaleUnSupported.Create(CNT_ERR_BUFFER_SCALENOTSUPPORTED);
end;
{$IFDEF BE_SILENT}
  {$WARNINGS ON}
  {$HINTS ON}
{$ENDIF}

{$IFDEF BE_SILENT}
  {$WARNINGS OFF}
  {$HINTS OFF}
{$ENDIF}
procedure TStorage.Remove(ByteIndex: int64; DataLength: integer);
var
  lTemp:      integer;
  lTop:       Int64;
  lBottom:    Int64;
  lToRead:    integer;
  lToPoll:    Int64;
  lPosition:  Int64;
  lTotal:     Int64;
  {$IFDEF USE_LOCAL_CACHE}
  lCache:       TStorageCache;
  {$ENDIF}
begin
  if mcScale in GetCapabilities() then
  begin
    if mcWrite in GetCapabilities() then
    begin
      if mcRead in GetCapabilities() then
      begin
        if DataLength > 0 then
        begin
          lTotal := GetSize();

          if (ByteIndex >= 0) and (ByteIndex < lTotal) then
          begin
            lTemp := ByteIndex + DataLength;
            if DataLength <> lTotal then
            begin
              if lTemp < lTotal then
              begin
                lToPoll := lTotal - (ByteIndex + DataLength);
                lTop := ByteIndex;
                lBottom := ByteIndex + DataLength;

                While lToPoll > 0 do
                begin
                  lPosition := lBottom;
                  {$IFDEF USE_LOCAL_CACHE}
                  lToRead := EnsureRange(SizeOf(lCache), 0, lToPoll);
                  DoReadData(lPosition, lCache, lToRead);
                  DoWriteData(lTop, lCache, lToRead);
                  {$ELSE}
                  lToRead := EnsureRange(SizeOf(FCache), 0, lToPoll);
                  DoReadData(lPosition, FCache, lToRead);
                  DoWriteData(lTop, FCache, lToRead);
                  {$ENDIF}

                  lTop := lTop + lToRead;
                  lBottom := lBottom + lToRead;
                  lToPoll := lToPoll - lToRead;
                end;
                DoShrinkDataBy(DataLength);
              end else
              Release;
            end else
            begin
              if lTemp > lTotal then
                Release()
              else
                DoShrinkDataBy(lTotal - DataLength);
            end;
          end else
          raise EStorageIndexViolation.CreateFmt
          (CNT_ERR_BUFFER_BYTEINDEXVIOLATION,[0, lTotal-1, ByteIndex]);
        end;
      end else
      raise EStorageReadUnSupported.Create(CNT_ERR_BUFFER_READNOTSUPPORTED);
    end else
    raise EStorageWriteUnSupported.Create(CNT_ERR_BUFFER_WRITENOTSUPPORTED);
  end else
  raise EStorageScaleUnSupported.Create(CNT_ERR_BUFFER_SCALENOTSUPPORTED);
end;
{$IFDEF BE_SILENT}
  {$WARNINGS ON}
  {$HINTS ON}
{$ENDIF}

function TStorage.Push(const Source; DataLength: integer; const Orientation: TStorageStackOrientation = soDownUp):  integer;
begin
  result := 0;

  case Orientation of
  soTopDown:
    begin
      // Data is inserted at the start of the buffer
      if not Empty then
        Insert(0, Source, DataLength)
      else
        Append(Source, DataLength);
      result := DataLength;
    end;
  soDownUp:
    begin
      // Data is appended at the bottom of the buffer
      Append(Source, DataLength);
      result := DataLength;
    end;
  end;
end;

function TStorage.Pop(var Target; DataLength: integer;
  const Orientation: TStorageStackOrientation = soDownUp):  integer;
var
  lTotal:   Int64;
  lRemains: Int64;
  lOffset:  int64;
begin
  result := 0;
  if mcScale in GetCapabilities() then
  begin
    if mcWrite in GetCapabilities() then
    begin
      if mcRead in GetCapabilities() then
      begin
        if DataLength > 0 then
        begin
          lTotal := GetSize();
          if lTotal > 0 then
          begin
            case Orientation of
            soTopDown:
              begin
                lRemains := lTotal - DataLength;
                if lRemains > 0 then
                begin
                  result := Read(0, DataLength, Target);
                  Remove(0, DataLength);
                end else
                begin
                  result := lTotal;
                  DoReadData(0, Target, lTotal);
                  Release;
                end;
              end;
            soDownUp:
              begin
                if DataLength > lTotal then
                  DataLength := lTotal;

                lOffset := lTotal - DataLength;
                if lOffset < 0 then
                  lOffset := 0;

                result := Read(lOffset, DataLength, Target);
                self.SetSize(lTotal - Result);
              end;
            end;
          end;
        end;
      end else
      raise EStorageReadUnSupported.Create(CNT_ERR_BUFFER_READNOTSUPPORTED);
    end else
    raise EStorageWriteUnSupported.Create(CNT_ERR_BUFFER_WRITENOTSUPPORTED);
  end else
  raise EStorageScaleUnSupported.Create(CNT_ERR_BUFFER_SCALENOTSUPPORTED);
end;

{$IFDEF BE_SILENT}
  {$WARNINGS OFF}
  {$HINTS OFF}
{$ENDIF}
function TStorage.HashCode: cardinal;
var
  i:        integer;
  x:        cardinal;
  lTotal:   Int64;
  lRead:    integer;
  lToRead:  integer;
  lIndex:   Int64;
  {$IFDEF USE_LOCAL_CACHE}
  lCache:   TStorageCache;
  {$ENDIF}
begin
  result := 0;
  if mcRead in GetCapabilities() then
  begin
    lTotal := GetSize();
    if lTotal > 0 then
    begin
      lIndex := 0;

      while lTotal > 0 do
      begin
        {$IFDEF USE_LOCAL_CACHE}
        lToRead := SizeOf(lCache);
        {$ELSE}
        lToRead := SizeOf(FCache);
        {$ENDIF}
        if lToRead > lTotal then
          lToRead := lTotal;

        {$IFDEF USE_LOCAL_CACHE}
        lRead := read(lIndex, lToRead, lCache);
        {$ELSE}
        lRead := read(lIndex, lToRead, FCache);
        {$ENDIF}

        if lRead > 0 then
        begin
          for i := 0 to lRead do
          begin
            {$IFDEF USE_LOCAL_CACHE}
            result := (result shl 4) + lCache[i];
            {$ELSE}
            result := (result shl 4) + FCache[i];
            {$ENDIF}

            x := result and $F0000000;
            if x <> 0 then
              result := result xor (x shr 24);

            result := result and (not x);
          end;

          lTotal := lTotal - lRead;
          lIndex := lIndex + lRead;
        end else
        Break;
      end;
    end else
    raise EStorageEmpty.Create(CNT_ERR_BUFFER_EMPTY);
  end else
  raise EStorageReadUnSupported.Create(CNT_ERR_BUFFER_READNOTSUPPORTED);
end;
{$IFDEF BE_SILENT}
  {$WARNINGS ON}
  {$HINTS ON}
{$ENDIF}

function TStorage.ToStream: TStream;
begin
  result := TMemoryStream.Create();
  SaveToStream(result);
  result.Seek(0, TSeekOrigin.soBeginning);
end;

procedure TStorage.FromStream(const Stream: TStream; const Disposable: boolean = false);
begin
  try
    LoadFromStream(Stream);
  finally
    if Disposable then
      Stream.free;
  end;
end;


function TStorage.ToStorage: IStorage;
var
  lData:  TStorage;
begin
  // Attempt to create a disk based buffer first
  try
    lData := TStorageFile.Create('', fmCreate);
  except
    on e: exception do
    begin
      // Fall back on memory buffer
      lData := TStorageMemory.Create();
      try
        lData.Assign(self);
      except
        on e: exception do
        begin
          lData.free;
          raise;
        end;
      end;
    end;
  end;

  // Move data over quickly
  try
    lData.Assign(self);
  except
    on e: exception do
    begin
      lData.free;
      raise;
    end;
  end;

  result := IStorage(lData);
end;

procedure TStorage.FromStorage(const Storage: IStorage);
begin
  Release();
  Append(Storage);
end;

procedure TStorage.FromStorage(const Storage: TStorage; const Disposable: boolean = false);
begin
  try
    Release();
    Append(Storage);
  finally
    if Disposable then
      Storage.free;
  end;
end;

procedure TStorage.LoadFromFile(Filename: string);
var
  lFile:  TFileStream;
begin
  lFile := TFileStream.Create(filename,fmOpenRead or fmShareDenyNone);
  try
    LoadFromStream(lFile);
  finally
    lFile.free;
  end;
end;

procedure TStorage.SaveToFile(Filename: string);
var
  mFile:  TFileStream;
begin
  mFile:=TFileStream.Create(filename,fmCreate or fmShareDenyNone);
  try
    SaveToStream(mFile);
  finally
    mFile.free;
  end;
end;

{$IFDEF BE_SILENT}
  {$WARNINGS OFF}
  {$HINTS OFF}
{$ENDIF}
procedure TStorage.SaveToStream(const Stream: TStream);
var
  lWriter:  TWriter;
  lTotal:   Int64;
  lToRead:  integer;
  lRead:    integer;
  lOffset:  Int64;
  {$IFDEF USE_LOCAL_CACHE}
  lCache:   TStorageCache;
  {$ENDIF}
begin
  if mcRead in GetCapabilities() then
  begin
    if Stream <> nil then
    begin
      lWriter := TWriter.Create(Stream, 1024);
      try
        lTotal := GetSize();
        lOffset := 0;

        While lTotal > 0 do
        begin
          {$IFDEF USE_LOCAL_CACHE}
          lToRead := SizeOf(lCache);
          {$ELSE}
          lToRead := SizeOf(FCache);
          {$ENDIF}
          if lToRead > lTotal then
            lToRead := lTotal;

          {$IFDEF USE_LOCAL_CACHE}
          lRead := Read(lOffset, lToRead, lCache);
          {$ELSE}
          lRead := Read(lOffset, lToRead, FCache);
          {$ENDIF}
          if LRead > 0 then
          begin
            {$IFDEF USE_LOCAL_CACHE}
            lWriter.Write(lCache, lRead);
            {$ELSE}
            lWriter.Write(FCache, lRead);
            {$ENDIF}
            lOffset := lOffset + lRead;
            lTotal :=  lTotal - lRead;
          end else
          Break;
        end;
      finally
        lWriter.FlushBuffer;
        lWriter.free;
      end;
    end else
    raise EStorage.Create(CNT_ERR_BUFFER_INVALIDDATATARGET);
  end else
  raise EStorageReadUnSupported.Create(CNT_ERR_BUFFER_READNOTSUPPORTED);
end;
{$IFDEF BE_SILENT}
  {$WARNINGS ON}
  {$HINTS ON}
{$ENDIF}

{$IFDEF BE_SILENT}
  {$WARNINGS OFF}
  {$HINTS OFF}
{$ENDIF}
procedure TStorage.LoadFromStream(const Stream: TStream);
var
  lReader:  TReader;
  lTotal:   Int64;
  lToRead:  integer;
  {$IFDEF USE_LOCAL_CACHE}
  lCache:   TStorageCache;
  {$ENDIF}
begin
  if mcWrite in GetCapabilities() then
  begin
    if mcScale in GetCapabilities() then
    begin
      if Stream <> nil then
      begin
        Release;

        lReader := TReader.Create(Stream, 1024);
        try
          lTotal := (Stream.Size - Stream.Position);
          While lTotal > 0 do
          begin
            {$IFDEF USE_LOCAL_CACHE}
            lToRead := SizeOf(lCache);
            {$ELSE}
            lToRead := SizeOf(FCache);
            {$ENDIF}
            if lToRead > lTotal then
              lToRead := lTotal;

            {$IFDEF USE_LOCAL_CACHE}
            lReader.read(lCache, lToRead);
            self.Append(lCache,  lToRead);
            {$ELSE}
            lReader.read(FCache, lToRead);
            self.Append(FCache,  lToRead);
            {$ENDIF}
            lTotal := lTotal - lToRead;
          end;
        finally
          lReader.free;
        end;
      end else
      raise EStorageInvalidDataSource.Create(CNT_ERR_BUFFER_INVALIDDATASOURCE);
    end else
    raise EStorageScaleUnSupported.Create(CNT_ERR_BUFFER_SCALENOTSUPPORTED);
  end else
  raise EStorageWriteUnSupported.Create(CNT_ERR_BUFFER_WRITENOTSUPPORTED);
end;
{$IFDEF BE_SILENT}
  {$WARNINGS ON}
  {$HINTS ON}
{$ENDIF}

{$IFDEF BE_SILENT}
  {$WARNINGS OFF}
  {$HINTS OFF}
{$ENDIF}
function TStorage.ExportTo(ByteIndex: Int64; DataLength: integer;
  const Writer: TWriter):  integer;
var
  lToRead:  integer;
  lRead:    integer;
  {$IFDEF USE_LOCAL_CACHE}
  lCache:   TStorageCache;
  {$ENDIF}
begin
  result := 0;
  if mcRead in GetCapabilities() then
  begin
    if DataLength > 0 then
    begin
      if Writer <> nil then
      begin

        While DataLength > 0 do
        begin
          {$IFDEF USE_LOCAL_CACHE}
          lToRead := EnsureRange( SizeOf(lCache), 0, DataLength);
          lRead := Read(ByteIndex, lToRead, lCache);
          {$ELSE}
          lToRead := EnsureRange( SizeOf(FCache), 0, DataLength);
          lRead := Read(ByteIndex, lToRead, FCache);
          {$ENDIF}
          if lRead > 0 then
          begin
            {$IFDEF USE_LOCAL_CACHE}
            Writer.Write(lCache, lRead);
            {$ELSE}
            Writer.Write(FCache, lRead);
            {$ENDIF}
            ByteIndex := ByteIndex + lRead;
            DataLength := DataLength - lRead;
            result := result + lRead;
          end else
          Break;
        end;

        Writer.FlushBuffer();
      end else
      raise EStorage.Create(CNT_ERR_BUFFER_INVALIDDATATARGET);
    end else
    raise EStorageEmpty.Create(CNT_ERR_BUFFER_EMPTY);
  end else
  raise EStorageReadUnSupported.Create(CNT_ERR_BUFFER_READNOTSUPPORTED);
end;
{$IFDEF BE_SILENT}
  {$WARNINGS ON}
  {$HINTS ON}
{$ENDIF}

function TStorage.Read(const ByteIndex:Int64; DataLength: integer; var Data): integer;
var
  lTotal:   Int64;
  lRemains: Int64;
begin
  result := 0;
  if mcRead in GetCapabilities() then
  begin
    if DataLength > 0 then
    begin
      lTotal := GetSize();
      if lTotal > 0 then
      begin
        if (ByteIndex >= 0) and (ByteIndex < lTotal) then
        begin
          lRemains := lTotal - ByteIndex;
          if lRemains > 0 then
          begin
            if DataLength > lRemains then
              DataLength := lRemains;

            DoReadData(ByteIndex, Data, DataLength);
            result := DataLength;
          end;
        end;
      end else
      raise EStorageEmpty.Create(CNT_ERR_BUFFER_EMPTY);
    end;
  end else
  raise EStorageReadUnSupported.Create(CNT_ERR_BUFFER_READNOTSUPPORTED);
end;

{$IFDEF BE_SILENT}
  {$WARNINGS OFF}
  {$HINTS OFF}
{$ENDIF}
function  TStorage.ImportFrom(ByteIndex: Int64;DataLength: integer;const Reader: TReader):  integer;
var
  lToRead:  integer;
  {$IFDEF USE_LOCAL_CACHE}
  lCache:   TStorageCache;
  {$ENDIF}
begin
  // Initialize
  result:=0;
  if mcWrite in GetCapabilities() then
  begin
    if Reader <> nil then
    begin
      While DataLength > 0 do
      begin
        {$IFDEF USE_LOCAL_CACHE}
        lToRead := EnsureRange(SizeOf(lCache), 0, DataLength);
        {$ELSE}
        lToRead := EnsureRange(SizeOf(FCache), 0, DataLength);
        {$ENDIF}
        if lToRead > 0 then
        begin
          {$IFDEF USE_LOCAL_CACHE}
          Reader.Read(lCache, lToRead);
          Write(ByteIndex, lToRead, lCache);
          {$ELSE}
          Reader.Read(FCache, lToRead);
          Write(ByteIndex, lToRead, FCache);
          {$ENDIF}
          result := result + lToRead;
          ByteIndex := ByteIndex + lToRead;
          DataLength := DataLength - lToRead;
        end else
        Break;
      end;
    end else
    raise EStorageInvalidDataSource.Create(CNT_ERR_BUFFER_INVALIDDATASOURCE);
  end else
  raise EStorage.Create(CNT_ERR_BUFFER_WRITENOTSUPPORTED);
end;
{$IFDEF BE_SILENT}
  {$WARNINGS ON}
  {$HINTS ON}
{$ENDIF}

function TStorage.Write(const ByteIndex: Int64; DataLength: integer; const Data):  integer;
var
  lTotal:   Int64;
  lending:  Int64;
  lExtra:   Int64;
begin
  result := 0;

  if mcWrite in GetCapabilities() then
  begin
    if DataLength > 0 then
    begin
      lTotal := GetSize();
      if lTotal>0 then
      begin
        if (ByteIndex >= 0) and (ByteIndex < lTotal) then
        begin
          lEnding := ByteIndex + DataLength;
          if lEnding > lTotal then
          begin
            lExtra := lEnding - lTotal;
            if mcScale in GetCapabilities() then
              DoGrowDataBy(lExtra)
            else
              DataLength := EnsureRange(DataLength - lExtra, 0, MAXINT);
          end;
          if DataLength > 0 then
            DoWriteData(ByteIndex, Data, DataLength);
          result := DataLength;
        end else
        raise EStorageIndexViolation.CreateFmt(CNT_ERR_BUFFER_BYTEINDEXVIOLATION,[0, lTotal-1, ByteIndex]);
      end else
      begin
        if mcScale in GetCapabilities() then
        begin
          DoGrowDataBy(DataLength);
          DoWriteData(0, Data, DataLength);
          result := DataLength;
        end else
        raise EStorageScaleUnSupported.Create(CNT_ERR_BUFFER_SCALENOTSUPPORTED);
      end;
    end;
  end else
  raise EStorage.Create(CNT_ERR_BUFFER_WRITENOTSUPPORTED);
end;

//##########################################################################
// TQTXStreamAdapter
//##########################################################################

constructor TStreamAdapter.Create(const SourceBuffer: IStorage);
begin
  inherited Create;
  if assigned(SourceBuffer) then
    FBufObj := SourceBuffer
  else
    raise EQTXStreamAdapter.Create(CNT_ERR_STREAMADAPTER_INVALIDBUFFER);
end;

function TStreamAdapter.GetSize: Int64;
begin
  result := FBufObj.GetSize();
end;

procedure TStreamAdapter.SetSize(const NewSize: Int64);
begin
  FBufObj.SetSize( EnsureRange(NewSize, 0, MaxLongInt) );
end;

function TStreamAdapter.Read(var Buffer;Count: longint):  longint;
begin
  result := FBufObj.Read(FOffset, Count, Buffer);
  inc(FOffset, result);
end;

function TStreamAdapter.Write(const Buffer;Count:longint):  longint;
begin
  if FOffset = FBufObj.GetSize() then
  begin
    FBufObj.Append(Buffer, Count);
    result := Count;
  end else
  result := FBufObj.Write(FOffset, Count, Buffer);
  inc(FOffset,  result);
end;

function TStreamAdapter.Seek(const Offset: Int64; Origin: TSeekOrigin): Int64;
begin
  Case Origin of
  sobeginning:
    begin
      if Offset >= 0 then
        FOffset := EnsureRange(Offset, 0, FBufObj.GetSize());
     end;
  soCurrent:
    begin
      FOffset := EnsureRange(FOffset + Offset, 0, FBufObj.GetSize());
    end;
  soEnd:
    begin
      if Offset > 0 then
        FOffset := FBufObj.GetSize()-1
      else
        FOffset := EnsureRange(FOffset-(abs(Offset)), 0, FBufObj.GetSize());
    end;
  end;
  result := FOffset;
end;

end.

