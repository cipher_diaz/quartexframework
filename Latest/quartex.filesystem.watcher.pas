// ############################################################################
// #
// # Quartex code library
// #        version 1.0.1
// ############################################################################
// #
// # Written by Jon L. Aasenden @ Quartex Components
// #
// # This software is released under Mozilla Public License V2
// #   https://www.mozilla.org/en-US/MPL/2.0/
// #
// ############################################################################

unit quartex.filesystem.watcher;

interface

uses Windows, Classes, SysUtils;

type

   TFileSystemFolderWatch = class;
   TFileSystemFileWatch = class;

   TFileSystemWatchMode = (
    wmFolderOnly      = $FC00,
    wmFolderRecursive = $FC01
    );

   {$MINENUMSIZE 4}
   TFileNotificationAction = (
      FILE_ACTION_ADDED = 1,
      FILE_ACTION_REMOVED,
      FILE_ACTION_MODIFIED,
      FILE_ACTION_RENAMED_OLD_NAME,
      FILE_ACTION_RENAMED_NEW_NAME
   );

  PFileNotifyInformation = ^TFileNotifyInformation;
  TFileNotifyInformation = record
    NextEntryOffset : DWORD;
    Action : TFileNotificationAction;
    FileNameLength : DWORD;
    FileName : array[0..0] of WideChar;
  end;

  TFileSystemFileWatchFolderChangedEvent =
    procedure (sender: TFileSystemFolderWatch) of object;

  TFileSystemFileWatchFileChangedEvent =
    procedure (sender: TFileSystemFileWatch; const FileName: string;
    FileAction: TFileNotificationAction) of object;

   // TFileSystemFolderWatch
  TFileSystemFolderWatch = class (TThread)
  strict private
    FMountPoint: string;
    FOnDirectoryChanged : TFileSystemFileWatchFolderChangedEvent;
    FMode : TFileSystemWatchMode;
    FNotifyHandle : array [0..1] of THandle;
    FLastChange : TDateTime;
  strict protected
    procedure Shutdown;
  public
    property  Mountpoint: String read FMountPoint;
    property  WatchMode: TFileSystemWatchMode read FMode;
    property  LastChange : TDateTime read FLastChange write FLastChange;

    property  OnFolderChanged : TFileSystemFileWatchFolderChangedEvent
              read FOnDirectoryChanged write FOnDirectoryChanged;

    procedure Execute; override;

    constructor Create(const aDirectory: String; WatchMode: TFileSystemWatchMode);
    destructor Destroy; override;
  end;

  // TFileSystemFileWatch
  TFileSystemFileWatch = class(TThread)
  strict private
    FDirectory: string;
    FOnFileChanged: TFileSystemFileWatchFileChangedEvent;
    FMode: TFileSystemWatchMode;
    FDirectoryHandle: THandle;
    FNotificationBuffer: packed array[0..4095] of byte;
    FNotifyFilter: DWORD;
    FOverlapped: TOverlapped;
    FPOverlapped: POverlapped;
    FBytesWritten: DWORD;
    FCompletionPort: THandle;
    FLastChange: TDateTime;
    FActive: boolean;

  strict protected
    procedure Shutdown;

  public
    property  Directory: string read FDirectory;
    property  Mode: TFileSystemWatchMode read FMode;
    property  LastChange: TDateTime read FLastChange write FLastChange;

    property  OnFileChanged: TFileSystemFileWatchFileChangedEvent
              read FOnFileChanged write FOnFileChanged;

    procedure Execute; override;

    constructor Create(Mountpoint: string; WatchMode: TFileSystemWatchMode);
    destructor Destroy; override;
  end;

implementation

//#############################################################################
// TFileSystemFolderWatch
//#############################################################################

constructor TFileSystemFolderWatch.Create(const aDirectory: String;
            WatchMode: TFileSystemWatchMode);
begin
   inherited Create(False);
   NameThreadForDebugging('FileSystemFolderWatcher');
   FMountPoint:=aDirectory;
   FMode:=WatchMode;
end;

destructor TFileSystemFolderWatch.Destroy;
begin
  Shutdown;
  inherited Destroy;
end;

procedure TFileSystemFolderWatch.Shutdown;
begin
  Terminate;
  SetEvent(FNotifyHandle[1]);
  WaitFor;
end;

procedure TFileSystemFolderWatch.Execute;
var
  status: Cardinal;
begin
  FNotifyHandle[0] := FindFirstChangeNotification
    ( PChar(FMountPoint), not (WatchMode = wmFolderOnly),
      FILE_NOTIFY_CHANGE_FILE_NAME
      + FILE_NOTIFY_CHANGE_DIR_NAME
      + FILE_NOTIFY_CHANGE_ATTRIBUTES
      + FILE_NOTIFY_CHANGE_SIZE
      + FILE_NOTIFY_CHANGE_LAST_WRITE
      + FILE_NOTIFY_CHANGE_SECURITY
    );

  if FNotifyHandle[0] = INVALID_HANDLE_VALUE then
  exit;

  FNotifyHandle[1]:=CreateEvent(nil, True, False, nil);
  try
    while not Terminated do
    begin
      status := WaitForMultipleObjects(2, @FNotifyHandle[0], False, 1000);
      case status of
      WAIT_TIMEOUT:
        begin
        end;
      WAIT_OBJECT_0:
        begin
          if not Terminated then
          begin
            FLastChange:=Now;
            if Assigned(FOnDirectoryChanged) then
            FOnDirectoryChanged(Self);
            if not Terminated then
            FindNextChangeNotification(FNotifyHandle[0]);
          end;
        end;
      else
        break;
      end;
    end;
  finally
    FindCloseChangeNotification( FNotifyHandle[0] );
    CloseHandle( FNotifyHandle[1] );
  end;
end;

//#############################################################################
// TFileSystemFileWatch
//#############################################################################

constructor TFileSystemFileWatch.Create(Mountpoint: string;
  WatchMode: TFileSystemWatchMode);
begin
   inherited Create(False);

   FDirectory := IncludeTrailingPathDelimiter(Mountpoint);
   FMode := WatchMode;

   FNotifyFilter:=  FILE_NOTIFY_CHANGE_FILE_NAME
                or  FILE_NOTIFY_CHANGE_DIR_NAME
                or  FILE_NOTIFY_CHANGE_ATTRIBUTES
                or  FILE_NOTIFY_CHANGE_SIZE
                or  FILE_NOTIFY_CHANGE_LAST_WRITE
                or  FILE_NOTIFY_CHANGE_SECURITY;

   FDirectoryHandle := CreateFile( PChar(FDirectory),
          FILE_LIST_DIRECTORY,
              FILE_SHARE_READ
          or  FILE_SHARE_WRITE
          or  FILE_SHARE_DELETE,
              nil,
              OPEN_EXISTING,
              FILE_FLAG_BACKUP_SEMANTICS
          or  FILE_FLAG_OVERLAPPED, 0);

  if FDirectoryHandle = INVALID_HANDLE_VALUE then
    RaiseLastOSError;

  FCompletionPort := CreateIoCompletionPort(FDirectoryHandle, 0, 1, 0);

  FBytesWritten := 0;
  if not ReadDirectoryChanges(FDirectoryHandle, @FNotificationBuffer,
    SizeOf(FNotificationBuffer), (FMode=wmFolderRecursive),
    FNotifyFilter, @FBytesWritten, @FOverlapped, nil) then
   RaiseLastOSError;
end;


destructor TFileSystemFileWatch.Destroy;
begin
  try
    if FActive then
    Shutdown;
  finally
    try
      if FDirectoryHandle<>0 then
      CloseHandle(FDirectoryHandle);
    finally
      try
        if FCompletionPort<>0 then
        CloseHandle(FCompletionPort);
      finally
      end;
    end;
  end;
  inherited;
end;

procedure TFileSystemFileWatch.Execute;
var
   numBytes : DWORD;
   {$if CompilerVersion < 23.0} // XE2
   completionKey : DWORD;
   {$else}
   completionKey : ULONG_PTR;
   {$ifend}
   fileOpNotification : PFileNotifyInformation;
   offset : Longint;
   fileName : String;
begin
  FActive:=True;
  NameThreadForDebugging('FileNotifier');
  while not Terminated do
  begin
    GetQueuedCompletionStatus(FCompletionPort, numBytes, completionKey, FPOverlapped, INFINITE);
    if completionKey<>0 then
    begin
      fileOpNotification:=@FNotificationBuffer;
      repeat
        offset:=fileOpNotification^.NextEntryOffset;
        if Assigned(FOnFileChanged) then begin
           SetString(fileName, fileOpNotification^.FileName,
           fileOpNotification^.FileNameLength div SizeOf(Char));
           FOnFileChanged(Self, FDirectory+fileName, fileOpNotification^.Action);
        end;
        fileOpNotification:=@PAnsiChar(fileOpNotification)[offset];
      until (offset = 0);

      FBytesWritten:=0;
      FillChar(FNotificationBuffer, 0, SizeOf(FNotificationBuffer));
      if not ReadDirectoryChanges(FDirectoryHandle, @FNotificationBuffer,
        SizeOf(FNotificationBuffer), (FMode=wmFolderRecursive), FNotifyFilter,
        @FBytesWritten, @FOverlapped, nil) then
          Terminate;
    end else
    Terminate;
   end;
   FActive:=False;
end;

procedure TFileSystemFileWatch.Shutdown;
begin
  if FActive then
  begin
    if not terminated then
      Terminate();

    if FCompletionPort <> 0 then
      PostQueuedCompletionStatus(FCompletionPort, 0, 0, nil);

    WaitFor();
  end;
end;

end.

