# Vectors for Object Pascal #

Under C++ Vector containers are a part of the standard library (std::vector) and represents a highly efficient and clever way of managing value sequences (lists, arrays and queues) in a uniform way.
While the benefits of vector containers are many; far to many to list here, it greatly simplify both advanced (low-level) and mundane (high-level) programming tasks alike. It helps remove boilerplate and repetitive code,
and turns tasks that would usually require thousands of lines of code - into the proverbial "one liner".

### What are the highlights of this library? ###

* Support for traditional C++ vectors (std::vector) as TVector<T>
* Unified, untyped buffer model (TStorage, TStorageMemory, TStorageFile)
* Typed view (TView<T>) to simplify working with untyped storage buffers
* Typed View supports TBit, allowing access and enumeration of untyped data on bit level
* Fully enumerable and persistent
* Easy to expand and integrate into your own projects
* No third party dependencies
* Compatible with both Delphi and Freepascal

## What is a vector? ##

The vector class itself (TVector) is little more than a unified front. It is a common, standardized class for working with sequences of data; where a sequence is typically an array, a list, a queue or a stack in form or fashion (dataset results are also sequences).
What is important to understand, is that TVector is a container class. It's only job is to house and offer methods to access the content, but TVector itself does not involve itself with managing that content.
That is dealt with by a second class, called an allocator.

For a more in-depth look at vectors, please see my article on the Embarcadero community website:
https://community.idera.com/developer-tools/b/blog/posts/vector-containers-for-delphi

## Allocators ##

The allocator class is reponsible for providing values on demand, deal with storage (be it memory or somewhere else), keep track of count and basically do all the work.
This decoupling between content and container is extremely useful (and powerful), because TVector has no idea where the data originates, on what medium it is kept, if it's local or remote, if it was text or binary;
As long as the allocator class delivers the content as TVector wants it - TVector could not care less about the details.

For example, you could implement an Allocator that read and write to a JSON or XML file. TVector would not care as long as the datatypes match it's generic type.
So the way data is stored (or where) is completely irellevant to TVector, it leaves that to TVectorAllocator.

The following allocator types are supported out of the box:

* TVectorAllocator
	* TVectorAllocatorDynamic
	* TVectorAllocatorBuffered
	* TVectorAllocatorSequence
	* TVectorAllocatorFile
	* TVectorAllocatorMapped

It is important to understand, that Vector containers are not powerful because of what they do -- but rather "how they do it".
Once the full potential hits you (see examples), you will wonder why this doesnt ship with Delphi by default.
Vectors by default is not that exciting, but when you combine them with a unified storage system for untyped (binary) data, and add TView<T> into the mix, you can do some amazing things with very little code!
	
But let's have a look at the allocator classes.

### Dynamic Allocator ###

The dynamic allocator is identical to how TArray<T> operates.

If your records (or whatever datatype you use) dont need to be perfectly aligned in memory (sequenced) then you should use dynamic.
If the amount of values you store in your TVector could grow to exhaust the heap, I would urge you to use a TVectorAllocatorFile instead. It will create a temporary file and store everything
seamlessly on disk.

### Sequence Allocator ###

The sequence allocator maintains a list of <T> within a single block of memory. This is a good choice when working with record-types that should be lined up one-after-the-other.
For example, if you need to quickly stream a memory based TVector<T> to disk, you can write the entire memory chunk to a file-stream in one operation, since it's maintained in a single block of memory.

The closest match to how the sequenced allocator operates, is a "packed array of record" in Pascal. Except TVector deals with the storage operations for you (insert, remove etc requires the storage space to be scaled).
The sequenced allocator is very efficient when reading data, since it can use simple maths to calculate the offset (as opposed to a linked-list which is costly in terms of CPU time).

**Note:** TVector.TVectorAllocatorSequence inherits from TVectorAllocatorBuffered, and creates an instance of TQTXStorage.TStorageMemory.

### File Allocator ##

The file allocator (TVector.TVectorAllocatorFile) is identical to the sequence allocator, except that it operates completely on the filesystem. Since both allocators inherit from
from a common ancestor (TVector.TVectorAllocatorBuffered), the only difference are the constructors.

**Note:** TVector.TVectorAllocatorFile inherits from TVectorAllocatorBuffered, and creates an instance of TQTXStorage.TStorageFile.

### Mapped Allocator ##

Where Sequence and File creates spesific storage-buffers, the mapped Allocator accepts a buffer of any type through it's constructor.
This is there to make it easier to use TStorage based buffers that loads and saves data over the network (for example).
TVector.TVectorAllocatorMapped represents an easy and open way of using new TStorage buffers with TVector's (without breaking the mold).

### Custom Allocator ###

Another way of getting custom data into a TVector, is to implement your own Allocator class.
For example, if you want to use TVector<TSomeRecord> and automatically map the content with a database table (populate the vector with the results of a query for example),
the best way of doing that is to implement the logic as a separate allocator. Simply inherit from **TVector<T>.TVectorAllocator** and override the abstract methods.

## Storage buffers ##

A buffer (untyped, allocated memory) is a common concept, but C++ have some highly efficient classes / functions that makes working with binary, untyped data easy and enjoyable.
Lately, languages like JavaScript (node.js in particular) has adopted buffers and views too, which makes JavaScript capable of amazing things, both client-side and server-side.

A storage class (read: buffer) is a class that makes working with binary (untyped) data easy. What is unique about these storage classes though, is that the
location (e.g memory, file, network) and medium (file-format) is completely abstracted away. Just like TVector abstracts away how values and sequences are stored, so does TStorage
abstract away where the data originates.

Just like you can extend TVector by writing your own TVectorAllocator, you can also implement you own TStorage based class too. So there is a lot of freedom in this framework.
Out of the box the library ships with two storage classes.

The classes are:

* TQTXStorage.TStorage [ancestor]
* TQTXStorage.TStorageMemory
* TQTXStorage.TStorageFile

The TVector containers and TView classes all use the buffers to make memory management simple. Buffers supports elaborate functionality like Insert() and Remove() which will literally
do just that. So if you want to inject data into a large, gigabyte sized file - then a buffer reduce that to a "one liner".

Buffers are also much faster that streams. On average, the File-Buffer is 3 times faster than TFileStream, due to the fact that it uses the filesystem directly (optional).

For example, writing 1000000 (one million) records of this type:

```
#!delphi
TDataRecord = record
  drId: integer;
  drIndex: integer;
  drName: shortstring;
  drValue: shortstring;
end;
```

That operation took 9 seconds using a file-buffer (inside VMWare) with the smallest cache model.
The same process using TFileStream took 28 seconds. So there is a considerable speed boost.

### TQTXStorage.TStreamAdapter ###

While buffers have easy to use load / save / export methods, if you need to access a buffer like a normal stream, simply create a TStreamAdapter for it:

```
#!delphi
var lAdapter := TQTXStorage.TStreamAdapter.Create(MyBuffer);
try
  // Issue the data where you need it
  LoadDataFromStream(lAdapter);
finally
  lAdapter.free;
end;
```

### TQTXStorage.TBitBuffer ###

The VCL has as TBits class, but it lacks a ton of features, and is incredibly slow. This is a clean re-implementation.
It has been included since it's often used as a map for records in flat-file database engines. It is optimized for finding free bits very quickly, a key element in database design.

## Typed Views ##

A typed-view (TView) is a generic class that allows you to work with an untyped-buffer (TStorage) as a normal, typed array. So if you have a chunk of binary data in a storage buffer,
you can access it like a normal TArray<T>. You can create as many views, with different types, into the same buffer as you like.
And since the storage buffers are universal, the data can be both on disk and in memory (or online for that matter).

So, if we have a binary file that contains 1000 records like this:

```
#!delphi
type
TDataRecord = record
  drId: integer;
  drIndex: integer;
  drName: shortstring;
  drValue: shortstring;
end;
```

We can create a typed-view into our buffer, and get the data out instantly:

```
#!delphi
var el: TDataRecord;
var lView := TQTXStorage.TQTXDataView<TDataRecord>.Create(MyBuffer);

for el in lView do
begin
  // process each record in the buffer
  // just like it was a normal array
end;
```

Or get the whole shabam:
```
#!delphi
var elements: TArray<TDataRecord>;
elements := lView.ToArray();
```

You also have the following handy functions:

* ToArray(): TArray<T>;
* ToArrayEx(var target: TList<T>);
* ToArrayEx(start, count): TArray<T>
* ToList(): TList<T>;
* ToList(var target: TList<T>);

So getting typed information out of an untyped buffer through TView, is elegant and simple.

### Support for bits ###

TView now supports a TBit datatype, meaning you can now access and traverse any storage buffer as a collection of bits!
Bit-buffers are very handy and are commonly used in both databases, multimedia and games to keep track of things.
Music and audio processing likewise operate with bit-streams and buffers, so this is definitively a nice addition.

So you can now do:
```
#!delphi
var lView: TView<TBit>.Create(MyBuffer);
var bit: TBit;
for bit in LView do
begin
  // process all bits here
end;
```
C++ had issues with this, as they used boolean to represent bits. we dont have that problem.

# License #

This unit is released as Mozilla Public License V2 (MPL):
https://www.mozilla.org/en-US/MPL/2.0/

I would however ask that you notify me regarding fixes or improvements, so that everyone can enjoy the benefits. If you spot a bug or make something better, PM me on Facebook or post to the
"Delphi Developer" or "FPC Developer" groups. I'm not going to be difficult about it, but it would be nice if we all help out.

You can read more about Vector containers in the C++ documentation here:
http://www.cplusplus.com/reference/vector/vector/
